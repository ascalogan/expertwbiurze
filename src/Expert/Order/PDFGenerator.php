<?php

namespace Expert\Order;

class PDFGenerator
{
    /**
     * @param $id
     * @return \mPDF
     * @throws \Zend_Db_Table_Exception
     */
    public function generate($id)
    {
        $TOrders = new \Orders_Model_DbTable_Orders();
        $order = $TOrders->find($id)->current();

        $properties = $this->getProperties($order);

        $view = new \App_View([
            'scriptPath' => APPLICATION_PATH . '/modules/orders/views/scripts/'
        ]);

        $view->order = $order;
        $view->properties = $properties;

        $mpdf = new \mPDF();
        $style = '<style type="text/css">' . file_get_contents(APPLICATION_PATH . '/modules/orders/assets/style.css') . '</style>';

        try {
            $mpdf->WriteHTML($style . $view->render('step/order.phtml'));
        }
        catch (Exception $e) {
            die($e->getMessage());
        }

        return $mpdf;
    }

    public function getProperties($order)
    {
        $creator = $order->getCreator();
        $verifier = $order->getVerifier();
        $oldVerifier = $order->getOldVerifier();

        $properties = array();
        $properties['Status'] = $order->getStatus();

        if (empty($order->reported)) {
            $properties['Rozpoczęto'] = date('d-m-Y', $order->created);
        } else {
            $creatorName = $creator->getName() . ' (' . $creator->login . ')';
            $properties['Stworzono'] = date('d-m-Y', $order->reported) . ' przez ' . $creatorName;
        }

        if ($order->status) {
            $pusher = $order->getPusher();

            $properties['Złożono'] = sprintf(
                '%s przez %s (%s)',
                date('d-m-Y', $order->pushed),
                $pusher->getName(),
                $pusher->login
            );
        }
        if ($order->status > 1) {
            $properties['Przyjęto do realizacji'] = date('d-m-Y', $order->accepted);
        }
        if ($order->status == '3') {
            $properties['Zrealizowano'] = date('d-m-Y', $order->received);
        }

        if (empty($order->status) && !empty($order->reported)) {
            $properties[$order->thrown ? 'Odrzucił' : 'Weryfikuje'] = $verifier->getName() . ' (' . $verifier->login . ')';
        }

        if ($oldVerifier) {
            $properties['Weryfikował'] = $oldVerifier->getName() . ' (' . $oldVerifier->login . ')';
        }

        return $properties;
    }
}
