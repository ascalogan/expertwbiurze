<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{    
//    protected function _initAutoload()
//    {
//        //Zend_Loader_Autoloader::getInstance()->setFallbackAutoloader(true);
//
////        $autoloader = new Zend_Application_Module_Autoloader(array(
////            'namespace' => '',
////            'basePath'  => dirname(__FILE__),
////        ));
//    var_dump($this->_pluginResources);die;
//        $this->bootstrap('db');die('c');
//        $this->getResource('db')->query('SET NAMES UTF8');
//        die('a');
//        return $autoloader;
//    }
    
    protected function _initRouterPlugin()
    {
        $this->bootstrap('db');
        $this->getResource('db')->query('SET NAMES UTF8');

        $this->bootstrap('frontcontroller');
        $front = $this->getResource('frontcontroller');
        $front->registerPlugin(new App_Controller_Plugin_Router());
        
        $this->bootstrap('view');
        $view = $this->getResource('view');
        
        $view->addHelperPath('../library/App/View/Helper', 'App_View_Helper');
        
        $front->registerPlugin(new Zend_Controller_Plugin_ErrorHandler(array(
            'module' => 'content',
            'controller' => 'site',
            'action' => 'page'
        )));

        Zend_Registry::set('Zend_Currency', new Zend_Currency('pl_PL'));
        
        Zend_Locale::setDefault('pl_PL');
    }

    protected function _initView()
    {
        $options = $this->getOption('resources')['view'];

        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
        $viewRenderer->setView($view = new Zend_View($options));

        return $view;
    }
    
    protected function _initPageCache()
    {
        return;
        
        $front = array('lifetime' => 7200, 'debug_header' => true, 'default_options' => array(
            'cache_with_post_variables' => true
        ));
        $back = array('cache_dir' => APPLICATION_PATH . '/../tmp/');
        
        $cache = Zend_Cache::factory('Page',
                             'File',
                             $front,
                             $back);
 
            $cache->start();
    }
}

