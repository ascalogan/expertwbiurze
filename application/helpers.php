<?php

if (!function_exists('env')) {
    function env($name, $default = null) {
        $value = getenv($name);

        if ($value === false) {
            return $default;
        }

        switch ($value) {
            case 'true':
                return true;
            case 'false':
                return false;
            default:
                return $value;
        }
    }
}