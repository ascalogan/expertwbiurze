Ext.define("Module.lite.Lite", {
    extend: "Step.app.Module",
    id: "Lite",
    
    controllers: [
        "Step"
    ],
    
    getActions: function() {
        return ["->", {
            text: "wyloguj",
            icon: "on_off_icon.png",
            controller: "Lite",
            action: "logout"
        }]
    }
})