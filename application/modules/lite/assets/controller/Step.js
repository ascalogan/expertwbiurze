Ext.define("Module.lite.controller.Step", {
    extend: "Step.app.Controller",
    
    init: function() {
        this.control({
            "treepanel": {
                itemaction: this.onTreeItemAction
            },
            "stepgrid": {
                "delete": this.onGridItemDelete
            }
        })
    },
    
    onTreeItemAction: function(tree, item, action, button) {
        if (!item) return;
        
        if (action.item) {
            action.params = Ext.apply(action.params, {item: item});
        }
        
        action.params = Ext.apply(action.params || {}, {itemId: item.get("id")});
        if (button && action.button !== false) {
            action.params.buttonId = button.el.id;
        }
        
        this.callAction(action);
    },
    
    onGridItemDelete: function(grid, item, button, params) {
        if (!item) return;
        params = params || {};
        
        var title = params.confirmTitle || "Potwierdzenie usunięcia",
            message = "Czy na pewno chcesz usunąć wybrany element?",
            id = item.get("id");
            
        if (params.confirmMsg) {
            message = (new Ext.Template(params.confirmMsg)).apply(item.data);
        }
        
        Ext.Msg.confirm(title, message, function(answer) {
            if (answer == "yes") {
                grid.getStore().remove(item);
                grid.getStore().sync();
                
                grid.fireEvent("deleted", grid, id);
            }
        });
    }
});