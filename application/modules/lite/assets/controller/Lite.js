Ext.define("Module.lite.controller.Lite", {
    extend: "Step.app.Controller",
    
    logout: function() {
        Lite.doLogout(function() {
            window.location.reload();
        });
    }
});