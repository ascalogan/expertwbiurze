<?php

class Lite_Model_DbTable_Settings extends App_Db_Table
{
    protected $_name = 'settings';
    
    protected $_primary = 'name';
    
    public function get($name)
    {
        return $this->fetchRow(array(
            'name = ?' => $name
        ));
    }
    
    public function set($name, $value)
    {
        $row = $this->get($name);
        
        if ($row === null) {
            $row = $this->createRow(array('name' => $name));
        }
        
        $row->value = $value;
        $row->save();
        
        return $row;
    }
}