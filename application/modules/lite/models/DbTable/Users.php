<?php

class Lite_Model_DbTable_Users extends App_Db_Table
{
    
    protected $_name = 'users';
    
    protected $_primary = 'id';
}