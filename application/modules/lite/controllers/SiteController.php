<?php

class Lite_SiteController extends App_Controller_Action_Site
{
    public function indexAction()
    {
        $TProducts = new Products_Model_DbTable_Products();
        $select = $TProducts->select();
        $select->setIntegrityCheck(false);
        
        $select  ->from('products')
                    ->where('promotion > 0')
        ->limit(4);
            
        $this->view->promos = $TProducts->fetchAll($select);
        
        $this->view->isHome = true;
    }
}