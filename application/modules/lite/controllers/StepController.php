<?php

class Lite_StepController extends App_Controller_Action_Step
{
    public function init()
    {
        parent::init();
        
        $this->_helper->viewRenderer->setNoRender(true);

        $layoutPath = $this->getFrontController()->getModuleDirectory('lite') . DIRECTORY_SEPARATOR . 'layouts';
        $this->_helper->layout->setLayoutPath($layoutPath);
        $this->_helper->layout->setLayout('layout-step');
    }
    
    
    public function doLogin($data)
    {
        $TUsers = new Lite_Model_DbTable_Users();
        $row = $TUsers->fetchRow(array(
            'login = ?' => $data['login'],
            'password = ?' => md5($data['password'])
        ));
        
        if ($row) {
            //$row->last_login = time();
            $row->save();
            
            $this->getSession()->uid = $row->id;
        }
        else {
            $this->send('success', false);
        }
    }
    
    public function doLogout()
    {
        $this->getSession()->uid = null;
    }
    
    public function indexAction()
    {
        $this->view->appClass = 'Step.Login';
        $appOptions = $this->getInvokeArg('bootstrap')->getOption('app');
        $this->view->appOptions = $appOptions;
        
        if ($this->getSession()->uid) {
            $this->_redirect('step/panel');
        }
    }
    
    public function panelAction()
    {
        if (!$this->getSession()->uid) {
            $this->_redirect('step');
        }
        
        $TUsers = new Lite_Model_DbTable_Users();
        $user = $TUsers->find($this->getSession()->uid)->current();
        
        $config = array('privilages' => array());
        $privilages = array(
            'clients', 'products', 'content', 'users'
        );
        
        foreach ($privilages as $p) {
            if (isset($user->$p) && !empty($user->$p)) {
                $config['privilages'][] = $p;
            }
        }
        
        $this->getSession()->privilages = $config['privilages'];
        
        $this->view->appOptions = $config;
        $this->view->appClass = 'Step.Application';
    }
    
    public function assetAction()
    {
        $request = $this->getRequest();
        $module = $request->getQuery('module');
        $path = $request->getQuery('path');
        $path = substr($path, 0, strpos($path, '?'));
        
        $modulePath = $this->getFrontController()->getModuleDirectory($module);
        
        $response = $this->getResponse();
        $response->setBody(file_get_contents($modulePath . DIRECTORY_SEPARATOR . 'assets' . $path));
        $response->setHeader('Content-type', 'text/javascript');
        
        $response->sendResponse();
        exit;
        die(file_get_contents($modulePath . DIRECTORY_SEPARATOR . 'assets' . $path));
    }
    
    public function apiAction()
    {
        $modules = $this->getInvokeArg('bootstrap')->modules;
        $app = $this->_getParam('app');
        
        $API = array();
        $CFG = array();
        
        foreach ($modules as $name => $module) {
            if (!method_exists($module, 'getDirectMethods')) continue;
            
            $direct = $module->getDirectMethods();
            
            if ($direct && isset($direct[$app])) {
                $API[$module->getModuleName()] = array('methods' => $direct[$app]);
            }
        }
        // convert API config to Ext.Direct spec
        $actions = array();
        foreach ($API as $aname=>&$a) {
                $methods = array();
                foreach ($a['methods'] as $mname=>&$m) {
                    if (isset($m['len'])) {
                        $md = array(
                                'name'=>$mname,
                                'len'=> (int) $m['len']
                        );
                    } else {
                        $md = array(
                            'name'=>$mname,
                            'params'=>$m['params']
                        );
                    }
                    if (isset($m['formHandler']) && $m['formHandler']) {
                            $md['formHandler'] = true;
                    }
                        $methods[] = $md;
                }
                $actions[$aname] = $methods;
        }

        $directCfg = array(
            'url' => 'step/router?app=' . $app,
            'type' => 'remoting',
            'actions' => $actions,
            'total' => 2200
        );
        
        $this->_helper->layout->disableLayout();

        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'text/javascript', true);
        $response->clearBody();
        
        $response->appendBody('Ext.ns("Ext.app"); Ext.app.REMOTING_API = ');
        $response->appendBody(json_encode($directCfg));
        $response->appendBody(';');
        
        $response->sendResponse();
    }
    
    public function routerAction()
    {
        $this->_helper->layout->disableLayout();

        $router = new App_Direct_Router_Plugin();
        Zend_Controller_Front::getInstance()->registerPlugin($router);
    }
}