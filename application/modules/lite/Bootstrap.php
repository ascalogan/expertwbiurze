<?php

class Lite_Bootstrap extends App_Application_Module_Bootstrap
{
    protected function _initRoutes()
    {
        $this->addRoutes(array(
            array('route' => 'step/:action',  'params' => array('controller' => 'step'))
        ));
    }
    
    public function getDirectMethods()
    {
        return array(
            'Step.Login' => array(
                'doLogin' => array('len' => 2, 'formHandler' => true)
            ),
            'Step.Application' => array(
                'doLogout' => array('len' => 0)
            )
        );
    }
}