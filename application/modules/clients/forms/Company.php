<?php

class Clients_Form_Company extends Zend_Form
{
    
    protected $_isRegister;
    
    public function __construct($options = false) {
        $this->_isRegister = $options;
        parent::__construct();
    }
    
    public function init()
    {
        $this->addElement('Note', 'basic', array(
            'value' => 'Dane firmy',
            'class' => 'header'
        ));
        $this->addElement('Text', 'name', array(
            'label' => 'Nazwa firmy',
            'required' => true
        ));
        $this->addElement('Text', 'street', array(
            'label' => 'Adres firmy',
            'required' => true
        ));
        $this->addElement('Text', 'citycode', array(
            'label' => 'Kod pocztowy',
            'required' => true
        ));
        $this->addElement('Text', 'city', array(
            'label' => 'Miejscowość',
            'required' => true
        ));
        $this->addElement('Text', 'nip', array(
            'label' => 'NIP',
            'required' => true
        ));
        $this->addElement('Text', 'regon', array(
            'label' => 'Regon'
        ));
        $this->addElement('Note', 'contact', array(
            'value' => 'Dane kontaktowe',
            'class' => 'header'
        ));
        $this->addElement('Note', 'copy_to_contact', array(
            'value' => '<a href="javascript:copyDataTo(\'contact\')">Kliknij, aby skopiować z danych głównych</a>'
        ));
        $this->addElement('Text', 'contact_street', array(
            'label' => 'Adres firmy',
            'required' => true
        ));
        $this->addElement('Text', 'contact_citycode', array(
            'label' => 'Kod pocztowy',
            'required' => true
        ));
        $this->addElement('Text', 'contact_city', array(
            'label' => 'Miejscowość',
            'required' => true
        ));
        $this->addElement('Note', 'delivery', array(
            'value' => 'Dane dostawy',
            'class' => 'header'
        ));
        $this->addElement('Note', 'copy_to_delivery', array(
            'value' => '<a href="javascript:copyDataTo(\'delivery\')">Kliknij, aby skopiować z danych głównych</a>'
        ));
        $this->addElement('Text', 'delivery_street', array(
            'label' => 'Adres firmy',
            'required' => true
        ));
        $this->addElement('Text', 'delivery_citycode', array(
            'label' => 'Kod pocztowy',
            'required' => true
        ));
        $this->addElement('Text', 'delivery_city', array(
            'label' => 'Miejscowość',
            'required' => true
        ));
        
        if ($this->_isRegister) {
            $form = $this;
            $this->addElement('Note', 'main', array(
                'value' => 'Dane konta głównego',
                'class' => 'header'
            ));
            $this->addElement('Text', 'account_login', array(
                'label' => 'Login',
                'required' => true
            ));

            $form->addElement('Text', 'account_name', array(
                'label' => 'Imię',
                'required' => true
            ));
            $form->addElement('Text', 'account_surname', array(
                'label' => 'Nazwisko',
                'required' => true
            ));
            $form->addElement('Text', 'account_mail', array(
                'label' => 'Adres e-mail',
                'required' => true
            ));
            $form->addElement('Text', 'account_phone', array(
                'label' => 'Tel. kontaktowy',
                'required' => true
            ));
        }

        
        $this->addElement('Submit', 'submit', array(
            'label' => $this->_isRegister ? 'załóż konto' : 'zapisz dane'
        ));
    }
}