<?php

class Clients_Form_Password extends Zend_Form
{
    public function init()
    {
        $this->addElement('Password', 'old_password', array(
            'label' => 'Obecne hasło',
            'required' => true
        ));
        $this->addElement('Password', 'password', array(
            'label' => 'Nowe hasło',
            'required' => true,
            'description' => 'Hasło musi zawierać przynajmniej 6 znaków oraz jedną literę oraz cyfrę'
        ));
        $this->addElement('Password', 'password_confirm', array(
            'label' => 'Powtórz hasło',
            'required' => true
        ));
        $this->addElement('Submit', 'submit', array(
            'label' => 'zmień hasło'
        ));
    }
}