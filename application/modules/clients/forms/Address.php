<?php

class Clients_Form_Address extends Zend_Form
{
    
    public function init()
    {
        $this->addElement('Text', 'name', array(
            'label' => 'Nazwa adresu',
            'required' => true
        ));
        $this->addElement('Text', 'street', array(
            'label' => 'Adres firmy',
            'required' => true
        ));
        $this->addElement('Text', 'citycode', array(
            'label' => 'Kod pocztowy',
            'required' => true
        ));
        $this->addElement('Text', 'city', array(
            'label' => 'Miejscowość',
            'required' => true
        ));
        
        $this->addElement('Submit', 'submit', array(
            'label' => 'zapisz'
        ));
    }
}