<?php

class Clients_Bootstrap extends App_Application_Module_Bootstrap
{
    
    public function getDirectMethods()
    {
        return array(
            'Step.Application' => array(
                'getClients' => array('len' => 1),
                'getClient' => array('len' => 1),
                'saveClient' => array('len' => 1, 'formHandler' => true),
                'removeClient' => array('params' => array('id')),
                'getAddresses' => array('len' => 1),
                'saveAddress' => array('len' => 1, 'formHandler' => true),
                'removeAddress' => array('len' => 1),
                'getAddress' => array('len' => 1),
                'getAccounts' => array('len' => 1),
                'getAccount' => array('len' => 1),
                'saveAccount' => array('len' => 1, 'formHandler' => true),
                'getAccountsList' => array('len' => 1),
                'getGroupsPromotion' => array('len' => 1),
                'saveGroupsPromotion' => array('len' => 1),
                'setGroupsPromotion' => array('len' => 1, 'formHandler' => true),
                'getOrderHistory' => array('len' => 1),
                'getProductsHistory' => array('len' => 1),
                'getSpecialOfert' => array('len' => 1),
                'getProductOfert' => array('len' => 1),
                'saveProductOfert' => array('len' => 1),
                'removeSpecialOfert' => array('len' => 1),
                'setOfertPrice' => array('len' => 1, 'formHandler' => true),
                'getAccountPayments' => array('len' => 1),
                'addAccountBudget' => array('len' => 1, 'formHandler' => true),
                'clearAccountBudget' => array('len' => 1),
                'generatePassword' => array('len' => 2),
                'beforeRemoveAccount' => array('len' => 1),
                'removeAccount' => array('len' => 1),
                'removeParentAccount' => array('len' => 1, 'formHandler' => true),
                'getAccountProductsGroups' => array('len' => 1),
                'getAccountGroupVariants' => array('len' => 2),
                'saveAccountBlockedProducts' => array('len' => 3)
            )
        );
    }
    
    public function getRoutes()
    {
        return array(
            'login' => array(
                'url' => ';login',
                'suffix' => FALSE
            ),
            'logout' => array(
                'url' => ';logout',
                'suffix' => FALSE
            ),
            'login-error' => array(
                'url' => ';login-error',
                'suffix' => FALSE
            ),
            'register' => 'rejestracja',
            'registered' => 'zalozono-konto',
            'basket' => 'koszyk',
            'history' => 'historia-zamowien',
            'thrown' => 'zamowienia-odrzucone',
            'stats' => 'zamowienia,statystyki',
            'stats-products' => 'produkty,statystyki',
            'order-action' => array(
                'url' => ';order-action-(\d+)-(.+)',
                'route' => 'regex',
                'map' => array(1 => 'id', 2 => 'do'),
                'reverse' => ';order-action-%d-%s',
                'suffix' => FALSE
            ),
            'fast-product-add' => array(
                'url' => ';fast-product-add',
                'suffix' => FALSE
            ),
            'order' => array(
                'url' => '(\d+),zamowienie',
                'route' => 'regex',
                'map' => array(1 => 'id'),
                'reverse' => '%d,zamowienie'
            ),
            'order-to-pdf' => array(
                'url' => '(\d+),zamowienie-pdf',
                'route' => 'regex',
                'map' => array(1 => 'id'),
                'reverse' => '%d,zamowienie-pdf'
            ),
            'settings' => 'ustawienia',
            'settings-accounts' => 'konta,ustawienia',
            'settings-addresses' => 'adresy,ustawienia',
            'address-add' => array(
                'url' => 'dodaj-adres,ustawienia',
                'action' => 'address-form'
            ),
            'address-edit' => array(
                'action' => 'address-form',
                'url' => '(\d+),edycja-adresu,ustawienia',
                'route' => 'regex',
                'map' => array(1 => 'id'),
                'reverse' => '%d,edycja-adresu,ustawienia'
            ),
            'account-add' => array(
                'url' => 'dodaj-konto,ustawienia',
                'action' => 'account-form'
            ),
            'account-edit' => array(
                'action' => 'account-form',
                'url' => '(\d+),edycja-konta,ustawienia',
                'route' => 'regex',
                'map' => array(1 => 'id'),
                'reverse' => '%d,edycja-konta,ustawienia'
            ),
            'account-limits' => array(
                'url' => '(\d+),budzet-konta,ustawienia',
                'route' => 'regex',
                'map' => array(1 => 'id'),
                'reverse' => '%d,budzet-konta,ustawienia'
            ),
            'company-data' => 'dane-firmy,ustawienia',
            'password-change' => 'zmiana-hasla,ustawienia'
        );
    }
}