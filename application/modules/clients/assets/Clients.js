Ext.define("Module.clients.Clients", {
    extend: "Step.app.Module",
    id: "Clients",
    
    controllers: [
        "Clients"
    ],
    
    models: [
        "ClientAccount"
    ],
    
    stores: [
        "Clients",
        "ClientsAccounts",
        "GroupsPromotion",
        "ProductsHistory"
    ],
    
    getActions: function() {
        return {
            text: "Zarządzanie klientami",
            icon: "users_icon.png",
            controller: "Clients",
            action: "main",
            space: true
        }
    }
});

Ext.data.Types.FLOAT2 = {
    convert: function(v) {
        if (v !== null)
        v = v.replace(",", ".");
        return Ext.data.Types.FLOAT.convert(v);
    },
    sortType: Ext.data.Types.FLOAT.sortType,
    type: "float2"
};