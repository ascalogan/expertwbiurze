Ext.define("Module.clients.view.ClientAddresses", {
    extend: "Step.grid.Panel",
    alias: "widget.clientaddresses",
    
    store: "ClientsAddresses",
    title: "Adresy dostawy",
    
    initComponent: function() {
        var me = this;
        
        me.columns = [{
            header: "ID", dataIndex: "id"
        }, {
            header: "nazwa adresu", dataIndex: "name", flex: 2
        }, {
            header: "miejscowość", dataIndex: "city", flex: 1
        }, {
            header: "adres", dataIndex: "street"
        }];
    
        me.tbar = [{
            text: "dodaj adres",
            iconCls: "icon-create",
            handler: me.bindAction("create")
        }, {
            text: "edytuj adres",
            iconCls: "icon-edit",
            handler: me.bindItemAction("edit")
        }, {
            text: "usuń adres",
            iconCls: "icon-delete",
            handler: me.bindItemAction("delete", {
                confirmMsg: "Czy na pewno chcesz usunąć adres <strong>{name}</strong>?"
            }),
            hidden: true
        }];
    
        me.callParent();
    }
}); 