Ext.define("Module.clients.view.SpecialOfert", {
    extend: "Step.grid.Panel",
    alias: "widget.clientsspecialofert",
    
    requires: [
        "Module.products.widget.ProductsSearch"
    ],
    
    emptyText: "Klient nie posiada żadnego produktu w ofercie specjalnej",
    store: {
        type: "clientsspecialofert"
    }, 
    title: "Oferta specjalna",
    
    rowColumns: true,
    
    initComponent: function() {
        var me = this
        
        me.tbar = [];
        
        me.tbar.push({
            text: "dodaj nowy produkt",
            iconCls: "icon-create",
            handler: me.bindAction("newproduct")
        }, {
            text: "edytuj produkt",
            iconCls: "icon-edit",
            handler: me.bindItemAction("editproduct"),
            disabled: true,
            itemId: "edit-prod-btn"
        }, "|", {
            text: "edytuj ofertę",
            iconCls: "icon-edit",
            handler: me.bindItemAction("edit")
        });
        me.tbar.push({
            text: "usuń ofertę",
            iconCls: "icon-delete",
            handler: me.bindItemAction("delete", {
                confirmMsg: "Czy na pewno chcesz usunąć wybrany produkt z oferty specjalnej?"
            })
        });
        
        me.tbar.push("->");
        me.tbar.push({
            xtype: "cycle",
            showText: true,
            prependText: "Szukaj w ",
            menu: {
                items: [{
                    text: "katalogu",
                    type: "search-active",
                    checked: true
                }, {
                    text: "nieaktywnych/specjalnych",
                    type: "search-inactive"
                }, {
                    text: "po indeksie",
                    type: "search-index"
                }]
            },
            changeHandler: function(cycle, item) {
                me.changeSearchInput(item.type);
            }
        });
        me.tbar.push({
            xtype: "productssearch",
            emptyText: "wpisz szukaną frazę",
            itemId: "search-active",
            listeners: {
                choose: function(v) {
                    me.fireEvent("product", me, v);
                }
            }
        }, {
            xtype: "productssearch",
            emptyText: "wpisz szukaną frazę",
            searchFn: "searchProductsArchive",
            listeners: {
                choose: function(v) {
                    me.fireEvent("product", me, v);
                }
            },
            hidden: true,
            itemId: "search-inactive"
        }, {
            xtype: "productssearch",
            emptyText: "wpisz index",
            searchFn: "searchProductsIndex",
            listeners: {
                choose: function(v) {
                    me.fireEvent("product", me, v);
                }
            },
            hidden: true,
            itemId: "search-index"
        });
        
        me.columns = [{
            header: "ID", dataIndex: "id"
        }, {
            header: "marka", dataIndex: "mark", flex: 1
        }, {
            header: "nazwa produktu", dataIndex: "name", flex: 2
        }, {
            header: "typ", dataIndex: "status", renderer: function(v) {
                return {
                    1: "normalny",
                    2: "specjalny"
                }[v];
            }
        }, {
            header: "produktów", dataIndex: "products"
        }];
    
        me.features = [{
            ftype: "grouping"
        }];
    
        me.callParent();
        
        me.on("selectionchange", me.onProductSelect, me);
    },
    
    onProductSelect: function() {
        var btn = this.down("#edit-prod-btn"),
            item = this.getSelectedItem();
        
        if (item && item.get("status") === "2") {
            btn.enable();
        }
        else {
            btn.disable();
        }
    },
    
    changeSearchInput: function(type) {
        var me = this;
        
        Ext.each(["search-active", "search-inactive", "search-index"], function(name) {
            me.down("#" + name)[type == name ? "show" : "hide"]();
        });
    }
});