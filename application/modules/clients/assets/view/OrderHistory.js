Ext.define("Module.clients.view.OrderHistory", {
    extend: "Step.grid.Panel",
    alias: "widget.clientsorderhistory",
    
    title: "Historia zamówień",
    store: {type: "clientsorderhistory"},
    
    initComponent: function() {
        var me = this;
        
        me.tbar = [{
            xtype: "datefield",
            format: "Y-m-d",
            itemId: "date-from",
            fieldLabel: "Zamówienia od"
        }, {
            xtype: "datefield",
            format: "Y-m-d",
            itemId: "date-to",
            fieldLabel: "Zamówienia do"
        }, "|", {
            text: "pokaż zamówienia z wybranego okresu",
            handler: this.filterOrders,
            scope: this
        }]
        
        me.features = [{
            id: "groupSummary",
            ftype: "groupingsummary",
            groupHeaderTpl: "<tpl if='name'>{name}</tpl><tpl if='name==\"\"'>Konta usunięte</tpl>",
            startCollapsed: true
            
        }, {
            ftype: "summary",
            dock: "bottom"
        }];
        
        me.columns = [{
            header: "nr zamówienia", dataIndex: "id", flex: 1, summaryType: "count", summaryRenderer: function(v) {
                return v + " zamówień";
            }
        }, {
            header: "wartość zamówienia", dataIndex: "price_sum", flex: 1, renderer: function(v) {
                return Ext.util.Format.currency(v, "zł", 2, true);
            }, summaryType: "sum", summaryRenderer: function(v) {
                return Ext.util.Format.currency(v, "zł", 2, true);
            }
        }, {
            header: "data zamówienia", dataIndex: "pushed", flex: 1, xtype: "datecolumn", format: "Y-m-d",
            summaryType: "max", summaryRenderer: function(v) {
                return "ostatnie: " + Ext.Date.format(v, "Y-m-d");
            }
        }];
    
        me.callParent();
    },
    
    filterOrders: function() {
        var dateFrom = this.down("#date-from").getValue(),
            dateTo = this.down("#date-to").getValue();
            
        if (dateFrom) {
            dateFrom = Ext.Date.format(dateFrom, "Y-m-d");
        }
        if (dateTo) {
            dateTo = Ext.Date.format(dateTo, "Y-m-d");
        }
        
        this.getStore().load({
            params: {
                id: this.clientId,
                date_from: dateFrom,
                date_to: dateTo
            }
        });
    }
}); 