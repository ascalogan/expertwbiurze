Ext.define("Module.clients.view.List", {
    extend: "Step.grid.Panel",
    alias: "widget.clientslist",
    
    store: "Clients",
    title: "Lista klientów",
    
    initComponent: function() {
        var me = this;
        
        me.columns = [{
            header: "ID", dataIndex: "id"
        }, {
            header: "nazwa klienta", dataIndex: "name", flex: 2
        }, {
            header: "miejscowość", dataIndex: "city", flex: 1
        }, {
            header: "NIP", dataIndex: "nip"
        }, {
            header: "handlowiec", dataIndex: "merchant_name", flex: 1,  renderer: function(v) {
                return v.length ? v : "-"
            }
        }];
    
        me.tbar = [{
            text: "dodaj klienta",
            iconCls: "icon-create",
            handler: me.bindAction("create")
        }, {
            text: "edytuj klienta",
            iconCls: "icon-edit",
            handler: me.bindItemAction("edit")
        }, {
            text: "usuń klienta",
            iconCls: "icon-delete",
            handler: me.bindItemAction("delete", {
                confirmMsg: "Czy na pewno chcesz usunąć klienta <strong>{name}</strong>?"
            }),
            hidden: true
        }];
    
        me.viewConfig = {
            getRowClass: function(record) {
                return !record.get("users_id") ? "x-marked" : "";
            }
        }
    
        me.callParent();
    }
}); 