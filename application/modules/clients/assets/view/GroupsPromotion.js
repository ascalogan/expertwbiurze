Ext.define("Module.clients.view.GroupsPromotion", {
    extend: "Step.grid.Panel",
    alias: "widget.clientsgroupspromotion",
    
    title: "Rabaty grup",
    selModel: "cellmodel",
    plugins: [{
        ptype: "cellediting",
        clicksToEdit: 1
    }],
    
    initComponent: function() {
        var me = this;
        
        me.columns = [{
            header: "nazwa grupy", dataIndex: "name", flex: 1
        }, {
            header: "rabat", dataIndex: "percent", renderer: function(v) {
                return (v || "0") + "%";
            }, editor: {
                xtype: "numberfield",
                increment: 1,
                minValue: 0,
                maxValue: 100
            }
        }];
    
        me.tbar = [{
            text: "ustaw rabat dla wszystkich grup",
            iconCls: "icon-fill",
            handler: me.bindAction("promoset")
        }]
        
        me.callParent();
    }
})