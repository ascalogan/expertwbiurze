Ext.define("Module.clients.view.GroupsPromotionSet", {
    extend: "Step.form.Panel",
    alias: "widget.clientsgroupspromotionset",
    
    title: "Ustawianie rabatu dla wszystkich grup",
    api: {
        submit: Clients.setGroupsPromotion
    },
    
    initComponent: function() {
        var me = this;
        
        me.items = [{
            xtype: "numberfield",
            name: "percent",
            allowBlank: false,
            increment: 1,
            minValue: 0,
            maxValue: 100,
            fieldLabel: "Rabat dla grup"
        }, {
            xtype: "hiddenfield",
            name: "clients_id",
            value: me.clientId
        }];
    
        me.callParent();
    }
});