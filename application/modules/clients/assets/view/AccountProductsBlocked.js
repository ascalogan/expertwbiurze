Ext.define("Module.clients.view.AccountProductsBlocked", {
    extend: "Step.grid.Panel",
    alias: "widget.accountproductsblocked",
    
    initComponent: function() {
        var me = this;
        
        var sm = Ext.create("Ext.selection.CheckboxModel");
        me.selModel = sm;
        
        me.tbar = [{
            text: "zapisz zablokowane produkty",
            handler: me.bindAction("saveblocked")
        }];
        
        me.store = Ext.create("Ext.data.Store", {
            fields: [{
                name: "id", type: "int"
            }, {
                name: "index", type: "string"
            }, {
                name: "mark", type: "string"
            }, {
                name: "name", type: "string"
            }, {
                name: "isBlocked", type: "int"
            }],
            proxy: {
                type: "direct",
                api: {
                    read: Clients.getAccountGroupVariants
                },
                reader: {root: "items"},
                paramOrder: ["id", "account_id"]
            }
        });
        
        me.columns = [{
            header: "index", dataIndex: "index", flex: 1
        }, {
            header: "marka", dataIndex: "mark", flex: 1
        }, {
            header: "nazwa", dataIndex: "name", flex: 1
        }];
    
        me.callParent();
        
        me.store.load({
            params: {id: me.groupId, account_id: me.accountId},
            callback: function(records) {
                Ext.each(records, function(r) {
                    if (r.get("isBlocked")) {
                        me.getSelectionModel().select(r, true);
                    }
                })
            }
        });
    }
});