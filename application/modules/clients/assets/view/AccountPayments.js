Ext.define("Module.clients.view.AccountPayments", {
    extend: "Step.grid.Panel",
    alias: "widget.clientsaccountpayments",
    
    store: {
        type: "clientsaccountpayments"
    },
    title: "Finanse",
    
    initComponent: function() {
        var me = this;
        
        me.tbar = [{
            text: "przyznaj dodatkowy budżet",
            iconCls: "icon-money",
            handler: me.bindAction("addbudget")
        }, {
            text: "wyzeruj budżet",
            iconCls: "icon-cancel",
            handler: me.bindAction("clearbudget")
        }];
        
        me.columns = [{
            header: "data", dataIndex: "created", xtype: "datecolumn", format: "Y-m-d"
        }, {
            header: "operacja", dataIndex: "type", flex: 1, renderer: function(v) {
                if (v == "0") {
                    
                }
                else if (v == "1") {
                    return "Przyznano limit miesięczny";
                }
                else if (v == "2") {
                    return "Przyznano limit kwartalny";
                }
                else if (v == "3") {
                    return "Przyznano półroczny limit";
                }
                else if (v == "4") {
                    return "Przyznano roczny limit";
                }
                else if (v == "5") {
                    return "Przyznano dodatkowy budżet";
                }
                else if (v == "6") {
                    return "Zamówienie";
                }
                else if (v == "7") {
                    return "Budżet został wyzerowany";
                }
            }
        }, {
            header: "kwota", dataIndex: "amount", renderer: function(v) {
                return Ext.util.Format.currency(v, "zł", 2, true);
            }
        }, {
            header: "budżet", dataIndex: "budget_after", renderer: function(v) {
                return Ext.util.Format.currency(v, "zł", 2, true);
            }
        }];
    
        me.callParent();
    }
});