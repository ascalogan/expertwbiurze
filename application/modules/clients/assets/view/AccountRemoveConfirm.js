Ext.define("Module.clients.view.AccountRemoveConfirm", {
    extend: "Step.form.Panel",
    alias: "widget.accountremoveconfirm",
    
    api: {
        submit: Clients.removeParentAccount
    },
    
    initComponent: function() {
        var me = this;
        
        me.items = [{
            xtype: "component",
            html: "Wybrane konto posiada przypisane do siebie inne konta. Należy wybrać konto, do którego te konta zostaną przypisane.",
            style: "margin-bottom: 20px"
        }, {
            xtype: "component",
            html: "Wszystkim podrzędnym kontom w stosunku do usuwanego konta, które posiadają je jako weryfikatora, zostanie przypisany weryfikator w postaci wybranego niżej konta.",
            style: "margin-bottom: 20px"
        }, {
            xtype: "hiddenfield",
            name: "id",
            value: me.accountId
        }, {
            xtype: "combo",
            name: "parent",
            valueField: "id",
            displayField: "name",
            store: Ext.create("Ext.data.Store", {
                data: me.accounts,
                fields: ["id", "name"]
            }),
            fieldLabel: "Wybierz nowe konto",
            allowBlank: false
        }];
    
        me.callParent();
    }
});