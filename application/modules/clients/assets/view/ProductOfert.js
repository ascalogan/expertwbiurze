Ext.define("Module.clients.view.ProductOfert", {
    extend: "Step.grid.Panel",
    alias: "widget.clientsproductofert",
    
    selType: "rowmodel",
    plugins: [{
        ptype: "rowediting",
        clicksToEdit: 1
    }],
    style: "padding-bottom: 30px",
    
    initComponent: function() {
        var me = this,
            columns = [];
            
        me.tbar = [{
            text: "ustaw cenę dla wszystkich wariantów",
            tooltip: "Włącza wszystkie warianty produktu do oferty specjalnej z podaną ceną",
            handler: me.bindAction("setproductprices")
        }];
    
        if (me.productStatus == "2") {
            me.tbar.push("->", {
                text: "edytuj produkt",
                iconCls: "icon-edit",
                handler: me.bindAction("editproduct")
            });
        }
        
        me.store = {
            fields: [{
                name: "products_id", type: "int"
            }, {
                name: "variants_id", type: "int"
            }, {
                name: "index", type: "string"
            }, {
                name: "clients_id", type: "int"
            }, {
                name: "normal_price", type: "float"
            }, {
                name: "price", type: "float2"
            }, {
                name: "fields", type: "auto"
            }, {
                name: "active", type: "bool"
            }],
            proxy: {
                type: "direct",
                api: {
                    read: Clients.getProductOfert,
                    update: Clients.saveProductOfert
                },
                reader: {root: "items"}
            },
            autoSync: true
        };
    
        columns.push({
            header: "w ofercie", dataIndex: "active", renderer: function(v, m, r) {
                return r.get("price") > 0 ? "tak" : "nie"
            }
        }, {
            header: "status", dataIndex: "status", renderer: function(v) {
                if (me.productStatus == "2") return "specjalny";
                else if (v == "1") return "katalogowy";
                return "wycofany";
            }
        }, {
            header: "index", dataIndex: "index", flex: 1
        });
            
        Ext.each(me.fields, function(item) {
            columns.push({
                header: item.name, dataIndex: "fields", flex: item.type == "textfield" ? 1 : 0, renderer: function(v) {
                    if (item.type == "textfield") return v[item.index];
                    return '<img src="colors/' + v[item.index] + '.gif" style="border: 1px solid #000">';
                }
            });
        });

        columns.push({
            header: "cena standardowa", dataIndex: "normal_price", flex: 1, renderer: function(v) {
                return Ext.util.Format.currency(v, "zł", 2, true);
            }
        }, {
            header: "cena specjalna", dataIndex: "price", flex: 1, renderer: function(v) {
                return Ext.util.Format.currency(v, "zł", 2, true);
            }, editor: {
                xtype: "textfield"
            }
        });
        
        me.columns = columns;
    
        me.callParent();
        
        me.getStore().load({
            params: {
                client_id: me.clientId,
                product_id: me.productId
            }
        });
    }
})