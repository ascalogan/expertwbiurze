Ext.define("Module.clients.view.AddressForm", {
    extend: "Step.form.Panel",
    alias: "widget.clientaddressform",
    
    title: "Adres dostawy",
    
    api: {
        submit: Clients.saveAddress
    },
    
    initComponent: function() {
        var me = this;
        
        me.items = [{
            xtype: "hidden",
            name: "id"
        }, {
            xtype: "hidden",
            name: "client_id"
        }, {
            xtype: "textfield",
            name: "name",
            fieldLabel: "Nazwa adresu",
            allowBlank: false
        }, {
            xtype: "textfield",
            name: "street",
            fieldLabel: "Adres firmy",
            allowBlank: false
        }, {
            xtype: "textfield",
            name: "citycode",
            fieldLabel: "Kod pocztowy",
            allowBlank: false,
            regex: /^\d{2}-\d{3}$/
        }, {
            xtype: "textfield",
            name: "city",
            fieldLabel: "Miejscowość",
            allowBlank: false
        }];
    
        this.callParent();
    }
});