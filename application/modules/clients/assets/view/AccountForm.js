Ext.define("Module.clients.view.AccountForm", {
    extend: "Step.form.Panel",
    alias: "widget.clientaccountform",
    
    api: {
        submit: Clients.saveAccount
    },
    
    initComponent: function() {
        var me = this,
            items = [];
        
        items.push({
            xtype: "hidden",
            name: "id"
        }, {
            xtype: "hidden",
            name: "clients_id",
            value: me.getMainAccount().get("clients_id")
        });
        
        // We are creating new account
        if (!me.values) {
            var types = [];
            types.push({
                boxLabel: "Konto drugiego poziomu", name: "type", inputValue: "2", checked: !me.hasManyAccounts()
            });
            if (me.hasManyAccounts()) {
                types.push({
                    boxLabel: "Konto trzeciego poziomu", name: "type", inputValue: "3"
                });
            }
            items.push({
                xtype: "radiogroup",
                fieldLabel: "Typ konta",
                columns: 1,
                allowBlank: false,
                items: types,
                listeners: {
                    change: me.onTypeChange,
                    scope: this
                }
            });
            
            items.push({
                xtype: "displayfield",
                itemId: "display_parents",
                fieldLabel: "Konto nadrzędne",
                value: me.getAccountLabel(me.getMainAccountId()),
                hidden: me.hasManyAccounts()
            });
            
            if (me.hasManyAccounts()) {
                items.push({
                    xtype: "combo",
                    itemId: "combo_parents",
                    fieldLabel: "Konto nadrzędne",
                    displayField: "label",
                    valueField: "id",
                    editable: false,
                    store: {
                        fields: ["id", "label"],
                        data: me.getParentAccounts()
                    },
                    emptyText: "- wybierz konto nadrzędne",
                    listeners: {
                        change: this.onParentChange,
                        scope: this
                    },
                    hidden: true
                });
            }
            
            items.push({
                xtype: "hidden",
                name: "parent",
                value: me.hasManyAccounts() ? null : me.getMainAccountId()
            });
        }
        else {
            me.buttons = [{
               text: "zablokowane produkty",
               handler: function() {
                   me.fireEvent("productsblocked", me.values.id);
               },
               hidden: me.values.parent == "0"
            }, "->", {
                text: "wygeneruj nowe hasło",
                handler: function() {
                    me.fireEvent("newpassword", me.values.id);
                }
            }];
        
            items.push({
                xtype: "displayfield",
                fieldLabel: "Konto nadrzędne",
                value: this.getParentValue()
            });
            
            // Account verifier
            if (me.values.parent == "0") {
                items.push({
                    xtype: "displayfield",
                    fieldLabel: "Weryfikator",
                    value: "Konto główne nie posiada weryfikatora."
                });
            }
            else {
                var verifiers = [],
                    account = me.accounts.getNodeById(me.values.parent);
                
                verifiers.push({boxLabel: "To konto nie posiada weryfikatora", name: "verifier", inputValue: "0"});
                verifiers.push({
                    boxLabel: (account.get("parent") == "0" ? "Konto główne" : "Konto drugiego poziomu") + ": " + me.getAccountLabel(me.values.parent),
                    name: "verifier", inputValue: me.values.parent
                });
                
                if (account.get("parent") != "0") {
                    account = me.accounts.getNodeById(account.get("parent"));
                    verifiers.push({
                        boxLabel: "Konto główne: " + me.getAccountLabel(account.get("id")),
                        name: "verifier", inputValue: account.get("id")
                    });
                }
                
                items.push({
                    xtype: "radiogroup",
                    fieldLabel: "Weryfikator",
                    allowBlank: false,
                    columns: 1,
                    items: verifiers
                });
            }
        }
        
        items.push({
            xtype: me.values ? "displayfield" : "textfield",
            name: "login",
            fieldLabel: "Login",
            allowBlank: false
        }, {
            xtype: "textfield",
            name: "name",
            fieldLabel: "Imię",
            allowBlank: false
        }, {
            xtype: "textfield",
            name: "surname",
            fieldLabel: "Nazwisko",
            allowBlank: false
        }, {
            xtype: "textfield",
            name: "mail",
            fieldLabel: "Adres e-mail",
            allowBlank: false,
            vtype: "email"
        }, {
            xtype: "textfield",
            name: "phone",
            fieldLabel: "Tel. kontaktowy",
            allowBlank: false
        }, {
            xtype: "checkboxfield",
            name: "allow_address",
            fieldLabel: "Zezwól na wybór adresu dostawy",
            inputValue: "1",
            uncheckedValue: "0",
            hidden: me.values && me.values.parent == "0"
        }, {
            xtype: "combo",
            name: "address_id",
            fieldLabel: "Domyślny adres dostawy",
            displayField: "name",
            valueField: "id",
            store: me.addresses,
            editable: false,
            lastQuery: "",
            value: "0"
        }, {
            xtype: "radiogroup",
            fieldLabel: "Rodzaj budżetu",
            allowBlank: false,
            columns: 1,
            hidden: me.values && me.values.parent == "0",
            items: [{
                boxLabel: "brak limitów", name: "limit_type", inputValue: "0"
            }, {
                boxLabel: "budżet miesięczny", name: "limit_type", inputValue: "1"
            }, {
                boxLabel: "budżet kwartalny", name: "limit_type", inputValue: "2"
            }, {
                boxLabel: "budżet półroczny", name: "limit_type", inputValue: "3"
            }, {
                boxLabel: "budżet roczny", name: "limit_type", inputValue: "4"
            }],
            listeners: {
                change: this.onLimitTypeChange,
                scope: this
            }
        }, {
            xtype: "numberfield",
            name: "limit",
            fieldLabel: "Kwota limitu",
            allowBlank: false,
            increment: 1,
            value: 0,
            minValue: 0,
            hidden: !me.values || me.values.limit_type == "0"
        }, {
            xtype: "numberfield",
            name: "budget",
            fieldLabel: "Budżet podstawowy",
            hidden: !me.values || me.values.limit_type == "0"
        }, {
            xtype: "numberfield",
            name: "budget_extra",
            fieldLabel: "Budżet dodatkowy",
            hidden: !me.values || me.values.limit_type == "0",
            description: "Budżet dodatkowy jest zawsze budżetem przechodzącym."
        }, {
            xtype: "checkboxfield",
            name: "limit_permanent",
            fieldLabel: "Limit przechodzący",
            inputValue: "1",
            uncheckedValue: "0",
            hidden: me.values && me.values.parent == "0"
        });
        
        me.items = items;
        
        if (me.values && me.values.parent != "0") {
            var groups = [],
                gvals = [];
            Ext.each(me.groups, function(g) {
                groups.push({boxLabel: g.name, name: "allowedGroups[]", inputValue: g.id});
                gvals.push(g.id);
            });
            groups.push({
                boxLabel: "zaznacz wszystkie grupy", name: "checkAllGroups", listeners: {
                    change: function(el, t) {
                        console.debug(t);
                        if (t) {
                            me.down("#gAccess").setValue({"allowedGroups[]": gvals, checkAllGroups: true});
                        }
                        else {
                            me.down("#gAccess").setValue({"allowedGroups[]": [], checkAllGroups: false});
                        }
                    }
                }
            });
            
            me.items.push({
                xtype: "checkboxgroup",
                items: groups,
                itemId: "gAccess",
                fieldLabel: "Dostęp do grup",
                value: {"allowedGroups[]": me.allowed},
                columns: 1
            });
        }
        
        if (me.values && me.values.parent != "0") {
            var bgroups = [],
                bgvals = [];
            Ext.each(me.groups, function(g) {
                bgroups.push({boxLabel: g.name, name: "bansGroups[]", inputValue: g.id});
                bgvals.push(g.id);
            });
            bgroups.push({
                boxLabel: "zaznacz wszystkie grupy", name: "checkAllGroups", listeners: {
                    change: function(el, t) {
                        if (t) {
                            me.down("#gBans").setValue({"bansGroups[]": gvals, checkAllGroups: true});
                        }
                        else {
                            me.down("#gBans").setValue({"bansGroups[]": [], checkAllGroups: false});
                        }
                    }
                }
            });
            
            me.items.push({
                xtype: "checkboxgroup",
                items: bgroups,
                itemId: "gBans",
                fieldLabel: "Zablokuj dostęp do oferty specjalnej w grupach",
                value: {"bansGroups[]": me.bans},
                columns: 1
            });
        }
    
        me.callParent();
    },
    
    getParentValue: function() {
        if (this.values.parent == "0") {
            return "To konto jest kontem głównym.";
        }
        
        return this.getAccountLabel(this.values.parent);
    },
    
    getAccountLabel: function(id) {
        var account = this.accounts.getNodeById(id);
        
        if (account) {
            return account.get("name") + " " + account.get("surname") + " (" + account.get("login") + ")";
        }
    },
    
    hasManyAccounts: function() {
        return this.accounts.tree.root.childNodes[0].childNodes.length;
    },
    
    getMainAccountId: function() {
        return this.accounts.tree.root.childNodes[0].get("id");
    },
    
    getMainAccount: function() {
        return this.accounts.tree.root.childNodes[0];
    },
    
    getParentAccounts: function() {
        var me = this,
            accounts = [];
        
        Ext.each(me.accounts.tree.root.childNodes[0].childNodes, function(node) {
            accounts.push({id: node.get("id"), label: me.getAccountLabel(node.get("id"))});
        });
        
        return accounts;
    },
    
    onTypeChange: function(el, value) {
        var display = this.down("#display_parents"),
            combo = this.down("#combo_parents"),
            parent = this.getForm().findField("parent");
            
        if (value.type == "2") {
            combo.hide();
            combo.allowBlank = true;
            display.show();
            parent.setValue(this.getMainAccountId());
        }
        else {
            display.hide();
            combo.show();
            combo.allowBlank = false;
            parent.setValue(combo.getValue());
        }
    },
    
    onParentChange: function(combo, value) {
        this.getForm().findField("parent").setValue(value);
    },
    
    onLimitTypeChange: function(el, value) {
        var me = this;
        
        Ext.each(["limit", "budget", "budget_extra", "limit_permanent"], function(n) {
            me.getForm().findField(n)[value.limit_type == "0" ? "hide" : "show"]();
        });
    }
})