Ext.define("Module.clients.view.ProductsHistory", {
    extend: "Step.grid.Panel",
    alias: "widget.clientsproductshistory",
    
    title: "Statystyki produktów",
    store: {type: "clientsproductshistory"},
    
    initComponent: function() {
        var me = this;
        
        me.features = [{
            ftype: "groupingsummary",
            startCollapsed: true,
            groupHeaderTpl: "<tpl if='name'>{name}</tpl><tpl if='name==\"\"'>Oferta specjalna</tpl>"
        }, {
            ftype: "summary",
            dock: "bottom"
        }];
    
        me.tbar = [{
            xtype: "datefield",
            format: "Y-m-d",
            itemId: "date-from",
            fieldLabel: "Data od"
        }, {
            xtype: "datefield",
            format: "Y-m-d",
            itemId: "date-to",
            fieldLabel: "Data do"
        }, "|", {
            text: "pokaż statystyki z wybranego okresu",
            handler: this.filterOrders,
            scope: this
        }, "->", {
            xtype: "combo",
            itemId: "account-id",
            fieldLabel: "Wybierz konto",
            emptyText: "- wybierz konto",
            valueField: "id",
            displayField: "name",
            store: {type: "clientsaccountslist"},
            queryMode: 'local',
            listeners: {
                change: this.filterOrders,
                scope: this
            }
        }, {
            text: "do excela",
            handler: this.doExcel,
            scope: this
        }];
    
        me.columns = [{
            header: "index", dataIndex: "index"
        }, {
            header: "marka", dataIndex: "mark"
        }, {
            header: "nazwa produktu", dataIndex: "name", flex: 1
        }, {
            header: "ilość", dataIndex: "quantity", summaryType: "sum"
        }, {
            header: "wartość", dataIndex: "price_sum", summaryType: "sum", renderer: function(v) {
                return Ext.util.Format.currency(v, "zł", 2, true);
            }, summaryRenderer: function(v) {
                return Ext.util.Format.currency(v, "zł", 2, true);
            }
        }];
    
        me.callParent();
        
        me.down("#account-id").getStore().load({
            params: {id: me.clientId}
        });
    },
    
    doExcel: function() {
        var url = 'clients/step/products-history-to-xls?id=' + this.clientId,
            dateFrom = this.down("#date-from").getValue(),
            dateTo = this.down("#date-to").getValue(),
            accountId = this.down("#account-id").getValue() || 0;
        
        if (dateFrom) {
            dateFrom = Ext.Date.format(dateFrom, "Y-m-d");
        }
        if (dateTo) {
            dateTo = Ext.Date.format(dateTo, "Y-m-d");
        }
        
        if (dateFrom)
            url += '&date_from=' + dateFrom;
        if (dateTo)
            url += '&date_to=' + dateTo;
        if (accountId)
            url += '&account_id=' + accountId;
        
        window.location.href = url;
    },
    
    filterOrders: function() {
        var dateFrom = this.down("#date-from").getValue(),
            dateTo = this.down("#date-to").getValue(),
            accountId = this.down("#account-id").getValue() || 0;
            
        if (dateFrom) {
            dateFrom = Ext.Date.format(dateFrom, "Y-m-d");
        }
        if (dateTo) {
            dateTo = Ext.Date.format(dateTo, "Y-m-d");
        }
        
        this.getStore().load({
            params: {
                id: this.clientId,
                date_from: dateFrom,
                date_to: dateTo,
                account_id: accountId
            }
        });
    }
});