Ext.define("Module.clients.view.AccountProductsGroups", {
    extend: "Step.grid.Panel",
    alias: "widget.accountproductsgroups",
    
    initComponent: function() {
        var me = this;
        
        me.columns = [{
            header: "nazwa grupy", dataIndex: "name", flex: 1
        }, {
            header: "ilość zablokowanych", dataIndex: "amount"
        }];
    
        me.store = Ext.create("Ext.data.Store", {
            fields: [
                "id", "name", "amount"
            ],
            data: me.groups
        });
        
        me.callParent();
    }
});