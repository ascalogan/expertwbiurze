Ext.define("Module.clients.view.ClientAccounts", {
    extend: "Ext.tree.Panel",
    alias: "widget.clientaccounts",
    
    rootVisible: false,
    useArrows: true,
    columnLines: true,
    rowLines: true,
    
    title: "Konta klienta",
    
    initComponent: function() {
        var me = this;
        
        me.store = {
            model: Module.clients.model.ClientAccount,
            proxy: {
                type: "direct",
                api: {
                    read: Clients.getAccounts
                },
                paramOrder: "id",
                reader: {
                    root: "items"
                }
            }
        };
        
        me.tbar = [{
            text: "dodaj konto",
            handler: function(button) {
                this.fireEvent("createaccount", button, me);
            }, scope: this
        }, {
            text: "edytuj konto",
            handler: function(button) {
                this.fireEvent("itemaction", this, this.getSelectedItem(), {
                    module: "Clients",
                    controller: "Clients",
                    action: "editAccount"
                }, button)
            },
            scope: this
        }, {
            text: "usuń konto",
            handler: function(button) {
                if (this.getSelectedItem())
                me.fireEvent("itemdelete", this, this.getSelectedItem(), button);
            },
            scope: this
        }]
        
        me.columns = [{
            header: "ID", dataIndex: "id"
        }, {
            xtype: "treecolumn", header: "adres e-mail", dataIndex: "mail", flex: 1, sortable: false
        }, {
            header: "imię", dataIndex: "surname", flex: 1, sortable: false
        }, {
            header: "nazwisko", dataIndex: "name", flex: 1, sortable: false
        }, {
            header: "login", dataIndex: "login", flex: 1, sortable: false
        }, {
            header: "weryfikator", dataIndex: "verifier", flex: 1, renderer: function(v, m, r) {
                if (v == "0") return "-";
                console.debug(r.store);
                return r.store.treeStore.getNodeById(v).get("login");
                return v;
                var account = r.getStore().getNodeById(v);
                return v;
                return account.get("login");
            }
        }, {
            header: "budżet", dataIndex: "budget", flex: 1, renderer: function(v, m, r) {
                if (!r.get('limit_type')) return '-';
                return v;
            }
        }, {
            header: "limit", dataIndex: "limit", flex: 1, renderer: function(v, m, r) {
                if (!r.get('limit_type')) return '-';
                return v;
            }
        }];
    
        this.callParent();
    },
    
    getSelectedItem: function() {
        return this.getSelectionModel().getSelection()[0];
    }
});