Ext.define("Module.clients.view.ClientForm", {
    extend: "Step.form.Panel",
    alias: "widget.clientform",
    
    title: "Dane klienta",
    
    api: {
        submit: Clients.saveClient
    },
    
    initComponent: function() {
        var me = this;
        
        me.items = [{
            xtype: "hidden",
            name: "id"
        }, {
            xtype: "checkboxfield",
            name: "enable_stats",
            fieldLabel: "Udostępnij statystyki",
            inputValue: "1",
            uncheckedValue: "0"
        }, {
            xtype: "combo",
            name: "users_id",
            displayField: "login",
            valueField: "id",
            fieldLabel: "Handlowiec",
            store: "Users",
            hidden: !Step.getApplication().hasAccess("clients")
        }, {
            xtype: "fieldset",
            title: "Dane firmy",
            collapsible: true,
            padding: 10,
            defaults: this.defaults,
            items: [{
                xtype: "textfield",
                name: "name",
                fieldLabel: "Nazwa klienta",
                allowBlank: false
            }, {
                xtype: "textfield",
                name: "street",
                fieldLabel: "Adres firmy",
                allowBlank: false
            }, {
                xtype: "textfield",
                name: "citycode",
                fieldLabel: "Kod pocztowy",
                allowBlank: false,
                regex: /^\d{2}-\d{3}$/
            }, {
                xtype: "textfield",
                name: "city",
                fieldLabel: "Miejscowość",
                allowBlank: false
            }, {
                xtype: "textfield",
                name: "nip",
                fieldLabel: "NIP",
                allowBlank: false
            }, {
                xtype: "textfield",
                name: "regon",
                fieldLabel: "Regon"
            }]
        }, {
            xtype: "fieldset",
            title: "Dane kontaktowe",
            collapsible: true,
            padding: 10,
            defaults: this.defaults,
            items: [{
                xtype: "button",
                text: "skopiuj z danych firmy",
                iconCls: "icon-copy",
                handler: this.copyFields,
                scope: this,
                targetType: "contact",
                anchor: "auto",
                style: "margin-bottom: 8px"
            }, {
                xtype: "textfield",
                name: "contact_street",
                fieldLabel: "Adres firmy",
                allowBlank: false
            }, {
                xtype: "textfield",
                name: "contact_citycode",
                fieldLabel: "Kod pocztowy",
                allowBlank: false,
                regex: /^\d{2}-\d{3}$/
            }, {
                xtype: "textfield",
                name: "contact_city",
                fieldLabel: "Miejscowość",
                allowBlank: false
            }]
        }, {
            xtype: "fieldset",
            title: "Dane dostawy",
            collapsible: true,
            padding: 10,
            defaults: this.defaults,
            items: [{
                xtype: "button",
                text: "skopiuj z danych firmy",
                iconCls: "icon-copy",
                handler: this.copyFields,
                scope: this,
                targetType: "delivery",
                anchor: "auto",
                style: "margin-bottom: 8px"
            }, {
                xtype: "textfield",
                name: "delivery_street",
                fieldLabel: "Adres firmy",
                allowBlank: false
            }, {
                xtype: "textfield",
                name: "delivery_citycode",
                fieldLabel: "Kod pocztowy",
                allowBlank: false,
                regex: /^\d{2}-\d{3}$/
            }, {
                xtype: "textfield",
                name: "delivery_city",
                fieldLabel: "Miejscowość",
                allowBlank: false
            }]
        }];
    
        if (!me.values) {
            me.items.push({
                xtype: "fieldset",
                title: "Dane konta głównego",
                collapsible: true,
                padding: 10,
                defaults: this.defaults,
                items: [{
                    xtype: "textfield",
                    name: "account_login",
                    fieldLabel: "Login",
                    allowBlank: false
                }, {
                    xtype: "textfield",
                    name: "account_name",
                    fieldLabel: "Imię",
                    allowBlank: false
                }, {
                    xtype: "textfield",
                    name: "account_surname",
                    fieldLabel: "Nazwisko",
                    allowBlank: false
                }, {
                    xtype: "textfield",
                    name: "account_mail",
                    fieldLabel: "Adres e-mail",
                    allowBlank: false,
                    vtype: "email"
                }, {
                    xtype: "textfield",
                    name: "account_phone",
                    fieldLabel: "Tel. kontaktowy",
                    allowBlank: false
                }]
            })
        }
    
        this.callParent();
    },
    
    copyFields: function(button) {
        var type = button.targetType + "_",
            form = this.getForm();
            
        Ext.each(["street", "citycode", "city"], function(name) {
            form.findField(type + name).setValue(form.findField(name).getValue());
        });
    }
})