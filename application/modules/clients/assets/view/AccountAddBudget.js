Ext.define("Module.clients.view.AccountAddBudget", {
    extend: "Step.form.Panel",
    alias: "widget.accountaddbudget",
    
    api: {
        submit: Clients.addAccountBudget
    },
    
    initComponent: function() {
        var me = this;
        
        me.items = [{
            xtype: "hiddenfield",
            name: "accounts_id",
            value: me.accountId
        }, {
            xtype: "numberfield",
            name: "amount",
            increment: 1,
            minValue: 1,
            fieldLabel: "Kwota dodatkowa"
        }];
    
        me.callParent();
    }
});