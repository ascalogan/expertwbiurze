Ext.define("Module.clients.view.ProductOfertPrice", {
    extend: "Step.form.Panel",
    alias: "widget.productofertprice",
    
    api: {
        submit: Clients.setOfertPrice
    },
    
    initComponent: function() {
        var me = this;
        
        me.items = [{
            xtype: "hiddenfield",
            name: "client_id",
            value: me.clientId
        }, {
            xtype: "hiddenfield",
            name: "product_id",
            value: me.productId
        }, {
            xtype: "textfield",
            name: "price",
            allowBlank: false,
            fieldLabel: "Cena specjalna"
        }];
    
        me.callParent();
    }
});