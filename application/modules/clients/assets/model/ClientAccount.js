Ext.define("Module.clients.model.ClientAccount", {
    extend: "Ext.data.Model",
    
    fields: [{
        name: "id", type: "int"
    }, {
        name: "mail", type: "string"
    }, {
        name: "name", type: "string"
    }, {
        name: "surname", type: "string"
    }, {
        name: "login", type: "string"
    }, {
        name: "parent", type: "string"
    }, {
        name: "verifier", type: "string"
    }, {
        name: "clients_id", type: "string"
    }, {
        name: "budget"
    }, {
        name: "limit"
    }, {
        name: "limit_type", type: "int"
    }]
});