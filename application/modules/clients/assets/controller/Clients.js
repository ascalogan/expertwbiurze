Ext.define("Module.clients.controller.Clients", {
    extend: "Step.app.Controller",
    
    views: [
        "List",
        "ClientForm",
        "ClientAddresses",
        "AddressForm",
        "ClientAccounts",
        "AccountForm",
        "GroupsPromotion",
        "GroupsPromotionSet",
        "OrderHistory",
        "ProductsHistory",
        "SpecialOfert",
        "ProductOfert",
        "ProductOfertPrice",
        "AccountPayments",
        "AccountAddBudget",
        "AccountRemoveConfirm",
        "AccountProductsGroups",
        "AccountProductsBlocked"
    ],
    stores: [
        "Clients",
        "ClientsAddresses",
        "ClientsAccounts",
        "OrderHistory",
        "ProductsHistory",
        "AccountsList",
        "SpecialOfert",
        "AccountPayments"
    ],
    
    viewSpace: "Clients",
    
    refs: [
        {selector: "clientsproductofert", ref: "clientsProductOfert"}
    ],
    
    init: function() {
        this.control({
            "clientslist": {
                itemdblclick: function(grid, item) {
                    this.callAction("client", {id: item.get("id")});
                },
                edit: function(grid, item) {      
                    this.callAction("client", {id: item.get("id")});
                },
                create: function(grid, button) {
                    this.createClient({buttonId: button.elId});
                },
                deleted: function(grid, id) {
                    this.removeView("client-" + id);
                }
            },
            "clientform": {
                submit: function(result, form) {                
                    if (result.isNew) {
                        this.getActiveView().getStore().reload();
                        form.up("window").close();
                        this.callAction("client", {id: result.data.id});
                    }
                }
            },
            "clientaccounts": {
                createaccount: function(button, grid) {
                    this.callAction("createAccount", {buttonId: button.el.id,client_id: grid.clientId});
                },
                itemdelete: this.removeAccount
            },
            "clientaddresses": {
                create: function(grid, button) {
                    this.callAction("createAddress", {buttonId: button.el.id, client_id: grid.clientId});
                },
                edit: function(grid, item, button) {
                    this.callAction("editAddress", {buttonId: button.el.id, item_id: item.get("id")});
                }
            },
            "steptabpanel": {
                clientaccounts: function(tabs, view) {
                    view.getStore().load({params: {id: view.clientId}});
                },
                clientaddresses: function(tabs, view) {
                    view.getStore().load({params: {id: view.clientId}});
                },
                clientsgroupspromotion: function(tabs, view) {
                    view.getStore().load({params: {id: view.clientId}});
                },
                clientsorderhistory: function(tabs, view) {
                    view.getStore().load({params: {id: view.clientId}});
                },
                clientsproductshistory: function(tabs, view) {
                    view.getStore().load({params: {id: view.clientId}});
                },
                clientsspecialofert: function(tabs, view) {
                    view.getStore().load({params: {id: view.clientId}});
                }
            },
            "clientaddressform": {
                submit: function(result, form) {
                    this.getActiveView().getStore().reload();
                    
                    form.up("window").close();
                }
            },
            "clientaccountform": {
                submit: function(result, form) {
                    this.getActiveView().getStore().reload();
                    
                    if (result.isNew) {
                        form.up("window").close();
                    }
                },
                newpassword: this.generatePassword,
                productsblocked: this.showAccountProductsGroups
            },
            "accountremoveconfirm": {
                submit: function(result, form) {
                    form.up("window").close();
                    this.getActiveView().getStore().reload();
                }
            },
            "#clientaccount": {
                clientsaccountpayments: function(tabs, view) {
                    view.getStore().load({params: {id: view.accountId}});
                }
            },
            "clientsaccountpayments": {
                addbudget: function(grid, button) {
                    this.showAddBudget({
                        accountId: grid.accountId,
                        buttonId: button.el.id,
                        store: grid.getStore()
                    });
                },
                clearbudget: function(grid) {
                    this.clearBudget(grid);
                }
            },
            "accountaddbudget": {
                submit: function(result, form) {
                    form.store.reload();
                    form.up("window").close();
                }
            },
            "clientsgroupspromotion": {
                promoset: function(grid, button) {
                    this.groupPromoSet({
                        clientId: grid.clientId,
                        buttonId: button.el.id
                    });
                }
            },
            "clientsgroupspromotionset": {
                submit: function(result, form) {
                    this.getActiveView().getStore().reload();
                    form.up("window").close();
                }
            },
            "clientsspecialofert": {
                product: function(view, id) {
                    this.ofertView({
                        clientId: view.clientId,
                        productId: id
                    });
                },
                edit: function(view, item) {
                    this.ofertView({
                        clientId: view.clientId,
                        productId: item.get("id")
                    });
                },
                editproduct: function(view, item) {
                    this.callAction({
                        module: "Products",
                        controller: "Products",
                        action: "product",
                        params: {
                            id: item.get("id"),
                            clientId: view.clientId,
                            space: "Clients"
                        }
                    });
                },
                newproduct: function(view, button) {
                    this.callAction({
                        module: "Products",
                        controller: "Products",
                        action: "createProduct",
                        params: {
                            buttonId: button.el.id,
                            clientId: view.clientId
                        }
                    });
                }
            },
            "clientsproductofert": {
                editproduct: function(view, item) {
                    this.callAction({
                        module: "Products",
                        controller: "Products",
                        action: "product",
                        params: {
                            id: view.productId,
                            clientId: view.clientId,
                            space: "Clients"
                        }
                    });
                },
                setproductprices: this.setOfertPrice
            },
            productofertprice: {
                submit: function(result, form) {
                    form.up("window").close();
                    
                    this.getClientsProductOfert().getStore().reload();
                }
            },
            "productsvariantform": {
                submit: function(result) {
                    if (result.clientId) {
                        var view = this.findView("client-" + result.clientId),
                            tab;
                    
                        if (view) {
                            tab = view.getActiveTab();
                            if (tab.xtype == "clientsspecialofert") {
                                tab.getStore().reload();
                            }
                        }
                    }
                }
            },
            "clientsorderhistory": {
                itemdblclick: function(grid, item) {
                    this.callAction({
                        module: "Orders",
                        controller: "Orders",
                        action: "showOrder",
                        params: {
                            id: item.get("id")
                        }
                    });
                }
            },
            "accountproductsgroups": {
                itemdblclick: this.showAccountBlockedProducts
            },
            "accountproductsblocked": {
                saveblocked: this.saveAccountBlockedProducts
            }
        })
    },
    
    main: function() {
        this.render({
            xtype: "clientslist",
            isMainView: true
        });
        this.getClientsStore().load();
    },
    
    client: function(params) {
        var me = this,
            win = Ext.Msg.wait("Trwa wczytywanie danych klienta");
        
        Clients.getClient(params.id, function(r) {
            win.close();
            
            me.render({
                xtype: "steptabpanel",
                title: "Klient: " + r.data.name,
                itemId: "client-" + r.data.id,
                closable: true,
                activeTab: params.tab || 0,
                items: [{
                    xtype: "clientform",
                    hideCancelButton: true,
                    values: r.data
                }, {
                    xtype: "clientaccounts",
                    clientId: r.data.id
                }, {
                    xtype: "clientaddresses",
                    clientId: r.data.id
                }, {
                    xtype: "clientsgroupspromotion",
                    clientId: r.data.id,
                    store: {
                        type: "clientsgroupspromotion",
                        autoSync: true
                    }
                }, {
                    xtype: "clientsorderhistory",
                    clientId: r.data.id
                }, {
                    xtype: "clientsproductshistory",
                    clientId: r.data.id
                }, {
                    xtype: "clientsspecialofert",
                    clientId: r.data.id
                }]
            });
        })
    },
    
    createClient: function(params) {
        var me = this;
        
        Ext.create("Step.window.Form", {
            title: "Dodawanie nowego klienta",
            animateTarget: params.buttonId,
            form: {
                xtype: "clientform",
                title: null
            }
        })
    },
    
    groupPromoSet: function(params) {
        Ext.create("Step.window.Form", {
            title: "Ustawienia rabatu dla grup",
            animateTarget: params.buttonId,
            form: {
                xtype: "clientsgroupspromotionset",
                title: null,
                clientId: params.clientId
            },
            submitBtnText: "ustaw",
            submitBtnCls: "icon-fill"
        });
    },
    
    editAccount: function(params) {
        var me = this;
        
        me.wait("Trwa wczytywanie danych konta...");
        Clients.getAccount(params.itemId, function(r) {
            me.wait();
            var store = new Module.clients.store.ClientsAddresses();
            store.load({params:{ id: '_' + r.data.clients_id}});
            Ext.create("Step.window.Form", {
                modal: true,
                title: "Konto: " + r.data.name + " " + r.data.surname,
                height: "90%",
                width: "90%",
                form: {
                    xtype: "clientaccountform",
                    values: r.data,
                    accounts: Ext.getCmp(params.buttonId).up("treepanel").getStore(),
                    allowed: r.allowed,
                    bans: r.bans,
                    groups: r.groups,
                    addresses: store
                },
                autoShow: true,
                animateTarget: params.buttonId
            });
        });
    },
    
    createAddress: function(params) {
        var me = this;
        
        
        Ext.create("Step.window.Form", {
            modal: true,
            title: "Dodawanie nowego adresu",
            form: {
                xtype: "clientaddressform",
                values: {client_id: params.client_id}
            },
            autoShow: true,
            animateTarget: params.buttonId
        });
    },
    
    editAddress: function(params) {
        var me = this;
        
        me.wait("Trwa wczytywanie danych adresu...");
        Clients.getAddress(params.item_id, function(r) {
            me.wait();
            
            Ext.create("Step.window.Form", {
                modal: true,
                title: "Dodawanie nowego adresu",
                form: {
                    xtype: "clientaddressform",
                    values: r.data
                },
                autoShow: true,
                animateTarget: params.buttonId
            });
        });
    },
    
    createAccount: function(params) {
        var me = this;
        var store = new Module.clients.store.ClientsAddresses();
        store.load({params:{ id: '_' + params.client_id}});
        Ext.create("Step.window.Form", {
            modal: true,
            title: "Dodawanie nowego konta",
            height: "90%",
            width: "90%",
            form: {
                xtype: "clientaccountform",
                accounts: Ext.getCmp(params.buttonId).up("treepanel").getStore(),
                addresses: store
            },
            autoShow: true,
            animateTarget: params.buttonId
        });
    },
    
    removeAccount: function(grid, item, button) {
        var me = this;
        
        if (!parseInt(item.get("parent"))) {
            Ext.Msg.alert("Błąd operacji", "Nie można usunąć głownego konta!");
            return;
        }
        
        me.confirm(null, "Czy na pewno chcesz usunąć wybrane konto?", function() {
           
            me.wait("Trwa usuwanie konta...");

            Clients.beforeRemoveAccount(item.get("id"), function(r) {
                if (r.needConfirm) {
                    me.wait();
                    Ext.create("Step.window.Form", {
                        animateTarget: button.el.id,
                        title: "Usuwanie konta",
                        submitBtnText: "Usuń konto",
                        form: {
                            accountId: r.account_id,
                            accounts: r.accounts,
                            xtype: "accountremoveconfirm"
                        }
                    });
                }
                else {
                    Clients.removeAccount(item.get("id"), function() {
                        me.wait();
                        grid.getStore().reload();
                    });
                }
            });
        });
    },
    
    showAddBudget: function(params) {
        var me = this;
        
        Ext.create("Step.window.Form", {
            animateTarget: params.buttonId,
            title: "Przyznawanie dodatkowe budżetu",
            submitBtnText: "przyznaj budżet",
            form: {
                xtype: "accountaddbudget",
                accountId: params.accountId,
                store: params.store
            }
        });
    },
    
    showAccountProductsGroups: function(id) {
        var me = this;
        
        me.wait("Trwa wczytywanie danych...");
        Clients.getAccountProductsGroups(id, function(r) {
            me.wait();
            
            Ext.create("Step.window.Form", {
                constrain: true,
                title: "Zarządzanie zablokowanymi produktami",
                form: {
                    xtype: "accountproductsgroups",
                    groups: r.groups,
                    accountId: id
                },
                accountId: id,
                isForm: false,
                hideButtons: true
            });
        });
    },
    
    showAccountBlockedProducts: function(grid, item) {
        var me = this;
    
        Ext.create("Step.window.Form", {
            constrain: true,
            title: "Wybierz produkty do zablokowania",
            form: {
                xtype: "accountproductsblocked",
                groupId: item.get("id"),
                accountId: grid.up("window").accountId
            },
            isForm: false,
            hideButtons: true
        });
    },
    
    saveAccountBlockedProducts: function(grid) {
        var me = this,
            variants = [];
        
        me.wait("Trwa zapisywanie...");
        
        Ext.each(grid.getSelectionModel().getSelection(), function(item) {
            variants.push(item.get("id"));
        });
        
        Clients.saveAccountBlockedProducts(grid.accountId, grid.groupId, variants, function() {
            me.wait();
            grid.up("window").close();
        });
    },
    
    clearBudget: function(grid) {
        Ext.Msg.confirm("Potwierdzenie operacji", "Czy na pewno chcesz wyzerować budżet dla tego konta?", function(a) {
            if (a == "yes") {
                Clients.clearAccountBudget(grid.accountId, function() {
                    grid.getStore().reload();
                });
            }
        });
    },
    
    ofertView: function(params) {
        var me = this,
            view;
        
        Products.getProductInfo(params.productId, function(r) {
            view = Ext.create("Step.window.Form", {
                constrain: true,
                title: "Oferta specjalna: " + r.data.mark + " " + r.data.name,
                form: {
                    xtype: "clientsproductofert",
                    fields: r.fields,
                    productId: r.data.id,
                    productStatus: r.data.status,
                    clientId: params.clientId
                },
                hideButtons: true,
                isForm: false
            });
        });
    },
    
    setOfertPrice: function(grid, button) {
        var me = this;
        
        Ext.create("Step.window.Form", {
            title: "Ustalanie ceny dla wszystkich wariantów",
            animateTarget: button.el.id,
            form: {
                xtype: "productofertprice",
                clientId: grid.clientId,
                productId: grid.productId
            }
        });
    },
    
    generatePassword: function(id) {
        var me = this;
        
        me.confirm(null, "Czy na pewno chcesz wygenerować nowe hasło dla tego konta?", function() {
            me.confirm(null, "Czy chcesz wysłać do wybranego konta nowe hasło?", function(answer) {
                Clients.generatePassword(id, answer == "yes" ? 1 : 0, function(result) {
                    Ext.Msg.alert("Wygenerowano hasło", "Nowe hasło do konta: " + result.password);
                });
            }, true)
        });
    }
});