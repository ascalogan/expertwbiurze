Ext.define("Module.clients.store.ProductsHistory", {
    extend: "Ext.data.Store",
    alias: "store.clientsproductshistory",
    
    fields: [{
        name: "index", type: "string"
    }, {
        name: "mark", type: "string"
    }, {
        name: "name", type: "string"
    }, {
        name: "quantity", type: "int"
    }, {
        name: "group", type: "string"
    }, {
        name: "price_sum", type: "float"
    }],

    groupField: "group",
    
    proxy: {
        type: "direct",
        api: {
            read: Clients.getProductsHistory
        },
        reader: {root: "items"}
    }
});