Ext.define("Module.clients.store.AccountPayments", {
    extend: "Ext.data.Store",
    alias: "store.clientsaccountpayments",
    
    fields: [{
        name: "created", type: "date", dateFormat: "timestamp"
    }, {
        name: "type", type: "string"
    }, {
        name: "amount", type: "float"
    }, {
        name: "budget_after", type: "float"
    }],

    proxy: {
        type: "direct",
        api: {
            read: Clients.getAccountPayments
        },
        reader: {root: "items"},
        paramOrder: "id"
    }
});