Ext.define("Module.clients.store.AccountsList", {
    extend: "Ext.data.Store",
    alias: "store.clientsaccountslist",
    
    fields: [{
        name: "id", type: "int"
    }, {
        name: "name", type: "string"
    }],

    proxy: {
        type: "direct",
        api: {read: Clients.getAccountsList},
        reader: {root: "items"},
        paramOrder: "id"
    }
})