Ext.define("Module.clients.store.OrderHistory", {
    extend: "Ext.data.Store",
    alias: "store.clientsorderhistory",
    
    fields: [{
        name: "id", type: "int"
    }, {
        name: "person", type: "string"
    }, {
        name: "pushed", type: "date", dateFormat: "timestamp"
    }, {
        name: "price_sum", type: "float"
    }],

    groupField: "person",
    
    proxy: {
        type: "direct",
        api: {
            read: Clients.getOrderHistory
        },
        reader: {root: "items"}
    }
});