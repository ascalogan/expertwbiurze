Ext.define("Module.clients.store.ClientsAddresses", {
    extend: "Ext.data.Store",
    
    fields: [{
        name: "id", type: "string"
    }, {
        name: "name", type: "string"
    }, {
        name: "city", type: "string"
    }, {
        name: "street", type: "string"
    }],

    proxy: {
        type: "direct",
        reader: {root: "items"},
        api: {
            read: Clients.getAddresses,
            destroy: Clients.removeAddress
        },
        paramOrder: "id"
    }
});