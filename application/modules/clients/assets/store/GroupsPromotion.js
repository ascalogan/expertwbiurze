Ext.define("Module.clients.store.GroupsPromotion", {
    extend: "Ext.data.Store",
    alias: "store.clientsgroupspromotion",
    
    fields: [{
        name: "clients_id", type: "string"
    }, {
        name: "groups_id", type: "string"
    }, {
        name: "name", type: "string"
    }, {
        name: "percent", type: "int"
    }],

    proxy: {
        type: "direct",
        api: {
            read: Clients.getGroupsPromotion,
            update: Clients.saveGroupsPromotion
        },
        reader: {root: "items"},
        paramOrder: "id"
    }
});