Ext.define("Module.clients.store.SpecialOfert", {
    extend: "Ext.data.Store",
    alias: "store.clientsspecialofert",
    
    fields: [{
        name: "id", type: "int"
    }, {
        name: "mark", type: "string"
    }, {
        name: "name", type: "string"
    }, {
        name: "products", type: "int"
    }, {
        name: "clients_id", type: "int"
    }, {
        name: "status", type: "string"
    }, {
        name: "group", type: "string"
    }],

    groupField: "group",

    proxy: {
        type: "direct",
        api: {
            read: Clients.getSpecialOfert,
            destroy: Clients.removeSpecialOfert
        },
        reader: {root: "items"},
        paramOrder: "id"
    }
});