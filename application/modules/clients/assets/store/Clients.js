Ext.define("Module.clients.store.Clients", {
    extend: "Ext.data.Store",
    
    fields: [{
        name: "id", type: "int"
    }, {
        name: "name", type: "string"
    }, {
        name: "city", type: "string"
    }, {
        name: "nip", type: "string"
    }, {
        name: "merchant_name", type: "string"
    }, {
        name: "users_id", type: "int"
    }],

    proxy: {
        type: "direct",
        reader: {root: "items"},
        api: {
            read: Clients.getClients,
            destroy: Clients.removeClient
        },
        paramOrder: "id"
    }
});