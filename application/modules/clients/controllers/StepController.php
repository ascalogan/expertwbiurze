<?php

class Clients_StepController extends App_Controller_Action_Step
{
    
    public function getClients()
    {
        
        $query = App_Db_Table::getDefaultAdapter()->select();
        $query  ->from('clients', array('id', 'name', 'city', 'nip', 'users_id'))
                ->joinLeft('users', 'clients.users_id = users.id', array('merchant_name' => 'CONCAT(users.name,\' \',users.surname)'))
                ->order('name');
        
        if (!$this->hasAccess('clients')) {
            $query  ->where('users_id = ?', $this->getSession()->uid);
        }
        
        
        $this->send('items', $query->query()->fetchAll());
    }
    
    public function getClient($id)
    {
       
        $TClients = new Clients_Model_DbTable_Clients();
        $client = $TClients->find($id)->current();
        
        $this->send('data', $client->toArray());
    }
    
    public function saveClient($data)
    {
        $TAccounts = new Clients_Model_DbTable_Accounts();
        $errors = array();
        
        if (empty($data['id'])) {
            if (!$TAccounts->checkMail($data['account_mail'])) {
                $errors['account_mail'] = 'Podany adres e-mail istnieje już w bazie danych.';
            }
            if (!$TAccounts->checkLogin($data['account_login'])) {
                $errors['account_login'] = 'Podany login jest już zajęty.';
            }
        }
        
        if (!empty($errors)) {
            $this->sendErrors($errors);
        }
        else {
            if (empty($data['id'])) {
                $data['users_id'] = $this->getSession()->uid;
            }
            
            $row = $this->_saveRow('Clients', $data);
            
            if (empty($data['id'])) {
                $password = App_Util_String::randomPassword();
                
                $account = $TAccounts->createRow(array(
                    'clients_id' => $row->id,
                    'login' => $data['account_login'],
                    'name' => $data['account_name'],
                    'surname' => $data['account_surname'],
                    'mail' => $data['account_mail'],
                    'phone' => $data['account_phone'],
                    'password' => md5($password),
                    'created' => time()
                ));
                $account->save();
                
                $mail = new App_Mailer('html');
                $mail->addTo($account->mail, $account->getName());
                $mail->setSubject('Dane dostępowe do konta expertwbiurze.pl');
                $mail->send(App_Mailer_Messages::accountCreated($account, $password));
            }
        }
    }
    
    public function removeClient($client)
    {
        //$this->_removeRow('Clients', $client->id);
    }
    
    public function getAddresses($client_id) {
        $items = array();
        
        if (substr($client_id, 0, 1) == '_') {
            $items[] = array('id' => '0', 'name' => '- podstawywe dane do wysyłki');
            $client_id = substr($client_id, 1);
        }
        $query = App_Db_Table::getDefaultAdapter()->select();
        
        $query  ->from('addresses')
                ->where('client_id = ?', $client_id)
                ->order('name');
        
        foreach ($query->query()->fetchAll() as $adr) {
            $items[] = $adr;
        }
        
        $this->send('items', $items);
    }
    
    public function getAddress($id) {
        $TAccounts = new Clients_Model_DbTable_Addresses();
        $account = $TAccounts->find($id)->current();
        
        $data = $account->toArray();
        
        $this->send('data', $data);
    }
    
    public function saveAddress($data)
    {
        $this->_saveRow('Addresses', $data);
    }
    
    public function getAccounts($client_id)
    {
        if ($client_id) {
            $items = $this->_getAccounts($client_id);
            $this->send('items', $items);
        }
    }
    
    public function getAccount($id)
    {
        $TAccounts = new Clients_Model_DbTable_Accounts();
        $account = $TAccounts->find($id)->current();
        
        $data = $account->toArray();
        
        $this->send('data', $data);
        
        $allowed = array();
        foreach ($account->getGroupsAccess() as $g) {
            $allowed[] = $g->groups_id;
        }
        $this->send('allowed', $allowed);
        
        $allowed = array();
        foreach ($account->getGroupsBans() as $g) {
            $allowed[] = $g->groups_id;
        }
        $this->send('bans', $allowed);
        
        $TGroups = new Products_Model_DbTable_Groups();
        $this->send('groups', $TGroups->fetchAll(null, 'order')->toArray());
    }
    
    public function saveAccount($data)
    {
        $TAccounts = new Clients_Model_DbTable_Accounts();
        $errors = array();
        
        if (!$TAccounts->checkMail($data['mail'], $data['id'])) {
            $errors['mail'] = 'Podany adres e-mail już istnieje w bazie.';
        }
        if (isset($data['login'])) {
            if (!($data['login'] = $TAccounts->checkLogin($data['login'], $data['clients_id']))) {
                $errors['login'] = 'Podany login jest już zajęty.';
            }
        }
        
        if (!empty($errors)) {
            $this->sendErrors($errors);
        }
        else {
            $row = $this->_saveRow('Accounts', $data, true);
            
            if (isset($data['allowedGroups'])) {
                $row->setGroupsAccess($data['allowedGroups']);
            }
            else if (!empty($data['id']) && !empty($row->parent)) {
                $row->setGroupsAccess(array());
            }
            if (isset($data['bansGroups'])) {
                $row->setGroupsBans($data['bansGroups']);
            }
            else if (!empty($data['id']) && !empty($row->parent)) {
                $row->setGroupsBans(array());
            }
            
            if (empty($data['id'])) {
                $password = App_Util_String::randomPassword();
                $row->password = md5($password);
                $row->save();
                
                $mail = new App_Mailer('html');
                $mail->addTo($row->mail, $row->getName());
                $mail->setSubject('Dane dostępowe do konta expertwbiurze.pl');
                $mail->send(App_Mailer_Messages::accountCreated($row, $password));
            }
        }
    }
    
    public function beforeRemoveAccount($id)
    {
        $TAccounts = new Clients_Model_DbTable_Accounts();
        $account = $TAccounts->find($id)->current();
        
        if ($account->getAccounts()->count()) {
            $this->send('needConfirm', true);
            
            $client = $account->getClient();
            $main = $client->getMainAccount();
            
            $accounts = array();
            $accounts[] = array('id' => $main->id, 'name' => $main->getName() . ' (' . $main->login . ')');
            foreach ($main->getAccounts() as $a) {
                if ($a->id == $account->id) continue;
                $accounts[] = array('id' => $a->id, 'name' => $a->getName() . ' (' . $a->login . ')');
            }
            
            $this->send('accounts', $accounts);
            $this->send('account_id', $account->id);
        }
    }
    
    public function removeAccount($id)
    {
        $TAccounts = new Clients_Model_DbTable_Accounts();
        $account = $TAccounts->find($id)->current();
        
        if ($account) {
            $account->login = 'del:' . $account->login;
            $account->mail = 'del:' . $account->mail;
            $account->removed = '1';
            $account->save();
        }
    }
    
    public function removeParentAccount($data)
    {
        $TAccounts = new Clients_Model_DbTable_Accounts();
        $account = $TAccounts->find($data['id'])->current();
        
        foreach ($account->getAccounts() as $a) {
            $a->parent = $data['parent'];
            
            if ($a->verifier == $account->id) {
                $a->verifier = $data['parent'];
            }
            
            $a->save();
        }

        $account->login = 'del:' . $account->login;
        $account->mail = 'del:' . $account->mail;
        $account->removed = '1';
        $account->save();
    }
    
    public function getAccountPayments($id)
    {
        $query = App_Db_Table::getDefaultAdapter()->select();
        
        $query  ->from('accounts_payments')
                ->where('accounts_id = ?', $id)
                ->order('id DESC');
        
        $this->send('items', $query->query()->fetchAll());
    }
    
    public function addAccountBudget($data)
    {
        $TAccounts = new Clients_Model_DbTable_Accounts();
        $account = $TAccounts->find($data['accounts_id'])->current();
        
        $account->addBudget($data['amount'], '5');
    }
    
    public function clearAccountBudget($id)
    {
        $TAccounts = new Clients_Model_DbTable_Accounts();
        $account = $TAccounts->find($id)->current();
        
        $account->clearBudget();
    }
    
    public function getAccountsList($id)
    {
        $query = App_Db_Table::getDefaultAdapter()->select();
        
        $query  ->from('accounts', array('id', 'name' => 'CONCAT(name,\' \',surname,\' (\',login,\')\')'))
                ->where('clients_id = ?', $id)
                ->order(array('surname', 'name'));
        
        $items = array_merge(array(
            array('id' => 0, 'name' => '- wszystkie konta')
        ), $query->query()->fetchAll());
        
        $this->send('items', $items);
    }
    
    public function getAccountProductsGroups($id)
    {
        $query = App_Db_Table::getDefaultAdapter()->select();
        
        $query  ->from('accounts_groups_access')
                ->joinInner('groups', 'groups.id = accounts_groups_access.groups_id', array('id', 'name'))
                ->where('accounts_id = ?', $id)
                ->order('groups.order');
        
        $items = $query->query()->fetchAll();
        
        $this->send('groups', $items);
    }
    
    public function getAccountGroupVariants($id, $account_id)
    {
        $query = App_Db_Table::getDefaultAdapter()->select();
        
        $query  ->from('products_variants', array('id', 'index'))
                ->joinInner('products', 'products.id = products_variants.products_id', array('name', 'mark'))
                ->joinLeft('accounts_variants_blockades', 'accounts_variants_blockades.variant_id = products_variants.id AND accounts_variants_blockades.account_id = ' . $account_id, array('isBlocked' => 'id'))
                ->where('products_variants.status = ?', '1')
                ->where('products.status = ?', '1')
                ->where('products.groups_id = ?', $id)
                ->order(array('mark', 'name'));
        try {
        $this->send('items', $query->query()->fetchAll());
        }
        catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
    public function saveAccountBlockedProducts($id, $group_id, $variants)
    {
        $adapter = App_Db_Table::getDefaultAdapter();
        
        $adapter->delete('accounts_variants_blockades', array(
            'account_id = ?' => $id,
            'group_id = ?' => $group_id
        ));
        
        foreach ($variants as $v) {
            $adapter->insert('accounts_variants_blockades', array(
                'account_id' => $id,
                'group_id' => $group_id,
                'variant_id' => $v
            ));
        }
    }
    
    public function getGroupsPromotion($client_id)
    {
        $query = App_Db_Table::getDefaultAdapter()->select();
        $items = array();
        
        $query  ->from('groups', array('name', 'groups_id' => 'id'))
                ->joinLeft(array('cgp' => 'clients_groups_promotion'), 'cgp.groups_id = groups.id AND cgp.clients_id = ' . $client_id, 'percent')
                ->order('name');
        
        foreach ($query->query()->fetchAll() as $item) {
            $item['clients_id'] = $client_id;
            $items[] = $item;
        }
        
        $this->send('items', $items);
    }
    
    public function saveGroupsPromotion($data)
    {
        $TPromotion = new Clients_Model_DbTable_GroupsPromotion();
        $row = $TPromotion->get($data->groups_id, $data->clients_id);
        
        if ($row === null) {
            $row = $TPromotion->create($data->groups_id, $data->clients_id);
        }
        
        $row->percent = $data->percent;
        $row->save();
    }
    
    public function setGroupsPromotion($data)
    {
        $TGroups = new Products_Model_DbTable_Groups();
        $TPromotion = new Clients_Model_DbTable_GroupsPromotion();
        
        $groups = $TGroups->fetchAll();
        foreach ($groups as $group) {
            $row = $TPromotion->get($group->id, $data['clients_id']);
            
            if ($row === null) {
                $row = $TPromotion->create($group->id, $data['clients_id']);
            }
            
            $row->percent = $data['percent'];
            $row->save();
        }
    }
    
    public function getOrderHistory($data)
    {
        $query = App_Db_Table::getDefaultAdapter()->select();
        
        $query  ->from('orders')
                ->where('orders.clients_id = ?', $data->id)
                ->where('pushed > 0')
                ->order('orders.id DESC')
                ->joinLeft('accounts', 'accounts.id = orders.owner_id', array('person' => 'CONCAT(name, \' \', surname)'));
        
        if (!empty($data->date_from)) {
            $query  ->where('FROM_UNIXTIME(`pushed`, \'%Y-%m-%d\') >= ?', $data->date_from);
        }
        if (!empty($data->date_to)) {
            $query  ->where('FROM_UNIXTIME(`pushed`, \'%Y-%m-%d\') <= ?', $data->date_to);
        }
        
        $this->send('items', $query->query()->fetchAll());
    }
    
    public function getProductsHistory($data)
    {
        $groups = array('Oferta specjalna');
        $TGroups = new Products_Model_DbTable_Groups();
        
        foreach ($TGroups->fetchAll() as $group) {
            $groups[$group->id] = $group->name;
        }
        
        $items = array();
        
        $query = App_Db_Table::getDefaultAdapter()->select();
        
        $query  ->from(array('oe' => 'orders_elements'), array('quantity', 'price_sum'))
                ->joinInner(array('pv' => 'products_variants'), 'pv.id = oe.variants_id', array('id', 'index'))
                ->joinInner(array('p' => 'products'), 'p.id = oe.products_id', array('name', 'mark', 'groups_id'))
                ->joinInner(array('o' => 'orders'), 'o.id = oe.orders_id', 'pushed')
                ->order(array('p.mark', 'p.name'))
                ->where('o.pushed > 0')
                ->where('o.clients_id = ?', $data->id);
        
        if (!empty($data->date_from)) {
            $query  ->where('FROM_UNIXTIME(pushed, \'%Y-%m-%d\') >= ?', $data->date_from);
        }
        if (!empty($data->date_to)) {
            $query  ->where('FROM_UNIXTIME(pushed, \'%Y-%m-%d\') <= ?', $data->date_to);
        }
        if (!empty($data->account_id)) {
            $query  ->where('o.owner_id = ?', $data->account_id);
        }
        
        try {
        foreach ($query->query()->fetchAll() as $item) {
            if (isset($items[$item['id']])) {
                $items[$item['id']]['quantity'] += $item['quantity'];
                $items[$item['id']]['price_sum'] += $item['price_sum'];
            }
            else {
                $items[$item['id']] = $item;
                $items[$item['id']]['group'] = $groups[$item['groups_id']];
            }
        }
        } catch (Exception $e) {
            die($e->getMessage());
        }
        
        $this->send('items', array_values($items));
    }
    
    public function productsHistoryToXlsAction()
    {
        $xls = new PHPExcel();
        
        $data = $this->getRequest()->getQuery();
        
        $this->getProductsHistory((object) $data);
        $items = $this->_directResponse['items'];
        
        $client = new Clients_Model_DbTable_Clients();
        $client = $client->find($data['id'])->current();
        
        // filename for download
        $filename = 'stats_' . $client->name . '_' . date('Y-m-d');
        if (!empty($data['account_id'])) {
            $account = new Clients_Model_DbTable_Accounts();
            $account = $account->find($data['account_id'])->current();
            $filename .= '_' . $account->login;
        }
        $filename .= '.xls';
        
        // Redirect output to a clientâ€™s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
        
        $row = 2;
        $cell = 'A';
        
        $xls->setActiveSheetIndex(0);
        
        $columns = array(
            'A' => 'grupa towarowa',
            'B' => 'index',
            'C' => 'marka',
            'D' => 'nazwa produktu',
            'E' => 'ilość',
            'F' => 'suma'
        );
        
        foreach ($columns as $col => $label) {
            $xls->getActiveSheet()->setCellValue($col . '1', $label);
            $xls->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
        }
        
        foreach ($items as $item) {
            $xls->getActiveSheet()->setCellValue('A' . $row, $item['group']);
            $xls->getActiveSheet()->setCellValue('B' . $row, $item['index']);
            $xls->getActiveSheet()->setCellValue('C' . $row, $item['mark']);
            $xls->getActiveSheet()->setCellValue('D' . $row, $item['name']);
            $xls->getActiveSheet()->setCellValue('E' . $row, $item['quantity']);
            $xls->getActiveSheet()->setCellValue('F' . $row, $item['price_sum']);
                
            $row++;
        }
        
        $xls->setActiveSheetIndex(0);
        $xls->getActiveSheet()->setAutoFilter(
            $xls->getActiveSheet()->calculateWorksheetDataDimension()
        );
     
        
        $objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }
    
      function cleanData(&$str)
  {
    $str = preg_replace("/\t/", "\\t", $str);
    $str = preg_replace("/\r?\n/", "\\n", $str);
    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
  }

    
    public function getSpecialOfert($id)
    {
        $query = App_Db_Table::getDefaultAdapter()->select();
        
        $query  ->from(array('cvp' => 'clients_variants_promotion'), array(
            'products' => 'COUNT(`variants_id`)', 'clients_id'
        ))
                ->joinInner('products', 'products.id = cvp.products_id', array('id', 'name', 'mark', 'status'))
                ->joinLeft('groups', 'groups.id = products.groups_id', array('group' => 'groups.name'))
                ->where('cvp.clients_id = ?', $id)
                ->group('products_id');
        
        try {
        $this->send('items', $query->query()->fetchAll());
        } catch(Exception $e) {
            die($e->getMessage());
        }
    }
    
    public function removeSpecialOfert($data)
    {
        $adapter = App_Db_Table::getDefaultAdapter();
        
        $adapter->delete('clients_variants_promotion', array(
            'products_id = ?' => $data->id,
            'clients_id = ?' => $data->clients_id
        ));
    }
    
    public function getProductOfert($data)
    {
        $query = App_Db_Table::getDefaultAdapter()->select();
        
        $query  ->from(array('pv' => 'products_variants'), array(
            'variants_id' => 'id',
            'products_id',
            'normal_price' => 'price',
            'fields',
            'index'
        ))
                ->joinLeft(array('cpv' => 'clients_variants_promotion'), 'cpv.variants_id = pv.id AND cpv.clients_id = ' . $data->client_id, array('price'))
                ->order(array('cpv.price DESC', 'order'))
                ->where('pv.products_id = ?', $data->product_id)
                ->where('pv.status != ?', '3');
        
        $items = array();
        foreach ($query->query()->fetchAll() as $item) {
            $item['clients_id'] = $data->client_id;
            $item['active'] = $item['price'] ? true : false;
            $item['fields'] = unserialize($item['fields']);
            $items[] = $item;
        }
        
        $this->send('items', $items);
    }
    
    public function saveProductOfert($items)
    {
        if (!is_array($items)) {
            $items = array($items);
        }
        
        $TPromotion = new Clients_Model_DbTable_VariantsPromotion();
        
        foreach ($items as $item) {
            $row = $TPromotion->get($item->clients_id, $item->variants_id);
            $price = str_replace(',', '.', $item->price);
            
            if (!$row && !$price) continue;
            if ($row && !$price) {
                $row->delete();
                continue;
            }
            
            if (!$row) {
                $row = $TPromotion->createRow(array(
                    'clients_id' => $item->clients_id,
                    'variants_id' => $item->variants_id,
                    'products_id' => $item->products_id
                ));
            }
            
            $row->price = str_replace(',', '.', $item->price);
            $row->save();
        }
    }
    
    public function setOfertPrice($data)
    {
        $TPromotion = new Clients_Model_DbTable_VariantsPromotion();
        $TProducts = new Products_Model_DbTable_Products();
        
        $product = $TProducts->find($data['product_id'])->current();
        $price = str_replace(',', '.', $data['price']);
        
        foreach ($product->getVariants() as $variant) {
            $row = $TPromotion->get($data['client_id'], $variant->id);
            
            if ($row) {
                $row->price = $price;
                $row->save();
            }
            else {
                $TPromotion->add($data['client_id'], $variant->products_id, $variant->id, $price);
            }
        }
    }
    
    protected function _getAccounts($client_id, $parent_id = 0)
    {
        $accounts = array();
        
        $query = App_Db_Table::getDefaultAdapter()->select();
        
        $query  ->from('accounts')
                ->where('clients_id = ?', $client_id)
                ->where('parent = ?', $parent_id)
                ->where('removed != ?', '1')
                ->order(array('surname', 'name'));
        
        foreach ($query->query()->fetchAll() as $item) {
            $children = $this->_getAccounts($client_id, $item['id']);
            
            if ($children) {
                $item['items'] = $children;
                $item['expanded'] = true;
            }
            else {
                $item['leaf'] = true;
            }
            
            $accounts[] = $item;
        }
        
        return $accounts;
    }
    
    public function generatePassword($id, $send)
    {
        $password = App_Util_String::randomPassword();
        $this->send('password', $password);
        
        $TUsers = new Clients_Model_DbTable_Accounts();
        $row = $TUsers->find($id)->current();
        
        $row->password = md5($password);
        $row->save();
        
        if ($send) {
            $mail = new App_Mailer('html');
            $mail->addTo($row->mail, $row->getName());
            $mail->setSubject('Dane dostępowe do konta www.expertwbiurze.pl');
            $mail->send(App_Mailer_Messages::passwordChanged($row, $password));
        }
    }
}