<?php

class Clients_FixerController extends Zend_Controller_Action
{
    public function checkMailAction()
    {
        $OrdersModel = new Orders_Model_DbTable_Orders();

        $order = $OrdersModel->find(11271)->current();

        $mail = new App_Mailer('html', true);
        $mail->addTo('ascalogan@gmail.com', 'Przemysław Wróbel');
        $mail->setSubject('Potwierdzenie złożenia zamówienia #' . $order->id);
        $mail->send(App_Mailer_Messages::orderSend($order));

        die('ok');
    }

    public function doPromotionAction()
    {
        $this->doRandomPromo(12);
    }
    
    public function doPromoFit($amount)
    {
        $adapter = Zend_Db_Table::getDefaultAdapter();
        $camount = $adapter->query('SELECT COUNT(`id`) FROM promotions_products WHERE status = \'2\'')->fetchColumn();
        
        if ($camount >= $amount) return;
        
        $exclude_ids = array(0);
        
        foreach ($adapter->query('SELECT `product_id` FROM promotions_products WHERE status = \'2\'')->fetchAll() as $p) {
            $exclude_ids[] = $p['product_id'];
        }
        
        $amount -= $camount;
        
        $this->doRandomPromo($amount, false, $exclude_ids);
    }
    
    public function doRandomPromo($amount = 0, $eraseCurrent = false, $exclude_ids = array(0))
    {
        $max_price = 1000;
        $min_price = 1;
        $percent = 15;
        
        $groups = array();
        $ids = array();
        $adapter = Zend_Db_Table::getDefaultAdapter();
        
        if ($eraseCurrent) {
            $adapter->delete('promotions_products', array('status != \'2\''));
        }

        foreach ($adapter->query('SELECT group_id FROM promotions_groups ORDER BY RAND() LIMIT ' . $amount)->fetchAll() as $g) {
            $groups[] = $g['group_id'];
        }
        if ($amount > count($groups)) {
            foreach ($adapter->query('SELECT group_id FROM promotions_groups ORDER BY RAND() LIMIT ' . ($amount - count($groups)))->fetchAll() as $g) {
                $groups[] = $g['group_id'];
            }
        }

        $TProducts = new Products_Model_DbTable_Products();
        foreach ($groups as $group_id) {
            $products = $TProducts->fetchAll(array(
                'groups_id = ?' => $group_id,
                'status = ?' => '1',
                'image != ?' => '',
                'id NOT IN(' . implode(',', $exclude_ids) . ')'
            ), 'RAND()');
            
            foreach ($products as $product) {
                if ($product->getMaxVariantPrice() <= $max_price && $product->getMinVariantPrice() >= $min_price) {
                    $ids[] = $product->id;
                    $exclude_ids[] = $product->id;
                    break;
                }
            }
        }
        
        foreach ($ids as $id) {
            $adapter->insert('promotions_products', array(
                'product_id' => $id,
                'percent' => $percent,
                'status' => '2',
                'day' => date('Y-m-d')
            ));
        }
        
        if (count($ids) < $amount) {
            $this->doPromoFit($amount);
        }
    }
    
    
    public function fixBadOrdersAction()
    {
        $TOrders = new Orders_Model_DbTable_Orders();
        
        echo '<meta http-equiv="content-type" content="text/html;charset=utf-8" />';
        
        foreach ($TOrders->fetchAll(array('status != ?' => '0', 'id > ?' => 7132), 'id DESC') as $order) {
            $price = $order->getTotalPrice();
            
            if ($price != $order->price_sum) {
                $client = $order->getClient();
                
                $creator = $order->getCreator();
                
                $diff = number_format($price - $order->price_sum, 2);
                
                
                echo 'Zamówienie z dnia ' . date('Y-m-d', $order->pushed) . ' nr #' . $order->id . '<br />Wartość zamówienia wyświetlana: ' . $this->view->currency($order->price_sum) . '<br />Wartość zamówienia rzeczywista: ' . $this->view->currency($price) . '<br />Zamawiający: ' . $creator->getName() . '<br />' . $client->name . ', ' . $client->getMainAccount()->mail . '<br /><br />';
                
                $creator->reduceBudget($diff);
                
                $order->price_sum = $price;
                $order->save();
            }
        }
        
        die;
    }
    
    public function fixAction()
    {
        set_time_limit(0);
        $adapter = Zend_Db_Table::getDefaultAdapter();
        
        $this->_fixUsers($adapter);
        
        $this->_fixLostVariants($adapter);
        $this->_fixVariantsPromotions($adapter);
        
        $this->_fixProducts($adapter);
        $this->_fixProductsStatus($adapter);
        $this->_fixSpecialProducts($adapter);
        
        $this->_fixIndexes($adapter);
        
        $this->_fixAccounts($adapter);
        $this->_fixOrders($adapter);
        
        $this->_fixVariantsPromotions($adapter);
        $this->_fixSpecialVariants($adapter);

        $this->_checkIndexes($adapter);
        die;
    }
    
    protected function _fixSpecialVariants($adapter)
    {
        $TVariants = new Products_Model_DbTable_Variants();
        
        foreach ($TVariants->fetchAll(array('status = ?' => '3')) as $v) {
            $adapter->delete('clients_variants_promotion', array('variants_id = ?' => $v->id));
        }
    }
    
    protected function _fixLostVariants($adapter)
    {
        $query = $adapter->select();
        $query  ->from(array('pv' => 'products_variants'), 'id')
                ->joinLeft('products', 'products.id = pv.products_id', array('product' => 'products.id'))
                ->having('product IS NULL');
        
        foreach ($query->query()->fetchAll() as $v) {
            $this->debug('Removed variant %d due to lack of parent product', $v['id']);
            $adapter->delete('products_variants', array('id = ?' => $v['id']));
        }
    }
    
    protected function _fixVariantsPromotions($adapter)
    {
        $count = $adapter->delete('clients_variants_promotion', array(
            'price = ?' => '0.00'
        ));
        $this->debug('Removing non-priced clients variants promotion: %d removed', $count);
    }
    
    protected function _fixUsers($adapter)
    {
        $adapter->query("ALTER TABLE `users` ADD `content` INT( 1 ) NOT NULL AFTER `products`");
    }
    
    protected function _fixAccounts($adapter)
    {
        $TAccounts = new Clients_Model_DbTable_Accounts();
        $account = $TAccounts->fetchRow();
        
        if (!isset($account->limit_type)) {
            $adapter->query("ALTER TABLE  `accounts` ADD  `limit_type` ENUM(  '0',  '1',  '2',  '3',  '4' ) NOT NULL AFTER  `password`");
            $adapter->query("ALTER TABLE  `accounts` ADD  `budget` DECIMAL( 12, 2 ) NOT NULL AFTER  `limit`");
            $adapter->query("ALTER TABLE  `accounts` ADD  `budget_extra` DECIMAL( 12, 2 ) NOT NULL AFTER  `budget`");
            $adapter->query("ALTER TABLE  `accounts` ADD  `limit_permanent` ENUM( '0', '1') NOT NULL AFTER `limit`");
            $adapter->query("ALTER TABLE  `clients` ADD  `enable_stats` ENUM(  '0',  '1' ) NOT NULL AFTER  `users_id` ;");
        }
        $adapter->delete('accounts_payments');
        
        foreach ($TAccounts->fetchAll() as $account) {
            if ($account->limit) {
                $account->limit_type = '1';
                $account->budget = 0;
                
                $account->addBudget($account->limit, $account->limit_type);
            }
        }
        
        $this->debug('Accounts were fixed');
    }
    
    protected function _fixOrders($adapter)
    {
        $TOrders = new Orders_Model_DbTable_Orders();
        $order = $TOrders->fetchRow();
        
        if (!isset($order->status)) {
            $adapter->query("ALTER TABLE `orders` ADD `status` ENUM( '0', '1', '2', '3') NOT NULL AFTER `id`");
            $adapter->query("ALTER TABLE `orders` ADD `accepted` INT(32) NOT NULL AFTER `pushed`");
            $adapter->query("ALTER TABLE `orders` ADD `used_budget` DECIMAL( 12, 2 ) NOT NULL AFTER `price_sum`");
            $adapter->query("ALTER TABLE `orders` ADD `used_budget_extra` DECIMAL( 12, 2 ) NOT NULL AFTER `used_budget`");
            $adapter->query("ALTER TABLE `orders` ADD `owner_price_sum` DECIMAL( 12, 2 ) NOT NULL AFTER `price_sum`");
            $adapter->query("ALTER TABLE `orders` ADD `owner_reported` INT(32) NOT NULL AFTER `reported`");
            
            foreach ($TOrders->fetchAll() as $order) {
                if (empty($order->pushed)) {
                    $order->status = '0';
                }
                else if (empty($order->received)) {
                    $order->status = '1';
                }
                else if (!empty($order->received)) {
                    if (empty($order->accepted)) {
                        $order->accepted = $order->received;
                    }
                    if ($order->status != '3') {
                        $order->status = '3';
                    }
                }

                $order->save();
            }
        }
        
        $this->debug('Orders were fixed');
    }
    
    public function _fixProducts($adapter)
    {
        $TProducts = new Products_Model_DbTable_Products();
        $product = $TProducts->fetchRow();
        
        if (!isset($product->status)) {
            $adapter->query("ALTER TABLE `products` ADD `status` ENUM( '1', '2') NOT NULL AFTER `id`");
            $adapter->query("ALTER TABLE `products_variants` ADD `status` ENUM( '1', '2', '3') NOT NULL AFTER `id`");
        }
        if (isset($product->symbol)) {
            $adapter->query("UPDATE `products` SET `name` = TRIM(CONCAT(`name`,' ',`symbol`))");
            $adapter->query("ALTER TABLE `products` DROP COLUMN `symbol`");
        }
    }
    
    public function _fixProductsStatus($adapter)
    {
        /*$TProducts = new Products_Model_DbTable_Products();
        foreach ($TProducts->fetchAll() as $product) {
            if ($product->status == '3' || $product->status == '5') {
                
                
                continue;
            }
            
            $variants = $product->getVariants();
            $count = $variants->count();
            
            $lastStatus = '0';
            $statusChanged = false;
            $statusMin = '1';
            
            foreach ($variants as $variant) {
                if (!$lastStatus) {
                    $lastStatus = $variant->status;
                    $statusMin = $variant->status;
                }
                else {
                    if ($lastStatus != $variant->status) {
                        $statusChanged = true;
                    }
                    
                    $lastStatus = $variant->status;
                    if ($lastStatus < $statusMin) {
                        $statusMin = $lastStatus;
                    }
                }
            }
            
            
            
        }*/
        
        $adapter->query("UPDATE `products` SET `status` = '1' WHERE `clients_id` = 0");
        $adapter->query("UPDATE `products` SET `status` = '2' WHERE `clients_id` != 0");
            
        $TProducts = new Products_Model_DbTable_Products();
        $products = $TProducts->fetchAll(array('clients_id = ?' => 0));
        
        foreach ($TProducts->fetchAll(array('clients_id = ?' => 0)) as $product)
        {
            $variants = $product->getVariants();
            $variantsCount = $variants->count();
            
            if (!$variantsCount) {
                $this->debug('Product %d has been removed due to lack of variants', $product->id);
                $product->delete();
                continue;
            }
            
            // Set correct status to variants
            foreach ($variants as $variant) {
                if ($variant->deleted || $product->deleted) {
                    $variant->status = '2';
                }
                else {
                    $variant->status = '1';
                }
                $variant->save();
            }
        }
    }
    
    public function _fixSpecialProducts($adapter)
    {
        $TProducts = new Products_Model_DbTable_Products();
        
        // Fix clients products
        $TClientVariants = new Clients_Model_DbTable_VariantsPromotion();
        
        $index = 1;
        
        foreach ($TProducts->fetchAll(array('clients_id != 0')) as $product) {
            $variants = $product->getVariants();
            $variantsCount = $variants->count();
            
            if (!$variantsCount) {
                $this->debug('Special product %d has been removed due to lack of variants', $product->id);
                $product->delete();
                continue;
            }
            
            foreach ($variants as $variant) {
                $variant->index = 'EXP' . str_repeat('0', 6 - strlen($index)) . $index;
                $index++;
                
                // Clear promotion if somehow exists
                $row = $TClientVariants->get($product->clients_id, $variant->id);
                if ($row !== null) {
                    $row->delete();
                }
                
                // Set as deleted if product is deleted or variant itself is deleted
                if ($variant->deleted || $product->deleted) {
                    $variant->status = '3';
                }
                else {
                    $variant->status = '1';
                }
                
                // If variant is active, add it to special offert
                if ($variant->status == '1') {
                    $TClientVariants->add($product->clients_id, $product->id, $variant->id, $variant->price);
                }
                $variant->save();
            }
        }
    }
    
    public function _fixIndexes($adapter)
    {
        // Match doubled indexes on line active => inactive
        $select = $adapter->select();
        $select ->from(array('pv' => 'products_variants'), array('id', 'index', 'status'))
                ->joinInner(array('pv2' => 'products_variants'), 'pv2.index = pv.index AND pv2.status = \'1\'', array('vid' => 'pv2.id'))
                ->where('pv.status = ?', '2')
                ->order('pv.index');
        
        foreach ($select->query()->fetchAll() as $v) {
            $adapter->update('products_variants', array(
                'status' => '3',
                'index' => 'OLD_' . date('Ymd') . '_' . $v['index']
            ), array('id = ?' => $v['id']));
            $this->debug('[AI] Duplicated index %s [%d,%d] - mark deleted', $v['index'], $v['id'], $v['vid']);
        }
        
        $q = "SELECT COUNT(`id`) as `count`, `index` FROM `products_variants` WHERE `status` = '2' GROUP BY `index` HAVING `count` > 1";
        
        // Match doubles indexes in inactive group and mark them as deleted
        foreach ($adapter->query($q)->fetchAll() as $v) {
            $this->debug('[II] Duplicated index %s, removing %d variants', $v['index'], $v['count']-1);
            
            $updateQ = "UPDATE `products_variants` ";
            $updateQ .= "SET `index` = 'OLD_" . date('Ymd') . '_' . $v['index'] . "', `status` = '3' ";
            $updateQ .= "WHERE `index` = '" . $v['index'] . "' AND `status` = '2' ";
            $updateQ .= "ORDER BY `id` LIMIT " . ($v['count']-1);
            
            $adapter->query($updateQ);
        }
    }
    
    protected function _checkIndexes(Zend_Db_Adapter_Abstract $adapter)
    {
        
        
        $TVariants = new Products_Model_DbTable_Variants();
        $variants = $TVariants->fetchAll(array('status = \'1\''));
        $checked = array();
        
        echo '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/></head><body>';
        echo '<table style="width: 100%" border="1">';
        echo '<thead><tr>';
        echo '<th rowspan="2">index</th><th rowspan="2">typ</th><th colspan="4">wariant 1</th><th colspan="4">wariant 2</th>';
        echo '</tr>';
        echo '<tr><th>id</th><td>produkt_id</th><th>produkt</th><th>grupa</th><th>id</th><td>produkt_id</th><th>produkt</th><th>grupa</th></tr>';
        echo '</thead><tbody>';
        
        foreach ($variants as $v) {
            if (in_array($v->id, $checked)) {
                continue;
            }
            $v2 = $TVariants->fetchRow(array(
                '`index` = ?' => $v->index,
                '`id` != ?' => $v->id,
                '`status` = ?' => $v->status
            ));
            
            if ($v2) {
                $checked[] = $v2->id;
                
                $p = $v->getProduct();
                $g = $p->getGroup();
                
                $p2 = $v2->getProduct();
                $g2 = $p2->getGroup();
                
                $status = $v->status == '1' ? 'aktywny' : 'nieaktywny';
                
                echo '<tr>';
                echo '<td>' . $v->index . '</td><td>' . $status . '</td>';
                echo '<td>' . $v->id . '</td><td>' . $p->id . '</td><td>' . $p->mark . ' ' . $p->name . '</td><td>' . $g->name . '</td>';
                echo '<td>' . $v2->id . '</td><td>' . $p2->id . '</td><td>' . $p2->mark . ' ' . $p2->name . '</td><td>' . $g2->name . '</td>';
                echo '</tr>';
            }
        }
        
        echo '</tbody></table></body></html>';
    }
    
    public function debug()
    {
        $args = func_get_args();
        echo call_user_func_array('sprintf', $args) . '<br />';
    }
}