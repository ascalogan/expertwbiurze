<?php

class Clients_SiteController extends App_Controller_Action_Site
{
    public function doLimitActAction()
    {
        $MAccounts = new Clients_Model_DbTable_Accounts();

        $body = "";
        
        foreach ($MAccounts->fetchAll(array('limit_type != ?' => '0'), 'clients_id') as $account) {
            $shouldBeChanged = $account->limit_type == '1';

            if ($shouldBeChanged) {
                $body .=  $account->mail . ' [budżet: ' . $account->budget . ']';
                if (!$account->limit_permanent) {
                    $body .= ' [budżet nie przechodzi]';
                    $account->budget = 0;
                }
                else {
                    $body .= ' [budżet przechodzi]';
                }

                $account->budget = $account->budget + $account->limit;

                $body .= ' [limit: ' . $account->limit . '] [nowy budżet: ' . ($account->budget) . ']' . "\n";

                $account->save();
            }
        }

        $mail = new App_Mailer();
        $mail->addTo('ascalogan@gmail.com');
        $mail->addTo('gkoziol@expertwbiurze.pl');
        $mail->setSubject('Raport z dołożenia limitów');
        $mail->send($body);
        
        die;
    }
    
    public function passwordGenAction()
    {
        if ($this->getRequest()->isPost()) {
            $aid = (int) $this->getRequest()->getPost('account_id');
            
            $TAccounts = new Clients_Model_DbTable_Accounts();
            $account = $TAccounts->find($aid)->current();
            
            $user = $this->getUser();

            if ($account && !$user->parent && $user->clients_id == $account->clients_id) {
                $password = App_Util_String::randomPassword();
                $account->password = md5($password);
                $account->save();
                
                $mail = new App_Mailer('html');
                $mail->addTo($account->mail, $account->getName());
                $mail->setSubject('Nowe hasło do Twojego konta.');
                $mail->send(App_Mailer_Messages::passwordChanged($account, $password));
                
                $this->messages->info('Nowe hasło została wygenerowane i wysłane na adres e-mail przypisany do tego konta.');
            }
        }
        
        $this->goToUrl('clients.account-edit', array('id' => $_POST['account_id']));
    }
    
    public function contactAction()
    {
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $data = $request->getPost();
            
            $TSettings = new Lite_Model_DbTable_Settings();
            $to = $TSettings->get('site_contactFormMail')->value;         
            
            $mail = new App_Mailer();
            $mail->addTo($to);
            $mail->setSubject('Nowa wiadomość ze strony expertwbiurze.pl');
            $mail->send("Wiadomość od: {$data['mail']}\n\n{$data['content']}");
        }
        
        die();
    }
    
    public function doHiddenLoginActAction()
    {
        $id = (int) $this->_getParam('id');
        $this->getSession()->uid = $id;
        $this->redirect('/');
    }
    
    public function loginAction()
    {
        $result = array('success' => false);
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $data = $request->getPost();
            
            $TAccounts = new Clients_Model_DbTable_Accounts();
            $account = $TAccounts->byCredentials($data['login'], $data['password']);
            
            if ($account !== null && !$account->removed) {
                $this->getSession()->uid = $account->id;
                $result['success'] = true;
            }
        }
        
        $this->_helper->json($result);
    }
    
    public function logoutAction()
    {
        $this->getSession()->uid = null;
        $this->redirect('/');
    }
    
    public function registerAction()
    {
        if ($this->getUser()) {
            $this->redirect('/');
        }
        
        $this->view->headNoIndex = true;
        
        $form = $form = new Clients_Form_Company(true);
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            if ($form->isValid($request->getPost())) {
                $TAccounts = new Clients_Model_DbTable_Accounts();
                
                if (!$TAccounts->checkLogin($form->getValue('account_login'))) {
                    $form->account_login->setErrors(array(
                        'Podany login jest już wykorzystywane w systemie.'
                    ));
                }
                else if (!$TAccounts->checkMail($form->getValue('account_mail'))) {
                    $form->account_mail->setErrors(array(
                        'Podany adres e-mail jest już wykorzystywany w systemie.'
                    ));
                }
                else {
                    $TClients = new Clients_Model_DbTable_Clients();
                    $client = $TClients->createRow($form->getValues());
                    $client->created = time();
                    $client->save();
                    
                    $password = App_Util_String::randomPassword();
                    
                    $account = $TAccounts->createRow(array(
                        'name' => $form->getValue('account_name'),
                        'surname' => $form->getValue('account_surname'),
                        'mail' => $form->getValue('account_mail'),
                        'phone' => $form->getValue('account_phone'),
                        'clients_id' => $client->id,
                        'login' => $form->getValue('account_login'),
                        'created' => time(),
                        'password' => md5($password)
                    ));
                    $account->save();

                    $mail = new App_Mailer('html');
                    $mail->addTo($account->mail, $account->getName());
                    $mail->setSubject('Potwierdzenie założenia konta');
                    $mail->send(App_Mailer_Messages::registerSite($account, $password));
                    
                    $this->goToUrl('clients.registered');
                }
            }
        }
        
        $this->view->form = $form;
    }
    
    public function registeredAction()
    {
        
    }
    
    public function loginErrorAction()
    {
        $this->_helper->layout->disableLayout();
    }
    
    public function fastProductAddAction()
    {
        $user = $this->checkUser();
        
        $client = $user->getClient();
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $index = $request->getPost('index');
            
            if (!empty($index)) {
                $TVariants = new Products_Model_DbTable_Variants();
                $variant = $TVariants->byIndex($index);
                
                if ($variant !== null && $user->hasAccessToVariant($variant)) {
                    $order = $this->getOrder(true);
                    $element = $order->addElement($variant, $client->getPriceForVariant($variant), 1);
                    //$order->price_sum += $client->getPriceForVariant($variant);

                    $this->messages->info('Produkt został dodany do koszyka.');

                    if ($user->hasLimits() && $user->id == $order->owner_id && $user->getTotalBudget() < $order->getTotalPrice()) {
                        $this->messages->error('Wartość Twojego koszyka przekroczyła posiadany budżet!');
                    }
                }
                else {
                    $this->messages->info('Wybrany produkt nie znajduje się obecnie w naszej ofercie lub nie masz do niego dostępu.');
                }
            }
        }
        
        $this->goToUrl('clients.basket');
    }
    
    public function basketAction()
    {
        $user = $this->checkUser();
        
        $this->view->order = $order = $this->getOrder();
        $orderId = $order === null ? 0 : $order->id;
        
        $TOrders = new Orders_Model_DbTable_Orders();
        
        $this->view->saved = $TOrders->fetchAll(array('owner_id = ?' => $user->id, 'reported = 0', 'id != ?' => $orderId), 'created');
	
        // Delivery address
        $TAddresses = new Clients_Model_DbTable_Addresses();
        $addressId = $order ? $order->address_id : 0;
        $addresses = array(1 => '- podstawowy adres dostawy');
        
        foreach ($TAddresses->fetchAll(array('client_id = ?' => $user->clients_id), 'name') as $adr) {
            $addresses[$adr->id] = $adr->name;
        }
        
        if (!$addressId && isset($addresses[$user->address_id])) {
            $addressId = $user->address_id;
        }
        
        $this->view->delAddresses = $addresses;
        $this->view->delAddressId = $addressId;
        $this->view->allowAddress = empty($user->parent) || $user->allow_address;
        
        // Get orders which should have user attention (veryfing)
        $this->view->attention = $TOrders->fetchAll(array('verifier = ?' => $user->id, 'thrown = 0'), 'created');

        // Get all active orders which user participated in
        $this->view->active = $TOrders->fetchAll(array('(owner_id = '. $user->id .' OR old_verifier = '. $user->id .' OR pusher_id = '. $user->id .') AND reported > 0 AND received = 0 AND thrown = 0'), array('reported'));
        
    }
    
    public function historyAction()
    {
        $user = $this->checkUser();
        
        $TOrders = new Orders_Model_DbTable_Orders();
        
        $this->view->rows = $TOrders->fetchAll(array('(owner_id = '.$user->id.' OR old_verifier ='. $user->id .' OR pusher_id = '. $user->id .') AND status = \'3\''), 'created');
    }
    
    public function thrownAction()
    {
        $user = $this->checkUser();
        
        $TOrders = new Orders_Model_DbTable_Orders();
        $this->view->rows = $TOrders->fetchAll(array('(owner_id = '.$user->id.' OR old_verifier = '.$user->id.' OR verifier ='.$user->id.') AND thrown != 0'), 'created');
    }
    
    public function statsAction()
    {
        $user = $this->checkUser();
        
        if (!$user->canSeeStats()) {
            $this->showError('Przepraszamy, ale Twoje konto nie posiada dostępu do statystyk');
            return;
        }
        
        $request = $this->getRequest();
        $TOrders = new Orders_Model_DbTable_Orders();
        $query = $TOrders->select();
        $query->setIntegrityCheck(false);
        
        $query  ->from('orders')
                ->where('orders.clients_id = ?', $user->clients_id)
                ->where('status > ?', '0')
                ->order('accounts.surname')
                ->joinLeft('accounts', 'accounts.id = orders.owner_id', array('person' => 'CONCAT(name, \' \', surname)'));
        
        if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $request->getPost('date_from'))) {
            $query  ->where('FROM_UNIXTIME(pushed, \'%Y-%m-%d\') >= ?', $request->getPost('date_from'));
        }
        if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $request->getPost('date_to'))) {
            $query  ->where('FROM_UNIXTIME(pushed, \'%Y-%m-%d\') <= ?', $request->getPost('date_to'));
        }
        
        $rows = $TOrders->fetchAll($query);
        $orders = array();
        
        foreach ($rows as $row) {
            $uid = $row->owner_id;
            
            if (!isset($orders[$uid])) {
                $orders[$uid] = array(
                    'label' => $row->person,
                    'items' => array(),
                    'price_sum' => 0,
                    'products' => 0,
                    'last' => 0
                );
            }
            
            $orders[$uid]['items'][] = $row;
            $orders[$uid]['price_sum'] += $row->price_sum;
            $orders[$uid]['products'] += $row->getElementsCount();
            
            if ($row->pushed > $orders[$uid]['last']) {
                $orders[$uid]['last'] = $row->pushed;
            }
        }
        
        $summary = array(
            'orders' => 0,
            'price_sum' => 0,
            'products' => 0,
            'last' => 0
        );
        
        foreach ($orders as $order) {
            $summary['orders'] += count($order['items']);
            $summary['price_sum'] += $order['price_sum'];
            $summary['products'] += $order['products'];
            
            if ($order['last'] > $summary['last']) {
                $summary['last'] = $order['last'];
            }
        }
        
        $this->view->orders = $orders;
        $this->view->summary = $summary;
        $this->view->date_from = $request->getPost('date_from');
        $this->view->date_to = $request->getPost('date_to');
    }
    
    public function statsProductsAction()
    {
        $user = $this->checkUser();
        
        if (!$user->canSeeStats()) {
            $this->showError('Przepraszamy, ale Twoje konto nie posiada dostępu do statystyk');
            return;
        }
        
        $TGroups = new Products_Model_DbTable_Groups();
        $request = $this->getRequest();
        
        foreach ($TGroups->fetchAll() as $group) {
            $groups[$group->id] = array(
                'label' => $group->name,
                'quantity' => 0,
                'price_sum' => 0,
                'items' => array()
            );
        }
        $groups[0] = array(
            'label' => 'oferta specjalna',
            'quantity' => 0,
            'price_sum' => 0,
            'items' => array()
        );
        
        $summary = array('quantity' => 0, 'price_sum' => 0);
        
        $query = App_Db_Table::getDefaultAdapter()->select();
        
        $query  ->from(array('oe' => 'orders_elements'), array('quantity' => 'SUM(quantity)', 'price_sum' => 'SUM(oe.price_sum)'))
                ->joinInner(array('pv' => 'products_variants'), 'pv.id = oe.variants_id', array('id', 'index'))
                ->joinInner(array('p' => 'products'), 'p.id = oe.products_id', array('name', 'mark', 'groups_id'))
                ->joinInner(array('o' => 'orders'), 'o.id = oe.orders_id', 'pushed')
                ->order(array('p.groups_id'))
                ->where('o.pushed > 0')
                ->where('o.clients_id = ?', $user->clients_id)
                ->group('oe.variants_id');
        
        $order = $this->_getSort(array('quantity', 'price_sum'));
        if ($order) {
            $query->order($order);
        }
        else {
            $query->order(array('p.mark', 'p.name'));
        }
        
        if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $request->getPost('date_from'))) {
            $query  ->where('FROM_UNIXTIME(pushed, \'%Y-%m-%d\') >= ?', $request->getPost('date_from'));
        }
        if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $request->getPost('date_to'))) {
            $query  ->where('FROM_UNIXTIME(pushed, \'%Y-%m-%d\') <= ?', $request->getPost('date_to'));
        }
        if ((int) $request->getParam('account_id')) {
            $query  ->where('o.owner_id = ?', (int) $request->getParam('account_id'));
        }//die($query);
        
        foreach ($query->query()->fetchAll() as $item) {
            $gid = $item['groups_id'];
            $vid = $item['id'];

            $groups[$gid]['items'][] = $item;
            
            $groups[$gid]['quantity'] += $item['quantity'];
            $groups[$gid]['price_sum'] += $item['price_sum'];
            
            $summary['quantity'] += $item['quantity'];
            $summary['price_sum'] += $item['price_sum'];
        }
        
        $this->view->items = $summary['quantity'] ? $groups : array();
        $this->view->summary = $summary;
        $this->view->date_from = $request->getPost('date_from');
        $this->view->date_to = $request->getPost('date_to');
        $this->view->account_id = (int) $request->getParam('account_id');
        $this->view->accounts = $user->getClient()->getAccounts();
        
        if ($request->getPost('toXls')) {
            $xls = new PHPExcel();

            $client = new Clients_Model_DbTable_Clients();
            $client = $client->find($user->clients_id)->current();

            // filename for download
            $filename = 'stats_' . $client->name . '_' . date('Y-m-d');
            if ($request->getParam('account_id')) {
                $account = new Clients_Model_DbTable_Accounts();
                $account = $account->find($request->getParam('account_id'))->current();
                $filename .= '_' . $account->login;
            }
            $filename .= '.xls';

            // Redirect output to a clientâ€™s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0

            $row = 2;
            $cell = 'A';

            $xls->setActiveSheetIndex(0);

            $columns = array(
                'A' => 'grupa towarowa',
                'B' => 'index',
                'C' => 'marka',
                'D' => 'nazwa produktu',
                'E' => 'ilość',
                'F' => 'suma'
            );

            foreach ($columns as $col => $label) {
                $xls->getActiveSheet()->setCellValue($col . '1', $label);
                $xls->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }

            foreach ($groups as $group) {
                foreach ($group['items'] as $item) {
                    $xls->getActiveSheet()->setCellValue('A' . $row, $group['label']);
                    $xls->getActiveSheet()->setCellValue('B' . $row, $item['index']);
                    $xls->getActiveSheet()->setCellValue('C' . $row, $item['mark']);
                    $xls->getActiveSheet()->setCellValue('D' . $row, $item['name']);
                    $xls->getActiveSheet()->setCellValue('E' . $row, $item['quantity']);
                    $xls->getActiveSheet()->setCellValue('F' . $row, $item['price_sum']);

                    $row++;
                }
            }

            $xls->setActiveSheetIndex(0);
            $xls->getActiveSheet()->setAutoFilter(
                $xls->getActiveSheet()->calculateWorksheetDataDimension()
            );


            $objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }
    }
    
    public function settingsAction()
    {
        $user = $this->checkUser();
        
        if ($user->parent) {
            $this->forward('password-change');
        }
        else {
            $this->forward('settings-accounts');
        }
    }
    
    public function settingsAddressesAction()
    {
        $user = $this->checkUser();
        
        $TAddresses = new Clients_Model_DbTable_Addresses();
        
        $this->view->addresses = $TAddresses->fetchAll(array(
            'client_id = ?' => $user->clients_id
        ), 'name');
        $this->view->account = $user;
    }
    
    public function addressFormAction()
    {
        $user = $this->checkUser();
        
        $form = new Clients_Form_Address();
        $request = $this->getRequest();
        
        $id = (int) $this->getParam('id');
        $TAddresses = new Clients_Model_DbTable_Addresses();
        $add = $TAddresses->find($id)->current();
        
        if ($add && $add->client_id != $user->clients_id) {
            die('Hack attempt.');
        }
        
        if ($request->isPost()) {
            if ($form->isValid($request->getPost())) {
                
                if (!$add) {
                    $add = $TAddresses->createRow(array(
                        'client_id' => $user->clients_id
                    ));
                }
                
                $add->setFromArray($form->getValues());
                $add->save();
                
                $this->messages->info('Adres dostawy został zapisany');
                $this->goToUrl('clients.settings-addresses');
            }
        }
        else if ($add){
            $form->setDefaults($add->toArray());
        }
        
        $this->view->form = $form;
    }
    
    public function settingsAccountsAction()
    {
        $user = $this->checkUser();
        
        $this->view->account = $user;
    }
    
    public function accountLimitsAction()
    {
        $user = $this->checkUser();
        
        $id = (int) $this->_getParam('id');
        $TAccounts = new Clients_Model_DbTable_Accounts();
        $row = $TAccounts->find($id)->current();
        
        if (empty($row) || $row->clients_id != $user->clients_id) {
            $this->goToUrl('clients.settings');
        }
        
        $form = $this->_getAccountLimitsForm($row);
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            if ($form->isValid($request->getPost())) {
                $data = $form->getValues();
                $row->setFromArray($data);
                $row->save();
                
                $form->setDefaults($row->toArray());
                
                $this->messages->info('Opcje budżetu konta zostały zmienione pomyślnie.');
            }
        }
        else {
            $form->setDefaults($row->toArray());
        }
        
        $this->view->row = $row;
        $this->view->form = $form;
    }
    
    public function accountFormAction()
    {
        $user = $this->checkUser();
        
        $id = (int) $this->_getParam('id');
        $TAccounts = new Clients_Model_DbTable_Accounts();
        $row = $TAccounts->find($id)->current();
        
        if (($id && empty($row)) || ($row && $row->clients_id != $user->clients_id)) {
            $this->goToUrl('clients.settings');
        }
        
        $form = $this->_getAccountForm($row);
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            if ($form->isValid($request->getPost())) {
                if (empty($row) && !$TAccounts->checkLogin($form->getValue('login'), $user->clients_id)) {
                    $form->login->setErrors(array(
                        'Podany login jest już przypisany do istniejącego konta'
                    ));
                }
                else if (!$TAccounts->checkMail($form->getValue('mail'), $row ? $row->id : null)) {
                    $form->mail->setErrors(array(
                        'Podany adres e-mail jest już przypisany do istniejącego konta'
                    ));
                }
                else {
                    if (empty($row)) {
                        $row = $TAccounts->createRow(array(
                            'clients_id' => $user->clients_id,
                            'created' => time()
                        ));
                    }
                    
                    $form->login->setValue($user->login . '_' . $form->getValue('login'));

                    $row->setFromArray($form->getValues());
                    $row->save();

                    if (empty($id)) {
                        $password = App_Util_String::randomPassword();
                        $row->password = md5($password);
                        $row->save();
                        
                        $this->messages->info('Nowe konto zostało utworzone.');
                        $this->messages->info('Można teraz przypisać do niego weryfikatora oraz zarządzać budżetem.');
                        
                        $mail = new App_Mailer('html');
                        $mail->addTo($row->mail, $row->getName());
                        $mail->setSubject('Informacje dotyczące Twojego konta');
                        $mail->send(App_Mailer_Messages::accountCreated($row, $password));
                        
                        $this->goToUrl('clients.account-edit', array('id' => $row->id));
                    }
                    else {
                        $this->messages->info('Dane konta zostały zapisane pomyślnie.');
                    }

                    if (is_array($form->getValue('groups'))) {
                        $row->setGroupsAccess($form->getValue('groups'));
                    }

                    $form = $this->_getAccountForm($row);
                    $form->setDefaults($row->toArray());
                }
            }
        }
        else if ($row !== null) {
            $form->setDefaults($row->toArray());
        }

        $this->view->row = $row;
        $this->view->form = $form;
    }
    
    public function companyDataAction()
    {
        $user = $this->checkUser();
        
        $client = $user->getClient();
        $form = new Clients_Form_Company();
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            if ($form->isValid($request->getPost())) {
                $client->setFromArray($form->getValues());
                $client->save();
                
                $this->messages->info('Dane firmy zostały zaktualizowane');
            }
        }
        
        $form->setDefaults($client->toArray());
        $this->view->form = $form;
    }
    
    public function passwordChangeAction()
    {
        $user = $this->checkUser();
        
        $form = new Clients_Form_Password();
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            if ($form->isValid($request->getPost())) {
                $data = $form->getValues();
                
                if ($user->password != md5($data['old_password'])) {
                    $form->old_password->setErrors(array(
                        'Podane hasło nie jest prawidłowe'
                    ));
                }
                else if (strlen($data['password']) < 6) {
                    $form->getElement('password')->setErrors(array(
                        'Hasło musi zawierać przynajmniej 6 znaków'
                    ));
                }
                else if (!preg_match('/\d/', $data['password'])) {
                    $form->password->setErrors(array(
                        'Hasło musi zawierać przynajmniej jedną cyfrę'
                    ));
                }
                else if (!preg_match('/\w/', $data['password'])) {
                    $form->password->setErrors(array(
                        'Hasło musi zawierać przynajmniej jedną literę'
                    ));
                }
                else if ($data['password'] != $data['password_confirm']) {
                    $form->password_confirm->setErrors(array(
                        'Podane hasła nie są identyczne'
                    ));
                }
                else {
                    $user->password = md5($data['password']);
                    $user->save();
                    
                    $this->messages->info('Hasło zostało zmienione pomyślnie');
                    $this->goToUrl('clients.password-change');
                }
            }
        }
        
        $this->view->form = $form;
    }
    
    protected function _getAccountForm($row)
    {
        $form = new Zend_Form();
        $user = $this->getUser();
        
        if (empty($row)) {
            $accounts = array(
                $user->id => $user->getName() .  ' (' . $user->login . ')'
            );
            foreach ($user->getAccounts() as $account) {
                $accounts[$account->id] = '-' . $account->getName() . ' (' . $account->login . ')';
            }
            
            $form->addElement('Select', 'parent', array(
                'label' => 'Konto nadrzędne',
                'required' => true,
                'multiOptions' => $accounts
            ));
        }
        else if (!empty($row->parent)) {
            $parent = $row->getParent();
            $form->addElement('Note', 'parent2', array(
                'label' => 'Konto nadrzędne',
                'value' => $parent->getName() . ' (' . $parent->login . ')'
            ));
        }
        if (empty($row)) {
            $form->addElement('Text', 'login', array(
                'label' => 'Login',
                'required' => true
            ));
        }
        else {
            $form->addElement('Note', 'login', array(
                'label' => 'Login'
            ));
        }
        if (!empty($row) && !empty($row->parent)) {
            $verifiers = array(0 => 'brak weryfikatora');
            $parent = $row->getParent();
            if ($parent->parent) {
                $user = $this->getUser();
                $verifiers[$user->id] = $user->getName() . ' (' . $user->login . ')';
            }
            $verifiers[$parent->id] = $parent->getName() . ' (' . $parent->login . ')';
            
            $form->addElement('Select', 'verifier', array(
                'label' => 'Weryfikator',
                'multiOptions' => $verifiers
            ));
            
        }
        $form->addElement('Text', 'name', array(
            'label' => 'Imię',
            'required' => true
        ));
        $form->addElement('Text', 'surname', array(
            'label' => 'Nazwisko',
            'required' => true
        ));
        $form->addElement('Text', 'mail', array(
            'label' => 'Adres e-mail',
            'required' => true
        ));
        $form->addElement('Text', 'phone', array(
            'label' => 'Tel. kontaktowy'
        ));
        
        if (empty($row) || !empty($row->parent)) {
            $TAddresses = new Clients_Model_DbTable_Addresses();
            $addresses = array(
                0 => '- podstawowy adres dostawy'
            );
            foreach ($TAddresses->fetchAll(array('client_id = ?' => $user->clients_id), 'name') as $adr) {
                $addresses[$adr->id] = $adr->name;
            }
            $form->addElement('Checkbox', 'allow_address', array(
                'label' => 'Zezwól na zmianę adresu dostawy'
            ));
            $form->addElement('Select', 'address_id', array(
                'label' => 'Domyślny adres dostawy',
                'multiOptions' => $addresses
            ));
            $TGroups = new Products_Model_DbTable_Groups();
            $groups = array();
            foreach ($TGroups->fetchAll(null, 'order') as $g) {
                $groups[$g->id] = $g->name;
            }
            $groups[0] = 'zaznacz wszystkie';

            $aGroups = array();
            if (!empty($row->parent)) {
                foreach ($row->getGroupsAccess() as $g) {
                    $aGroups[] = $g->groups_id;
                }
            }

            $form->addElement('MultiCheckbox', 'groups', array(
                'label' => 'Dostęp do grup towarowych',
                'multiOptions' => $groups,
                'value' => $aGroups
            ));
        }
        
        $form->addElement('Submit', 'submit', array(
            'label' => empty($row) ? 'dodaj konto' : 'zapisz dane'
        ));
        
        return $form;
    }
    
    protected function _getAccountLimitsForm($row)
    {
        $form = new Zend_Form();
        
        $form->addElement('Radio', 'limit_type', array(
            'label' => 'Rodzaj limitu',
            'required' => true,
            'multiOptions' => array(
                '0' => 'brak limitu',
                '1' => 'limit miesięczny',
                '2' => 'limit kwartalny',
                '3' => 'limit półroczny',
                '4' => 'limit roczny'
            ),
            'onChange' => 'changeLimitType(this)',
            'description' => 'Możesz ustalić dla tego konta limit budżetu, którego nie będzie mógł przekroczyć. Limity przyznawane są automatycznie w wybranych okresach budżetowania.'
        ));
        $form->addElement('Text', 'limit', array(
            'label' => 'Kwota limitu',
            'filters' => array('Int'),
            'description' => 'Wprowadź kwotę limitu, która będzie przyznawana automatycznie w wybranym wcześniej okresie budżetowania.'
        ));
        $form->addElement('Checkbox', 'limit_permanent', array(
            'label' => 'Limit trwały',
            'description' => 'Zaznacz, jeśli budżet z limitu ma przechodzić pomiędzy następnymi okresami'
        ));
        $form->addElement('Text', 'budget', array(
            'label' => 'Budżet podstawowy',
            'description' => 'Obecny budżet tego konta ze środków otrzymywanych automatycznie. Jeśli nie została wybrana opcja \'limit trwały\', to budżet ten jest zerowany na koniec każdego okresu.'
        ));
        $form->addElement('Text', 'budget_extra', array(
            'label' => 'Budżet dodatkowy',
            'description' => 'Wartość budżetu dodatkowego, który można zwiększyć w dowolnej chwili. Budżet dodatkowy nie jest zerowany automatycznie.'
        ));
        $form->addElement('Submit', 'submit', array(
            'label' => 'zapisz zmiany'
        ));
        
        return $form;
    }

    public function orderToPdfAction()
    {
        $id = (int) $this->_getParam('id');
        $TOrders = new Orders_Model_DbTable_Orders();
        $order = $TOrders->find($id)->current();

        $user = $this->checkUser();
        if (!$order || !$user->hasAccessToOrder($order)) {
            $this->showError('Wybrane zamówienie nie istnieje lub nie posiadasz do niego dostępu.');
            return;
        }

        $generator = new \Expert\Order\PDFGenerator();
        $mpdf = $generator->generate($id);

        $file = 'orders/expert_zam_' . $id . '_' . date('Ymd_Hi') . '.pdf';

        $mpdf->Output($file, 'F');
        $this->_redirect($file);
        exit;
    }
    
    public function orderAction()
    {
        $id = (int) $this->_getParam('id');
        $TOrders = new Orders_Model_DbTable_Orders();
        $order = $TOrders->find($id)->current();
        
        $hash = $this->_getParam('hash');
        $sig = $this->_getParam('sig');
        
        if ($order && $sig && $hash && $hash == md5($id) && $sig == md5($id . $hash)) {
            
        }
        else {
            $user = $this->checkUser();
            if (!$order || !$user->hasAccessToOrder($order)) {
                $this->showError('Wybrane zamówienie nie istnieje lub nie posiadasz do niego dostępu.');
                return;
            }
        }
        
        $creator = $order->getCreator();
        $verifier = $order->getVerifier();
        $oldVerifier = $order->getOldVerifier();
        
        $properties = array();
        $properties['Status'] = $order->getStatus();
        
        if (empty($order->reported)) {
            $properties['Rozpoczęto'] = date('d-m-Y', $order->created);
        }
        else  {
            $properties['Stworzono'] = date('d-m-Y', $order->reported) . ' przez ' . ($creator ? $creator->getName() : '-');
        }
        
        if ($order->status) {
            $properties['Złożono'] = date('d-m-Y', $order->pushed) . ' przez ' . $order->getPusher()->getName();
        }
        if ($order->status > 1) {
            $properties['Przyjęto do realizacji'] = date('d-m-Y', $order->accepted);
        }
        if ($order->status == '3') {
            $properties['Zrealizowano'] = date('d-m-Y', $order->received);
        }
        
        if (empty($order->status) && !empty($order->reported)) {
            $properties[$order->thrown ? 'Odrzucił' : 'Weryfikuje'] = ($verifier ? $verifier->getName() : '-');
        }
        
        if ($oldVerifier) {
            $properties['Weryfikował'] = $oldVerifier->getName();
        }
        
        $this->view->order = $order;
        $this->view->properties = $properties;
        $this->view->returnTo = $this->_getParam('_s');
    }
    
    public function orderActionAction()
    {
        $user = $this->checkUser();
        
        $id = (int) $this->_getParam('id');
        $action = $this->_getParam('do');
        
        $TOrders = new Orders_Model_DbTable_Orders();
        $order = $TOrders->find($id)->current();
        
        if (!$id) {
            $order = $this->getOrder();
        }
        
        $backUrl = '';
        
        if ($this->getRequest()->getPost('recount')) {
            $action = 'recount';
        }
        else if ($this->getRequest()->getPost('send')) {
            $action = 'send';
        }
        else if ($this->getRequest()->getPost('accept')) {
            $action = 'accept';
        }
        
        switch ($action) {
            case 'open':
                if (($order->owner_id == $user->id && !$order->reported) || $order->verifier == $user->id) {
                    if (($curOrder = $this->getOrder()) && !$curOrder->hasElements()) {
                        $curOrder->delete();
                    }
                    $this->getSession()->order = $order->id;
                }
                break;
            case 'delete':
                $this->_deleteOrder($order);
                break;
            case 'copy':
                $backUrl = $this->_copyOrder($order);
                break;
            case 'accept':
                $this->_acceptOrder($order);
                break;
            case 'throw':
                $this->_throwOrder($order);
                break;
            case 'clear':
                if (($order = $this->getOrder()) && $order->owner_id == $user->id) {
                    $order->reset();
                }
                break;
            case 'recount':
                if (($order = $this->getOrder())) {
                    $TElements = new Orders_Model_DbTable_Elements();
                    
                    foreach ($this->getRequest()->getPost('quantity') as $id => $quantity) {
                        $element = $TElements->find($id)->current();
                        
                        if ($element === null || $element->orders_id != $order->id) {
                            continue;
                        }
                        
                        if ($quantity != $element->quantity && is_numeric($quantity)) {
                            $order->price_sum -= $element->price_sum;
                            
                            if (empty($quantity)) {
                                $element->delete();
                            }
                            else {
                                $element->quantity = $quantity;
                                $element->price_sum = $element->price * $element->quantity;
                                $order->price_sum += $element->price_sum;
                                $element->save();
                            }
                        }
                    }

                    foreach ($this->getRequest()->getPost('quantity') as $id => $quantity) {
                        $element = $TElements->find($id)->current();

                        if ($element === null || $element->orders_id != $order->id) {
                            continue;
                        }

                        if ($quantity != $element->quantity && is_numeric($quantity)) {
                            $order->price_sum -= $element->price_sum;

                            if (empty($quantity)) {
                                $element->delete();
                            }
                            else {
                                $element->quantity = $quantity;
                                $element->price_sum = $element->price * $element->quantity;
                                $order->price_sum += $element->price_sum;
                                $element->save();
                            }
                        }
                    }

                    $this->messages->info('Zawartość Twojego koszyka została przeliczona');

                    if ($user->hasLimits() && $order->owner_id == $user->id && $user->getTotalBudget() < $order->getTotalPrice()) {
                        $this->messages->error('Wartość Twojego koszyka przekroczyła posiadany budżet!');
                    }

                    $order->save();
                }
                break;
            case 'send':
                $this->_sendOrder();
                break;
        }
        
        if (empty($backUrl)) {
            $backUrl = 'clients.basket';
        }
        $this->goToUrl($backUrl);
    }
    
    protected function _deleteOrder($order)
    {
        $user = $this->getUser();
        
        if ($order->owner_id == $user->id && empty($order->reported)) {
            $order->delete();
            
            $this->messages->info('Wybrane zamówienie zostało usunięte z systemu');
        }
    }
    
    
    protected function _acceptOrder($order)
    {		
        $user = $this->getUser();
        
        if ($order->verifier != $user->id) {
            return;
        }
        
        $creator = $order->getCreator();
        
        if (array_key_exists('comments', $_POST)) {
            $order->comments = $_POST['comments'];
        }
        
        if (!empty($_POST['address_id'])) {
            $order->address_id = $_POST['address_id'];
        }
        
        // No more verifiers, send order! :)
        if (empty($user->verifier)) {
            $order->pusher_id = $user->id;
            $order->verifier = 0;
            $order->status = '1';
            $order->price_sum = $order->getTotalPrice();
            $order->pushed = time();
        
            // Check current order price
            if ($order->owner_reported && $creator->limit_type && $order->price_sum != $order->owner_price_sum) {
                // Current price is lower then original
                if ($order->price_sum < $order->owner_price_sum) {
                    $diff = $order->owner_price_sum - $order->price_sum;
                    
                    if ($order->used_budget_extra > 0) {
                        if ($diff > $order->used_budget_extra) {
                            $creator->addBudget($order->used_budget_extra, '5');
                            $diff -= $order->used_budget_extra;
                        }
                        else {
                            $creator->addBudget($diff, '5');
                            $diff = 0;
                        }
                    }
                    
                    if ($diff > 0 && $order->used_budget > 0 && $creator->isCurrentLimitPeriod($order->owner_reported)) {
                        if ($diff > $order->used_budget) {
                            $creator->addBudget($order->used_budget, $creator->limit_type);
                        }
                        else {
                            $creator->addBudget($diff, $creator->limit_type);
                        }
                    }
                }
                // Current price is higher them original
                else if ($creator->isCurrentLimitPeriod($order->owner_reported)) {
                    $diff = $order->price_sum - $order->owner_price_sum;
                    $creator->budget -= min($diff, $creator->budget);
                    $creator->save();
                }
            }
            
            $TUsers = new Users_Model_DbTable_Users();
            $client = $user->getClient();
            
            $delAdr = "Adres dostawy:\n";
            
            $TAddresses = new Clients_Model_DbTable_Addresses();
            $adr = $TAddresses->find($order->address_id)->current();
            
            if (!$adr) {
                $delAdr .= $client->delivery_street . "\n";
                $delAdr .= $client->delivery_citycode . " " . $client->delivery_city;
            }
            else {
                $delAdr .= $adr->street . "\n";
                $delAdr .= $adr->citycode . " " . $adr->city;
            }
            
            $order->comments .= "\n\n"  . $delAdr;
            
            // Send mail to verifier
            $mail = new App_Mailer('html');
            $mail->addTo($user->mail, $user->getName());
            $mail->setSubject('Zamówienie #' . $order->id . ' zostało złożone');
            $mail->send(App_Mailer_Messages::orderAccept($order));
            
            // Send mail to creator
            $mail = new App_Mailer('html');
            $mail->addTo($creator->mail, $creator->getName());
            $mail->setSubject('Zamówienie #' . $order->id . ' zostało zaakceptowane');
            $mail->send(App_Mailer_Messages::orderAcceptCreator($order));
            
            // Send mail to previous verifier
            if ($order->old_verifier) {
                $verifier = $order->getOldVerifier();

                $mail = new App_Mailer('html');
                $mail->addTo($verifier->mail, $verifier->getName());
                $mail->setSubject('Zamówienie #' . $order->id . ' zostało złożone');
                $mail->send(App_Mailer_Messages::orderAcceptVerifier($order));
            }
            
            
            if (!$client->users_id) $client->users_id = 24;
            $admin = $TUsers->find($client->users_id)->current();

            $mail = new App_Mailer('html');
            $mail->addTo($admin->mail);
            $mail->setSubject('Nowe zamówienie #' . $order->id);
            $mail->send(App_Mailer_Messages::orderNewInfo($order));
            
            $this->messages->info('Dziękujemy! Zamówienie zostało złożone i oczekuje na przyjęcie do realizacji.');
        }
        else {
                $order->verifier = $user->verifier;
                $order->old_verifier = $user->id;

                $verifier = $user->getVerifier();
                
                // Send mail to verifier
                $mail = new App_Mailer('html');
                $mail->addTo($verifier->mail, $verifier->getName());
                $mail->setSubject('Nowe zamówienie do weryfikacji');
                $mail->send(App_Mailer_Messages::orderToVerify($order));

                $this->messages->info('Dziękujemy! Zamówienie zostało zaakceptowane i przekazane do dalszej weryfikacji.');
        }

        // If it's an active order, unpin it
        if ($this->getSession()->order == $order->id) {
            $this->getSession()->order = null;
        }

        $order->save();
    }
    
    /**
     * 
     * @param   Orders_Model_Order $order
     * @return type
     */
    protected function _throwOrder($order)
    {
        $user = $this->getUser();
        
        if ($user->id != $order->verifier) {
            return;
        }

        $creator = $order->getCreator();
        
        // Check if we should give back budget
        if ($creator->limit_type && $order->owner_reported) {
            // User used extra budget, give it back
            if ($order->used_budget_extra) {
                $creator->addBudget($order->used_budget_extra, '5');
            }
            // User used normal budget and his budget is permanent or it's still same limit period, give it back
            if ($order->used_budget && ($creator->limit_permanent || $creator->isCurrentLimitPeriod($order->owner_reported))) {
                $creator->addBudget($order->used_budget, $creator->limit_type);
            }
        }

        $products = App_Mailer::createProductsList($order);

        // Send mail to verifier
        $mail = new App_Mailer('html');
        $mail->addTo($user->mail, $user->getName());
        $mail->setSubject('Potwierdzenie odrzucenia zamówienia #' . $order->id);
        $mail->send(App_Mailer_Messages::orderThrow($order));

        // Send mail to creator
        $mail = new App_Mailer('html');
        $mail->addTo($creator->mail, $creator->getName());
        $mail->setSubject('Zamówienie #' . $order->id . ' zostało odrzucone');
        $mail->send(App_Mailer_Messages::orderThrowCreator($order, $user));

        // If it's an active order, unpin it
        if ($this->getSession()->order == $order->id) {
            $this->getSession()->order = null;
        }
        
        // Now we can remove that order from system
        $order->thrown = time();
        $order->save();

        $this->messages->info('Zamówienie zostało odrzucone i nie zostanie przekazane do realizacji.');
    }
    
    protected function _sendOrder()
    {
        $user = $this->getUser();
        $order = $this->getOrder();
        $request = $this->getRequest();
        
        if ($order === null) {
            return;
        }
        
        // Check if user has limit and budget
        if ($user->limit_type) {
            $budget = $user->getTotalBudget();
            
            if ($order->price_sum > $budget) {
                $this->messages->error('Zamówienie nie zostało złożone, ponieważ jego wartość przekracza obecny budżet.');
                return;
            }
        }
        
        $order->comments = $request->getPost('comments');
        $order->reported = time();
        
        if (!empty($_POST['address_id'])) {
            $order->address_id = $_POST['address_id'];
        }
        				
        if (empty($user->verifier)) { // user has no verifier
            $order->pusher_id = $user->id;
            $order->pushed = time();
            $order->price_sum = $order->getTotalPrice();
            $order->status = '1';
            
            if ($user->limit_type) {
                $user->reduceBudget($order->price_sum);
            }
             
            $mail = new App_Mailer('html');
            $mail->addTo($user->mail, $user->getName());
            $mail->setSubject('Potwierdzenie złożenia zamówienia #' . $order->id);
            $mail->send(App_Mailer_Messages::orderSend($order));

            $this->messages->info('Dziękujemy! Twoje zamówienie zostało złożone. Poinformujemy Cię mailowo, gdy zamówienie zostanie przekazane do realizacji.');
            
            $TUsers = new Users_Model_DbTable_Users();
            $client = $user->getClient();
            if (!$client->users_id) $client->users_id = 24;
            $admin = $TUsers->find($client->users_id)->current();
            
            $delAdr = "Adres dostawy:\n";
            
            $TAddresses = new Clients_Model_DbTable_Addresses();
            $adr = $TAddresses->find($order->address_id)->current();
            
            if (!$adr) {
                $delAdr .= $client->delivery_street . "\n";
                $delAdr .= $client->delivery_citycode . " " . $client->delivery_city;
            }
            else {
                $delAdr .= $adr->street . "\n";
                $delAdr .= $adr->citycode . " " . $adr->city;
            }
            
            $order->comments .= "\n\n"  . $delAdr;
            
            $mail = new App_Mailer('html');
            $mail->addTo($admin->mail);
            $mail->setSubject('Nowe zamówienie #' . $order->id);
            $mail->send(App_Mailer_Messages::orderNewInfo($order));
        } else {
            // User has a limit for orders, we need to do some more things
            if ($user->limit_type) {
                // Set date of original reported time
                $order->owner_reported = time();
                // Reduce user budget
                $reduced = $user->reduceBudget($order->price_sum);
                // Set info about used budget
                $order->used_budget = $reduced['budget'];
                $order->used_budget_extra = $reduced['budget_extra'];
                // Set info about original price
                $order->owner_price_sum = $order->price_sum;
            }
            
            // Assign verifier to order
            $verifier = $user->getVerifier();
            $order->verifier = $verifier->id;
            
            // Send mail to user
            $mail = new App_Mailer('html');
            $mail->addTo($user->mail, $user->getName());
            $mail->setSubject('Zamówienie #' . $order->id . ' zostało przekazane do weryfikacji');
            $mail->send(App_Mailer_Messages::orderToVerification($order));
            
            // Send mail to verifier
            $mail = new App_Mailer('html');
            $mail->addTo($verifier->mail, $verifier->getName());
            $mail->setSubject('Nowe zamówienie do weryfikacji');
            $mail->send(App_Mailer_Messages::orderToVerify($order));
						
            $this->messages->info('Dziękujemy! Twoje zamówienie zostało przekazane do weryfikacji.');
        }
        
        $order->save();
        $this->getSession()->order = null;
    }
    
    protected function _copyOrder($order)
    {
        $user = $this->getUser();
        
        if (!$user->hasAccessToOrder($order)) {
            return;
        }
        
        $TOrders = new Orders_Model_DbTable_Orders();
        $o = $TOrders->createRow();

        $o->clients_id = $user->clients_id;
        $o->owner_id = $user->id;
        $o->created = time();
        $o->status = '0';
        $o->save();

        $client = $user->getClient();

        foreach ($order->getElements() as $e) {
            $variant = $e->getVariant();
            
            if (!$client->hasAccessToVariant($variant)) {
                continue;
            }
            
            $product = $variant->getProduct();
            
            //if (!$Account->hasAccessToGroup($Product->groups_id) && !empty($this->user->parent)) continue;

            $price = $client->getPriceForVariant($variant);
            $Row = $o->addElement($variant, $price, $e->quantity);
            $o->price_sum += $Row->price_sum;
        }

        $o->save();
        
        if ($o->price_sum > 0) {
            if (($curOrder = $this->getOrder()) && !$curOrder->hasElements()) {
                $curOrder->delete();
            }
            $this->getSession()->order = $o->id;
            
            $this->messages->info('Wybrane zamówienie zostało powielone i znajduje się w Twoim aktywnym koszyku');
        }
        else {
            $o->delete();
            $this->messages->info('Żaden z produktów w wybranym zamówieniu nie jest już dostępny.');
        }
        
        
        return $this->getRequest()->getQuery('_s');
    }
    
    public function checkUser()
    {
        $user = $this->getUser();
        
        if ($user === null) {
            $this->goToUrl('clients.register');
        }
        
        return $user;
    }
}
