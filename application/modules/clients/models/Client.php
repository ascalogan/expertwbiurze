<?php

class Clients_Model_Client extends App_Db_Table_Row
{
    public function hasProductInOffer($id)
    {
        $TPromotion = new Clients_Model_DbTable_VariantsPromotion();
        return $TPromotion->fetchRow(array(
            'clients_id = ?' => $this->id,
            'products_id = ?' => $id
        )) !== null;
    }
    
    public function getAdmin()
    {
        if (!$this->users_id) {
            return null;
        }
        
        $TUsers = new Users_Model_DbTable_Users();
        return $TUsers->find($this->users_id)->current();
    }
    
    public function hasVariantInOffer($id)
    {
        return $this->getVariantOffer($id) !== null;
    }
    
    public function getVariantOffer($id)
    {
        $TPromotion = new Clients_Model_DbTable_VariantsPromotion();
        return $TPromotion->fetchRow(array(
            'clients_id = ?' => $this->id,
            'variants_id = ?' => $id
        ));
    }
    
    public function getPromotionForGroup($id)
    {
        $TPromotion = new Clients_Model_DbTable_GroupsPromotion();
        $row = $TPromotion->fetchRow(array(
            'clients_id = ?' => $this->id,
            'groups_id = ?' => $id
        ));
        
        return $row === null ? 0 : $row->percent;
    }
    
    public function hasAccessToVariant($variant)
    {
        if (!$variant instanceof Products_Model_Variant) {
            $TVariants = new Products_Model_DbTable_Variants();
            $variant = $TVariants->find($variant)->current();
        }
        // Variant does not exist or is marked as deleted
        if ($variant === null || $variant->status == '3') {
            return false;
        }
        
        $product = $variant->getProduct();
        
        
        // Product or variantis not active and client has no this variant in special offer
        if (($product->status != '1' || $variant->status != '1') && !$this->hasVariantInOffer($variant->id)) {
            return false;
        }
        
        return true;
    }
    
    public function getPriceForVariant($variant)
    {
        if (!$variant instanceof Products_Model_Variant) {
            $TVariants = new Products_Model_DbTable_Variants();
            $variant = $TVariants->find($variant)->current();
        }
        
        $product = $variant->getProduct();
        $price = $variant->price;
        
        if (($special = $this->getVariantOffer($variant->id))) {
            $price = $special->price;
        }
        else if (($promo = $this->getPromotionForGroup($product->groups_id))) {
            $price = number_format($variant->price*((100-$promo)/100), 2, '.', '');
        }
        
        if (($productPromo = $product->getPromotion())) {
            $price2 = number_format($variant->price*((100-$productPromo)/100), 2, '.', '');
            if ($price2 < $price) {
                $price = $price2;
            }
        }
        
        return $price;
    }
    
    public function getAccounts()
    {
        $TAccounts = new Clients_Model_DbTable_Accounts();
        return $TAccounts->fetchAll(array(
            'clients_id = ?' => $this->id
        ), array('surname', 'name'));
    }
    
    public function getMainAccount()
    {
        $TAccounts = new Clients_Model_DbTable_Accounts();
        return $TAccounts->fetchRow(array(
            'clients_id = ?' => $this->id,
            'parent = ?' => 0
        ));
    }
}