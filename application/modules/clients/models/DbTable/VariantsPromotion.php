<?php

class Clients_Model_DbTable_VariantsPromotion extends App_Db_Table
{
    protected $_name = 'clients_variants_promotion';
    
    public function get($clients_id, $variants_id)
    {
        return $this->fetchRow(array(
            'clients_id = ?' => $clients_id,
            'variants_id = ?' => $variants_id
        ));
    }
    
    public function add($clients_id, $products_id, $variants_id, $price)
    {
        $row = $this->createRow(array(
            'clients_id' => $clients_id,
            'products_id' => $products_id,
            'variants_id' => $variants_id,
            'price' => $price,
            'active' => 1
        ));
        $row->save();
    }
}