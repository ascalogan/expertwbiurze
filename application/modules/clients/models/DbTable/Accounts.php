<?php

class Clients_Model_DbTable_Accounts extends App_Db_Table
{
    protected $_name = 'accounts';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Clients_Model_Account';
    
    public function checkMail($mail, $id = null)
    {
        $row = $this->fetchRow(array('mail = ?' => $mail));
        
        return !$row || $row->id == $id;
    }
    
    public function checkLogin($login, $clients_id = null)
    {
        if ($clients_id) {
            $main = $this->getMainAccount($clients_id);
            $login = $main->login . '_' . strtolower($login);
        }
        else {
            $login = strtolower($login);
        }
        
        $row = $this->fetchRow(array('login = ?' => $login));
        
        return $row ? false : $login;
    }
    
    public function getMainAccount($clients_id) 
    {
        return $this->fetchRow(array('clients_id = ?' => $clients_id, 'parent = ?' => 0));
    }
    
    public function byCredentials($login, $password) {
        return $this->fetchRow(array(
            'login = ?' => $login,
            'password = ?' => md5($password)
        ));
    }
}