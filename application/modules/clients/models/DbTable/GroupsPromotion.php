<?php

class Clients_Model_DbTable_GroupsPromotion extends App_Db_Table
{
    protected $_name = 'clients_groups_promotion';
    
    protected $_primary = 'id';
    
    public function get($group_id, $client_id)
    {
        return $this->fetchRow(array(
            'groups_id = ?' => $group_id,
            'clients_id = ?' => $client_id
        ));
    }
    
    public function create($group_id, $client_id)
    {
        return $this->createRow(array(
            'groups_id' => $group_id,
            'clients_id' => $client_id
        ));
    }
}