<?php

class Clients_Model_DbTable_Clients extends App_Db_Table
{
    protected $_name = 'clients';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Clients_Model_Client';
}