<?php

class Clients_Model_Account extends App_Db_Table_Row
{

    public function hasLimits()
    {
        return $this->limit_type != '0';
    }

    public function getCurrentOrderValue()
    {
        $session = new Zend_Session_Namespace('/site');

        if (!isset($session->order)) {
            return 0;
        }

        $MOrders = new Orders_Model_DbTable_Orders();
        $order = $MOrders->find($session->order)->current();

        return $order->getTotalPrice();
    }

    public function getCurrentOrderElementsCount()
    {
        $session = new Zend_Session_Namespace('/site');

        if (!isset($session->order)) {
            return 0;
        }

        $MOrders = new Orders_Model_DbTable_Orders();
        $order = $MOrders->find($session->order)->current();

        return $order->getElementsCount();
    }

    public function getParent()
    {
        if (!$this->parent) {
            return null;
        }
        
        return $this->getTable()->find($this->parent)->current();
    }
    
    public function getVerifier()
    {
        if (!$this->verifier) {
            return null;
        }
        
        return $this->getTable()->find($this->verifier)->current();
    }
    
    public function getName()
    {
        return $this->name . ' ' . $this->surname;
    }
    
    public function getAccounts()
    {
        return $this->getTable()->fetchAll(array(
            'parent = ?' => $this->id,
            'removed != ?' => '1'
        ), array('surname', 'name'));
    }
    
    public function getLimitTypeLabel()
    {
        $labels = array(
            'brak limitu', 'miesięczny', 'kwartalny', 'półroczny', 'roczny'
        );
        return $labels[$this->limit_type];
    }
    /**
     * 
     * @return  Clients_Model_Client
     */
    public function getClient()
    {
        $TClients = new Clients_Model_DbTable_Clients();
        return $TClients->find($this->clients_id)->current();
    }
    
    public function addBudget($amount, $type)
    {   
        if ($type == '5') {
            $this->budget_extra += $amount;
        }
        else {
            $this->budget += $amount;
        }
        
        $this->save();
    }
    
    public function reduceBudget($amount)
    {
        $reduced = array(
            'budget' => 0,
            'budget_extra' => 0
        );
        
        if ($this->budget) {
            $budget_after = $this->budget - $amount;
            
            if ($budget_after >= 0) {
                $reduced['budget'] = $amount;
                $this->budget = $budget_after;
                $amount = 0;
            }
            else {
                $reduced['budget'] = $this->budget;
                $amount -= $this->budget;
                $this->budget = 0;
            }
        }
        if ($amount > 0) {
            $this->budget_extra -= $amount;
            $reduced['budget_extra'] = $amount;
        }
        
        $this->save();
        return $reduced;
    }
    
    public function clearBudget()
    {
        $this->addBudget(-$this->getTotalBudget(), '7');
        $this->budget = 0;
        $this->budget_extra = 0;
        $this->save();
    }
    
    public function getTotalBudget()
    {
        return $this->budget + $this->budget_extra;
    }
    
    public function isCurrentLimitPeriod($timestamp)
    {
        if ($this->limit_permanent) return true;
        
        list ($month, $year) = explode('-', date('n-Y', $timestamp));
        list ($cmonth, $cyear) = explode('-', date('n-Y'));
        
        if ($year != $cyear) {
            return false;
        }
        
        if ($this->limit_type == '1') {
            return $month == $cmonth;
        }
        else if ($this->limit_type == '2') {
            if ($cmonth <= 3) {
                return in_array($month, array('1', '2', '3'));
            }
            else if ($cmonth <= 6) {
                return in_array($month, array('4', '5', '6'));
            }
            else if ($cmonth <= 9) {
                return in_array($month, array('7', '8', '9'));
            }
            else {
                return in_array($month, array('10', '11', '12'));
            }
        }
        else if ($this->limit_type == '3') {
            return (($cmonth <= 6 && $month <= 6) || ($cmonth > 6 && $month > 6));
        }
        
        return true;
    }
    
    public function hasAccessToOrder($order)
    {
        return (
            $order->owner_id == $this->id || $order->old_verifier == $this->id || 
            ($order->clients_id == $this->clients_id && empty($this->parent))
        );
    }
    
    public function canSeeStats()
    {
        if (!empty($this->parent)) {
            return false;
        }
        
        return $this->getClient()->enable_stats;
    }
    
    public function hasAccessToVariant($variant)
    {
        if (!$variant instanceof Products_Model_Variant) {
            $TVariants = new Products_Model_DbTable_Variants();
            $variant = $TVariants->find($variant)->current();
        }
        $client = $this->getClient();
        $product = $variant->getProduct();
        
        if ($product->status == '1' && $variant->status == '1') {
            if (!$client->hasVariantInOffer($variant->id)) {
                return $this->hasAccessToGroup($product->groups_id);
            }
            return true;
        }
         
        if (!$client->hasAccessToVariant($variant)) {
            return false;
        }
        
        return true;
    }
    
    public function getGroupsAccess()
    {
        $TAccess = new Clients_Model_DbTable_AccountsGroupsAccess();
        return $TAccess->fetchAll(array(
            'accounts_id = ?' => $this->id
        ));
    }
    
    public function getGroupsBans()
    {
        $TAccess = new Clients_Model_DbTable_AccountsGroupsBans();
        return $TAccess->fetchAll(array(
            'accounts_id = ?' => $this->id
        ));
    }
    
    public function hasGroupBanned($id)
    {
        if (!$this->parent) {
            return false;
        }
        
        $TAccess = new Clients_Model_DbTable_AccountsGroupsBans();
        return $TAccess->fetchRow(array(
            'accounts_id = ?' => $this->id,
            'groups_id = ?' => $id
        )) !== null;
    }
    
    public function hasAccessToGroup($id)
    {
        if (!$this->parent) {
            return true;
        }
        
        $TAccess = new Clients_Model_DbTable_AccountsGroupsAccess();
        return $TAccess->fetchRow(array(
            'accounts_id = ?' => $this->id,
            'groups_id = ?' => $id
        )) !== null;
    }
    
    public function setGroupsAccess($groups)
    {
        foreach ($this->getGroupsAccess() as $g) {
            if (!in_array($g->groups_id, $groups)) {
                $g->delete();
            }
            else {
                $key = array_search($g->groups_id, $groups);
                unset($groups[$key]);
            }
        }
        
        if (!empty($groups)) {
            $TAccess = new Clients_Model_DbTable_AccountsGroupsAccess();
            
            foreach ($groups as $gid) {
                $TAccess->createRow(array(
                    'accounts_id' => $this->id,
                    'groups_id' => $gid
                ))->save();
            }
        }
    }
    
    public function setGroupsBans($groups)
    {
        foreach ($this->getGroupsBans() as $g) {
            if (!in_array($g->groups_id, $groups)) {
                $g->delete();
            }
            else {
                $key = array_search($g->groups_id, $groups);
                unset($groups[$key]);
            }
        }
        
        if (!empty($groups)) {
            $TAccess = new Clients_Model_DbTable_AccountsGroupsBans();
            
            foreach ($groups as $gid) {
                $TAccess->createRow(array(
                    'accounts_id' => $this->id,
                    'groups_id' => $gid
                ))->save();
            }
        }
    }
}