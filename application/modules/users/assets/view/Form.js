Ext.define("Module.users.view.Form", {
    extend: "Step.form.Panel",
    alias: "widget.usersform",
    
    api: {
        submit: Users.saveUser
    },
    
    initComponent: function() {
        var me = this;
        
        me.items = [{
            xtype: "hidden",
            name: "id"
        }, {
            xtype: "textfield",
            name: "login",
            fieldLabel: "Login",
            allowBlank: false,
            vtype: "alpha"
        }, {
            xtype: "textfield",
            name: "position",
            allowBlank: true,
            fieldLabel: "Stanowisko"
        }, {
            xtype: "textfield",
            name: "name",
            fieldLabel: "Imię",
            allowBlank: false
        }, {
            xtype: "textfield",
            name: "surname",
            fieldLabel: "Nazwisko",
            allowBlank: false
        }, {
            xtype: "textfield",
            name: "mail",
            fieldLabel: "Adres e-mail",
            allowBlank: false,
            vtype: "email"
        }, {
            xtype: "textfield",
            name: "phone",
            fieldLabel: "Numer telefonu",
            allowBlank: true
        }, {
            xtype: "filefield",
            name: "image",
            fieldLabel: "Zdjęcie"
        }, {
            xtype: "checkboxgroup",
            fieldLabel: "Uprawnienia",
            columns: 1,
            items: [{
                boxLabel: "Zarządzanie klientami", name: "clients", inputValue: "1", uncheckedValue: "0"
            }, {
                boxLabel: "Zarządzanie produktami", name: "products", inputValue: "1", uncheckedValue: "0"
            }, {
                boxLabel: "Zarządzanie stroną", name: "content", inputValue: "1", uncheckedValue: "0"
            }, {
                boxLabel: "Zarządzanie użytkownika", name: "users", inputValue: "1", uncheckedValue: "0"
            }]
        }];
    
        me.callParent();
    }
})