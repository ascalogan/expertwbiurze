Ext.define("Module.users.view.Users", {
    extend: "Step.grid.Panel",
    alias: "widget.userslist",
    
    title: "Lista użytkowników",
    store: "Users",
    
    initComponent: function() {
        var me = this;
        
        me.tbar = [{
            text: "dodaj użytkownika",
            iconCls: "icon-create",
            handler: me.bindAction("newitem")
        }, {
            text: "edytuj użytkownika",
            iconCls: "icon-edit",
            handler: me.bindItemAction("edit")
        }, "|", {
            text: "wygeneruj hasło",
            handler: me.bindItemAction("password")
        }];
        
        me.columns = [{
            header: "imię", dataIndex: "name", flex: 1
        }, {
            header: "nazwisko", dataIndex: "surname", flex: 1
        }, {
            header: "login", dataIndex: "login", flex: 1
        }, {
            header: "adres e-mail", dataIndex: "mail", flex: 1
        }, {
            header: "dodano", dataIndex: "created", xtype: "datecolumn", format: "Y-m-d"
        }];
    
        me.callParent();
    }
});