Ext.define("Module.users.store.Users", {
    extend: "Ext.data.Store",
    
    fields: [{
        name: "id", type: "string"
    }, {
        name: "name", type: "string"
    }, {
        name: "surname", type: "string"
    }, {
        name: "login", type: "string"
    }, {
        name: "mail", type: "string"
    }, {
        name: "created", type: "date", dateFormat: "timestamp"
    }],

    proxy: {
        type: "direct",
        api: {
            read: Users.getUsers
        },
        reader: {root: "items"}
    }
});