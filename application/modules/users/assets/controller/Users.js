Ext.define("Module.users.controller.Users", {
    extend: "Step.app.Controller",
    viewSpace: "Users",
    
    stores: [
        "Users"
    ],
    views: [
        "Users",
        "Form"
    ],
    
    init: function() {
        this.control({
            "userslist": {
                newitem: function(grid, button) {
                    this.createClient({
                        buttonId: button.el.id
                    });
                },
                edit: function(grid, item, button) {
                    this.editClient({
                        id: item.get("id"),
                        buttonId: button.el.id
                    });
                },
                password: function(grid, item) {
                    this.generatePassword(item.get("id"));
                }
            },
            "usersform": {
                submit: function(result, form) {
                    form.up("window").close();
                    this.getUsersStore().reload();
                    
                    if (result.password) {
                        Ext.Msg.alert("Wygenerowano hasło", "Hasło do nowo utworzonego konta: " + result.password);
                    }
                }
            }
        })
    },
    
    main: function(params) {
        this.render("userslist");
        this.getUsersStore().load();
    },
    
    createClient: function(params) {
        var me = this;
        
        Ext.create("Step.window.Form", {
            title: "Dodawanie nowego użytkownika",
            animateTarget: params.buttonId,
            form: {
                xtype: "usersform"
            }
        });
    },
    
    editClient: function(params) {
        var me = this;
        
        me.wait("Wczytuję dane użytkownika...");
        
        Users.getUser(params.id, function(r) {
            me.wait();
            
            Ext.create("Step.window.Form", {
                title: "Edycja użytkownika",
                animateTarget: params.buttonId,
                form: {
                    xtype: "usersform",
                    values: r.data
                }
            });
        });
    },
    
    generatePassword: function(id) {
        var me = this;
        
        me.confirm(null, "Czy na pewno chcesz wygenerować nowe hasło dla tego konta?", function() {
            me.confirm(null, "Czy chcesz wysłać do wybranego konta nowe hasło?", function(answer) {
                Users.generatePassword(id, answer == "yes" ? 1 : 0, function(result) {
                    Ext.Msg.alert("Wygenerowano hasło", "Hasło do nowo utworzonego konta: " + result.password);
                });
            }, true)
        });
    }
})