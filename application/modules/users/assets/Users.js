Ext.define("Module.users.Users", {
    extend: "Step.app.Module",
    id: "Users",
    
    controllers: [
        "Users"
    ],
    stores: [
        "Users"
    ],
    
    getActions: function() {
        if (Step.getApplication().hasAccess("users"))
        return {
            text: "Zarządzanie użytkownikami",
            icon: "products_icon.png",
            controller: "Users",
            action: "main",
            space: true
        }
    }
})