<?php

class Users_StepController extends App_Controller_Action_Step
{
    public function checkMailsAction()
    {
        $mail = new Zend_Mail_Storage_Pop3(array(
            'host' => 'expertwbiurze.pl',
            'user' => 'mailer@expertwbiurze.pl',
            'password' => 'EXPbiu2013'
        ));
        
        foreach ($mail as $msg) {
            var_dump($msg->getHeaders());
        }
    }
    
    public function getUsers()
    {
        $query = App_Db_Table::getDefaultAdapter()->select();
        
        $query  ->from('users', array('id', 'name', 'surname', 'login', 'mail', 'created'))
                ->order(array('surname', 'name'));
        
        $this->send('items', $query->query()->fetchAll());
    }
    
    public function getUser($id)
    {
        $TUsers = new Users_Model_DbTable_Users();
        $user = $TUsers->find($id)->current();
        
        $this->send('data', $user->toArray());
    }
    
    public function saveUser($data)
    {
        $TUsers = new Users_Model_DbTable_Users();
        
        $image = App_Utility_Image::getUploaded('image');
           
        if (!$TUsers->checkMail($data['mail'], $data['id'])) {
            $this->sendErrors(array('mail' => 'Podany adres e-mail jest już przypisany do innego konta'));
        }
        else if (!$TUsers->checkLogin($data['login'], $data['id'])) {
            $this->sendErrors(array('login' => 'Podany login jest już przypisany do innego konta'));
        }
        else {
            if (empty($data['id'])) {
                $password = App_Util_String::randomPassword();
                $data['password'] = md5($password);
                
                $this->send('password', $password);
                
                $mail = new App_Mailer();
                $mail->addTo($data['mail'], $data['name'] . ' ' . $data['surname']);
                $mail->setSubject('Dane dostępowe do administracji www.expertwbiurze.pl');
                
                $body = "Otrzymałeś tą wiadomość, ponieważ zostało dla Ciebie utworzone konta administracyjne w systemie expertwbiurze.pl\n\n";
                $body .= "Twój login: {$data['login']}\n";
                $body .= "Twoje hasło: {$password}";
                
                $mail->send($body);
            }
            
            if ($image) {
                $upl = new App_Uploader('image', 'admins');
                $upl->upload();
                $data['image'] = $upl->getUniqueName();
            }
            
            $this->_saveRow($TUsers, $data);
        }
    }
    
    public function generatePassword($id, $send)
    {
        $password = App_Util_String::randomPassword();
        $this->send('password', $password);
        
        $TUsers = new Users_Model_DbTable_Users();
        $row = $TUsers->find($id)->current();
        
        $row->password = md5($password);
        $row->save();
        
        if ($send) {
            $mail = new App_Mailer();
            $mail->addTo($row->mail, $row->name . ' ' . $row->surname);
            $mail->setSubject('Dane dostępowe do administracji www.expertwbiurze.pl');

            $body = "Otrzymałeś tą wiadomość, ponieważ zostało dla Ciebie wygenerowane nowe hasło do konta administracyjnego w systemie expertwbiurze.pl\n\n";
            $body .= "Twój login: {$row->login}\n";
            $body .= "Twoje hasło: {$password}";

            $mail->send($body);
        }
    }
}