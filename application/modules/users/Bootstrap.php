<?php

class Users_Bootstrap extends App_Application_Module_Bootstrap
{
    
    public function getDirectMethods()
    {
        return array(
            'Step.Application' => array(
                'getUsers' => array('len' => 1),
                'getUser' => array('len' => 1),
                'saveUser' => array('len' => 1, 'formHandler' => true),
                'generatePassword' => array('len' => 2)
            )
        );
    }
    
    
}