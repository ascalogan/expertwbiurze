<?php

class Users_Model_DbTable_Users extends App_Db_Table
{
    protected $_name = 'users';
    
    protected $_primary = 'id';
    
    public function checkMail($mail, $id)
    {
        $row = $this->fetchRow(array('mail = ?' => $mail));
        
        return !$row || $row->id == $id;
    }
    
    public function checkLogin($login, $id)
    {
        $row = $this->fetchRow(array('login = ?' => $login));
        
        return !$row || $row->id == $id;
    }
}