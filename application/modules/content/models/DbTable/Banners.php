<?php

class Content_Model_DbTable_Banners extends App_Db_Table
{
    protected $_name = 'banners';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Content_Model_Banner';
}