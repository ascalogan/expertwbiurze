<?php

class Content_Model_DbTable_Sliders extends App_Db_Table
{
    protected $_name = 'sliders';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Content_Model_Slider';
}