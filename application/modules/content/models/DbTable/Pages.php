<?php

class Content_Model_DbTable_Pages extends App_Db_Table
{
    protected $_name = 'pages';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Content_Model_Page';
    
    public function checkAlias($alias, $id = null)
    {
        $row = $this->fetchRow(array('alias = ?' => $alias));
        return !$row || $row->id == $id;
    }
}