<?php

class Content_Model_Slider extends App_Db_Table_Row
{
    public function delete()
    {
        @unlink('sliders/' . $this->image);
        @unlink('sliders/mini/' . $this->image);
        
        return parent::delete();
    }
}