<?php

class Content_Model_Banner extends App_Db_Table_Row
{
    public function delete()
    {
        @unlink('banners/' . $this->image);
        @unlink('banners/mini/' . $this->image);
        
        if ($this->file) {
            @unlink('files/' . $this->file);
        }
        
        return parent::delete();
    }
}