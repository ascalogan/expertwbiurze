<?php

class Content_Model_Page extends App_Db_Table_Row
{
    public function getMenuTitle()
    {
        $title = $this->title;
        
        if ($this->list_title) {
            $title = $this->list_title;
        }
        
        return $title;
    }
}