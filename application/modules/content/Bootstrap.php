<?php

class Content_Bootstrap extends App_Application_Module_Bootstrap
{
    public function getDirectMethods()
    {
        return array(
            'Step.Application' => array(
                'getPages' => array('len' => 0),
                'getPage' => array('len' => 1),
                'savePage' => array('len' => 1, 'formHandler' => true),
                'removePage' => array('params' => array('id')),
                'getSliders' => array('len' => 0),
                'getSlider' => array('len' => 1),
                'saveSlider' => array('len' => 1, 'formHandler' => true),
                'removeSlider' => array('params' => array('id')),
                'getBanners' => array('len' => 0),
                'getBanner' => array('len' => 1),
                'saveBanner' => array('len' => 1, 'formHandler' => true),
                'removeBanner' => array('params' => array('id')),
                'getSettings' => array('len' => 0),
                'saveSettings' => array('len' => 1, 'formHandler' => true),
                'getGroups' => array('len' => 0),
                'saveGroupsMeta' => array('len' => 1, 'formHandler' => true),
                'getLimitReports' => array('len' => 0),
                'runLimits' => array('len' => 2),
                'loadLimitReport' => array('len' => 1)
            )
        );
    }
}