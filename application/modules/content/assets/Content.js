Ext.define("Module.content.Content", {
    extend: "Step.app.Module",
    id: "Content",
    
    controllers: [
        "Content"
    ],
    
    getActions: function() {
       if (Step.getApplication().hasAccess("content"))
        return {
            text: "Zarządzanie stroną",
            icon: "shop_icon.png",
            controller: "Content",
            action: "main",
            space: true
        }
    }
});