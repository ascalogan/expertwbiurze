Ext.define("Module.content.store.Sliders", {
    extend: "Ext.data.Store",
    
    fields: [{
        name: "id", type: "int"
    }, {
        name: "image", type: "string"
    }, {
        name: "name", type: "string"
    }, {
        name: "order", type: "int"
    }, {
        name: "active", type: "string"
    }],

    proxy: {
        type: "direct",
        reader: {root: "items"},
        api: {
            read: Content.getSliders,
            destroy: Content.removeSlider
        }
    }
});