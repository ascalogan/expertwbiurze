Ext.define("Module.content.store.Pages", {
    extend: "Ext.data.Store",
    
    fields: [{
        name: "id", type: "string"
    }, {
        name: "title", type: "string"
    }, {
        name: "list_visible", type: "string"
    }, {
        name: "list_order", type: "int"
    }, {
        name: "alias", type: "string"
    }],

    proxy: {
        type: "direct",
        api: {
            read: Content.getPages
        },
        reader: {root: "items"}
    }
});