Ext.define("Module.content.store.Banners", {
    extend: "Ext.data.Store",
    
    fields: [{
        name: "id", type: "int"
    }, {
        name: "image", type: "string"
    }, {
        name: "name", type: "string"
    }, {
        name: "active", type: "string"
    }],

    proxy: {
        type: "direct",
        api: {
            read: Content.getBanners,
            destroy: Content.removeBanner
        },
        reader: {root: "items"}
    }
})