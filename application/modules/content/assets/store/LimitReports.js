Ext.define("Module.content.store.LimitReports", {
    extend: "Ext.data.Store",
    
    fields: [{
        name: "id", type: "int"
    }, {
        name: 'created_at', type: 'string'
    }],

    proxy: {
        type: "direct",
        api: {
            read: Content.getLimitReports
        },
        reader: {root: "items"}
    }
});