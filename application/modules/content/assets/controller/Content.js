Ext.define("Module.content.controller.Content", {
    extend: "Step.app.Controller",
    viewSpace: "Content",
    
    views: [
        "Sliders",
        "SliderForm",
        "Banners",
        "BannerForm",
        "Pages",
        "PageForm",
        "Settings",
        'GroupsMeta',
        'LimitReports'
    ],
    
    stores: [
        "Sliders",
        "Banners",
        "Pages",
        'LimitReports'
    ],
    
    init: function() {
        this.control({
            "#content-tabpanel": {
                bannerslist: function(tabs, view) {
                    view.getStore().load();
                },
                pageslist: function(tabs, view) {
                    view.getStore().load();
                },
                sliderslist: function(tabs, view) {
                    view.getStore().load();
                },
                contentsettings: function(tabs, view) {
                    view.load();
                },
                limitreports: function(tabs, view) {
                    view.getStore().load();
                }
            },
            "bannerslist": {
                newitem: function(view, button) {
                    this.createBanner({
                        buttonId: button.el.id
                    });
                },
                edit: function(view, item, button) {
                    this.editBanner({
                        id: item.get("id"),
                        buttonId: button.el.id
                    });
                }
            },
            "bannerform": {
                submit: function(result, form) {
                    this.getActiveView().getStore().reload();
                    form.up("window").close();
                }
            },
            "pageslist": {
                newitem: function(view, button) {
                    this.createPage({
                        buttonId: button.el.id
                    });
                },
                edit: function(view, item, button) {
                    this.editPage({
                        id: item.get("id"),
                        buttonId: button.el.id
                    });
                }
            },
            "pageform": {
                submit: function(result, form) {
                    this.getActiveView().getStore().reload();
                    form.up("window").close();
                }
            },
            "sliderslist": {
                newitem: function(view, button) {
                    this.createSlider({
                        buttonId: button.el.id
                    });
                },
                edit: function(view, item, button) {
                    this.editSlider({
                        id: item.get("id"),
                        buttonId: button.el.id
                    });
                }
            },
            "sliderform": {
                submit: function(result, form) {
                    this.getActiveView().getStore().reload();
                    form.up("window").close();
                }
            },
            'limitreports': {
                run: function(view) {
                    if (confirm('Czy na pewno chcesz uruchomić dokładanie limitów?')) {
                        this.runLimits(view.getStore(), false);
                    }
                },
                rundry: function(view) {
                    this.runLimits(view.getStore(), true);
                },
                itemdblclick: function(view, item) {
                    this.loadLimitReport(item.get('id'));
                }
            }
        });
    },
    
    main: function(params) {
        var me = this;
        
        me.render({
            xtype: "steptabpanel",
            itemId: "content-tabpanel",
            title: "Zarządzanie stroną",
            activeTab: parseInt(params.tab),
            items: [{
                xtype: "pageslist"
            }, {
                xtype: "sliderslist"
            }, {
                xtype: "bannerslist"
            }, {
                xtype: 'limitreports'
            }, {
                xtype: "contentsettings"
            }, {
                xtype: 'groupsmeta'
            }]
        });
        
        if (!parseInt(params.tab)) {
            me.getPagesStore().load();
        }
    },
    
    createPage: function(params) {
        var me = this;
        
        Ext.create("Step.window.Form", {
            title: "Dodawanie nowej strony",
            animateTarget: params.buttonId,
            form: {
                xtype: "pageform"
            }
        });
    },
    
    editPage: function(params) {
        var me = this;
        
        me.wait("Trwa wczytywanie danych strony...");
        Content.getPage(params.id, function(result) {
            me.wait();
            
            Ext.create("Step.window.Form", {
                title: "Edycja strony",
                animateTarget: params.buttonId,
                form: {
                    xtype: "pageform",
                    values: result.data
                }
            });
        });
    },
    
    createSlider: function(params) {
        var me = this;
        
        Ext.create("Step.window.Form", {
            title: "Dodawanie slidera",
            animateTarget: params.buttonId,
            form: {
                xtype: "sliderform"
            }
        });
    },
    
    editSlider: function(params) {
        var me = this;
        
        me.wait("Trwa wczytywanie slidera...");
        Content.getSlider(params.id, function(result) {
            me.wait();
            
            Ext.create("Step.window.Form", {
                title: "Edycja slidera",
                animateTarget: params.buttonId,
                form: {
                    xtype: "sliderform",
                    values: result.data
                }
            });
        });
    },
    
    createBanner: function(params) {
        var me = this;
        
        Ext.create("Step.window.Form", {
            title: "Dodawanie nowego bannera",
            animateTarget: params.buttonId,
            form: {
                xtype: "bannerform"
            }
        });
    },
    
    editBanner: function(params) {
        var me = this;
        
        me.wait("Trwa wczytywanie danych bannera...");
        Content.getBanner(params.id, function(result) {
            me.wait();
            
            Ext.create("Step.window.Form", {
                title: "Edycja bannera",
                animateTarget: params.buttonId,
                form: {
                    xtype: "bannerform",
                    values: result.data
                }
            });
        });
    },

    runLimits: function(store, dry, force) {
        var me = this;

        Content.runLimits(dry, force || false, function(response) {
            if (response.requireForce) {
                if (confirm('Limity były już dokładane w tym miesiącu! Czy na pewno chcesz kontynuować?')) {
                    me.runLimits(store, false, true);
                }
            } else {
                if (!dry) {
                    store.load();
                }

                me.showLimitReport(response.content);
            }
        });
    },

    loadLimitReport: function(id) {
        var me = this;

        Content.loadLimitReport(id, function(response) {
            me.showLimitReport(response.content);
        });
    },

    showLimitReport: function(content) {
        Ext.create("Ext.window.Window", {
            items: [{
                xtype: "component",
                html: content,
                style: "background-color: #fff; padding: 10px"
            }],
            autoScroll: true,
            modal: true,
            width: 900,
            autoShow: true,
            maxHeight: Ext.getBody().getHeight() - 50
        });
    }
})