Ext.define("Module.content.view.Settings", {
    extend: "Step.form.Panel",
    alias: "widget.contentsettings",
    
    title: "Ustawienia strony",
    api: {
        submit: Content.saveSettings,
        load: Content.getSettings
    },
    hideCancelButton: true,
    
    initComponent: function() {
        var me = this;
        
        me.items = [{
            xtype: "fieldset",
            title: "Meta dane",
            defaults: {
                anchor: "90%",
                labelWidth: 200
            },
            items: [{
                xtype: "textfield",
                name: "meta_title",
                fieldLabel: "Tytuł strony"
            }, {
                xtype: "textfield",
                name: "meta_description",
                fieldLabel: "Opis strony"
            }, {
                xtype: "textfield",
                name: "meta_keywords",
                fieldLabel: "Słowa kluczowe"
            }]
        }, {
            xtype: "fieldset",
            title: "Szablon strony - menu górne",
            defaults: {
                anchor: "90%",
                labelWidth: 200
            },
            items: [{
                xtype: "combo",
                name: "site_topAbout",
                fieldLabel: "Strona `O firmie`",
                valueField: "id",
                displayField: "title",
                store: "Pages"
            }, {
                xtype: "combo",
                name: "site_topContact",
                fieldLabel: "Strona `Kontakt`",
                valueField: "id",
                displayField: "title",
                store: "Pages"
            }]
        }, {
            xtype: "fieldset",
            title: "Szablon strony - niebieski blok",
            defaults: {
                anchor: "90%",
                labelWidth: 200
            },
            items: [{
                xtype: "textfield",
                name: "site_phoneUp1",
                fieldLabel: "Numer telefonu 1."
            }, {
                xtype: "textfield",
                name: "site_phoneUp2",
                fieldLabel: "Numer telefonu 2."
            }, {
                xtype: "textareafield",
                name: "site_underPhone",
                fieldLabel: "Tekst pod telefonami",
                height: 48
            }, {
                xtype: "filefield",
                name: "site_catalogCover",
                fieldLabel: "Okładka katalogu",
                description: "Plik graficzny o wymiarach 160 x 190 pikseli"
            }, {
                xtype: "filefield",
                name: "site_catalog",
                fieldLabel: "Plik katalogu"
            }]
        }, {
            xtype: "fieldset",
            title: "Szablon strony - o firmie",
            defaults: {
                anchor: "90%",
                labelWidth: 200
            },
            items: [{
                xtype: "textfield",
                name: "site_aboutTitle",
                fieldLabel: "Duży napis"
            }, {
                xtype: "textfield",
                name: "site_aboutSlogan",
                fieldLabel: "Mały napis"
            }, {
                xtype: "textareafield",
                name: "site_aboutContent",
                fieldLabel: "Treść"
            }, {
                xtype: "combo",
                name: "site_aboutPage",
                fieldLabel: "Strona docelowa",
                valueField: "id",
                displayField: "title",
                store: "Pages"
            }]
        }, {
            xtype: "fieldset",
            title: "Szablon strony - menu stron",
            defaults: {
                anchor: "90%",
                labelWidth: 200
            },
            items: [{
                xtype: "textfield",
                name: "site_menuTitle",
                fieldLabel: "Nagłówek"
            }, {
                xtype: "textareafield",
                name: "site_menuContent",
                fieldLabel: "Treść"
            }, {
                xtype: "combo",
                name: "site_menuPage",
                fieldLabel: "Strona docelowa",
                valueField: "id",
                displayField: "title",
                store: "Pages"
            }]
        }, {
            xtype: "fieldset",
            title: "Szablon strony - dane kontaktowe",
            defaults: {
                anchor: "90%",
                labelWidth: 200
            },
            items: [{
                xtype: "textareafield",
                name: "site_contactAddress",
                fieldLabel: "Dane firmy"
            }, {
                xtype: "textareafield",
                name: "site_contactPhones",
                fieldLabel: "Dane telefoniczne"
            }, {
                xtype: "textfield",
                name: "site_contactMail",
                fieldLabel: "Adres e-mail"
            }, {
                xtype: "textfield",
                name: "site_contactFormMail",
                fieldLabel: "Formularz - e-mail",
                description: "Adres e-mail, na który mają być kierowane zapytania z formularza"
            }]
        }, {
            xtype: "fieldset",
            title: "Szablon strony - stopka",
            defaults: {
                anchor: "90%",
                labelWidth: 200
            },
            items: [{
                xtype: "textfield",
                name: "site_bottomFB",
                fieldLabel: "Adres Facebook"
            }, {
                xtype: "textfield",
                name: "site_bottomG+",
                fieldLabel: "Adres G+"
            }, {
                xtype: "textfield",
                name: "site_bottomVi",
                fieldLabel: "Adres Vimeo"
            }, {
                xtype: "textfield",
                name: "site_bottomSlogan",
                fieldLabel: "Treść w stopce"
            }]
        }];
    
        me.callParent();
    }
});