Ext.define("Module.content.view.SliderForm", {
    extend: "Step.form.Panel",
    alias: "widget.sliderform",
    
    api: {
        submit: Content.saveSlider
    },
    
    initComponent: function() {
        var me = this;
        
        me.items = [{
            xtype: "hidden",
            name: "id"
        }, {
            xtype: "checkbox",
            name: "active",
            fieldLabel: "Aktywny",
            inputValue: "1",
            uncheckedValue: "1"
        }, {
            xtype: "textfield",
            name: "name",
            fieldLabel: "Nazwa slidera",
            description: "Nazwa slidera jest używana jako tekst alternatywny"
        }, {
            xtype: "filefield",
            name: "image",
            fieldLabel: "Plik slidera",
            allowBlank: !!me.values,
            description: "Plik graficzny o wymiatach 1100 x 350 pikseli"
        }, {
            xtype: "textfield",
            name: "url",
            fieldLabel: "Adres www",
            description: "Podaj adres www, na który ma kierować slider (opcjonalnie)"
        }, {
            xtype: "checkboxfield",
            name: "external",
            fieldLabel: "Nowe okno",
            description: "Jeśli podano adres www zaznacz, aby otwierał się on w nowym oknie",
            inputValue: "1",
            uncheckedValue: "0"
        }, {
            xtype: "numberfield",
            name: "order",
            fieldLabel: "Kolejność",
            description: "Kolejność w jakiej slidery są wyświetlane na stronie",
            allowBlank: false,
            minValue: 1,
            value: 1,
            increment: 1
        }];
    
        me.callParent();
    }
});