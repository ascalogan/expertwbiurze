Ext.define("Module.content.view.Banners", {
    extend: "Step.grid.Panel",
    alias: "widget.bannerslist",
    
    store: "Banners",
    title: "Zarządzanie bannerami",
    
    initComponent: function() {
        var me = this;
        
        me.tbar = [{
            text: "dodaj banner",
            iconCls: "icon-create",
            handler: me.bindAction("newitem")
        }, {
            text: "edytuj banner",
            iconCls: "icon-edit",
            handler: me.bindItemAction("edit")
        }, {
            text: "usuń banner",
            iconCls: "icon-delete",
            handler: me.bindItemAction("delete")
        }];
            
        me.columns = [{
            header: null, dataIndex: "image", width: 150, renderer: function(v) {
                return '<img src="banners/mini/' + v + '" width=150 height=28>';
            }
        }, {
            header: "nazwa bannera", dataIndex: "name", flex: 1
        }, {
            header: "aktywny", dataIndex: "active", renderer: function(v) {
                return v == "1" ? "tak" : "nie";
            }
        }];
    
        me.callParent();
    }
});