Ext.define("Module.content.view.Sliders", {
    extend: "Step.grid.Panel",
    alias: "widget.sliderslist",
    
    title: "Zarządzanie sliderami",
    store: "Sliders",
    
    initComponent: function() {
        var me = this;
        
        me.tbar = [{
            text: "dodaj slider",
            iconCls: "icon-create",
            handler: me.bindAction("newitem")
        }, {
            text: "edytuj slider",
            iconCls: "icon-edit",
            handler: me.bindItemAction("edit")
        }, {
            text: "usuń slider",
            iconCls: "icon-delete",
            handler: me.bindItemAction("delete")
        }];
    
        me.columns = [{
            dataIndex: "image", width: 200, renderer: function(v) {
                return '<img src="sliders/mini/' + v + '" width=200>';
            }
        }, {
            dataIndex: "name", header: "nazwa", flex: 1
        }, {
            dataIndex: "active", header: "aktywny", renderer: function(v) {
                return v == "1" ? "tak" : "nie";
            }
        }, {
            dataIndex: "order", header: "kolejność"
        }];
    
        me.callParent();
    }
})