Ext.define("Module.content.view.Pages", {
    extend: "Step.grid.Panel",
    alias: "widget.pageslist",
    
    title: "Zarządzanie stronami",
    store: "Pages",
    
    initComponent: function() {
        var me = this;
        
        me.tbar = [{
            text: "dodaj stronę",
            iconCls: "icon-create",
            handler: me.bindAction("newitem")
        }, {
            text: "edytuj stronę",
            iconCls: "icon-edit",
            handler: me.bindItemAction("edit")
        }, {
            text: "usuń stronę",
            iconCls: "icon-delete",
            handler: me.bindItemAction("delete")
        }];
        
        me.columns = [{
            header: "ID", dataIndex: "id"
        }, {
            header: "tytuł strony", dataIndex: "title", flex: 1
        }, {
            header: "na liście", dataIndex: "list_visible", renderer: function(v) {
                return v == "0" ? "nie" : "tak";
            }
        }, {
            header: "kolejność", dataIndex: "list_order", renderer: function(v, m, r) {
                if (r.get("list_visible") == "0") {
                    return "-";
                }
                return v;
            }
        }];
    
        me.callParent();
    }
});