Ext.define("Module.content.view.LimitReports", {
    extend: "Step.grid.Panel",
    alias: "widget.limitreports",
    
    store: "LimitReports",
    title: "Raporty limitów",
    
    initComponent: function() {
        var me = this;
        
        me.tbar = [{
            text: "uruchom",
            iconCls: "icon-create",
            handler: me.bindAction("run")
        }, {
            text: "uruchom próbnie",
            iconCls: "icon-edit",
            handler: me.bindAction("rundry")
        }];
            
        me.columns = [{
            header: "data", dataIndex: "created_at", flex: 1
        }];
    
        me.callParent();
    }
});