Ext.define("Module.content.view.BannerForm", {
    extend: "Step.form.Panel",
    alias: "widget.bannerform",
    
    api: {
        submit: Content.saveBanner
    },
    
    initComponent: function() {
        var me = this;
        
        me.items = [{
            xtype: "hidden",
            name: "id"
        }, {
            xtype: "checkbox",
            name: "active",
            fieldLabel: "Banner aktywny",
            inputValue: "1",
            uncheckedValue: "0"
        }, {
            xtype: "textfield",
            name: "name",
            fieldLabel: "Nazwa bannera",
            allowBlank: false,
            description: "Nazwa bannera wyświetlana jest również jako alternatywny tekst"
        }, {
            xtype: "filefield",
            name: "image",
            fieldLabel: "Plik bannera",
            allowBlank: !!me.values,
            description: "Plik graficzny o wymiarach 790 x 150 pikseli"
        }, {
            xtype: "filefield",
            name: "file",
            fieldLabel: "Załącznik",
            description: "Dowolny plik, który będzie podłączony pod ten banner",
        }, {
            xtype: "textfield",
            name: "url",
            fieldLabel: "Adres www",
            description: "Adres www, na który będzie przekierowywać banner. Jeśli podano, adres www zastępuje załącznik!"
        }];
    
        me.callParent();
    }
});