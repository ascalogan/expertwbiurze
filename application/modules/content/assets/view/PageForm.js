Ext.define("Module.content.view.PageForm", {
    extend: "Step.form.Panel",
    alias: "widget.pageform",
    
    api: {
        submit: Content.savePage
    },
    
    initComponent: function() {
        var me = this;
        
        me.items = [{
            xtype: "hidden",
            name: "id"
        }, {
            xtype: "textfield",
            name: "title",
            fieldLabel: "Tytuł strony",
            allowBlank: false
        }, {
            xtype: "textareafield",
            name: "content",
            fieldLabel: "Treść",
            labelAlign: "top",
            height: 220
        }, {
            xtype: "textfield",
            name: "alias",
            fieldLabel: "Alias strony",
            allowBlank: false,
            description: "Alias pod jakim strona będzie dostępna",
            vtype: "alphanum",
        }, {
            xtype: 'textfield',
            name: 'meta_description',
            fieldLabel: 'Meta-Description (opis)',
            description: 'Opis strony tekstowej widoczny dla wyszukiwarek'
        }, {
            xtype: "checkboxfield",
            name: "list_visible",
            fieldLabel: "Wyświetlaj w menu",
            inputValue: "1",
            uncheckedValue: "0",
            description: "Zaznacz, jeśli strona ma się wyświetlać na liście stron",
            listeners: {
                change: this.onListVisibleChange,
                scope: this
            }
        }, {
            xtype: "numberfield",
            name: "list_order",
            fieldLabel: "Kolejność w menu",
            allowBlank: false,
            minValue: 1,
            value: 1,
            increment: 1,
            hidden: true
        }, {
            xtype: "textfield",
            name: "list_title",
            fieldLabel: "Tytuł w menu",
            description: "Możesz podać inny tytuł do wyświetlania w menu",
            hidden: true
        }];
        
        me.callParent();
    },
    
    onListVisibleChange: function(field, value) {
        var me = this,
            items = ["list_order", "list_title"];
        
        Ext.each(items, function(name) {
            me.getForm().findField(name)[value ? "show" : "hide"]();
        });
    }
});