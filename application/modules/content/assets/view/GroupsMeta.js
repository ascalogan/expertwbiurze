Ext.define('Module.content.view.GroupsMeta', {
    extend: 'Step.form.Panel',
    alias: 'widget.groupsmeta',
    
    title: 'Meta-tagi grup',
    hideCancelBtn: true,
    
    api: {
        submit: Content.saveGroupsMeta
    },
    
    initComponent: function() {
        var me = this;
        
        me.callParent();
        
        Content.getGroups(function(r) {
            Ext.each(r.groups, function(group) {
                me.add({
                    xtype: 'fieldset',
                    title: group.name,
                    layout: 'anchor',
                    defaults: me.defaults,
                    items: [{
                        xtype: 'textfield',
                        name: 'group[' + group.id + '][meta_title]',
                        fieldLabel: 'Meta-Title (tytuł)',
                        description: 'Tytuł widoczny w przeglądarce podczas przeglądania produktów z tej grupy',
                        value: group.meta_title
                    }, {
                        xtype: 'textfield',
                        name: 'group[' + group.id + '][meta_description]',
                        fieldLabel: 'Meta-Description (opis)',
                        description: 'Opis dla wyszukiwarek dotyczące tej grupy',
                        value: group.meta_description
                    }]
                });
            });
        });
    }
})