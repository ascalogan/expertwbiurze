<?php

class Content_SiteController extends App_Controller_Action_Site
{
    public function pageAction()
    {
        $request = $this->getRequest();
        $alias = str_replace('.html', '', $request->getParam('controller'));
        
        $TPages = new Content_Model_DbTable_Pages();
        $page = $TPages->fetchRow(array('alias = ?' => $alias));
        
        if (!$page) {
            $errors = $this->_getParam('error_handler');
 
            switch ($errors->type) {
                case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
                case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
                case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                    // 404 error -- controller or action not found
                    $this->getResponse()
                         ->setRawHeader('HTTP/1.1 404 Not Found');

                    $page = (object) array(
                        'title' => 'Nie znaleziono strony',
                        'content' => 'Przepraszamy, ale taka strona nie istnieje.'
                    );
                    break;
                default:
//                    $mail = new Zend_Mail('UTF-8');
//                    $mail->addTo('pwrobel@es-multimedia.pl');
//                    $mail->setSubject('EXPERTWBIURZE: Błąd!');
//
//                    $exception = $errors->exception;
//                    $params = print_r($this->getAllParams(), true);
//                    $mail->setBodyText("{$exception->getMessage()}\n\n{$exception->getTraceAsString()}\n\n{$params}");
//                    $mail->send(new Zend_Mail_Transport_Smtp('expertwbiurze.pl', array(
//                        'auth' => 'login',
//                        'username' => 'notify@expertwbiurze.pl',
//                        'password' => 'EXPnotBIU2013'
//                    )));
                    
                    $page = (object) array(
                        'title' => 'Wystąpił błąd',
                        'content' => 'Przepraszamy, ale podczas wykonywanej operacji wystąpił błąd.<br><br>Wiadomość o tym błędzie została już wysłana do administratora naszego serwisu.'
                    );

                    $page->content .= $errors->exception->getMessage();
                    
                    break;
            }
        }
        else {
            $this->view->headTitle = $page->title . ' - Expert w biurze';
            if ($page->meta_description) {
                $this->view->headDescription = $page->meta_description;
            }
        }
        
        $this->view->pageInfo = $page;
    }
    
    public function doSitemapAction()
    {
        $content = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
        $content .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n";

        $baseUrl = 'http://' . $_SERVER['SERVER_NAME'];
        
        $urls = array(
            array(
                'loc' => 'http://' . $_SERVER['SERVER_NAME']
            )
        );
        
        $TPages = new Content_Model_DbTable_Pages();
        foreach ($TPages->fetchAll() as $page) {
            $urls[] = array('loc' => $page->alias. '.html');
        }
        
        $TGroups = new Products_Model_DbTable_Groups();
        foreach ($TGroups->fetchAll(null, 'order') as $group) {
            $urls[] = array('loc' => $group->getUrl());
            
            $TProducts = new Products_Model_DbTable_Products();
            $select = $TProducts->select();
            $select->setIntegrityCheck(false);

            $select  ->from('products_variants', array(
                    'variants' => 'COUNT(`products_id`)'
                ))
                    ->joinInner('products', 'products_variants.products_id = products.id')
                    ->where('products.groups_id = ?', $group->id)
                    ->order(array('mark', 'name'))
                    ->where('products.status = ?', '1')
                    ->where('products_variants.status = ?', '1')
                    ->group('products_id');
            
            foreach ($TProducts->fetchAll($select) as $prod) {
                $urls[] = array('loc' => $prod->getUrl());
            }
        }
        
        foreach ($urls as $url) {
            if (substr($url['loc'], 0, 1) != '/') {
                $url['loc'] = '/' . $url['loc'];
            }
            $content .= "\t" . '<url>' . "\n";
            $content .= "\t\t" . '<loc>' . $baseUrl . $url['loc'] . '</loc>' . "\n";
            $content .= "\t" . '</url>' . "\n";
        }
        
        $content .= '</urlset>';
        
        file_put_contents('sitemap.xml', $content);
    }
}