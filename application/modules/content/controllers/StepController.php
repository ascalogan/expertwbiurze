<?php

class Content_StepController extends App_Controller_Action_Step
{
    
    public function getGroups()
    {
        $query = App_Db_Table::getDefaultAdapter()->select();
        $query  ->from('groups')
                ->order('order');
        
        $this->send('groups', $query->query()->fetchAll());
    }
    
    public function saveGroupsMeta($data)
    {
        $TGroups = new Products_Model_DbTable_Groups();
        
        foreach ($data['group'] as $id => $d) {
            $group = $TGroups->find($id)->current();
            if ($group) {
                $group->meta_title = $d['meta_title'];
                $group->meta_description = $d['meta_description'];
                $group->save();
            }
        }
    }
    
    public function getPages()
    {
        $query = App_Db_Table::getDefaultAdapter()->select();
        $query  ->from('pages')
                ->order(array('list_visible DESC', 'list_order', 'title'));
        
        $this->send('items', $query->query()->fetchAll());
    }
    
    public function getPage($id)
    {
        $TPages = new Content_Model_DbTable_Pages();
        $row = $TPages->find($id)->current();
        
        $this->send('data', $row->toArray());
    }
    
    public function savePage($data)
    {
        $TPages = new Content_Model_DbTable_Pages();
        
        if (!$TPages->checkAlias($data['alias'], $data['id'])) {
            $this->sendErrors(array('alias' => 'Ten alias jest już przypisany do innej strony.'));
        }
        else {
            $this->_saveRow($TPages, $data);
        }
    }
    
    public function getSliders()
    {
        $query = App_Db_Table::getDefaultAdapter()->select();
        $query  ->from('sliders')
                ->order(array('active DESC', 'order', 'id'));
        
        $this->send('items', $query->query()->fetchAll());
    }
    
    public function getSlider($id)
    {
        $TSliders = new Content_Model_DbTable_Sliders();
        $row = $TSliders->find($id)->current();
        
        $this->send('data', $row->toArray());
    }
    
    public function saveSlider($data)
    {
        $errors = array();
        
        if ($this->_isUploaded('image')) {
            $image = App_Utility_Image::getUploaded('image');

            if (!$image) {
                $errors['image'] = 'Wybrany plik nie jest plikiem graficznym.';
            }
            else if ($image->getWidth() != 1100 || $image->getHeight() != 350) {
                $errors['image'] = 'Plik graficzny musi mieć wymiary 1100 x 350 pikseli.';
            }
            else {
                $uploader = new App_Uploader('image', array(
                    'destination' => 'sliders',
                    'thumbs' => array(
                        array(
                            'destination' => 'sliders/mini',
                            'scale' => array('byWidth', 200)
                        )
                    )
                ));
                $uploader->upload();
                $data['image'] = $uploader->getUniqueName();
            }
        }
        
        if (!empty($errors)) {
            $this->sendErrors($errors);
        }
        else {
            $this->_saveRow('Sliders', $data);
        }
    }
    
    public function removeSlider($data)
    {
        $this->_removeRow('Sliders', $data->id);
    }
    
    public function getBanners()
    {
        $query = App_Db_Table::getDefaultAdapter()->select();
        $query  ->from('banners')
                ->order(array('active DESC', 'id DESC'));
        
        $this->send('items', $query->query()->fetchAll());
    }
    
    public function getBanner($id)
    {
        $TBanners = new Content_Model_DbTable_Banners();
        $row = $TBanners->find($id)->current();
        
        $this->send('data', $row->toArray());
    }
    
    public function saveBanner($data)
    {
        $errors = array();
        
        if ($this->_isUploaded('image')) {
            $image = App_Utility_Image::getUploaded('image');

            if (!$image) {
                $errors['image'] = 'Wybrany plik nie jest plikiem graficznym.';
            }
            else if ($image->getWidth() != 790 || $image->getHeight() != 150) {
                $errors['image'] = 'Plik graficzny musi mieć wymiary 790 x 150 pikseli.';
            }
            else {
                $uploader = new App_Uploader('image', array(
                    'destination' => 'banners',
                    'thumbs' => array(
                        array(
                            'destination' => 'banners/mini',
                            'scale' => array('byWidth', 150)
                        )
                    )
                ));
                $uploader->upload();
                $data['image'] = $uploader->getUniqueName();
            }
        }
        if (empty($errors) && $this->_isUploaded('file')) {
            $uploader = new App_Uploader('file', 'files');
            $uploader->upload();
            $data['file'] = $uploader->getUniqueName();
        }
        
        if (!empty($errors)) {
            $this->sendErrors($errors);
        }
        else {
            $this->_saveRow('Banners', $data);
        }
    }
    
    public function removeBanner($data)
    {
        $this->_removeRow('Banners', $data->id);
    }
    
    public function getSettings()
    {
        $data = array();
        
        $TSettings = new Lite_Model_DbTable_Settings();
        foreach ($TSettings->fetchAll() as $row) {
            $data[$row->name] = $row->value;
        }
        
        $this->send('data', $data);
    }
    
    public function saveSettings($data)
    {
        $errors = array();
        
        if ($this->_isUploaded('site_catalogCover')) {
            $image = App_Utility_Image::getUploaded('site_catalogCover');
            
            if (!$image) {
                $errors['site_catalogCover'] = 'Wybrany plik nie jest plikiem graficznym';
            }
            else if ($image->getWidth() != 160 || $image->getHeight() != 190) {
                $errors['site_catalogCover'] = 'Plik graficzny musi mieć wymiary 160 x 190 pikseli';
            }
            else {
                $uploader = new App_Uploader('site_catalogCover', 'files');
                $uploader->upload();
                $data['site_catalogCover'] = $uploader->getUniqueName();
            }
        }
        if ($this->_isUploaded('site_catalog')) {
            $uploader = new App_Uploader('site_catalog', 'files');
            $uploader->upload();
            $data['site_catalog'] = $uploader->getUniqueName();
        }
        
        if (!empty($errors)) {
            $this->sendErrors($errors);
        }
        else {
            foreach ($data as $name => $value) {
                $this->setting($name, $value);
            }
        }
    }

    public function getLimitReports()
    {
        $select = App_Db_Table::getDefaultAdapter()->select();

        $select
            ->from('limit_reports')
            ->columns(array('id', 'created_at'))
            ->order('created_at DESC');

        //$this->send('items', []);
        $this->send('items', $select->query()->fetchAll());
    }

    public function runLimits($dryRun, $force = false)
    {
        $response = [];
        $data = [];

        $allowed = [
            '1' => ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
            '2' => ['1', '4', '7', '10'],
            '3' => ['1', '7']
        ];

        $month = date('n');

        if (!$dryRun && !$force) {
            $select = App_Db_Table::getDefaultAdapter()->select();
            $select
                ->from('limit_reports')
                ->limit(1)
                ->where('created_at LIKE "' . date('Y-m-d') . '%"');
            $report = $select->query()->fetch();

            if ($report) {
                $this->send('requireForce', true);
                return;
            }
        }

        $MAccounts = new Clients_Model_DbTable_Accounts();

        foreach ($MAccounts->fetchAll(array('limit_type != ?' => '0'), 'clients_id') as $account) {
            $shouldBeChanged = in_array($month, $allowed[$account->limit_type]);

            if ($shouldBeChanged) {
                $item = [];
                $item['login'] = $account->login;
                $item['mail'] = $account->mail;
                $item['budget'] = $account->budget;
                $item['limit'] = $account->limit;
                $item['limit_type'] = $account->limit_type;
                $item['limit_permanent'] = $account->limit_permanent;

                //$body .=  $account->mail . ' [budżet: ' . $account->budget . ']';
                if (!$account->limit_permanent) {
                    //$body .= ' [budżet nie przechodzi]';
                    $account->budget = 0;
                }
                else {
                    //$body .= ' [budżet przechodzi]';
                }

                $account->budget = $account->budget + $account->limit;

                //$body .= ' [limit: ' . $account->limit . '] [nowy budżet: ' . ($account->budget) . ']' . "\n";

                $item['new_budget'] = $account->budget;

                if (!$dryRun) {
                    $account->save();
                }

                $data[] = $item;
            }
        }

        if (!$dryRun) {
            App_Db_Table::getDefaultAdapter()->insert('limit_reports', [
                'data' => serialize($data),
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }

        $content = $this->renderReportContent(date('Y-m-d H:i:s'), $data);

        $response['content'] = $content;

        $this->send($response);
    }

    public function loadLimitReport($id)
    {
        $report = App_Db_Table::getDefaultAdapter()->select()
            ->from('limit_reports')
            ->where('id = ?', $id)
            ->query()
            ->fetch();

        $data = unserialize($report['data']);

        $this->send([
            'content' => $this->renderReportContent($report['created_at'], $data)
        ]);
    }

    protected function renderReportContent($date, array $data)
    {
        $this->view->date = $date;
        $this->view->data = $data;

        return $this->view->render('step/limit_report.phtml');
    }
}
