Ext.define("Module.products.view.Products", {
    extend: "Step.grid.Panel",
    alias: "widget.productslist",
    
    emptyText: "Nie wybrano grupy towarowej lub wybrana grupa nie zawiera produktów",
    store: "Products",
    
    rowColumns: true,
    
    initComponent: function() {
        var me = this
        
        me.tbar = [];
        
        if (me.type == "1") {
            me.tbar.push({
                text: "dodaj produkt",
                iconCls: "icon-create",
                handler: me.bindAction("create")
            });
        }
        me.tbar.push({
            text: "edytuj produkt",
            iconCls: "icon-edit",
            handler: me.bindItemAction("edit")
        });
        me.tbar.push({
            text: "usuń produkt",
            iconCls: "icon-delete",
            handler: me.bindItemAction("delete", {
                confirmMsg: "Czy na pewno chcesz usunąć wybrany produkt?<br>Usunięcie produktu jest operacją nieodwracalną!"
            })
        });
        
        if (me.type == "1" || me.type == "2") {
            if (me.type == "1") {
                me.tbar.push({
                    text: "wycofuj produkt",
                    iconCls: "icon-deactive",
                    handler: me.bindItemAction("deactivateitem")
                });
            }
            else {
                me.tbar.push({
                    text: "przenieś do katalogu",
                    handler: me.bindItemAction("activateitem")
                });
            }
        }
        
        me.columns = [{
            header: "ID", dataIndex: "id"
        }, {
            header: "marka", dataIndex: "mark", flex: 1
        }, {
            header: "nazwa produktu", dataIndex: "name", flex: 2
        }, {
            header: "ilość", dataIndex: "variants"
        }, {
            header: "promocja", dataIndex: "promotion", renderer: function(v) {
                return v ? '<span style="color: red">' + v + '%</span>' : '';
            }
        }];
    
        if (me.type == "3") {
            me.columns.push({
                header: "handlowiec", dataIndex: "user_name", flex: 1
            });
        }
    
        me.callParent();
    }
});