Ext.define("Module.products.view.Fields", {
    extend: "Step.grid.Panel",
    alias: "widget.productsfields",
    
    title: "parametry produktu",
    store: {
        type: "productsfields"
    },
    emptyText: "Ten produkt nie posiada jeszcze żadnych parametrów",
    
    initComponent: function() {
        var me = this;
        
        me.tbar = [{
            text: "dodaj nowy parametr",
            iconCls: "icon-create",
            handler: me.bindAction("create")
        }, {
            text: "edytuj parametr",
            iconCls: "icon-edit",
            handler: me.bindItemAction("edit")
        }, {
            text: "usuń parametr",
            iconCls: "icon-delete",
            handler: me.bindItemAction("delete", {
                confirmMsg: "Czy na pewno chcesz usunąć ten parametr?"
            })
        }];
    
        me.columns = [{
            header: "nazwa pola", dataIndex: "name", flex: 2
        }, {
            header: "typ pola", dataIndex: "type", flex: 1, renderer: function(v) {
                return {color: "kolor", textfield: "pole tekstowe"}[v];
            }
        }];
    
        me.callParent();
    }
})