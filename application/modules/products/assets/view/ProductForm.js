Ext.define("Module.products.view.ProductForm", {
    extend: "Step.form.Panel",
    alias: "widget.productform",
    
    title: "dane produktu",
    hideCancelButton: true,
    
    api: {
        submit: Products.saveProduct
    },
    
    initComponent: function() {
        var me = this;
        
        me.items = [{
            xtype: "hidden",
            name: "id"
        }, {
            xtype: "hidden",
            name: "client_id",
            value: me.clientId
        }, {
            xtype: "numberfield",
            name: "promotion",
            allowBlank: false,
            value: 0,
            fieldLabel: "Promocja",
            description: "% upust na ten produkt",
            hidden: !me.values || me.values.status !== "1"
        }, {
            xtype: "filefield",
            name: "image",
            fieldLabel: "Zdjęcie produktu"
        }, {
            xtype: "image",
            anchor: "auto",
            itemId: "imagePreview",
            src: "products/mini/" + me.imageFile,
            hidden: !me.imageFile
        }, {
            xtype: "textfield",
            name: "mark",
            fieldLabel: "Marka produktu",
            allowBlank: false
        }, {
            xtype: "textfield",
            name: "name",
            fieldLabel: "Nazwa produktu",
            allowBlank: false
        }, {
            xtype: "combo",
            name: "groups_id",
            fieldLabel: "Grupa towarowa",
            allowBlank: false,
            editable: false,
            displayField: "name",
            valueField: "id",
            store: "GroupsList",
            value: me.groupId
        }, {
            xtype: "textarea",
            name: "description",
            fieldLabel: "Opis produktu",
            labelAlign: "top"
        }, {
            xtype: 'textfield',
            name: 'meta_description',
            fieldLabel: 'Meta-Description (opis)',
            description: 'Krótki opis produktu widoczny dla wyszukiwarek'
        }];
    
        me.callParent();
    }
});