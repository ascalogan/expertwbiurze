Ext.define("Module.products.view.VariantForm", {
    extend: "Step.form.Panel",
    alias: "widget.productsvariantform",
    
    requires: [
        "Step.form.field.ComboGroup"
    ],
    
    api: {
        submit: Products.saveVariant
    },
    
    initComponent: function() {
        var me = this;
        
        me.items = [{
            xtype: "hidden",
            name: "id",
            value: me.value("id")
        }, {
            xtype: "hidden",
            name: "products_id",
            value: me.productId
        }, {
            xtype: "hidden",
            name: "client_id",
            value: me.clientId
        }, {
            xtype: "textfield",
            name: "index",
            fieldLabel: "Index",
            allowBlank: me.productStatus === "2" && !me.value("id"),
            disabled: me.productStatus === "2" && !me.value("id"),
            value: me.value("index")
        }]
    
        Ext.each(me.fields, function(field) {
            if (field.type == "textfield") {
                me.items.push({
                    xtype: "textfield",
                    name: field.index,
                    fieldLabel: field.name,
                    value: me.value(field.index, true)
                });
            }
            else {
                me.items.push({
                    xtype: "combogroup",
                    name: field.index,
                    fieldLabel: field.name || "Kolor",
                    store: "Colors",
                    value: me.value(field.index, true),
                    editable: false,
                    queryMode: "local",
                    displayField: "name",
                    valueField: "file",
                    tpl: '<div class="x-boundlist-item"><img src="colors/{file}.gif"> {name}</div>'
                })
            }
        });
        
        me.items.push({
            xtype: "textfield",
            name: "price",
            fieldLabel: "Cena netto",
            allowBlank: false,
            value: me.value("price")
        })
        
        me.callParent();
    },
    
    value: function(name, isField) {
        if (!this.record) return null;
        
        if (!isField) {
            return this.record.get(name);
        }
        return this.record.get("values")[name];
    }
});