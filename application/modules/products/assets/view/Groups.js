Ext.define("Module.products.view.Groups", {
    extend: "Ext.tree.Panel",
    alias: "widget.productsgroups",
    
    title: "Grupy towarowe",
    store: "Groups",
    
    rootVisible: false,
    hideHeaders: true,
    
    initComponent: function() {
        var me = this;
        
        me.columns = [{
            xtype: "treecolumn", dataIndex: "name", flex: 1
        }];
    
        me.callParent();
    }
})