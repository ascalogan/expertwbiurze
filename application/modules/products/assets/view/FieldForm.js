Ext.define("Module.products.view.FieldForm", {
    extend: "Step.form.Panel",
    alias: "widget.productsfieldform",
    
    api: {
        submit: Products.saveField
    },
    
    initComponent: function() {
        var me = this;
        
        me.items = [{
            xtype: "hidden",
            name: "id"
        }, {
            xtype: "textfield",
            name: "name",
            allowBlank: false,
            fieldLabel: "Nazwa parametru"
        }, {
            xtype: "hiddenfield",
            name: "products_id",
            value: me.productId
        }, {
            xtype: "checkboxfield",
            name: "in_mail",
            fieldLabel: "Załącz w mailu",
            inputValue: "1",
            uncheckedValue: "0"
        }, {
            xtype: "radiogroup",
            fieldLabel: "Rodzaj parametru",
            columns: 1,
            allowBlank: false,
            disabled: !!me.record,
            items: [{
                boxLabel: "pole tekstowe", name: "type", inputValue: "textfield", checked: true
            }, {
                boxLabel: "kolor", name: "type", inputValue: "color"
            }]
        }];
    
        me.callParent();
    }
}); 