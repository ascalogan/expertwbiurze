Ext.define("Module.products.view.Variants", {
    extend: "Step.grid.Panel",
    alias: "widget.productsvariants",
    
    requires: [
        "Step.grid.plugin.Order"
    ],
    
    title: "warianty produktu",
    store: {type: "productsvariants"},
    
    viewConfig: {
        plugins: {
            ptype: "gridviewdragdrop",
            dragText: "Przeciągnij, aby ustalić kolejność"
        },
        getRowClass: function(record) {
            return record.get("status") == "2" ? "x-deleted" : "";
        }
    },

    selType: "rowmodel",

    plugins: [{
        ptype: "gridorder",
        writer: Products.saveVariantsOrder
    }, {
        ptype: "rowediting",
        clicksToEdit: 1
    }],
    
    initComponent: function() {
        var me = this;
        
        me.tbar = [{
            text: "dodaj wariant",
            iconCls: "icon-create",
            handler: me.bindAction("create")
        }, {
            text: "edytuj wariant",
            iconCls: "icon-edit",
            handler: me.bindItemAction("edit")
        }, {
            text: "usuń wariant",
            iconCls: "icon-delete",
            handler: me.bindItemAction("delete", {
                confirmMsg: "Czy na pewno chcesz usunąć ten wariant?<br>Ta operacja jest nieodwracalna."
            })
        }];
    
        if (me.productStatus == "1") {
            me.tbar.push("|", {
                text: "przenieś do aktywnych",
                itemId: "btn-do-active",
                handler: me.bindItemAction("itemactive"),
                disabled: true
            }, {
                text: "przenieś do nieaktywnych",
                itemId: "btn-do-inactive",
                iconCls: "icon-deactive",
                handler: me.bindItemAction("itemdeactive"),
                disabled: true
            });
            
            me.on("selectionchange", me.checkItemStatus, me);
        }
        
        me.columns = [];
        
        me.callParent();
    },
    
    checkItemStatus: function() {
        var item = this.getSelectedItem(),
            btnA = this.down("#btn-do-active"),
            btnD = this.down("#btn-do-inactive");
        
        if (!item) {
            btnA.disable();
            btnD.disable();
        }
        else if (item.get("status") == "1") {
            btnA.disable();
            btnD.enable();
        }
        else {
            btnA.enable();
            btnD.disable();
        }
    }
});