Ext.define("Module.products.view.Connect", {
    extend: "Step.grid.Panel",
    alias: "widget.productsconnect",
    
    requires: [
        "Module.products.widget.ProductsSearch"
    ],
    
    title: "produkty powiązane",
    
    initComponent: function() {
        var me = this;
        
        me.tbar = [{
            xtype: "productssearch",
            listeners: {
                
            }
        }];
        
        me.columns = [];
        
        me.callParent();
    }
});