Ext.define("Module.products.controller.Products", {
    extend: "Step.app.Controller",
    viewSpace: "Products",
    
    views: [
        "Groups",
        "Products",
        "ProductForm",
        "Fields",
        "FieldForm",
        "Variants",
        "VariantForm",
        "Connect"
    ],
    
    stores: [
        "Products",
        "Variants"
    ],
    
    refs: [{
        ref: "productsList", selector: "#products-main steptabpanel"
    }],
    
    init: function() {
        this.control({
            productsgroups: {
                itemdblclick: function(tree, item) {
                    this.setActiveGroup(item.get("id"), item.get("name"));
                }
            },
            "productslist": {
                create: function(grid, button) {
                    var tabs = grid.up("steptabpanel");
                    
                    if (!tabs.group_id) {
                        Ext.Msg.alert("Nie wybrano grupy", "Proszę wybrać grupę towarową z listy, aby móc dodać produkt.");
                    }
                    else {
                        this.createProduct({groupId: tabs.group_id, buttonId: button.el.id});
                    }
                },
                itemdblclick: function(grid, item) {
                    this.callAction("product", {id: item.get("id")});
                },
                edit: function(grid, item) {
                    this.callAction("product", {id: item.get("id")});
                },
                deactivateitem: function(grid, item) {
                    this.deactivateProduct(item);
                },
                activateitem: function(grid, item) {
                    this.activateProduct(item);
                },
                deleted: function(grid, id) {
                    this.removeView("product-" + id);
                }
            },
            "productform": {
                submit: function(result, form) {
                    if (result.isNew) {
                        form.up("window").close();
                        var list = this.getProductsList();
                        if (list) {
                            this.getProductsList().getActiveTab().getStore().reload();
                        }
                        this.callAction("product", {
                            id: result.data.id,
                            space: result.data.status == "1" ? this.viewSpace : "Clients",
                            clientId: result.clientId
                        });
                        return false;
                    }
                    return true;
                }
            },
            "steptabpanel": {
                productsfields: function(tabs, view) {
                    view.getStore().load({params: {id: view.productId}});
                },
                productsvariants: function(tabs, view) {
                    if (!view.hasFields) this.configureVariantsFields(view);
                    view.getStore().load({params: {id: view.productId}});
                },
                productslist: function(tabs, view) {
                    if (!Ext.isDefined(tabs.group_id)) return;
                    
                    view.getStore().load({
                        params: {group_id: tabs.group_id, type: view.type}
                    });
                }
            },
            "productsfields": {
                create: function(grid, button) {
                    this.fieldForm({
                        buttonId: button.el.id,
                        productId: grid.productId
                    });
                },
                edit: function(grid, item, button) {
                    this.fieldForm({
                        buttonId: button.el.id,
                        productId: grid.productId,
                        item: item
                    });
                },
                deleted: function(grid) {
                    grid.up("steptabpanel").down("productsvariants").hasFields = false;
                }
            },
            "productsfieldform": {
                submit: function(result, form) {
                    this.getActiveView().getStore().reload();
                    form.up("window").close();
                    
                    this.getActiveView().up("steptabpanel").down("productsvariants").hasFields = false;
                }
            },
            "productsvariants": {
                create: function(grid, button) {
                    this.variantForm({
                        buttonId: button.el.id,
                        fields: grid.fieldsItems,
                        productId: grid.productId,
                        clientId: grid.clientId,
                        productStatus: grid.productStatus
                    });
                },
                edit: function(grid, item, button) {
                    if (button) {
                        this.variantForm({
                            item: item,
                            buttonId: button.el.id,
                            fields: grid.fieldsItems,
                            productId: grid.productId
                        });
                    } else {
                        Products.updateVariantPrice(
                            item.record.get('id'),
                            item.record.get('price')
                        );

                        item.record.commit();
                    }
                },
                deleteitem: function(grid, item) {
                    this.checkVariantDeletion(grid, item.get("id"));
                },
                itemdeactive: function(grid, item) {
                    this.deactivateVariant(item);
                },
                itemactive: function(grid, item) {
                    this.activateVariant(item);
                }
            },
            "productsvariantform": {
                submit: function(result, form) {
                    this.getActiveView().getStore().reload();
                    form.up("window").close();
                }
            }
        })
    },
    
    main: function(params) {
        var me = this;
        
        me.render({
            xtype: "panel",
            title: "Zarządzanie produktami",
            itemId: "products-main",
            layout: "border",
            bodyPadding: 5,
            isMainView: true,
            items: [{
                xtype: "productssearch",
                emptyText: "wpisz index",
                searchFn: "searchProductsLikeIndex",
                listeners: {
                    choose: function(v) {
                        me.product({
                            id: v,
                            tab: 2
                        });
                    }
                },
                region: 'north'
            }, {
                xtype: "productsgroups",
                region: "west",
                width: 250,
                style: "margin-right: 10px"
            }, {
                xtype: "steptabpanel",
                title: "Lista produktów",
                activeTab: params.tab || 0,
                items: [{
                    xtype: "productslist",
                    title: "produkty katalogowe",
                    type: "1"
                }, {
                    xtype: "productslist",
                    title: "produkty wycofane",
                    type: "2"
                }, {
                    xtype: "productslist",
                    type: "3",
                    title: "produkty specjalne"
                }],
                region: "center"
            }],
            getStateParams: function() {
                var view = me.getProductsList(),
                    params = {};
            
                if (view.group_id) {
                    params.group_id = view.group_id;
                    params.title = view.title;
                }
                params.tab = view.getActiveTabIndex();
                return params;
            }
        });
        
        if (params.group_id) {
            this.setActiveGroup(params.group_id, params.title);
        }
    },
    
    createProduct: function(params) {
        Ext.create("Step.window.Form", {
            title: "Dodawanie nowego produktu",
            animateTarget: params.buttonId,
            form: {
                xtype: "productform",
                groupId: params.groupId,
                clientId: params.clientId,
                title: null
            }
        });
    },
    
    product: function(params) {
        var me = this,
            win = Ext.Msg.wait("Trwa wczytywanie danych produktu...");
        
        Products.getProduct(params.id, function(r) {
            win.close();
            
            if (r.data.status == "3" || r.data.status == "5") {
                Ext.Msg.alert("Produkt niedostępny", "Przepraszamy, ale ten produkt został usunięty z systemu.");
                
                if (!params.clientId) {
                    me.getProductsList().getActiveTab().getStore().reload();
                }
                else {
                    var view = me.getActiveView();
                    view.getStore().reload();
                }
                
                return;
            }
            
            me.render({
                xtype: "steptabpanel",
                title: "Produkt: (" + r.data.id + ") " + r.data.name,
                itemId: "product-" + r.data.id,
                closable: true,
                activeTab: params.tab || 0,
                space: params.space,
                items: [{
                    xtype: "productform",
                    values: r.data,
                    imageFile: r.data.image
                }, {
                    xtype: "productsfields",
                    productId: r.data.id
                }, {
                    xtype: "productsvariants",
                    productId: r.data.id,
                    clientId: params.clientId,
                    productStatus: r.data.status
                }]
            });
        });
    },
    
    fieldForm: function(params) {
        var me = this,
            win;
        
        win = Ext.create("Step.window.Form", {
            title: params.item ? "Edycja opcji" : "Dodawanie opcji",
            animateTarget: params.buttonId,
            form: {
                xtype: "productsfieldform",
                record: params.item,
                productId: params.productId,
                hasColorField: this.getActiveView().getStore().findExact("type", "color")
            }
        });
        
        if (params.item) {
            win.loadRecord(params.item);
        }
    },
    
    configureVariantsFields: function(view) {
        var columns = [];
        
        Products.getFields(view.productId, function(data) {
            columns.push({
                header: "index", dataIndex: "index", flex: 1
            });
            
            Ext.each(data.items, function(item) {
                columns.push({
                    header: item.name, dataIndex: "values", flex: item.type == "textfield" ? 1 : 0, renderer: function(v) {
                        if (item.type == "textfield") return v[item.index];
                        return '<img src="colors/' + v[item.index] + '.gif" style="border: 1px solid #000">';
                    }
                });
            });
            
            columns.push({
                header: "cena netto", dataIndex: "price", flex: 1, editor: {
                    xtype: 'textfield'
                }, renderer: function(v) {
                    return Ext.util.Format.currency(v, "zł", 2, true);
                }
            });
            
            view.reconfigure(null, columns);
            view.hasFields = true;
            view.fieldsItems = data.items;
        })
    },
    
    variantForm: function(params) {
        Ext.create("Step.window.Form", {
            title: params.item ? "Edycja wariantu" : "Dodawanie nowego wariantu",
            animateTarget: params.buttonId,
            form: {
                xtype: "productsvariantform",
                record: params.item,
                fields: params.fields,
                productId: params.productId,
                clientId: params.clientId,
                productStatus: params.productStatus
            }
        });
    },
    
    checkVariantDeletion: function(grid, id) {
        var me = this;
        
        me.confirm(null, "Czy na pewno chcesz usunąć ten wariant? Wszyscy klienci utracą dostęp do tego wariantu automatycznie. Ta operacja jest nieodwracalna!", function() {
            me.wait("Trwa usuwanie wariantu...");
            
            Products.checkVariantDeletion(id, function(r) {
                me.wait();
                
                if (r.isLastVariant) {
                   me.confirm(null, "Uwaga! Po usunięciu wariantu ten produkt również zostanie usunięty, ponieważ jest to jego ostatni wariant! Czy chcesz kontynuować?", function() {
                       
                   });
                }
            });
        });
    },
    
    setActiveGroup: function(id, title) {
        var view = this.getProductsList(),
            tab = view.getActiveTab();

        if (title) {
            view.setTitle(title);
        }
        view.group_id = id;

        tab.getStore().load({
            params: {group_id: id, type: tab.type}
        });
    },
    
    activateVariant: function(item) {
        var me = this, 
            view = me.getActiveView(),
            store = view.getStore();
    
        me.confirm(null, "Czy na pewno chcesz przenieść ten wariant do aktywnych?", function() {
            me.wait("Trwa przenoszenie wariantu...");
            
            Products.activateVariant(item.get("id"), function() {
                view.getSelectionModel().deselectAll();
                store.reload();
                me.wait();
            });
        });
    },
    
    deactivateVariant: function(item) {
        var me = this, 
            view = me.getActiveView(),
            store = view.getStore();
    
        me.confirm(null, "Czy na pewno chcesz przenieść ten wariant do nieaktywnych?", function() {
            me.wait("Trwa przenoszenie wariantu...");
            
            Products.deactivateVariant(item.get("id"), function() {
                view.getSelectionModel().deselectAll();
                store.reload();
                me.wait();
            });
        });
    },
    
    activateProduct: function(item) {
        var me = this,
            view = this.getProductsList().getActiveTab(),
            store = view.getStore();
    
        me.confirm(null, "Czy na pewno chcesz przenieść wybrany produkt do produktów katalogowych?", function() {
            me.wait("Trwa przenoszenie produktu...");
            
            Products.activateProduct(item.get("id"), function() {
                store.remove(item);
                me.wait();
            });
        });
    },
    
    deactivateProduct: function(item) {
        var me = this,
            view = this.getProductsList().getActiveTab(),
            store = view.getStore();
    
        me.confirm(null, "Czy na pewno chcesz wycofać produkt?", function() {
            me.wait("Trwa przenoszenie produktu...");
            
            Products.deactivateProduct(item.get("id"), function() {
                store.remove(item);
                me.wait();
            });
        });
    }
})