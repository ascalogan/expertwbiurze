Ext.define("Module.products.Products", {
    extend: "Step.app.Module",
    id: "Products",
    
    controllers: [
        "Products"
    ],
    stores: [
        "Groups",
        "GroupsList",
        "Products",
        "Fields",
        "Variants",
        "Colors"
    ],
    
    getActions: function() {
        if (Step.getApplication().hasAccess("products"))
        return {
            text: "Zarządzanie produktami",
            icon: "products_icon.png",
            controller: "Products",
            action: "main",
            space: true
        }
    }
})