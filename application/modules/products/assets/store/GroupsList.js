Ext.define("Module.products.store.GroupsList", {
    extend: "Ext.data.Store",
    
    fields: [{
        name: "id", type: "string"
    }, {
        name: "name", type: "string"
    }],

    proxy: {
        type: "direct",
        api: {
            read: Products.getGroupsList
        },
        reader: {root: "items"}
    },
    
    autoLoad: true
})