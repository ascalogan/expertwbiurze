Ext.define("Module.products.store.Products", {
    extend: "Ext.data.Store",
    alias: "store.products",
    
    fields: [{
        name: "id", type: "string"
    }, {
        name: "name", type: "string"
    }, {
        name: "mark", type: "string"
    }, {
        name: "variants", type: "int"
    }, {
        name: "user_name", type: "string"
    }, {
        name: "promotion", type: "int"
    }],

    proxy: {
        type: "direct",
        api: {
            read: Products.getProducts,
            destroy: Products.deleteProduct
        },
        reader: {root: "items"},
        paramOrder: ["group_id", "type"]
    }
})