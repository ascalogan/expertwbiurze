Ext.define("Module.products.store.Groups", {
    extend: "Ext.data.TreeStore",
    
    fields: [{
        name: "id", type: "string"
    }, {
        name: "name", type: "string"
    }],

    proxy: {
        type: "direct",
        api: {
            read: Products.getGroups
        },
        reader: {root: "items"}
    }
})