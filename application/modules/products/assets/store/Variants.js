Ext.define("Module.products.store.Variants", {
    extend: "Ext.data.Store",
    alias: "store.productsvariants",
    
    fields: [{
        name: "id", type: "int"
    }, {
        name: "index", type: "string"
    }, {
        name: "price", type: "string"
    }, {
        name: "order", type: "int"
    }, {
        name: "values", type: "auto"
    }, {
        name: "status", type: "string"
    }],

    proxy: {
        type: "direct",
        api: {
            read: Products.getVariants,
            destroy: Products.removeVariant
        },
        reader: {root: "items"},
        paramOrder: "id"
    }
});