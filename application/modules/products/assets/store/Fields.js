Ext.define("Module.products.store.Fields", {
    extend: "Ext.data.Store",
    alias: "store.productsfields",
    
    fields: [{
        name: "id", type: "int"
    }, {
        name: "name", type: "string"
    }, {
        name: "type", type: "string"
    }, {
        name: "order", type: "int"
    }],

    proxy: {
        type: "direct",
        api: {
            read: Products.getFields,
            destroy: Products.removeField
        },
        reader: {root: "items"},
        paramOrder: "id"
    }
});