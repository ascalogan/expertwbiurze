Ext.define("Module.products.store.Colors", {
    extend: "Ext.data.Store",
    //autoLoad: true,
    
    fields: [{
        name: "group", type: "string"
    }, {
        name: "name", type: "string"
    }, {
        name: "isGroup", type: "int"
    }, {
        name: "file", type: "string"
    }],

    proxy: {
        type: "direct",
        api: {
            read: Products.getColors
        },
        reader: {root: "items"}
    },
    
    autoLoad: true
});