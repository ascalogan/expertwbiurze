Ext.define("Module.products.widget.ProductsSearch", {
    extend: "Ext.container.Container",
    alias: "widget.productssearch",
    
    border: false,
    searchFn: "searchProducts",
    emptyText: "wyszukaj w katalogu",
    
    initComponent: function() {
        var me = this;
        
        me.items = [{
            xtype: "combo",
            hideTrigger: true,
            width: "80%",
            typeAhead: true,
            width: 400,
            valueField: "id",
            displayField: "name",
            minChars: 2,
            emptyText: me.emptyText,
            listConfig: {
                loadingText: "Szukam produktu",
                emptyText: "Nie znaleziono żadnego pasującego produktu"
            },
            store: {
                fields: [
                    "id", "name", "photo"
                ],
                proxy: {
                    type: "direct",
                    reader: {root: "items"},
                    api: {
                        read: Products[me.searchFn]
                    }
                }
            },
            listeners: {
                change: function(combo, value) {
                    if (parseInt(value))
                    me.fireEvent("choose", value);
                }
            }
        }];
    
        me.callParent();
    }
});