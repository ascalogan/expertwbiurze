<?php

class Products_StepController extends App_Controller_Action_Step
{
    public function getColors()
    {
        $items = array();
        $groups = array();
        $types = array();
        
        $dir = opendir('colors');
        
        while ($file = readdir($dir)) {
            if ($file == '.' || $file == '..') continue;
            
            list ($type, $name) = explode('_', substr($file, 0, -4));
            
            if (!in_array($type, $types)) {
                $groups[] = array(
                    'group' => $type,
                    'name' => $type,
                    'isGroup' => 1
                );
                $types[] = $type;
            }
            
            $items[] = array(
                'group' => $type,
                'name' => $name,
                'file' => $type . '_' . $name,
                'isGroup' => 0
            );
        }
        
        closedir($dir);
        
        $this->send('items', array_merge($items, $groups));
    }
    
    public function getGroups()
    {
        $items = array();
        $query = App_Db_Table::getDefaultAdapter()->select();
        
        $query  ->from('groups')
                ->order('order');
        
        foreach ($query->query()->fetchAll() as $item) {
            $item['leaf'] = true;
            $items[] = $item;
        }
        
        $items[] = array(
            'id' => 0,
            'leaf' => true,
            'name' => 'OFERTA SPECJALNA STARE'
        );
        
        $this->send('items', $items);
    }
    
    public function getGroupsList()
    {
        $query = App_Db_Table::getDefaultAdapter()->select();
        
        $query  ->from('groups')
                ->order('order');
        
        $this->send('items', $query->query()->fetchAll());
    }
    
    public function getProducts($group_id, $type)
    {
        $query = App_Db_Table::getDefaultAdapter()->select();
        
        
        
        if ($type == '1') {
            $query  ->from('products_variants', array(
                'variants' => 'COUNT(`products_id`)'
            ))
                ->joinInner('products', 'products_variants.products_id = products.id')
                ->where('products.groups_id = ?', $group_id)
                ->order(array('mark', 'name'))
                ->where('products.status = ?', '1')
                ->where('products_variants.status = ?', '1')
                ->group('products_id');
        }
        else if ($type == '2') {
            $query  ->from('products_variants', array(
                'variants' => 'COUNT(`products_id`)'
            ))
                ->joinInner('products', 'products_variants.products_id = products.id')
                ->where('products.groups_id = ?', $group_id)
                ->order(array('mark', 'name'))
                ->where('products.status = ?', '1')
                ->where('products_variants.status = ?', '2')
                ->group('products_id');
        }
        else {
            $query  ->from('products_variants', array(
                'variants' => 'COUNT(`products_id`)'
            ))
                ->joinInner('products', 'products_variants.products_id = products.id')
                ->joinLeft('users', 'users.id = products.user_id', array('user_name' => 'CONCAT(users.name,\' \',users.surname)'))
                ->where('products.groups_id = ?', $group_id)
                ->order(array('mark', 'products.name'))
                ->where('products.status = ?', '2')
                ->where('products_variants.status = ?', '1')
                ->group('products_id');
        }
        
        try {
            $query->query()->fetchAll();
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
        
        $this->send('items', $query->query()->fetchAll());
    }
    
    public function getProduct($id)
    {
        $TProducts = new Products_Model_DbTable_Products();
        $row = $TProducts->find($id)->current();
        
        $this->send('data', $row->toArray());
    }
    
    public function saveProduct($data)
    {
        if (empty($data['id'])) {
            $data['status'] = empty($data['client_id']) ? '1' : '2';
        }
        if (!empty($data['client_id'])) {
            $this->send('clientId', $data['client_id']);
        }
        
        $errors = array();
        
        if ($this->_isUploaded('image')) {
            $size = getimagesize($_FILES['image']['tmp_name']);
            
            if (!$size) {
                $errors['image'] = 'Wybrany plik nie jest plikiem graficznym.';
            }
            else {
                $uploader = new App_Uploader('image', array(
                    'destination' => 'products',
                    'thumbs' => array(
                        array(
                            'destination' => 'products/medium',
                            'scale' => array('bySizeLimit', 200, 130)
                        ),
                        array(
                            'destination' => 'products/mini',
                            'scale' => array('bySizeLimit', 100, 65)
                        )
                    )
                ));
                
                if ($uploader->upload()) {
                    $data['image'] = $uploader->getUniqueName();
                }
            }
        }
        
        if ($errors) {
            $this->sendErrors($errors);
        }
        else 
        $this->_saveRow('Products', $data);
    }
    
    public function deleteProduct($data)
    {
        $TProducts = new Products_Model_DbTable_Products();
        $row = $TProducts->find($data->id)->current();
        
        $adapter = $TProducts->getAdapter();
        $adapter->delete('clients_variants_promotion', array('products_id = ?' => $row->id));
        
        $indexP = Zend_Search_Lucene::open(APPLICATION_PATH . '/data/lucene/products');
        $indexA = Zend_Search_Lucene::open(APPLICATION_PATH . '/data/lucene/archive');
        
        foreach ($row->getVariants() as $variant) {
            if ($variant->status == '2' || $row->status == '2') {
                $this->_removeLuceneVariant($indexA, $variant);
            }
            else {
                $this->_removeLuceneVariant($indexP, $variant);
            }
        }
        
        $adapter->update('products_variants', array(
            'status' => '3'
        ), array('products_id = ?' => $row->id));
        
        $indexP->commit();
        $indexA->commit();
    }
    
    public function activateProduct($id)
    {
        $TProducts = new Products_Model_DbTable_Products();
        $row = $TProducts->find($id)->current();
        
        $indexP = Zend_Search_Lucene::open(APPLICATION_PATH . '/data/lucene/products');
        $indexA = Zend_Search_Lucene::open(APPLICATION_PATH . '/data/lucene/archive');
        
        foreach ($row->getVariants() as $variant) {
            if ($variant->status == '2') {
                $this->_removeLuceneVariant($indexA, $variant);
                $this->_addLuceneVariant($indexP, $variant);
            }
        }
        
        $adapter = $TProducts->getAdapter();
        $adapter->update('products_variants', array(
            'status' => '1'
        ), array('products_id = ?' => $row->id, 'status = ?' => '2'));
        
        $indexP->commit();
        $indexA->commit();
    }
    
    public function deactivateProduct($id)
    {
        $TProducts = new Products_Model_DbTable_Products();
        $row = $TProducts->find($id)->current();
        
        $indexP = Zend_Search_Lucene::open(APPLICATION_PATH . '/data/lucene/products');
        $indexA = Zend_Search_Lucene::open(APPLICATION_PATH . '/data/lucene/archive');
        
        foreach ($row->getVariants() as $variant) {
            if ($variant->status == '1') {
                $this->_removeLuceneVariant($indexP, $variant);
                $this->_addLuceneVariant($indexA, $variant);
            }
        }
        
        $adapter = $TProducts->getAdapter();
        $adapter->update('products_variants', array(
            'status' => '2'
        ), array('products_id = ?' => $row->id, 'status = ?' => '1'));
        
        $indexP->commit();
        $indexA->commit();
    }
    
    public function getProductInfo($id)
    {
        $TProducts = new Products_Model_DbTable_Products();
        $row = $TProducts->find($id)->current();
        
        $this->send('data', $row->toArray());
        $this->send('fields', $row->getFields()->toArray());
    }
    
    public function getFields($id)
    {
        $query = App_Db_Table::getDefaultAdapter()->select();
        $query  ->from('products_fields')
                ->where('products_id = ?', $id)
                ->order('order');
        
        $this->send('items', $query->query()->fetchAll());
    }
    
    public function saveField($data)
    {
        $TFields = new Products_Model_DbTable_Fields();
        
        if (empty($data['id'])) {
            $data['index'] = $TFields->createIndex($data['products_id']);
        }
        
        $this->_saveRow($TFields, $data);
    }
    
    public function removeField($data)
    {
        $this->_removeRow('Fields', $data->id);
    }
    
    public function getVariants($id)
    {
        $TVariants = new Products_Model_DbTable_Variants();
        $items = array();
        $rows = $TVariants->fetchAll(array('products_id = ?' => $id), 'order');
        
        foreach ($rows as $row) {
            $item = $row->toArray();
            $item['values'] = unserialize($item['fields']);
            
            $items[] = $item;
        }
        
        $this->send('items', $items);
    }
    
    public function saveVariant($data)
    {
        $TVariants = new Products_Model_DbTable_Variants();
        
        if (empty($data['index'])) {
            $indexNum = $this->setting('lastSpecialVariantIndex');
            $indexNum++;
            
            $data['index'] = 'EXP' . str_repeat('0', 6 - strlen($indexNum)) . $indexNum;
            $this->setting('lastSpecialVariantIndex', $indexNum);
        }
        
        if (!$TVariants->checkIndex($data['index'], $data['id'])) {
            $this->sendErrors(array(
                'index' => 'Podany indeks jest już zajety przez inny produkt.'
            ));
            return;
        }

        $values = array();
        $TFields = new Products_Model_DbTable_Fields();
        $fields = $TFields->fetchAll(array('products_id = ?' => $data['products_id']));
        
        foreach ($fields as $field) {
            $values[$field->index] = isset($data[$field->index]) ? $data[$field->index] : '';
        }
        
        $data['fields'] = serialize($values);
        $data['price'] = str_replace(',', '.', $data['price']);
        $row = $this->_saveRow($TVariants, $data);
        
        if (empty($data['id']) && !empty($data['client_id'])) {
            $TVPromotions = new Clients_Model_DbTable_VariantsPromotion();
            $TVPromotions->add($data['client_id'], $row->products_id, $row->id, $row->price);
            $this->send('clientId', $data['client_id']);
        }
        
        $product = $row->getProduct();
        
        if (empty($data['id'])) {
            if ($product->status == '2') {
                $this->_addLuceneVariant('archive', $row, true);
            }
            else {
                $this->_addLuceneVariant('products', $row, true);
            }
        }
        else {
            if ($product->status == '2' || $row->status == '2') {
                $index = Zend_Search_Lucene::open(APPLICATION_PATH . '/data/lucene/archive');
                $this->_removeLuceneVariant($index, $row);
                $this->_addLuceneVariant($index, $row, true);
            }
            else {
                $index = Zend_Search_Lucene::open(APPLICATION_PATH . '/data/lucene/products');
                $this->_removeLuceneVariant($index, $row);
                $this->_addLuceneVariant($index, $row, true);
            }
        }
    }

    public function updateVariantPrice($id, $price)
    {
        $TVariants = new Products_Model_DbTable_Variants();

        $variant = $TVariants->find($id)->current();

        if ($variant) {
            $price = str_replace(',', '.', $price);

            $variant->price = $price;
            $variant->save();
        }

        die;
    }
    
    public function saveVariantsOrder($items)
    {
        $TVariants = new Products_Model_DbTable_Variants();
        
        foreach ($items as $item) {
            $TVariants->update(array('order' => $item->order), array('id = ?' => $item->id));
        }
    }
    
    public function activateVariant($id)
    {
        $TVariants = new Products_Model_DbTable_Variants();
        $variant = $TVariants->find($id)->current();
        
        if ($variant->status == '2') {
            $variant->status = '1';
            $variant->save();
            
            $this->_removeLuceneVariant('archive', $variant, true);
            $this->_addLuceneVariant('products', $variant, true);
        }
    }
    
    public function deactivateVariant($id)
    {
        $TVariants = new Products_Model_DbTable_Variants();
        $variant = $TVariants->find($id)->current();
        
        if ($variant->status == '1') {
            $variant->status = '2';
            $variant->save();
            
            try {
                $this->_removeLuceneVariant('products', $variant, true);
                $this->_addLuceneVariant('archive', $variant, true);
            }
            catch (Exception $e) {
                die($e->getMessage());
            }
        }
    }
    
    public function removeVariant($data)
    {
        $TVariants = new Products_Model_DbTable_Variants();
        $variant = $TVariants->find($data->id)->current();
        
        if ($variant->status != '3') {
            $adapter = $TVariants->getAdapter();
            $adapter->delete('clients_variants_promotion', array('variant_id = ?' => $variant->id));
            
            $product = $variant->getProduct();
            
            if ($product->status == '2' || $variant->status == '2') {
                $this->_removeLuceneVariant('archive', $variant, true);
            }
            else {
                $this->_removeLuceneVariant('products', $variant, true);
            }
        }
    }
    
    protected function _removeLuceneVariant($index, $variant, $commit = false)
    {
        if (is_string($index)) {
            $index = Zend_Search_Lucene::open(APPLICATION_PATH . '/data/lucene/' . $index);
        }
        
        $query = new Zend_Search_Lucene_Search_Query_Term(
            new Zend_Search_Lucene_Index_Term($variant->id, 'variant_id')
        );
        
        $result = $index->find($query);

        foreach ($result as $hit) {
            $index->delete($hit->id);
        }
        
        if ($commit) {
            $index->commit();
        }
    }
    
    protected function _addLuceneVariant($index, $variant, $commit = false)
    {
        if (is_string($index)) {
            $index = Zend_Search_Lucene::open(APPLICATION_PATH . '/data/lucene/' . $index);
        }
        
        $product = $variant->getProduct();
        
        $productLabel = $product->getName();
        $normalizedLabel = $productLabel;
        $normalizedLabel = mb_strtolower($normalizedLabel, 'UTF-8');
        $normalizedLabel = str_replace(array(
            'ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ż', 'ź'
        ), array(
            'a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z'
        ), $normalizedLabel);

        $doc = new Zend_Search_Lucene_Document();

        $doc->addField(Zend_Search_Lucene_Field::keyword('index', $variant->index));
        $doc->addField(Zend_Search_Lucene_Field::keyword('variant_id', $variant->id));
        $doc->addField(Zend_Search_Lucene_Field::keyword('product_id', $product->id));
        $nField = Zend_Search_Lucene_Field::text('name', $normalizedLabel, 'utf-8');
        $nField->boost = 2.5;
        $doc->addField($nField);
        $doc->addField(Zend_Search_Lucene_Field::unIndexed('orig_name', $productLabel, 'utf-8'));
        $doc->addField(Zend_Search_Lucene_Field::unStored('description', $product->description, 'utf-8'));

        $fields = unserialize($variant->fields);
        $values = array_values($fields);

        if (!empty($values)) {
            $doc->addField(Zend_Search_Lucene_Field::unStored('details', implode(' ', $values), 'utf-8'));
        }

        $index->addDocument($doc);
        
        if ($commit) {
            $index->commit();
        }
    }
    
    public function searchProductsIndex($data)
    {
        $string = $data->query;
        
        $TVariants = new Products_Model_DbTable_Variants();
        $variant = $TVariants->byIndex($string);
        
        $items = array();
        
        if ($variant && $variant->status != '3') {
            $product = $variant->getProduct();
            
            $items[] = array('id' => $product->id, 'name' => $product->getName());
        }
        
        $this->send('items', $items);
    }

    public function searchProductsLikeIndex($data)
    {
        $string = $data->query;

        $TVariants = new Products_Model_DbTable_Variants();
        $variants = $TVariants->byIndexLike($string);

        $items = [];
        $products = [];

        if (strlen($string) >= 2) {
            foreach ($variants as $v) {
                if ($v->status == '3' || in_array($v->products_id, $products)) {
                    continue;
                }

                $products[] = $v->products_id;

                $product = $v->getProduct();
                $items[] = array('id' => $product->id, 'name' => $product->getName());
            }
        }

        $this->send('items', $items);
    }
    
    public function searchProductsArchive($data)
    {
        $index = Zend_Search_Lucene::open(APPLICATION_PATH . '/data/lucene/archive');
        $string = $data->query;

        $string = mb_strtolower($string, 'UTF-8');
        $string = str_replace(array(
            'ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ż', 'ź',
        ), array(
            'a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z'
        ), $string);
        
        $query = new Zend_Search_Lucene_Search_Query_MultiTerm();
        
        foreach (explode(' ', $string) as $term) {
            $query->addTerm(new Zend_Search_Lucene_Index_Term($term));
        }
        
        $terms = explode(' ', $string);
        foreach ($terms as $i => $term) {
            $terms[$i] = '+' . $term;
        }
        
        
        $result = $index->find(implode(' ', $terms));
        $items = array();
        $ids = array();
        
        foreach ($result as $hit) {
            if (in_array($hit->product_id, $ids)) continue;
            $ids[] = $hit->product_id;
            
            $items[] = array(
                'id' => $hit->product_id,
                'name' => $hit->orig_name
            );
        }
        
        $this->send('items', $items);
    }
    
    public function searchProducts($data)
    {
        $index = Zend_Search_Lucene::open(APPLICATION_PATH . '/data/lucene/products');
        $string = $data->query;

        $string = mb_strtolower($string, 'UTF-8');
        $string = str_replace(array(
            'ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ż', 'ź',
        ), array(
            'a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z'
        ), $string);
        
        $query = new Zend_Search_Lucene_Search_Query_MultiTerm();
        
        foreach (explode(' ', $string) as $term) {
            $query->addTerm(new Zend_Search_Lucene_Index_Term($term));
        }
        
        $terms = explode(' ', $string);
        foreach ($terms as $i => $term) {
            $terms[$i] = '+' . $term;
        }
        
        
        $result = $index->find(implode(' ', $terms));
        $items = array();
        $ids = array();
        
        foreach ($result as $hit) {
            if (in_array($hit->product_id, $ids)) continue;
            $ids[] = $hit->product_id;
            
            $items[] = array(
                'id' => $hit->product_id,
                'name' => $hit->orig_name
            );
        }
        
        $this->send('items', $items);
    }
    
    public function checkAction()
    {
        $index = Zend_Search_Lucene::open(APPLICATION_PATH . '/data/lucene/products');
        
        $result = $index->getFieldNames();
        var_dump($result);
        
        die;
    }
    
    public function luceneAction()
    {
        error_reporting(E_ALL & E_NOTICE);
        set_time_limit(0);
        
        $index = Zend_Search_Lucene::create(APPLICATION_PATH . '/data/lucene/products');
        $index2 = Zend_Search_Lucene::create(APPLICATION_PATH . '/data/lucene/archive');

        $TProducts = new Products_Model_DbTable_Products();
        $products = $TProducts->fetchAll(array());
        
        foreach ($products as $product) {
            $variants = $product->getVariants();

            $productLabel = $product->getName();
            $normalizedLabel = $productLabel;
            $normalizedLabel = mb_strtolower($normalizedLabel, 'UTF-8');
            $normalizedLabel = str_replace(array(
                'ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ż', 'ź'
            ), array(
                'a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z'
            ), $normalizedLabel);
            
            foreach ($variants as $variant) {
                if ($variant->status == '3') continue;
                
                $doc = new Zend_Search_Lucene_Document();
                
                $doc->addField(Zend_Search_Lucene_Field::keyword('index', $variant->index));
                $doc->addField(Zend_Search_Lucene_Field::keyword('variant_id', $variant->id));
                $doc->addField(Zend_Search_Lucene_Field::keyword('product_id', $product->id));
                $nField = Zend_Search_Lucene_Field::text('name', $normalizedLabel, 'utf-8');
                $nField->boost = 2.5;
                $doc->addField($nField);
                $doc->addField(Zend_Search_Lucene_Field::unIndexed('orig_name', $productLabel, 'utf-8'));
                $doc->addField(Zend_Search_Lucene_Field::unStored('description', $product->description, 'utf-8'));
                
                $fields = unserialize($variant->fields);
                $values = array_values($fields);
                
                if (!empty($values)) {
                    $doc->addField(Zend_Search_Lucene_Field::unStored('details', implode(' ', $values), 'utf-8'));
                }
                
                if ($product->status == '1' && $variant->status == '1') {
                    $index->addDocument($doc);
                }
                else {
                    $index2->addDocument($doc);
                }
            }
        }
    
        
        die ('ok');
    }
}