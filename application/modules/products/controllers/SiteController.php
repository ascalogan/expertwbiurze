<?php

class Products_SiteController extends App_Controller_Action_Site
{
    protected function _showNoProduct()
    {
        $this->forward('no-product');
    }

    public function searchByStringAction()
    {
        $needle = $this->getParam('q');


        $items = ['Lemon', 'Orange', 'Lemoniada'];

        $this->getResponse()->setHeader('Content-Type', 'application/json', true);
        $this->getResponse()->setBody(json_encode($items));

        $this->getResponse()->sendResponse();
        exit;
    }
    
    public function noProductAction()
    {
        
    }

    public function promotionsAction()
    {
        $pageQ = $this->_getParam('p');
        $page = (int) $pageQ;

        if (!empty($pageQ) && !is_numeric($pageQ)) {
            $this->redirect($this->view->url(array(), 'products.promotions') . '?p=' . $page, array('code' => 301));
        }
        if ($pageQ === '0' || $pageQ === '1') {
            $this->view->headCanonical = $this->view->url(array(), 'products.promotions');
        }

        $TProducts = new Products_Model_DbTable_Products();
        $select = $TProducts->select();
        $select->setIntegrityCheck(false);

        $select  ->from('products_variants', array(
            'variants' => 'COUNT(`products_id`)'
        ))
            ->joinInner('products', 'products_variants.products_id = products.id')
            ->order(array('mark', 'name'))
            ->where('products.status = ?', '1')
            ->where('products_variants.status = ?', '1')
            ->where('promotion > ?', 0)
            ->group('products_id');

        $paginator = Zend_Paginator::factory($select);
        $paginator->setItemCountPerPage(48);
        $paginator->setCurrentPageNumber($page);

        $this->view->products = $paginator;
    }
    
    public function groupAction()
    {
        $id = (int) $this->_getParam('id');
        
        $TGroups = new Products_Model_DbTable_Groups();
        
        $this->view->group = $group = $TGroups->find($id)->current();
        
        $pageQ = $this->_getParam('p');
        $page = (int) $pageQ;

        if (!empty($pageQ) && !is_numeric($pageQ)) {
            $this->redirect($group->getUrl() . '?p=' . $page, array('code' => 301));
        }
        if ($pageQ == '0' || $pageQ == '1') {
            $this->view->headCanonical = $group->getUrl();
        }
        
        $TProducts = new Products_Model_DbTable_Products();
        $select = $TProducts->select();
        $select->setIntegrityCheck(false);
        
        $select  ->from('products_variants'/*, array(
                'variants' => 'COUNT(`products_id`)'
            )*/)
                ->joinInner('products', 'products_variants.products_id = products.id')
                ->where('products.groups_id = ?', $id)
                ->order(array('TRIM(mark)', 'TRIM(name)'))
                ->where('products.status = ?', '1')
                ->where('products_variants.status = ?', '1')
                ->group('products_id');
        
        $paginator = Zend_Paginator::factory($select);
        $paginator->setItemCountPerPage(48);
        $paginator->setCurrentPageNumber($page);
        
        $this->view->products = $paginator;
        
        if ($group->meta_title) {
            $this->view->headTitle = $group->meta_title;
        }
        if ($group->meta_description) {
            $this->view->headDescription = $group->meta_description;
        }
    }
    
    public function specialOfferAction()
    {
        if (!$this->getUser()) {
            $this->redirect('/');
        }
        
        $user = $this->getUser();
        $TProducts = new Products_Model_DbTable_Products();
        
        $query = $TProducts->select();
        $query->setIntegrityCheck(false);
        $query  ->from(array('cvp' => 'clients_variants_promotion'), array(
            'products' => 'COUNT(`variants_id`)'
        ))
                ->joinInner('products', 'products.id = cvp.products_id', array('id', 'name', 'mark', 'image', 'status'))
                ->where('cvp.clients_id = ?', $user->clients_id)
                ->group('products_id');
        
        $paginator = Zend_Paginator::factory($query);
        $paginator->setItemCountPerPage(48);
        $paginator->setCurrentPageNumber((int) $this->_getParam('p'));
        
        $this->view->products = $paginator;
    }
    
    public function productAction()
    {
        $id = (int) $this->_getParam('id');
        $TProducts = new Products_Model_DbTable_Products();
        
        $product = $TProducts->find($id)->current();
        
        // No product or product is marked as deleted
        if (!$product) {
            $this->_showNoProduct();
            return;
        }
        
        $user = $this->getUser();
        $client = $user === null ? null : $user->getClient();
        
        // Product is not active
        if ($product->status == '2') {
            // Product is not in client special offer
            if (!$user || !$client->hasProductInOffer($id)) {
                $this->_showNoProduct();
                return;
            }    
        }
        
        // Get group promotion
        $groupPromo = 0;
        if ($product->status == '1' && $client) {
            $groupPromo = $client->getPromotionForGroup($product->groups_id);
        }
        $productPromo = $product->getPromotion();
        
        $variants = array();
        
        $allowBuy = $allowBuyAny = $user && ($product->status == '3' || $user->hasAccessToGroup($product->groups_id));
        
        foreach ($product->getVariants() as $v) {
            // Product is inactive and is not in client special offer
            if ($v->status == '2' && (!$client || !$client->hasVariantInOffer($v->id))) {
                continue;
            }
            
            $variant = $v->toArray();
            $variant['fields'] = unserialize($v->fields);
            $variant['allowBuy'] = $allowBuy;
            
            // Client has special offer for variant
            if ($client && ($special = $client->getVariantOffer($v->id))) {
                $variant['price'] = $special->price;
                $variant['isPromo'] = true;
                $variant['allowBuy'] = true;
                $allowBuyAny = true;
            }
            else if ($groupPromo) {
                $variant['price'] = number_format($v->price*((100-$groupPromo)/100), 2, '.', '');
            }
            
            if ($productPromo) {
                
                $price = number_format($v->price*((100-$productPromo)/100), 2, '.', '');
                if ($price < $variant['price']) {
                    $variant['price'] = $price;
                    $variant['isPromo'] = true;
                }
            }

            $variant['price'] = $this->view->currency($variant['price']);
            if (!isset($variant['isPromo'])) {
                $variant['isPromo'] = false;
            }
            
            $variants[$v->id] = $variant;
        }
        
        if (empty($variant)) {
            $this->_showNoProduct();
            return;
        }
        
        $this->view->product = $product;
        $this->view->fields = $product->getFields();
        $this->view->variants = $variants;
        $this->view->header = $product->status == '1' ? $product->getGroup()->name : 'Oferta specjalna';
        $this->view->allowBuy = $allowBuyAny;
        
        if ($product->groups_id) {
            $this->view->random = $TProducts->fetchAll(array(
                'groups_id = ?' => $product->groups_id,
                'id != ?' => $product->id,
                'status = ?' => '1'
            ), 'RAND()', 4);
        }
        
        $title = $product->mark . ' ' . $product->name;
        if ($product->groups_id) {
            $title .= ' - ' . $product->getGroup()->name;
        }
        $title .= ' | opinie | cena | Expert w biurze';
        $this->view->headTitle = $title;
        
        if (!$product->meta_description) {
            $this->view->headDescriptionSkip = true;
        }
        else {
            $this->view->headDescription = $product->meta_description;
        }
    }
    
    public function searchIndexAction()
    {
        $row = null;
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $string = $request->getPost('string');
            
            $TVariants = new Products_Model_DbTable_Variants();
            $row = $TVariants->fetchRow(array('`index` = ?' => $string));
            
            if ($row) {
                $user = $this->getUser();
                $product = $row->getProduct();
                
                if ($row->status == '3' || $product->status == '3' || $product->status == '5') {
                    $row = null;
                }
                else if (empty($user) && ($row->status != '1' || $product->status != '1')) {
                    $row = null;
                }
                else if ($user) {
                    if (!$user->getClient()->hasAccessToVariant($row->id)) {
                        $row = null;
                    }
                }
            }
        }
        
        if ($row) {
            $this->redirect($row->getProduct()->getUrl());
        }
    }
    
    public function searchAction()
    {
        $request = $this->getRequest();
        $string = $request->getParam('string');
        
        if (strlen($string) < 2) {
            return;
        }
        
        $string = mb_strtolower($string, 'UTF-8');
        $string = str_replace(array(
            'ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ż', 'ź',
        ), array(
            'a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z'
        ), $string);
        
        $terms = array();
        
        foreach (explode(' ', $string) as $term) {
            if (strlen($term) < 1) {
                continue;
            }
            
            $terms[] = trim($term);
        }
        
        $queryD = array();
        foreach ($terms as $term) {
            $queryD[] = '+' . $term . '~0.5';
        }

        $index = Zend_Search_Lucene::open(APPLICATION_PATH . '/data/lucene/products');
        $query = Zend_Search_Lucene_Search_QueryParser::parse(implode(' ', $queryD));
        
        try {
            $result = $index->find($query);
        }
        catch (Exception $e) {
            $query = Zend_Search_Lucene_Search_QueryParser::parse(str_replace('~0.5', '', implode(' ', $queryD)));
            
            try {
                $result = $index->find($query);
            }
            catch (Exception $e) {
                $this->view->products = array();
                return;
            }   
        }

        $TProducts = new Products_Model_DbTable_Products();
        $products_ids = array();
        $products = array();
        $status = array();
        
        foreach ($result as $hit) {
            if (!in_array($hit->product_id, $products_ids)) {
                $products_ids[] = $hit->product_id;
                $products[] = $TProducts->find($hit->product_id)->current();
            }
        }
        
        $this->view->products = $products;
    }
    
    public function addToBasketAction()
    {
        $user = $this->getUser();
        $request = $this->getRequest();
        
        if (!$user || !$request->isPost()) {
            $this->goBack();
        }
        
        $items = $request->getPost('quantity', array());
        
        if (count($items) && array_sum($items) > 0) {
            $order = $this->getOrder(true);
            $client = $user->getClient();
            
            $TVariants = new Products_Model_DbTable_Variants();
            
            foreach ($items as $id => $quantity) {
                if (!is_string($quantity) || empty($quantity)) {
                    continue;
                }
                
                $variant = $TVariants->find($id)->current();
                if ($variant === null) {
                    continue;
                }
                
                if (!$user->hasAccessToVariant($variant)) {
                    continue;
                }
                
                $price = $client->getPriceForVariant($variant);
                
                $element = $order->addElement($variant, $price, $quantity);
                $order->price_sum += $element->price_sum;
            }
 
            if ($order->price_sum) {
                $order->save();
            }
            
            $this->messages->info('Wybrane produkty zostały dodane do koszyka');

            if ($user->hasLimits() && $user->id = $order->owner_id && $order->getTotalPrice() > $user->getTotalBudget()) {
                $this->messages->error('Wartość Twojego koszyka przekroczyła posiadany budżet!');
            }
        }
        
        $this->redirect($this->url('clients.basket'));
    }
}