<?php

class Products_Bootstrap extends App_Application_Module_Bootstrap
{
    public function getDirectMethods()
    {
        return array(
            'Step.Application' => array(
                'getGroups' => array('len' => 0),
                'getGroupsList' => array('len' => 0),
                'getProducts' => array('len' => 2),
                'getProduct' => array('len' => 1),
                'getProductInfo' => array('len' => 1),
                'saveProduct' => array('len' => 1, 'formHandler' => true),
                'deleteProduct' => array('params' => array('id')),
                'activateProduct' => array('len' => 1),
                'deactivateProduct' => array('len' => 1),
                'getFields' => array('len' => 1),
                'saveField' => array('len' => 1, 'formHandler' => true),
                'removeField' => array('params' => array('id')),
                'getVariants' => array('len' => 1),
                'saveVariant' => array('len' => 1, 'formHandler' => true),
                'saveVariantsOrder' => array('len' => 1),
                'updateVariantPrice' => array('len' => 2),
                'removeVariant' => array('params' => array('id')),
                'getColors' => array('len' => 0),
                'searchProducts' => array('len' => 1),
                'searchProductsArchive' => array('len' => 1),
                'searchProductsIndex' => array('len' => 1),
                'searchProductsLikeIndex' => array('len' => 1),
                'checkVariantDeletion' => array('len' => 1),
                'deactivateVariant' => array('len' => 1),
                'activateVariant' => array('len' => 1)
            )
        );
    }
    
    public function getRoutes()
    {
        return array(
            'search' => 'szukaj',
            'search-index' => array(
                'url' => ';szukaj-index',
                'suffix' => FALSE
            ),
            'search-by-string' => array(
                'url' => 'search-string/aa.json',
                'suffix' => FALSE
            ),
            'promotions' => 'promocje',
            'group' => array(
                'url' => '(.*?),(\d+),oferta',
                'route' => 'regex',
                'map' => array(1 => 'name', 2 => 'id'),
                'reverse' => '%s,%d,oferta'
            ),
            'product' => array(
                'url' => '(.*?),(\d+),produkt',
                'route' => 'regex',
                'map' => array(1 => 'name', 2 => 'id'),
                'reverse' => '%s,%d,produkt'
            ),
            'add-to-basket' => array(
                'url' => ';add-to-basket',
                'suffix' => false
            ),
            'special-offer' => 'oferta-specjalna'
        );
    }
}