<?php

class Products_Model_Group extends App_Db_Table_Row
{
    public function getUrl()
    {
        $router = Zend_Controller_Front::getInstance()->getRouter();
        return $router->assemble(array(
            'name' => $this->clean('name'),
            'id' => $this->id
        ), 'products.group');
    }
}