<?php

class Products_Model_DbTable_Fields extends App_Db_Table
{
    protected $_name = 'products_fields';
    
    protected $_primary = 'id';
    
    public function createIndex($product_id)
    {
        
        do {
            $index = md5(time() . uniqid());
        } while ($this->fetchRow(array('`index` = ?' => $index, 'products_id = ?' => $product_id)));
       
        return $index;
    }
}