<?php

class Products_Model_DbTable_Variants extends App_Db_Table
{
    protected $_name = 'products_variants';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Products_Model_Variant';
    
    public function checkIndex($index, $id)
    {
        $row = $this->fetchRow(array('`index` = ?' => $index));
        
        return !$row || $row->id == $id;
    }
    
    public function byIndex($index)
    {
        return $this->fetchRow(array('`index` = ?' => $index));
    }

    public function byIndexLike($index)
    {
        return $this->fetchAll(array('`index` LIKE "%' . $index . '%"'), 'index');
    }
}