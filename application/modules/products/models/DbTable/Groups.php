<?php

class Products_Model_DbTable_Groups extends App_Db_Table
{
    protected $_name = 'groups';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Products_Model_Group';
}