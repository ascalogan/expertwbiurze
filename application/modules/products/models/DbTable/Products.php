<?php

class Products_Model_DbTable_Products extends App_Db_Table
{
    protected $_name = 'products';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Products_Model_Product';
}