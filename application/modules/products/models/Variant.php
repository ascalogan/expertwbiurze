<?php

class Products_Model_Variant extends App_Db_Table_Row
{
    public function parse()
    {
        if (!is_array($this->fields)) {
            $this->fields = unserialize($this->fields);
        }
    }
    
    public function getProduct()
    {
        $TProducts = new Products_Model_DbTable_Products();
        return $TProducts->find($this->products_id)->current();
    }
}