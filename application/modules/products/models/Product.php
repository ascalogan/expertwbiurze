<?php

class Products_Model_Product extends App_Db_Table_Row
{
    public function getUrl()
    {
        return $this->_url('products.product', array(
            'id' => $this->id,
            'name' => $this->clean($this->getName())
        ));
    }
    
    public function getName()
    {
        return $this->mark . ' ' . $this->name;
    }
    
    public function getGroup()
    {
        $TGroups = new Products_Model_DbTable_Groups();
        return $TGroups->find($this->groups_id)->current();
    }
    
    public function getVariants()
    {
        $TVariants = new Products_Model_DbTable_Variants();
        return $TVariants->fetchAll(array(
            'products_id = ?' => $this->id,
            'status != ?' => '3'
        ), 'order');
    }
    
    public function getFields()
    {
        $TFields = new Products_Model_DbTable_Fields();
        return $TFields->fetchAll(array(
            'products_id = ?' => $this->id
        ), 'order');
    }
    
    public function hasColor()
    {
        return $this->getColorFieldIndex() !== null;
    }
    
    public function getColorFieldIndex()
    {
        $TFields = new Products_Model_DbTable_Fields();
        $field = $TFields->fetchRow(array(
            'products_id = ?' => $this->id,
            'type = ?' => 'color'
        ));
        
        return $field ? $field->index : null;
    }
    
    public function deactivate()
    {
        if ($this->status != '1') {
            return;
        }
        
        $adapter = $this->getTable()->getAdapter();
        $adapter->update('products_variants', array(
            'status' => '2'
        ), array(
            'products_id = ?' => $this->id,
            'status = ?' => '1'
        ));
    }
    
    public function getMaxVariantPrice()
    {
        $TVariants = new Products_Model_DbTable_Variants();
        $variant = $TVariants->fetchRow(array(
            'status = ?' => '1',
            'products_id = ?' => $this->id
        ), 'price DESC');
        
        return $variant ? $variant->price : 0;
    }
    
    public function getMinVariantPrice()
    {
        $TVariants = new Products_Model_DbTable_Variants();
        $variant = $TVariants->fetchRow(array(
            'status = ?' => '1',
            'products_id = ?' => $this->id
        ), 'price');
        
        return $variant->price;
    }
    
    public function getMaxVariantPromo($promo)
    {
        $price = $this->getMaxVariantPrice();
        
        $price = number_format(((100-$promo)/100) * $price, 2, '.', '');
        return $price;
    }
    
    public function getPromotion()
    {
        return $this->promotion;
    }
}