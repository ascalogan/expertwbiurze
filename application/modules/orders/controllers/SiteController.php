<?php

class Orders_SiteController extends App_Controller_Action_Site
{
    public function confirmOrderAction()
    {
        $request = $this->getRequest();
        
        $id = $request->getParam('id');
        $hash = $request->getParam('hash');
        
        if (md5($id) == $hash) {
            $TOrders = new Orders_Model_DbTable_Orders();
            $order = $TOrders->find($id)->current();
            
            if ($order) {
                
                if ($order->status == '1') {
                    $order->status = '2';
                    $order->accepted = time();
                    $order->save();

                    $creator = $order->getCreator();

                    try {
                        $mail = new App_Mailer('html');
                        $mail->addTo($creator->mail, $creator->getName());
                        $mail->setSubject('Zamówienie #' . $order->id . ' zostało przyjęte do realizacji');
                        $mail->send(App_Mailer_Messages::orderGotCreator($order));
                    }
                    catch (Exception $e) {

                    }

                    $this->messages->info('Zamówienie zostało przyjęte do realizacji.');
                }
                
                $this->_forward('order', 'site', 'clients', array(
                    'id' => $id,
                    'hash' => $hash,
                    'sig' => md5($id . $hash)
                ));
                return;
            }
        }
        
        $this->redirect('/');
    }
}