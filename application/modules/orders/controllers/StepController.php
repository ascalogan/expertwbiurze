<?php

class Orders_StepController extends App_Controller_Action_Step
{
    public function printableAction()
    {
        if (!$this->getSession()->uid) {
            $this->_redirect('step');
        }

        $id = $this->getParam('id');

        try {
            $generator = new \Expert\Order\PDFGenerator();
            $mpdf = $generator->generate($id);

            $file = 'orders/expert_zam_' . $id . '_' . date('Ymd_Hi') . '.pdf';

            $mpdf->Output($file, 'F');
            $this->_redirect($file);
        }
        catch (Exception $e) {
            die($e->getMessage());
        }

        die;
    }

    public function getOrders($data)
    {
        $query = App_Db_Table::getDefaultAdapter()->select();
        
        $query  ->from('orders')
                ->joinLeft('clients', 'clients.id = orders.clients_id', array('client' => 'name'))
                ->joinLeft('accounts', 'accounts.id = orders.owner_id', array('user' => 'CONCAT(accounts.name,\' \',accounts.surname)'))
                ->where('orders.status = ?', $data->type);
        
        if ($data->type == '1') {
            $query  ->order('pushed');
        }
        
        if (!$this->hasAccess('clients')) {
            $clients_id = array(0);
            $TClients = new Clients_Model_DbTable_Clients();
            foreach ($TClients->fetchAll(array('users_id = ?' => $this->getSession()->uid)) as $c) {
                $clients_id[] = $c->id;
            }
            $query  ->where('orders.clients_id IN(' . implode(',', $clients_id) . ')');
        }
        
        $this->sendRemoteItems($query, $data, null, array(
            'id' => 'orders.id'
        ));
    }
    
    public function getOrderInfo($id)
    {
        $TOrders = new Orders_Model_DbTable_Orders();
        $order = $TOrders->find($id)->current();
        
        $creator = $order->getCreator();
        $verifier = $order->getVerifier();
        $oldVerifier = $order->getOldVerifier();
        
        $properties = array();
        $properties['Status'] = $order->getStatus();
        
        if (empty($order->reported)) {
            $properties['Rozpoczęto'] = date('d-m-Y', $order->created);
        }
        else  {
            $properties['Stworzono'] = date('d-m-Y', $order->reported) . ' przez ' . $creator->getName();
        }
        
        if ($order->status) {
            $properties['Złożono'] = date('d-m-Y', $order->pushed) . ' przez ' . $order->getPusher()->getName();
        }
        if ($order->status > 1) {
            $properties['Przyjęto do realizacji'] = date('d-m-Y', $order->accepted);
        }
        if ($order->status == '3') {
            $properties['Zrealizowano'] = date('d-m-Y', $order->received);
        }
        
        if (empty($order->status) && !empty($order->reported)) {
            $properties[$order->thrown ? 'Odrzucił' : 'Weryfikuje'] = $verifier->getName();
        }
        
        if ($oldVerifier) {
            $properties['Weryfikował'] = $oldVerifier->getName();
        }
        
        $this->view->properties = $properties;
        $this->view->order = $order;
        
        $response = array(
            'orderHtml' => $this->view->render('step/order.phtml'),
            'status' => $order->status
        );
        
        $this->send($response);

        return $response;
    }
    
    public function acceptOrder($id)
    {
        $TOrders = new Orders_Model_DbTable_Orders();
        $order = $TOrders->find($id)->current();
        
        if ($order->status == '1') {
            $order->status = '2';
            $order->accepted = time();
            $order->save();
            
            $creator = $order->getCreator();
            
            $mail = new App_Mailer('html');
            $mail->addTo($creator->mail, $creator->getName());
            $mail->setSubject('Zamówienie #' . $order->id . ' zostało przyjęte do realizacji');
            $mail->send(App_Mailer_Messages::orderAcceptCreator($order));
        }
    }
    
    public function realizeOrder($id)
    {
        $TOrders = new Orders_Model_DbTable_Orders();
        $order = $TOrders->find($id)->current();
        
        if ($order->status == '2') {
            $order->status = '3';
            $order->received = time();
            $order->save();
            
            $creator = $order->getCreator();
            
            $mail = new App_Mailer('html');
            $mail->addTo($creator->mail, $creator->getName());
            $mail->setSubject('Zamówienie #' . $order->id . ' zostało zrealizowane');
            $mail->send(App_Mailer_Messages::orderCompleteCreator($order));
        }
    }
}