<?php

class Orders_Bootstrap extends App_Application_Module_Bootstrap
{
    
    public function getDirectMethods()
    {
        return array(
            'Step.Application' => array(
                'getOrders' => array('len' => 1),
                'getOrderInfo' => array('len' => 1),
                'acceptOrder' => array('len' => 1),
                'realizeOrder' => array('len' => 1)
            )
        );
    }
    
    public function getRoutes()
    {
        return array(
            'confirm-order' => array(
                'url' => 'confirm-order',
                'suffix' => false
            )
        );
    }
}