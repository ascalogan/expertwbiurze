Ext.define("Module.orders.Orders", {
    extend: "Step.app.Module",
    id: "Orders",
    
    controllers: [
        "Orders"
    ],
    
    stores: [
        //"Orders"
    ],
    
    getActions: function() {
       
        return {
            text: "Zarządzanie zamówieniami",
            icon: "shop_icon.png",
            controller: "Orders",
            action: "main",
            space: true
        }
    }
})