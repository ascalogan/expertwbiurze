Ext.define("Module.orders.store.Orders", {
    extend: "Ext.data.Store",
    alias: "store.orders",
    
    fields: [{
        name: "id", type: "int"
    }, {
        name: "client", type: "string"
    }, {
        name: "user", type: "string"
    }, {
        name: "price_sum", type: "float"
    }, {
        name: "pushed", type: "date", dateFormat: "timestamp"
    }, {
        name: "accepted", type: "date", dateFormat: "timestamp"
    }, {
        name: "received", type: "date", dateFormat: "timestamp"
    }],

    remoteSort: true,
    remoteFilter: true,

    proxy: {
        type: "direct",
        api: {
            read: Orders.getOrders
        },
        reader: {root: "items"},
        simpleSortMode: true
    }
});