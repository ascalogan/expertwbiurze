Ext.define("Module.orders.controller.Orders", {
    extend: "Step.app.Controller",
    viewSpace: "Orders",
    
    stores: [
        "Orders"
    ],
    views: [
        "Orders"
    ],
    
    init: function() {
        this.control({
            "orderslist": {
                itemdblclick: function(grid, item) {
                    this.showOrder({
                        id: item.get("id")
                    });
                },
                acceptitem: function(grid, item) {
                    this.showAccept(item.get("id"));
                },
                realizeitem: function(grid, item) {
                    this.showRealize(item.get("id"));
                },
                dofilter: this.filterOrders,
                clearfilter: this.clearFilter
            },
            "steptabpanel": {
                orderslist: function(tabs, view) {
                    view.getStore().load({
                        params: {type: view.type}
                    });
                }
            }
        });
        
        Ext.util.CSS.swapStyleSheet("orders_styles", "modules/orders/assets/style.css");
    },
    
    main: function(params) {
        var me = this,
            view, tab;
        
        view = me.render({
            xtype: "steptabpanel",
            title: "Lista zamówień",
            activeTab: params.tab || 0,
            items: [{
                xtype: "orderslist",
                title: "zamówienia oczekujące",
                type: "1"
            }, {
                xtype: "orderslist",
                title: "zamówienia przyjęte",
                type: "2"
            }, {
                xtype: "orderslist",
                title: "zamówienia zrealizowane",
                type: "3"
            }]
        });
        
        tab = view.getActiveTab();
        tab.getStore().load({params: {type: tab.type}});
    },
    
    showOrder: function(params) {
        var me = this,
            win, tbar = [];
    
        win = Ext.Msg.wait("Trwa wczytywanie danych o zamówieniu...");
        Orders.getOrderInfo(params.id, function(r) {
            win.close();
            
            if (r.status === "1" && me.hasAccess("clients")) {
                tbar.push({
                    text: "przyjmij to zamówienie",
                    handler: function() {
                        me.showAccept(params.id);
                    }
                });
            }
            else if (r.status === "2" && me.hasAccess("clients")) {
                tbar.push({
                    text: "oznacz jako zrealizowane",
                    handler: function() {
                        me.showRealize(params.id);
                    }
                })
            }

            tbar.push({
                text: 'zapisz jako pdf',
                handler: function() {
                    me.printOrder(params.id);
                }
            });
            
            Ext.create("Ext.window.Window", {
                items: [{
                    xtype: "component",
                    html: r.orderHtml,
                    style: "background-color: #fff; padding: 10px"
                }],
                tbar: tbar,
                autoScroll: true,
                modal: true,
                width: 900,
                autoShow: true,
                maxHeight: Ext.getBody().getHeight() - 50
            });
        });
    },
    
    showAccept: function(id) {
        var me = this;
        
        Ext.Msg.confirm("Potwierdzenie", "Czy na pewno przyjąć wybrane zamówienie?", function(a) {
            if (a === "yes") {
                Orders.acceptOrder(id, function() {
                    me.getActiveView().getStore().reload();
                });
            }
        });
    },
    
    showRealize: function(id) {
        var me = this;
        
        Ext.Msg.confirm("Potwierdzenie", "Czy na pewno oznaczyć wybrane zamówienie jako zrealizowane?", function(a) {
            if (a === "yes") {
                Orders.realizeOrder(id, function() {
                    me.getActiveView().getStore().reload();
                });
            }
        });
    },
    
    filterOrders: function(grid) {
        var store = grid.getStore(),
            clientId = grid.down("#clientId").getValue(),
            dateFrom = grid.down("#dateFrom").getValue(),
            dateTo = grid.down("#dateTo").getValue(),
            filters = [];
    
        if (clientId) {
            filters.push({property: "orders.clients_id", value: clientId});
        }
        if (dateFrom) {
            filters.push({property: "FROM_UNIXTIME(`received`, '%Y-%m-%d')", value: Ext.Date.format(dateFrom, "Y-m-d"), type: ">="});
        }
        
        store.clearFilter(true);
        
        if (filters.length) {
            store.filter(filters);
        }
    },
    
    clearFilter: function(grid) {
        grid.getStore().clearFilter(false);
        grid.down("#clientId").setValue(null);
        grid.down("#dateFrom").setValue(null);
        grid.down("#dateTo").setValue(null);
    },

    printOrder: function(id) {
        window.location.href = '/orders/step/printable/id/' + id;
    }
});