Ext.define("Module.orders.view.Orders", {
    extend: "Step.grid.Panel",
    alias: "widget.orderslist",
    
    store: {
        type: "orders",
        pageSize: 100
    },
    
    initComponent: function() {
        var me = this;
        
        me.store = Ext.StoreMgr.lookup(me.store);
        me.store.proxy.extraParams = {type: me.type};
        
        me.columns = [{
            header: "nr", dataIndex: "id"
        }, {
            header: "nazwa klienta", dataIndex: "client", flex: 1
        }, {
            header: "złożył", dataIndex: "user", flex: 1
        }, {
            header: "wartość", dataIndex: "price_sum", renderer: function(v) {
                return Ext.util.Format.currency(v, "zł", 2, true);
            }
        }, {
            header: "data złożenia", dataIndex: "pushed", xtype: "datecolumn", format: "Y-m-d"
        }];
    
        var canManage = Step.getApplication().hasAccess("clients");
    
        if (me.type != "1") {
            me.columns.push({
                header: "data przyjęcia", dataIndex: "accepted", xtype: "datecolumn", format: "Y-m-d"
            });
        }
        if (me.type == "3") {
            me.columns.push({
                header: "data realizacji", dataIndex: "received", xtype: "datecolumn", format: "Y-m-d"
            });
        }
    
        if (me.type == "1") {
            if (canManage)
            me.tbar = [{
                text: "przyjmij zamówienie",
                handler: me.bindItemAction("acceptitem")
            }];
        }
        else if (me.type == "2") {
            if (canManage)
            me.tbar = [{
                text: "oznacz jako zrealizowane",
                handler: me.bindItemAction("realizeitem")
            }];
        }
        else {
            me.tbar = [{
                xtype: "combo",
                itemId: "clientId",
                displayField: "name",
                valueField: "id",
                store: "Clients",
                width: 200,
                emptyText: "- wybierz klienta",
                matchFieldWidth: false
            }, {
                xtype: "datefield",
                itemId: "dateFrom",
                format: "Y-m-d",
                emptyText: "zamówienia od"
            }, {
                xtype: "datefield",
                itemId: "dateTo",
                format: "Y-m0d",
                emptyText: "zamówienia do"
            }, {
                text: "filtruj zamowienia",
                handler: me.bindAction("dofilter")
            }, {
                text: "wyczyść filtr",
                handler: me.bindAction("clearfilter")
            }]
        }
        
        me.bbar = Ext.create('Ext.PagingToolbar', {
            store: me.store,
            displayInfo: true
        }),
        
        me.callParent();
    }
})