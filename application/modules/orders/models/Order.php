<?php

class Orders_Model_Order extends App_Db_Table_Row
{
    public function getElements()
    {
        $TElements = new Orders_Model_DbTable_Elements();
        
        return $TElements->fetchAll(array(
            'orders_id = ?' => $this->id
        ), 'id');
    }
    
    public function getElementsCount()
    {
        $query = $this->getTable()->getAdapter()->select();
        
        $query  ->from('orders_elements', 'SUM(`quantity`)')
                ->where('orders_id = ?', $this->id);
        
        $count = $query->query()->fetchColumn();
        return $count ? $count : 0;
    }
    
    public function getTotalPrice()
    {
        $adapter = $this->getTable()->getAdapter();
        
        $query  = $adapter->select();
        
        $query  ->from('orders_elements', 'SUM(price_sum)')
                ->where('orders_id = ?', $this->id);
        
        try {
        $price =  $query->query()->fetchColumn();
        }
        catch (Exception $e) {
            die($e->getMessage());
        }
        
        return $price ? $price : 0;
    }
    
    public function hasElements()
    {
        return $this->getElements()->count() > 0;
    }
    
    public function addElement($variant, $price, $quantity)
    {
        $TElements = new Orders_Model_DbTable_Elements();
        $element = $TElements->createRow(array(
            'orders_id' => $this->id,
            'products_id' => $variant->products_id,
            'variants_id' => $variant->id,
            'price' => $price,
            'price_sum' => $price * ((int) $quantity),
            'quantity' => $quantity,
            'created' => time()
        ));
        $element->save();
        
        return $element;
    }
    
    /**
     * 
     * @return  Clients_Model_Account
     */
    public function getCreator()
    {
        $TAccounts = new Clients_Model_DbTable_Accounts();
        return $TAccounts->find($this->owner_id)->current();
    }
    
    /**
     * 
     * @return  Clients_Model_Account
     */
    public function getPusher()
    {
        if (!$this->pusher_id) {
            return null;
        }
        
        $TAccounts = new Clients_Model_DbTable_Accounts();
        return $TAccounts->find($this->pusher_id)->current();
    }
    
    public function getVerifier()
    {
        if (!$this->verifier) {
            return null;
        }
        
        $TAccounts = new Clients_Model_DbTable_Accounts();
        return $TAccounts->find($this->verifier)->current();
    }
    
    public function getOldVerifier()
    {
        if (!$this->old_verifier) {
            return null;
        }
        
        $TAccounts = new Clients_Model_DbTable_Accounts();
        return $TAccounts->find($this->old_verifier)->current();
    }
    
    public function getStatus()
    {
        if (!$this->reported) {
            return 'nie złożone';
        }

        $list = array(
            0 => 'weryfikowane',
            1 => 'złożone',
            2 => 'realizowane',
            3 => 'zrealizowane'
        );
        
        return $list[$this->status];
    }
    
    public function reset()
    {
        $adapter = $this->getTable()->getAdapter();
        $adapter->delete('orders_elements', array('orders_id = ?' => $this->id));
        
        $this->price_sum = 0.00;
        $this->save();
    }
    
    public function delete()
    {
        $adapter = $this->getTable()->getAdapter();
        $adapter->delete('orders_elements', array('orders_id = ?' => $this->id));
        
        return parent::delete();
    }
    
    public function getClient()
    {
        $TClients = new Clients_Model_DbTable_Clients();
        return $TClients->find($this->clients_id)->current();
    }
}