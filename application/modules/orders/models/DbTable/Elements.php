<?php

class Orders_Model_DbTable_Elements extends App_Db_Table
{
    protected $_name = 'orders_elements';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Orders_Model_Element';
}