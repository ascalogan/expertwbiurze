<?php

class Orders_Model_DbTable_Orders extends App_Db_Table
{
    protected $_name = 'orders';
    
    protected $_primary = 'id';
    
    protected $_rowClass = 'Orders_Model_Order';
}