<?php

class Orders_Model_Element extends App_Db_Table_Row
{
    public function getVariant()
    {
        $TVariants = new Products_Model_DbTable_Variants();
        return $TVariants->find($this->variants_id)->current();
    }
    
    public function getProduct()
    {
        $TProducts = new Products_Model_DbTable_Products();
        return $TProducts->find($this->products_id)->current();
    }
}