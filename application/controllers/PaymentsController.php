<?php

class PaymentsController extends Zend_Controller_Action
{
    public function init()
    {
        $this->_helper->layout->disableLayout();
    }
    
    public function factureAction()
    {
        
        
        $hash = $this->_getParam('id');
        
        $MDocuments = new Model_DbTable_PaymentsDocuments();
        $doc = $MDocuments->getDocument($hash);
        
        $MPayments = new Model_DbTable_Payments();
        
        $this->view->doc = $order = $doc->getOrder();
        $this->view->invoice = $doc;
        $this->view->payment = $MPayments->fetchRow(array('order_id = ?' => $order->id), 'id DESC');
        
        if ($this->_getParam('pdf') == 'true') {
            $this->view->isPDF = true;
        }
    }
    
    public function invoiceInfoAction()
    {
        $hash = $this->_getParam('id');
        
        $MDocuments = new Model_DbTable_PaymentsDocuments();
        $doc = $MDocuments->getDocument($hash);
        
        $data = $doc->toArray();
        $data['invoiceUrl'] = 'http://' . $_SERVER['SERVER_NAME'] . '/facture/id/' . $doc->token . $doc->id;
        
        $this->_helper->json($data);
    }
    
    public function orderAction()
    {
        $hash = $this->_getParam('id');
        
        $MDocuments = new Model_DbTable_PaymentsOrders();
        $doc = $MDocuments->find($hash)->current();
        
        $this->view->doc = $doc;
        $this->view->invoice = $doc;
    }
    
    public function removeOrderAction()
    {
        $id = (int) $this->_getParam('id');
        
        $MOrders = new Model_DbTable_PaymentsOrders();
        $order = $MOrders->find($id)->current();
        
        if ($order) {
            $order->delete();
        }
        
        $this->_helper->json(array('success' => true));
    }
    
    public function doPaymentAction()
    {
        require_once 'payu/openpayu.php';
        
        OpenPayU_Configuration::setEnvironment('secure');
        OpenPayU_Configuration::setMerchantPosId('155144');
        OpenPayU_Configuration::setPosAuthKey('Lz9BQha');
        OpenPayU_Configuration::setClientId('155144');
        OpenPayU_Configuration::setClientSecret('0c85f533eb2e16329524838a776f918b');
        OpenPayU_Configuration::setSignatureKey('3efeb54cf0a6273697e7e8fee2c2dad5');
        
        $hash = $this->_getParam('id');
        
        $MDocuments = new Model_DbTable_PaymentsDocuments();
        $invoice = $MDocuments->getDocument($hash);
        $doc = $invoice->getOrder();
        
        $MPayments = new Model_DbTable_Payments();
        $payment = $MPayments->fetchRow(array('order_id = ?' => $doc->id), 'id DESC');
        
        if ($payment && $payment->status != Model_Payment::STATUS_CANCEL) {
            $this->_redirect('facture/id/' . $invoice->getHash());
        }
        
        $payment = $doc->createPayment();
        
        $products = array();
        foreach ($doc->getProducts() as $product) {
            $products[] = array('ShoppingCartItem' => array(
                'Quantity' => $product->quantity,
                'Product' => array (
                    'Name' => $product->name,
                    'UnitPrice' => array (
                        'Gross' => $product->getBrutto() * 100, 
                        'Net' => $product->price * 100, 
                        'Tax' => $product->getVat() * 100, 
                        'TaxRate' => $product->taxRate, 
                        'CurrencyCode' => 'PLN'
                    )
                )
            ));
        }

        $shoppingCart = array(
            'GrandTotal' => $doc->brutto * 100,
            'CurrencyCode' => 'PLN',
            'ShoppingCartItems' => $products
        );
        
        $myUrl = 'http://' . $_SERVER['SERVER_NAME'];
        
        $order = array (
            'MerchantPosId' => OpenPayU_Configuration::getMerchantPosId(),
            'SessionId' => $payment->token . $payment->id,
            //'OrderUrl' => $myUrl . '/payment-cancel/id/' . $->token, // is url where customer will see in myaccount, and will be able to use to back to shop.
            'OrderCreateDate' => date("c"),
            'OrderDescription' => 'Płatność za fakturę',
            'MerchantAuthorizationKey' => OpenPayU_Configuration::getPosAuthKey(),
            'OrderType' => 'VIRTUAL', // options: MATERIAL or VIRTUAL
            'ShoppingCart' => $shoppingCart,
            'InvoiceDisabled' => true
        );
        
        $OCReq = array (
            'ReqId' =>  md5(rand()),
            'CustomerIp' => $_SERVER['REMOTE_ADDR'],
            'NotifyUrl' => $myUrl . '/payment-notify', // url where payu service will send notification with order processing status changes
            'OrderCancelUrl' => $myUrl . '/payment-cancel',
            'OrderCompleteUrl' => $myUrl . '/payment-result/id/' . $invoice->token . $invoice->id,
            'Order' => $order
        );
        
        # if logged customer in eshop
        $customer = array(
            'Email' => $doc->mail,
            'FirstName' => 'A',
            'LastName' => 'A',
            'Language' => 'pl_PL',
            'Invoice' => array(
                'Street' => $doc->billing_street,
                'HouseNumber' => $doc->billing_house,
                'ApartmentNumber' => $doc->billing_apartment,
                'PostalCode' => $doc->billing_postal,
                'City' => $doc->billing_city,
                'CountryCode' => 'PL',
                'AddressType' => 'BILLING',
                'RecipientName' => $doc->billing_company,
                'TIN' => $doc->billing_nip
            )
        );
        
        //$OCReq['Customer'] = $customer;
        
        $result = OpenPayU_Order::create($OCReq);
        
        if ($result->getSuccess()) {
            $result = OpenPayU_OAuth::accessTokenByClientCredentials();
            
            $url = OpenPayu_Configuration::getSummaryUrl();
            $url .= '?sessionId=' . $payment->getHash();
            $url .= '&oauth_token=' . $result->getAccessToken();
            $url .= '&showLoginDialog=False';
            
            $this->redirect($url);
        }
        else {
            echo '<h4>Debug console</h4><pre>';
            OpenPayU_Order::printOutputConsole();
            echo '<br/><strong>ERROR:</strong><br />' . $result->getError() . ' ' . $result->getMessage() . '</pre>';
        }
        
        die;
    }
    
    public function paymentNotifyAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        
        require_once 'payu/openpayu.php';
        
        OpenPayU_Configuration::setEnvironment('secure');
        OpenPayU_Configuration::setMerchantPosId('155144');
        OpenPayU_Configuration::setPosAuthKey('Lz9BQha');
        OpenPayU_Configuration::setClientId('155144');
        OpenPayU_Configuration::setClientSecret('0c85f533eb2e16329524838a776f918b');
        OpenPayU_Configuration::setSignatureKey('3efeb54cf0a6273697e7e8fee2c2dad5');
        
        try {        
                // Processing notification received from payu service.
                // Variable $notification contains array with OrderNotificationRequest message.
                $result = OpenPayU_Order::consumeMessage($_POST['DOCUMENT'], true, false);
                $token = $result->getSessionId();
                
                write_to_file("debug.txt", "$token\n");
                
                if ($result->getMessage() == 'OrderNotifyRequest') {
                    
                    
                        $result = OpenPayU_Order::retrieve($token);
                        
                        $MPayments = new Model_DbTable_Payments();
                        $payment = $MPayments->getPayment($token);
                        
                        if (!$payment) {
                            write_to_file("debug.txt", "no token $token: \n\n " . serialize($result->getResponse()) . "\n\n");
                            return;
                        }
                        write_to_file("debug.txt", "order details: \n\n " . serialize($result->getResponse()) . "\n\n"); 
                        $response = $result->getResponse();
                        $orderData = $response['OpenPayU']['OrderDomainResponse']['OrderRetrieveResponse'];
                        $orderStatus = $orderData['OrderStatus'];
                        $paymentStatus = isset($orderData['PaymentStatus']) ? $orderData['PaymentStatus'] : null;
                        $paymentType = $orderData['PayType'];
                        $newPaymentStatus = Model_Payment::getStatusCode($paymentStatus, $orderStatus);
                        
                        $doc = $payment->getOrder();
                        
                        // Update customer data
                        if (!empty($orderData['CustomerRecord'])) {
                            $updateData = array();
                            $updateData['payment_name'] = $orderData['CustomerRecord']['FirstName'];
                            $updateData['payment_surname'] = $orderData['CustomerRecord']['LastName'];
                            $updateData['payment_mail'] = $orderData['CustomerRecord']['Email'];
                            $updateData['payment_phone'] = $orderData['CustomerRecord']['Phone'];
                            
                            $doc->setFromArray($updateData);
                        }
                        
                        // Update payment status
                        if ($payment->updateStatus($newPaymentStatus, $paymentType)) {
                            $doc->payment_status = $payment->status;
                            $doc->payment_type = $paymentType;
                            
                            if ($doc->payment_status == Model_Payment::STATUS_COMPLETE) {
                                $doc->status = '1';
                                $doc->payment_ts = time();
                                
                                $invoice = $doc->getDocument();
                                if ($invoice->invoice_type == 'pro') {
                                    $MDocuments = new Model_DbTable_PaymentsDocuments();
                                    
                                    $data = $invoice->toArray();
                                    $data['invoice_type'] = 'vat';
                                    $data['number'] = $MDocuments->getLastNumber('vat') + 1;
                                    $data['token'] = $MDocuments->generateToken();
                                    $data['created'] = time();
                                    unset($data['id']);
                                    
                                    $vat = $MDocuments->createRow($data);
                                    $vat->save();
                                }
                            }
                            
                            
                        }
                        
                        $doc->save();
                                       
                }                
        } catch (Exception $e) {
                write_to_file("debug.txt", $e->getMessage());
                write_to_file("debug.txt", OpenPayU_Order::printOutputConsole());
        }
    }
    
    public function pdfAction()
    {
        $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            
        $token = $this->_getParam('id');
        
        $MDocuments = new Model_DbTable_PaymentsDocuments();
        $doc = $MDocuments->getDocument($token);
        
        $this->view->doc = $order = $doc->getOrder();
        $this->view->invoice = $doc;
        $this->view->isPDF = true;
        
        $pdf = new App_Pdf(Zend_Pdf_Page::SIZE_A4);
        $root = $pdf->getRoot();
        
        $invoiceLabel = 'FAKTURA ';
        $invoiceLabel .= $doc->invoice_type == 'pro' ? 'proforma' : 'VAT';
        $invoiceLabel .= ' nr ' . $doc->number . '/ON';
        
        // Set CSS
        $pdf->addCss('sideLabel', array('bold' => true, 'marginBottom' => 10));
        // Add fonts
        $pdf->addFont('Verdanaaa', 'verdana.ttf');
        $pdf->addFont('Verdana-Bolda', 'verdanab.ttf');
        
        $root->setStyle(array(
            'fontSize' => 10,
            'lineHeight' => 14,
            'fontName' => 'Verdana'
        ));
        
        $root->addElement(
            $root->block(array(
                $root->image(APPLICATION_PATH . '/../public/images/logo.jpg', null, array('width' => 230, 'height' => 70)),
                $root->block(null, array('border' => true, 'margin' => '15 0'))
            ))
        );
        $root->addElement(
            $root->hbox(array(
                $root->node($invoiceLabel, array('bold' => true, 'fontSize' => 16), array('width' => '55%')),
                $root->block(array(
                    $root->node('Miejscowość: Gliwice'),
                    $root->node('Data wystawienia: ' . $order->date_create),
                    $root->node('Data sprzedaży: ' . $order->date_sell)
                ))
            ), null)
        );
        $root->addElement('oryginał');
        $root->addElement(
            $root->hbox(array(
                // Sprzedawca
                $root->block(array(
                    $root->node('Sprzedawca', null, array('class' => 'sideLabel')),
                    'es multimedia Anna Kłosowska',
                    'ul. Jasna 14A/47, 44-100 Gliwice',
                    'tel. 032 270 69 16 fax. 032 750 01 50',
                    $root->hbox(array(
                        $root->node('Bank:', null, array('width' => 80)),
                        $root->node('mBank')
                    )),
                    $root->hbox(array(
                        $root->node('Number konta:', null, array('width' => 80)),
                        $root->node('21 1140 2004 0000 3102 3356 8564')
                    )),
                    $root->hbox(array(
                        $root->node('NIP:', null, array('width' => 80)),
                        $root->node('631-232-40-23', array('bold' => true))
                    ), array('marginTop' => 14)),
                    $root->hbox(array(
                        $root->node('Regon:', null, array('width' => 80)),
                        $root->node('278153567')
                    ))
                ), null, array('width' => '55%')),
                // Nabywca
                $root->block(array(
                    $root->node('Nabywca', null, array('class' => 'sideLabel')),
                    $order->billing_company,
                    'ul. ' . $order->billing_street . ' ' . $order->billing_house . ', ' . 
                    $order->billing_postal . ' ' . $order->billing_city
                ))
            ), array('marginTop' => 20))
        );
        
        $data = array();
        foreach ($doc->getProducts() as $i => $product) {
            $data[] = array(
                $i + 1,
                array('content' => $product->name, 'style' => array('textAlign' => 'left')),
                'szt.',
                $product->quantity,
                $product->price,
                $product->netto_total,
                $product->taxRate,
                $product->vat_total,
                $product->brutto_total
            );
        }
        
        $data[] = array(
            'style' => array('bold' => true),
            'elements' => array(
                array(
                    'content' => 'Razem:', 'colspan' => 5, 'style' => array('border' => false, 'textAlign' => 'right')
                ),
                $order->netto,
                '23',
                $order->tax,
                $order->brutto
            )
        );
        $data[] = array(
            'style' => array('bold' => true, 'marginTop' => 10),
            'elements' => array(
                array('content' => '', 'colspan' => 2, 'style' => array('border' => false)),
                array('content' => 'Razem do zapłaty:', 'colspan' => 3, 'style' => array('border' => false, 'textAlign' => 'left')),
                array('content' => $order->brutto . ' zł', 'colspan' => 4, 'style' => array('border' => false, 'textAlign' => 'right'))
            )
        );
        $data[] = array(
            'style' => array('bold' => true, 'marginTop' => 10),
            'elements' => array(
                array('content' => '', 'colspan' => 2, 'style' => array('border' => false)),
                array('content' => 'Słownie:', 'colspan' => 3, 'style' => array('border' => false, 'textAlign' => 'left')),
                array('content' => AmountInWords::getZl($order->brutto) . ' złotych ' . round(AmountInWords::getGr($order->brutto)) . '/100', 'colspan' => 4, 'style' => array('border' => false, 'textAlign' => 'right'))
            )
        );
        $data[] = array(
            'style' => array('marginTop' => 10),
            'elements' => array(
                array('content' => '', 'colspan' => 2, 'style' => array('border' => false)),
                array('content' => 'Forma płatności:', 'colspan' => 3, 'style' => array('border' => false, 'textAlign' => 'left', 'bold' => true)),
                array('content' => 'przelew', 'colspan' => 4, 'style' => array('border' => false, 'textAlign' => 'left'))
            )
        );
        $data[] = array(
            'elements' => array(
                array('content' => '', 'colspan' => 2, 'style' => array('border' => false)),
                array('content' => 'Termin płatności:', 'colspan' => 3, 'style' => array('border' => false, 'textAlign' => 'left', 'bold' => true)),
                array('content' => $order->date_payline, 'colspan' => 4, 'style' => array('border' => false, 'textAlign' => 'left'))
            )
        );
        
        $root->addElement(
            $root->table(array(
                array('header' => 'Lp.', 'width' => 30),
                array('header' => 'Nazwa', 'flex' => 5),
                array('header' => 'Jm', 'width' => 30),
                array('header' => 'Ilosc', 'width' => 30),
                array('header' => 'Cena netto'),
                array('header' => 'Wartość netto'),
                array('header' => 'VAT%', 'width' => 30),
                array('header' => 'VAT'),
                array('header' => 'Wartość brutto')
            ), $data, 
                array('textAlign' => 'center', 'marginTop' => 20), 
                array(
                    'cellStyle' => array('border' => true, 'padding' => '4 3'),
                    'headerStyle' => array('bold' => true, 'border' => true, 'padding' => '4 3')
                )
            )
        );
        
        require_once 'phpqrcode/qrlib.php';
        
        // QR CODE
        $url = 'http://' . $_SERVER['SERVER_NAME'] . '/facture/id/' . $token;
        $path = APPLICATION_PATH . '/data/qrs/' . $token . '.png';
        
        QRcode::png('Faktura on-line:' . "\n" . $url, $path);
        
        $root->addElement(
            $root->image(APPLICATION_PATH . '/data/qrs/' . $token . '.png')
        );
        
        $pdfData = $pdf->draw();
        $response = $this->getResponse();
        //die;
        $response->setHeader('Content-Type', 'application/x-pdf', true);
        $response->setHeader('Content-Disposition', 'attachment; filename=Faktura.pdf', true);
        $response->setBody($pdfData);
        
        return;
        /**
        require_once 'tcpdf/tcpdf.php';
        $pdf = new TCPDF();
        $pdf->SetFont('dejavusans', '', 10);
        $pdf->AddPage();
        $pdf->writeHTML(str_replace("\n", '', $this->view->render('payments/facture.phtml')), false, false, false, false, '');
        
        $pdf->Output('lele.pdf', 'D');
        exit;
        
        return;*/
        include 'dompdf/dompdf_config.inc.php';
        require_once 'dompdf/include/canvas.cls.php';
        require_once 'dompdf/include/dompdf.cls.php';
        require_once 'dompdf/include/stylesheet.cls.php';
        require_once 'dompdf/include/frame_tree.cls.php';
        require_once 'dompdf/include/frame.cls.php';
        require_once 'dompdf/include/style.cls.php';
        require_once 'dompdf/include/css_color.cls.php';
        require_once 'dompdf/include/attribute_translator.cls.php';
        require_once 'dompdf/include/canvas_factory.cls.php';
        require_once 'dompdf/include/cpdf_adapter.cls.php';
        
        require_once 'dompdf/include/font_metrics.cls.php';
        
        $pdf = new DOMPDF();
        $pdf->set_paper('A4');

        $pdf->load_html(iconv('UTF-8', 'WINDOWS-1250', $this->view->render('payments/facture.phtml')));
        $pdf->render();
        
        $pdf->stream('lala.pdf');
        exit;
    }
    
    public function createAction()
    {
        $response = array('success' => false);
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $data = $request->getPost();
            
            if (!empty($data['sig']) && !empty($data['ts']) && md5($data['ts']) == $data['sig']) {
                $products = unserialize(stripslashes($data['products']));
                
                $netto_total = 0;
                $brutto_total = 0;
                $tax_total = 0;

                foreach ($products as $i => $product) {
                    $product = (array) $product;

                    $amount = $product['quantity'];
                    $price = $product['price'];
                    $tax = $product['taxRate'];

                    $netto_total += ($price * $amount);
                    $tax_value = number_format(($price * ($tax/100)), 2);
                    $tax_total += ($tax_value * $amount);
                    $brutto_value = $price + $tax_value;
                    $brutto_total += ($brutto_value * $amount);

                    $product['brutto_total'] = number_format($brutto_value * $amount, 2);
                    $product['netto_total'] = number_format($price * $amount, 2);
                    $product['vat_total'] = number_format($tax_value * $amount, 2);
                    $product['price'] = number_format($price, 2);
                    
                    if (empty($product['data'])) {
                        $product['data'] = array();
                    }
                    $product['data'] = serialize($product['data']);

                    $products[$i] = $product;
                }
                
                /**
                 * Create order
                 */
                if (empty($data['date_sell'])) {
                    $data['date_sell'] = date('d-m-Y');
                }
                if (empty($data['date_create'])) {
                    $data['date_create'] = date('d-m-Y');
                }
                if (empty($data['date_payline'])) {
                    $data['date_payline'] = date('d-m-Y');
                }
                
                $MOrders = new Model_DbTable_PaymentsOrders();
                $values = array(
                    'type' => $data['type'],
                    'client_id' => $data['client_id'],
                    'date_sell' => $data['date_sell'],
                    'date_create' => $data['date_create'],
                    'date_payline' => $data['date_payline'],
                    'mail' => $_POST['mail'],
                    'netto' => number_format($netto_total, 2),
                    'tax' => number_format($tax_total, 2),
                    'brutto' => number_format($brutto_total, 2),
                    'created' => time()
                );
                $data['billing'] = unserialize(stripslashes($data['billing']));
                foreach ($data['billing'] as $name => $value) {
                    $values['billing_' . $name] = $value;
                }
                
                $order = $MOrders->createRow($values);
                $order->save();
                
                foreach ($products as $product) {
                    $attachments = isset($product['attachments']) ? $product['attachments'] : array();
                    $product = $order->addProduct($product);
                    
                    foreach ($attachments as $attachment) {
                        $filename = $this->_uploadAttachment($attachment['key']);
                        
                        if ($filename) {
                            $product->addAttachment($filename, $_FILES[$attachment['key']]['type'], $attachment['type']);
                        }
                    }
                }
                
                $MDocuments = new Model_DbTable_PaymentsDocuments();
                
                $values = array(
                    'token' => $MDocuments->generateToken(),
                    'invoice_type' => $data['invoice_type'],
                    'number' => $MDocuments->getLastNumber($data['invoice_type']) + 1,
                    'order_id' => $order->id,
                    'created' => time()
                );
                
                $doc = $MDocuments->createRow($values);
                $doc->save();
                
                
                
                $response['success'] = true;
                $response['docId'] = $doc->id;
                $response['token'] = $doc->token;
                $response['docUrl'] = 'http://' . $_SERVER['SERVER_NAME'] . '/facture/id/' . $doc->token . $doc->id;
            }
        }
        
        $this->_helper->json($response);
    }
    
    protected function _uploadAttachment($name)
    {
        if (empty($_FILES[$name]['tmp_name'])) return;
        
        $filename = $_FILES[$name]['name'];
        $filename = str_replace('', '-', $filename); // Replaces all spaces with hyphens.
        $filename = preg_replace('/[^A-Za-z0-9\-\.]/', '', $filename); // Removes special chars.
        $newFilename = $filename;
        
        while (file_exists('payments/attachments/' . $newFilename)) {
            $newFilename = uniqid() . '-' . $filename;
        }
        
        return move_uploaded_file($_FILES[$name]['tmp_name'], 'payments/attachments/' . $newFilename) ? $newFilename : null;
    }
}

function write_to_file($file, $data) {
        file_put_contents($file, $data, FILE_APPEND);
};