<?php

class App_Uploader_Cleaner
{
    /**
     * File name
     * @var string
     */
    protected $_file;

    /**
     * List of directories
     * @var array
     */
    protected $_directories;

    /**
     * Cleaner constructor
     *
     * @param  string $file
     * @param  array $directories
     * @return void
     */
    public function  __construct($file, array $directories)
    {
        $this->setFile($file);
        $this->setDirectories($directories);
    }

    /**
     * Get file name
     *
     * @return string
     */
    public function getFile()
    {
        return $this->_file;
    }

    /**
     * Set file name
     *
     * @param  string $file
     * @return App_Uploader_Cleaner
     */
    public function setFile($file)
    {
        $this->_file = (string) $file;
        return $this;
    }

    /**
     * Get directories
     *
     * @return array
     */
    public function getDirectories()
    {
        return $this->_directories;
    }

    /**
     * Set directories
     *
     * @param  array $directories
     * @return App_Uploader_Cleaner
     */
    public function setDirectories(array $directories)
    {
        $this->_directories = $directories;
        return $this;
    }

    /**
     * Remove files
     *
     * @return void
     */
    public function clean()
    {
        $file = $this->getFile();
        $directories = $this->getDirectories();

        if (!empty($directories)) {
            foreach ($directories as $directory) {
                if (substr($directory, -1) != '/') {
                    $directory .= '/';
                }
                @unlink ($directory . $file);
            }
        }
    }
}