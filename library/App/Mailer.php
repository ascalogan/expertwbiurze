<?php

class App_Mailer 
{
    
    protected $_login = 'shop@expertwbiurze.pl';
    
    protected $_password = '12345qwer';
    
    protected $_host = 'ssl0.ovh.net';
    
    /**
     *
     * @var Zend_Mail
     */
    protected $_mail;
    
    protected $_type = 'txt';
    
    public static function createProductsList($order)
    {
        $elements = $order->getElements();
					
        $products = "
            <p style='font-weight: bold'>
                Zamówienie nr: {$order->id}
            </p>
            <table class='listing'>
                <thead>
                    <tr>
                        <th>index</th>
                        <th>produkt</th>
                        <th style='width: 30px'>kolor</th>
                        <th>cena/szt</th>
                        <th>ilość</th>
                        <th>wartość</th>
                        </tr>
                </thead>
                <tbody>
        ";

        foreach ($elements as $item) {
            $variant = $item->getVariant();
            $product = $item->getProduct();
            $color = '';
                        
            if ($product->hasColor()) {
                $variant->parse();
                $color = $variant->fields[$product->getColorFieldIndex()];
                $color = '<img src="http://www.expertwbiurze.pl/public/colors/' . $color . '.gif" style="border: 1px solid #dadada" />';
            }
                        
            $products .= "
                <tr>	
                    <td>{$variant->index}</td>
                    <td><a href='oferta/produkt/{$product->id}' target='_blank'>{$product->getName()}</a></td>
                    <td>" . $color . "</td>
                    <td>". str_replace('.', ',', $item->price) ." zł</td>
                    <td>{$item->quantity}</td>
                    <td>". str_replace('.', ',', $item->price_sum) ." zł</td>
                </tr>
            ";
        }

        $products .=
		"
						</tbody>
					</table>
		
					<div style='font-weight: bold; text-align: right'>{$elements->count()} produktów za łączną kwotę ". str_replace('.', ',', $Order->price_sum) ." zł netto.</div>
					<div style='font-weight: bold; text-align: right'>Podane ceny nie zawierają podatku VAT.</div>
					";
		
		if (!empty($order->comments)) {
			$products .=
			"
			<div class='header'>Dodatkowe uwagi:</div>
			<div class='value='>". str_replace("\n", '<br />', stripslashes($Order->comments)) ."</div>";
		}
		
		return $products;
	
    }
    
    public function __construct($type = 'txt', $smtp = true)
    {
        $this->_mail = new Zend_Mail('UTF-8');
        $this->_type = $type;
        $this->smtp = $smtp;
    }
    
    public function __call($name, $arguments)
    {
        call_user_func_array(array($this->_mail, $name), $arguments);
        return $this;
    }
    
    public function getType()
    {
        return $this->_type;
    }
    
    public function send($content)
    {
        $this->_mail->setFrom($this->_login, 'expertwbiurze.pl');
        $transport = new Zend_Mail_Transport_Smtp($this->_host, array(
            'auth' => 'login',
            'username' => $this->_login,
            'password' => $this->_password,
            'ssl' => 'ssl'
        ));
        
        if ($this->getType() == 'txt') {
            $this->_mail->setBodyText($content);
        }
        else {
            $this->_prepareHtml($content);
        }
        
        try {
            $this->_mail->send($this->smtp ? $transport : null);
        }
        catch (Exception $e) {
            $mail = new App_Mailer();
            $mail->addTo('ascalogan@gmail.com');
            $mail->setSubject('Sending mail error');
            $mail->send("{$e->getMessage()}\n\n" . $this->_mail->getSubject() . "\n\n" . serialize($this->_mail->getRecipients()));
        }
    }
    
    protected function _prepareHtml($content)
    {
        $content = str_replace("<a href='", "<a href='http://www.expertwbiurze.pl/", $content);
				
        $body = "
        <html>
        <head>
        <title>Wiadomość ze strony www.expertwbiurze.pl</title>
        <style type='text/css'>
        h1 {
                font-weight: bold;
                color: #01b4cb;
                font-size: 14px;
                margin-bottom: 20px;
        }
        .blue {
            color: #01b4cb;
        }
        .value {
                margin: 5px 0px 10px 0px;
        }
        .message {
                margin-bottom: 20px;
                font-weight: bold
        }
        body {
                margin: 10px;
        }
        div, p {
                font-family: Arial;
                font-size: 12px;
                color: #000;
        }
        table.listing {
                width: 100%;
                border-collapse: collapse;
        }
        table.listing th, table.listing td {
                font-size: 11px;
                color: #000;
                font-family: Tahoma;
                padding: 4px;
                border-bottom: 1px solid #b4b6af;
                text-align: left;
                margin-top: 10px;
        }
        table.listing a img { border: 0px }
        a {
                color: #000;
                font-size: 12px;
                font-family: Arial;
                text-decoration: none;
        }
        a:hover, a.active {
                color: #01b4cb;
        }
        </style>
        </head>
        <body>
                {$content}
                <div style='margin-top: 20px'>-----</div>
                <div>Expert w biurze Sp. z o.o</div>
                <div>44-100 Gliwice</div>
                <div>ul. Chorzowska 44C</div>
                <div style='margin-top: 10px'>telefon: +48 32 231 04 72, +48 32 231 04 73</div>
                <div>fax +48 32 232 4169</div>
                <div style='margin-top: 20px; font-style: italic'>Ta wiadomość została wygenerowana automatycznie 
                ze strony www.expertwbiurze.pl. Prosimy na nią nie odpowiadać.</div>
        </body>
        </html>
        ";

        // To send HTML mail, the Content-type header must be set
        $this->_mail->setBodyHtml($body);
    }
}