<?php

class App_Db_Table_Row extends Zend_Db_Table_Row_Abstract
{
    public function clean($field)
    {
        $value = trim(isset($this->$field) ? $this->$field : $field);
        $value = mb_strtolower($value, 'UTF-8');
        $value = str_replace(array(
            'ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ż', 'ź', ' '
        ), array(
            'a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z', '-'
        ), $value);
        
        return preg_replace('/[^a-zA-Z0-9_-]/', '', $value);
    }
    
    protected function _url($name, $params = array())
    {
        $router = Zend_Controller_Front::getInstance()->getRouter();
        return $router->assemble($params, $name);
    }
}