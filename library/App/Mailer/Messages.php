<?php

class App_Mailer_Messages
{
    public static function _parse($content)
    {
        $body = str_replace('<h1', '<h1 style="font-weight: bold; font-size: 16px; color: #01b4cb"', $content);
        $body = str_replace('class="blue"', 'style="color: #01b4cb"', $body);
        $body = str_replace('class="orderLabel"', 'style="color: #01b4cb; margin-top: 20px; font-weight: bold; font-size: 14px"', $body);
        
        $body = '<div style="font-family: Arial; font-size: 12px; color: #333">' . $body . '</div>';
        
        return $body;
    }
    
    public static function registerSite($account, $password)
    {
        $body = "<h1>Potwierdzenie założenia konta</h1>";
        $body .= "<p>Dziękujemy za założenie konta w serwisie expertwbiurze.pl</p>";
        $body .= "<p>Poniżej znajdują się dane do Twojego konta:</p>";
        $body .= '<div><span class="blue">Twój login:</span> ' . $account->login . '</div>';
        $body .= '<div><span class="blue">Twoje hasło:</span> ' . $password . '</div>';
        $body .= '<p>Już teraz możesz się zalogować. Życzymy udanych zakupów!</p>';
        
        return self::_parse($body);
    }
    
    public static function accountCreated($account, $password)
    {
        $body = '<h1>Dane dostępowe do konta</h1>';
        $body .= '<p>Otrzymujesz tą wiadomość, ponieważ zostało dla Ciebie założone nowe konto w systemie expertwbiurze.pl</p>';
        $body .= "<p>Poniżej znajdziesz dane umożliwiające zalogowanie się w systemie.</p>";
        $body .= '<div><span class="blue">Login:</span> '. $account->login .'</div>';
        $body .= '<div><span class="blue">Hasło:</span> '. $password .'</p>';
        
        return self::_parse($body);
    }
    
    public static function passwordChanged($account, $password)
    {
        $body = "<h1>Nowe hasło</h1>";
        $body .= "<p>Zostało wygenerowane nowe hasło do Twojego konta.</p>";
        $body .= "<p>Poniżej znajdują się dane do Twojego konta:</p>";
        $body .= '<div><span class="blue">Twój login:</span> ' . $account->login . '</div>';
        $body .= '<div><span class="blue">Twoje hasło:</span> ' . $password . '</div>';
        
        return self::_parse($body);
    }
    
    public static function orderSend($order)
    {
        $body = '<h1>Potwierdzenie złożenia zamówienia</h1>';
        $body .= '<p>Dziękujemy za złożenie zamówienia.</p>';
        $body .= '<p>Twoje zamówienie oczekuje obecnie na przyjęcie do realizacji. Poinformujemy Cię o jego przyjęciu drogą mailową.</p>';
        $body .= self::orderToTable($order);
        
        return self::_parse($body);
    }
    
    public static function orderToVerification($order)
    {
        $body = '<h1>Zamówienie przekazane do weryfikacji</h1>';
        $body .= '<p>Twoje zamówienie zostało przekazane do weryfikacji.</p>';
        $body .= self::orderToTable($order);
        
        return self::_parse($body);
    }
    
    public static function orderToVerify($order)
    {
        $creator = $order->getCreator();
        
        $body = '<h1>Nowe zamówienie do weryfikacji</h1>';
        $body .= '<p>Otrzymałeś nowe zamówienie do zweryfikowania.</p>';
        $body .= '<p>Zamówienie zostało wysłane przez <span class="blue">'. $creator->getName() . '</span></p>';
        $body .= self::orderToTable($order);
        
        return self::_parse($body);
    }
    
    public static function orderThrow($order)
    {
        $creator = $order->getCreator();
        
        $body = '<h1>Zamówienie zostało odrzucone</h1>';
        $body .= '<p>Odrzuciłeś poniższe zamówienie i nie zostanie ono przekazane do realizacji.</p>';
        $body .= '<p>Zamówienie zostało stworzone przez <span class="blue">'. $creator->getName() . '</span></p>';
        $body .= self::orderToTable($order);
        
        return self::_parse($body);
    }
    
    public static function orderThrowCreator($order, $verifier)
    {
        $body = '<h1>Zamówienie zostało odrzucone</h1>';
        $body .= '<p>Twoje zamówienie zostało odrzucone przez <span class="blue">'. $verifier->getName() . '</span></p>';
        $body .= '<p>Zamówienie nie zostanie przekazane do realizacji.</p>';
        $body .= self::orderToTable($order);
        
        return self::_parse($body);
    }
    
    public static function orderAccept($order)
    {
        $body = '<h1>Zaakceptowałeś zamówienie</h1>';
        $body .= '<p>Zaakceptowałeś poniższe zamówienie.</p>';
        $body .= '<p>Zamówienie oczekuje obecnie na przyjęcie do realizacji. Poinformujemy Cię o jego przyjęciu drogą mailową.</p>';
        $body .= self::orderToTable($order);
        
        return self::_parse($body);
    }
    
    public static function orderAcceptVerifier($order)
    {
        $body = '<h1>Zamówienie zostało złożone</h1>';
        $body .= '<p>Zamówienie, które weryfikowałeś, zostało złożone.</p>';
        $body .= self::orderToTable($order);
        
        return self::_parse($body);
    }
    
    public static function orderAcceptCreator($order)
    {
        $body = '<h1>Zamówienie zostało zaakceptowane</h1>';
        $body .= '<p>Twoje zamówienie zostało zaakceptowane.</p>';
        $body .= '<p>Zamówienie oczekuje obecnie na przyjęcie do realizacji. Poinformujemy Cię o jego przyjęciu drogą mailową.</p>';
        $body .= self::orderToTable($order);
        
        return self::_parse($body);
    }
    
    public static function orderGotCreator($order)
    {
        $body = '<h1>Zamówienie zostało przyjęte do realizacji</h1>';
        $body .= '<p>Twoje zamówienie zostało przyjęte do realizacji.</p>';
        $body .= '<p>Gdy Twoje zamówienie zostanie przez nas przygotowane, poinformuję Cię o tym drogą mailową.</p>';
        $body .= self::orderToTable($order);
        
        return self::_parse($body);
    }
    
    public static function orderCompleteCreator($order)
    {
        $body = '<h1>Zamówienie zostało zrealizowane</h1>';
        $body .= '<p>Twoje zamówienie zostało przez nas przygotowane.</p>';
        $body .= '<p>Wkrótce zamówienione produkty zostaną do Ciebie dostarczone.</p>';
        $body .= self::orderToTable($order);
        
        return self::_parse($body);
    }
    
    public static function orderNewInfo($order)
    {
        $view = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->view;
        
        $url = 'http://' . $_SERVER['SERVER_NAME'];
        $url .= $view->baseUrl() . '/confirm-order?id=' . $order->id . '&hash=' . md5($order->id);
        
        $client = $order->getClient();
        
        $body = '<h1>Nowe zamówienie w systemie</h1>';
        $body .= '<p>Klient '. $client->name . ' złożył nowe zamówienie.</p>';
        $body .= self::orderToTable($order);
        $body .= '<p><a style="color: #fff; background-color: #01b4cb; padding: 5px; display: inline-block" href="'. $url . '">potwierdź przyjęcie do realizacji</a></p>';
        
        return self::_parse($body);
    }
    
    public static function orderToTable($order)
    {
        $elements = $order->getElements();
					
        $products = '
            <p class="orderLabel">
                Zamówienie nr: ' . $order->id . '
            </p>
            <table class="listing" style="width: 100%; border-collapse: collapse">
                <thead>
                    <tr>
                        <th style="border-bottom: 1px solid #333; padding: 3px; text-align: left">lp</th>
                        <th style="border-bottom: 1px solid #333; padding: 3px; text-align: left">index</th>
                        <th style="border-bottom: 1px solid #333; padding: 3px; text-align: left">produkt</th>
                        <th style="width: 30px; border-bottom: 1px solid #333; padding: 3px; text-align: left">kolor</th>
                        <th  style="border-bottom: 1px solid #333; padding: 3px; text-align: left">cena/szt</th>
                        <th style="border-bottom: 1px solid #333; padding: 3px; text-align: left">ilość</th>
                        <th style="border-bottom: 1px solid #333; padding: 3px; text-align: left">wartość</th>
                    </tr>
                </thead>
                <tbody>
        ';
        $lp = 0;
        
        foreach ($elements as $item) {
            $variant = $item->getVariant();
            $product = $item->getProduct();
            $color = '';
            $lp++;
                        
            if ($product->hasColor()) {
                $variant->parse();
                $color = $variant->fields[$product->getColorFieldIndex()];
                $color = '<img src="http://www.expertwbiurze.pl/public/colors/' . $color . '.gif" style="border: 1px solid #dadada" />';
            }
                        
            $products .= '
                <tr>	
                    <td style="border-bottom: 1px solid #333; padding: 3px">' . $lp . '</td>
                    <td style="border-bottom: 1px solid #333; padding: 3px">' . $variant->index . '</td>
                    <td style="border-bottom: 1px solid #333; padding: 3px">' . $product->getName() . '</td>
                    <td style="border-bottom: 1px solid #333; padding: 3px">' . $color .'</td>
                    <td style="border-bottom: 1px solid #333; padding: 3px">' . str_replace('.', ',', $item->price) .' zł</td>
                    <td style="border-bottom: 1px solid #333; padding: 3px">' . $item->quantity . '</td>
                    <td style="border-bottom: 1px solid #333; padding: 3px">' . str_replace('.', ',', $item->price_sum) .' zł</td>
                </tr>
            ';
        }

        $products .=
		"
						</tbody>
					</table>
		
					<div style='font-weight: bold; text-align: right'>{$order->getElementsCount()} produktów za łączną kwotę ". str_replace('.', ',', $order->getTotalPrice()) ." zł netto.</div>
					<div style='font-weight: bold; text-align: right'>Podane ceny nie zawierają podatku VAT.</div>
					";
		
		if (!empty($order->comments)) {
			$products .=
			"
			<p class='blue'>Dodatkowe uwagi:</p>
			" . nl2br(stripslashes($order->comments));
		}
		
		return $products;
    }
}