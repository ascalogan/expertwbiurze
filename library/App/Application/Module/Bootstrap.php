<?php

class App_Application_Module_Bootstrap extends Zend_Application_Module_Bootstrap
{
    public function addRoutes(array $routes)
    {
        $this->bootstrap('frontcontroller');
        $front = $this->getResource('frontcontroller');
        
        $router = $front->getRouter();
        $moduleName = strtolower($this->getModuleName());
        
        foreach ($routes as $name => $route) {
            $routeClass = 'Zend_Controller_Router_Route';
            if (isset($route['class'])) {
                $routeClass .= '_' . ucfirst($route['class']);
            }
            
            if (is_numeric($name)) {
                $name = $moduleName . '-' . $name;
            }
            
            if (!isset($route['params']['module'])) {
                $route['params']['module'] = $moduleName;
            }
            if (!isset($route['reqs'])) {
                $route['reqs'] = array();
            }
            
            $router->addRoute($name, new $routeClass($route['route'], $route['params'], $route['reqs']));
        }
    }
}