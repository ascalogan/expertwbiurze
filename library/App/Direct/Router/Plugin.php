<?php

class App_Direct_Router_Plugin extends Zend_Controller_Plugin_Abstract
{
    
    protected $_stack = array();
    
    protected $_results = array();
    
    protected $_isForm;
    
    protected $_isUpload;
    
    public function __construct() {
        $isForm = false;
        $isUpload = false;
        
        $response = Zend_Controller_Front::getInstance()->getResponse();
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $appClass = $request->getParam('app');
       
        if((isset($HTTP_RAW_POST_DATA) || ($HTTP_RAW_POST_DATA = file_get_contents('php://input'))) && ($data = json_decode($HTTP_RAW_POST_DATA))) 
        {
            $response->setHeader('Content-Type', 'text/javascript');
        } 
        else if (isset($_POST['extAction'])) 
        { // form post
            $post = $request->getPost();
            $isForm = true;
            $isUpload = $post == 'true';
            
            $data = new App_Direct_Action();
            $data->action = $post['extAction'];
            $data->method = $post['extMethod'];
            $data->tid = isset($post['extTID']) ? $post['extTID'] : null; // not set for upload
            $data->data = array($post, $_FILES);
        } 
        else 
        {
            die('Invalid request.');
        }
        
        $this->_isForm = $isForm;
        $this->_isUpload = $isUpload;

        $r = null;
        if (is_array($data)) {
            $r = array();
            foreach($data as $d){
                $r[] = $this->doRpc($d, $appClass);
            }
            
        } else {
            $r = $this->doRpc($data, $appClass);
        }
    }
    
     public function doRpc($cdata, $appClass)
    {
        try {
            $action = strtolower($cdata->action);
            
            // Load module api
            if (!isset($this->_api[$action])) {
                // Get list of bootstrap modules
                $modules = Zend_Controller_Front::getInstance()->getParam('bootstrap')->modules;
                
                // Check if requested module was bootstrapped and has direct methods
                if (isset($modules[$action]) && method_exists($modules[$action], 'getDirectMethods')) {
                    $direct = $modules[$action]->getDirectMethods();

                    // Assign methods to api
                    if ($direct && isset($direct[$appClass])) {
                        $this->_api[$action] = array('methods' => $direct[$appClass]);
                    }
                }
            }
            
            // Api for module does not exist
            if (!isset($this->_api[$action])){
                throw new Exception('Call to undefined action: ' . $cdata->action);
            }

            $a = $this->_api[$action];

            //$this->doAroundCalls($a['before'], $cdata);

            $method = $cdata->method;
            $mdef = $a['methods'][$method];
            
            if(!$mdef){
                throw new Exception("Call to undefined method: $method on action $action");
            }
            
           // $this->doAroundCalls($mdef['before'], $cdata);

            $r = array(
                'type'=>'rpc',
                'tid'=>$cdata->tid,
                'action'=>$action,
                'method'=>$method
            );

 
            if (isset($mdef['len'])) {
                $params = isset($cdata->data) && is_array($cdata->data) ? $cdata->data : array();
            } else {
                $params = array($cdata->data);
            }
            
            $this->pushStack($method, $action, $r, $params);
        }
        catch(Exception $e){
            throw $e;
        }
        
        return $r;
    }
    
    public function pushStack($action, $module, $request, $params = array())
    {
        array_push($this->_stack, array(
            'module' => $module,
            'params' => array(
                'routeAction' => $action,
                'routeParams' => $params,
                'routeRequest' => $request
            )
        ));
    }
    
    public function getNext()
    {
        return array_shift($this->_stack);
    }
    
    public function hasNext()
    {
        return count($this->_stack) > 0;
    }
    
    public function postDispatch(Zend_Controller_Request_Abstract $request) 
    {
        if ($request->getParam('routeRequest')) {
            $this->_results[] = $request->getParam('routeRequest');
        }
        
        if (!$request->isDispatched()) {
            return;
        }
        
        if (!$this->hasNext()) {
            $this->sendResults();
        }
        else {
            $next = $this->getNext();

            $request    ->setModuleName($next['module'])
                        ->setControllerName('step')
                        ->setActionName('route')
                        ->setParams($next['params'])
                        ->setDispatched(false);
        }
    }
    
    public function sendResults()
    {
        $result = count($this->_results) > 1 ? $this->_results : $this->_results[0];
        $response = $this->getResponse();

        if ($this->_isForm && $this->_isUpload) {
            $response->setBody(
                '<html><body><textarea>' . 
                json_encode($result) .
                '</textarea></body></html>'
            );
        }
        else {
            $response->setBody(json_encode($result));
        }
    }
}