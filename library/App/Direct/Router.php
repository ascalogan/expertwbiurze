<?php

class App_Direct_Router
{
    /**
     *
     * @var App_Direct_Router_Plugin
     */
    protected $_stack;
    
    /**
     *
     * @var array
     */
    protected $_api = array();
    
    public function __construct() 
    {
        // Create router plugin
        $this->_stack = new App_Direct_Router_Plugin();
        Zend_Controller_Front::getInstance()->registerPlugin($this->_stack);
    }
    
    public function route()
    {
        $isForm = false;
        $isUpload = false;
        
        $response = $this->getResponse();
        $request = $this->getRequest();
        $appClass = $request->getParam('app');
        
        if((isset($HTTP_RAW_POST_DATA) || ($HTTP_RAW_POST_DATA = file_get_contents('php://input'))) && ($data = json_decode($HTTP_RAW_POST_DATA))) 
        {
            $response->setHeader('Content-Type', 'text/javascript');
        } 
        else if (isset($_POST['extAction'])) 
        { // form post
            $post = $request->getPost();
            $isForm = true;
            $isUpload = $post == 'true';
            
            $data = new App_Direct_Action();
            $data->action = $post['extAction'];
            $data->method = $post['extMethod'];
            $data->tid = isset($post['extTID']) ? $post['extTID'] : null; // not set for upload
            $data->data = array($post, $_FILES);
        } 
        else 
        {
            die('Invalid request.');
        }
        
        $this->_isForm = $isForm;
        $this->_isUpload = $isUpload;
        
        $r = null;
        if (is_array($data)) {
            $r = array();
            foreach($data as $d){
                $r[] = $this->doRpc($d, $appClass);
            }
            
        } else {
            $r = $this->doRpc($data, $appClass);
        }
    }
    
    public function _shutdown()
    {
        $response = $this->_stack->getResponse();
        
        if ($this->_isForm && $this->_isUpload) {
            $response->setBody(
                '<html><body><textarea>' . 
                json_encode($this->getResponse()) .
                '</textarea></body></html>'
            );
        }
        else {
            $response->setBody(json_encode($this->getResponse()));
        }
        
        $response->sendResponse();
        exit;
    }
    
    public function doRpc($cdata, $appClass)
    {
        try {
            $action = strtolower($cdata->action);
            
            if (!isset($this->_api[$action])) {
                $modules = $this->getFront()->getParam('bootstrap')->modules;
                
                if (isset($modules[$action]) && method_exists($modules[$action], 'getDirectMethods')) {
                    $direct = $modules[$action]->getDirectMethods();
                    
                    if ($direct && isset($direct[$appClass])) {
                        $this->_api[$action] = array('methods' => $direct[$appClass]);
                    }
                }
            }
            
            
            if (!isset($this->_api[$action])){
                throw new Exception('Call to undefined action: ' . $cdata->action);
            }

            $a = $this->_api[$action];

            //$this->doAroundCalls($a['before'], $cdata);

            $method = $cdata->method;
            $mdef = $a['methods'][$method];
            
            if(!$mdef){
                throw new Exception("Call to undefined method: $method on action $action");
            }
            
           // $this->doAroundCalls($mdef['before'], $cdata);

            $r = array(
                'type'=>'rpc',
                'tid'=>$cdata->tid,
                'action'=>$action,
                'method'=>$method
            );

 
            if (isset($mdef['len'])) {
                $params = isset($cdata->data) && is_array($cdata->data) ? $cdata->data : array();
            } else {
                $params = array($cdata->data);
            }
            
            $this->_stack->pushStack($method, $action, $params);

            //$r['result'] = $o->getData();
            
            //$this->doAroundCalls($mdef['after'], $cdata, $r);
            //$this->doAroundCalls($a['after'], $cdata, $r);
        }
        catch(Exception $e){
                $r['type'] = 'exception';
                $r['message'] = $e->getMessage();
                $r['where'] = $e->getTraceAsString();
        }
        
        return $r;
    }

    
    function doAroundCalls(&$fns, &$cdata, &$returnData=null)
    {
        if(!$fns){
                return;
        }
        if(is_array($fns)){
                foreach($fns as $f){
                        $f($cdata, $returnData);
                }
        }else{
                $fns($cdata, $returnData);
        }
    }
    
    /**
     * Get front controller
     * 
     * @return  Zend_Controller_Front
     */
    public function getFront()
    {
        return Zend_Controller_Front::getInstance();
    }
    
    /**
     *  Get request
     * 
     * @return  Zend_Controller_Request_Abstract
     */
    public function getRequest()
    {
        return $this->getFront()->getRequest();
    }
    
    public function getResponse()
    {
        return $this->getFront()->getResponse();
    }
}