<?php

class App_View_Helper_Bb extends Zend_View_Helper_Abstract
{
    public function bb($value)
    {
        $value = preg_replace('/\[\[(.*?)\]\]/is', '<strong>$1</strong>', $value);
        $value = preg_replace('/{{(.+)}}/is', '<span class="color">$1</span>', $value);
        
        return nl2br($value);
    }
}