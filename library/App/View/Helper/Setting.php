<?php

class App_View_Helper_Setting extends Zend_View_Helper_Abstract
{
    protected $_settings = array();
    
    public function setting($name, $default = null, $useBB = false)
    {
        if (empty($this->_settings)) {
            $TSettings = new Lite_Model_DbTable_Settings();
            foreach ($TSettings->fetchAll() as $row) {
                $this->_settings[$row->name] = $row->value;
            }
        }
        
        $value = isset($this->_settings[$name]) ? $this->_settings[$name] : $default;
        
        if ($value && $useBB) {
            $value = $this->view->bb($value);
        }
        
        return $value;
    }
}