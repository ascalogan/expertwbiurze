<?php

class App_View_Helper_TableTh extends Zend_View_Helper_Abstract
{
    public function tableTh($label, $field)
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        
        $url = $this->view->baseUrl() . $request->getPathInfo();
        
        $dir = 'asc';
        $sorted = false;
        
        if ($request->getParam('sort') == $field) {
            $sorted = true;
            if ($request->getParam('dir') == 'asc') {
                $dir = 'desc';
            }
        }
        
        $html = '<a href="' . $url . '?sort=' . $field . '&dir=' . $dir . '"';
        $html .= '>' . $label;
        
        if ($sorted) {
            $html .= ' ' . ($dir == 'asc' ? '&uarr;' : '&darr;');
        }
        
        $html .= '</a>';

        return $html;
    }
}