<?php

class App_View_Helper_TableForm extends Zend_View_Helper_Abstract
{
    public function tableForm($form)
    {
        $form = (string) $form;
        
        $form = str_replace(array('<dl class="zend_form"', '/dl>'), array('<table class="form"', '/table>'), $form);
        $form = str_replace('<dt', '<tr><td class="labelSpace"', $form);
        $form = str_replace('</dt>', '</td>', $form);
        $form = str_replace('<dd', '<td class="fieldSpace"', $form);
        $form = str_replace('</dd>', '</td></tr>', $form);
        
        return $form;
    }
}