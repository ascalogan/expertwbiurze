<?php

class App_View_Helper_Btn extends Zend_View_Helper_Abstract
{
    public function btn($class, $url, $label = '', $title = '')
    {
        $html = '<a href="' . $url . '"';
        
        if (empty($label)) {
            $html .= ' class="action ' . $class . '"';
            if (!empty($title)) {
                $html .= ' title="' . $title . '"';
            }
            $html .= '>';
        }
        else {
            $html .= ' class="button"><span class="action ' . $class . '"></span><span>' . $label . '</span>';
        }
        
        $html .= '</a>';
        
        return $html;
        //<a href=".." class="button"><span class="action add"></span><span>dodaj nowe konto</span></a>
    }
}