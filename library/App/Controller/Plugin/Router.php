<?php

class App_Controller_Plugin_Router extends Zend_Controller_Plugin_Abstract
{
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request) 
    {
        if ($request->getControllerName() === 'step') return;
        
        $view = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->view;
        
        $TGroups = new Products_Model_DbTable_Groups();
        $groups = array();
        $groups[] = array(
            'label' => 'promocje',
            'url' => $view->baseUrl()
        );
        
        foreach ($TGroups->fetchAll(null, 'order') as $group) {
            $groups[] = array(
                'label' => mb_strtolower($group->name, 'UTF-8'),
                'url' => $group->getUrl()
            );
        }
        
        $view->groups = $groups;
        
        // Banners
        $TBanners = new Content_Model_DbTable_Banners();
        $view->banner = $TBanners->fetchRow(array('active = ?' => '1'), 'RAND()');
        
        // Pages
        $TPages = new Content_Model_DbTable_Pages();
        $rows = $TPages->fetchAll(array('list_visible = ?' => '1'), array('list_order', 'title'));
        $pages = array(array(), array());
        
        foreach ($rows as $i => $row) {
            $pages[($i % 2)][] = $row;
        }
        $view->pagesList = $pages;
        
        // Sliders
        $TSliders = new Content_Model_DbTable_Sliders();
        $view->sliders = $TSliders->fetchAll(array('active = ?' => '1'), array('order', 'id'));
    }
    
    public function routeShutdown(Zend_Controller_Request_Abstract $request) 
    {
        $front = Zend_Controller_Front::getInstance();
        $dispatcher = $front->getDispatcher();
        
        if (!$dispatcher->isDispatchable($request) || $request->getControllerName() != 'step') {
            $modules = $front->getParam('bootstrap')->modules;
            $router = $front->getRouter();
            
            foreach ($modules as $module) {
                if (!method_exists($module, 'getRoutes')) {
                    continue;
                }
                
                $this->checkModuleRoutes($module);
            }
        }
        
        $router = $front->getRouter();
        $router->route($request);
    }
    
    public function checkModuleRoutes($module)
    {
        $front = Zend_Controller_Front::getInstance();
        $router = $front->getRouter();
        $moduleName = strtolower($module->getModuleName());
        
        foreach ($module->getRoutes() as $name => $route) {
            if (is_string($route)) {
                $route = array('url' => $route);
            }
            if (empty($route['route'])) {
                $route['route'] = 'static';
            }
            
            $suffix = '.html';
            if (array_key_exists('suffix', $route)) {
                if (is_string($route['suffix'])) {
                    $suffix = $route['sufix'];
                }
                else if ($route['suffix'] === FALSE) {
                    $suffix = null;
                }
            }
            
            if ($suffix) {
                $route['url'] .= $suffix;
            }
            
            // Set up destination parts
            if (empty($route['module'])) {
                $route['module'] = $moduleName;
            }
            if (empty($route['controller'])) {
                $route['controller'] = 'site';
            }
            if (empty($route['action'])) {
                $route['action'] = $name;
            }
            
            // Route defaults
            $defaults = array_merge(array(
                'module' => $route['module'],
                'controller' => $route['controller'],
                'action' => $route['action']
            ), empty($route['defaults']) ? array() : $route['defaults']);
            
            // Set route name
            $name = $moduleName . '.' . $name;
            
            // Create route
            switch ($route['route']) {
                case 'static':
                    $r = new Zend_Controller_Router_Route_Static($route['url'], $defaults);
                    break;
                case 'regex':
                    $map = empty($route['map']) ? array() : $route['map'];
                    $reverse = empty($route['reverse']) ? null : $route['reverse'];
                    
                    if ($reverse && $suffix) {
                        $reverse .= $suffix;
                    }
                    
                    $r = new Zend_Controller_Router_Route_Regex(
                        $route['url'], $defaults, $map, $reverse
                    );
                    break;
            }
           
            $router->addRoute($name, $r);
        }
    }
}