<?php

//class Messages {
//
//}

class App_Controller_Action_Step extends Zend_Controller_Action
{

    protected $_directResponse = array();
    
    public function init()
    {
        $this->_session = new Zend_Session_Namespace($this->view->baseUrl() . '#step');
    }
    
    public final function routeAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $action = $this->_getParam('routeAction');
        $params = $this->_getParam('routeParams');
        $request = $this->_getParam('routeRequest');
        
        // Call requested action with params
        call_user_func_array(array($this, $action), $params);
        
        // Ensure success key is set
        if (!array_key_exists('success', $this->_directResponse)) {
            $this->_directResponse['success'] = true;
        }
        
        $request['result'] = $this->_directResponse;
        
        $this->getRequest()->setParam('routeRequest', $request);
    }
    
    public function getSession()
    {
        return $this->_session;
    }
    
    public function hasAccess($name)
    {
        $session = $this->getSession();
        
        if (!isset($session->privilages)) {
            return false;
        }
        
        return in_array($name, $session->privilages);
    }
    
    protected function _strip($value) {
        if (is_string($value)) {
            $value = stripslashes($value);
        }
        else if (is_array($value)) {
            foreach ($value as $i => $v) {
                $value[$i] = $this->_strip($v);
            }
        }
        
        return $value;
    }
    
    protected function _isUploaded($name)
    {
        return !empty($_FILES[$name]['name']);
    }
    
    public function setting($name, $value = null)
    {
        $TSettings = new Lite_Model_DbTable_Settings();
        
        if ($value === null) {
            $row = $TSettings->get($name);
            return $row === null ? null : $row->value;
        }
        else {
            $TSettings->set($name, $value);
        }
    }
    
    public function send($name, $value = null)
    {
        if (is_array($name)) {
            foreach ($name as $n => $v) {
                $this->send($n, $v);
            }
        }
        else {
            $value = $this->_strip($value);
            $this->_directResponse[$name] = $value;
        }
    }
    
    /**
     * 
     * @param   Zend_Db_Select $query
     * @param type $data
     * @param type $options
     */
    public function sendRemoteItems($query, $data, $options = array(), $map = array())
    {
        if (!is_array($options)) {
            $options = array();
        }
        
        if (empty($options['skipSort']) && !empty($data->sort)) {
            $query  ->reset(Zend_Db_Select::ORDER);
            if (isset($map[$data->sort])) {
                $data->sort = $map[$data->sort];
            }
            $query  ->order($data->sort . ' ' . $data->dir);
        }
        
        if (empty($options['skipFilter']) && !empty($data->filter)) {
            foreach ($data->filter as $filter) {
                $type = '=';
                if (!empty($filter->type)) {
                    $type = $filter->type;
                }
                $query  ->where($filter->property . ' ' . $type . ' ?', $filter->value);
            }
        }
        
        if (empty($options['skipLimit'])) {
            $q = clone $query;
            $from = $query->getPart(Zend_Db_Select::FROM);
            $from = $from[key($from)];
            
            $q  ->reset(Zend_Db_Table::COLUMNS);
            $q  ->columns('COUNT(`' . $from['tableName'] . '`.`id`)');
            
            $this->send('total', $q->query()->fetchColumn());
            $query  ->limit($data->limit, $data->start);
        }
        
        $this->send('items', $query->query()->fetchAll());
    }
    
    public function sendErrors(array $errors)
    {
        $this->send(array(
            'success' => false,
            'errors' => $errors
        ));
    }
    
    protected function _saveRow($modelCls, array $data, $send = true)
    {
        $model = $modelCls;
        
        if (!$model instanceof App_Db_Table) {
            list ($moduleName, $tmp) = explode('_', get_class($this));
            $modelCls = $moduleName . '_Model_DbTable_' . ucfirst($modelCls);
            $model = new $modelCls();
        }
        
        $row = null;
        $isNew = false;
        
        if (isset($data['id'])) {
            $row = $model->find($data['id'])->current();
        }
        
        unset($data['id']);
        
        if ($row === null) {
            $isNew = true;
            $row = $model->createRow();
            
            if (isset($row->users_id)) {
                $row->users_id = $this->getSession()->uid;
            }
            if (isset($row->created)) {
                $row->created = time();
            }
        }
        $row->setFromArray($data);
        $row->save();
        
        if ($send !== false) {
            $this->send(array(
                'data' => $row->toArray(),
                'isNew' => $isNew
            ));
        }
        
        return $row;
    }
    
    protected function _removeRow($modelCls, $id)
    {
        list ($moduleName, $tmp) = explode('_', get_class($this));
        $modelCls = $moduleName . '_Model_DbTable_' . ucfirst($modelCls);
        $model = new $modelCls();
        
        $row = $model->find($id)->current();
        
        if ($row !== null) {
            $row->delete();
        }
        
        $this->send('removed', $id);
    }
}