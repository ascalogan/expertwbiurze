<?php

class App_Controller_Action_Site extends Zend_Controller_Action
{
    /**
     *
     * @var Zend_Session_Namespace
     */
    protected $_session;
    
    protected $_user;
    
    /**
     *
     * @var     Messages
     */
    public $messages;
    
    public function init()
    {
        if (1 || $this->getRequest()->getParam('beta')) {
            Zend_Layout::getMvcInstance()->setLayout('layout2');
        }

        $this->_session = new Zend_Session_Namespace($this->view->baseUrl . '/site');
        $this->_helper->viewRenderer->setNoController(true);
        
        if (empty($this->_session->messages)) {
            $this->_session->messages = new Messages();
        }
        $this->messages = $this->_session->messages;
    }
    
    /**
     * 
     * @return  Zend_Session_Namespace
     */
    public function getSession()
    {
        return $this->_session;
    }
    
    /**
     * 
     * @return  Clients_Model_Account
     */
    public function getUser()
    {
        if ($this->_user === null && !empty($this->getSession()->uid)) {
            $TUsers = new Clients_Model_DbTable_Accounts();
            $this->_user = $TUsers->find($this->getSession()->uid)->current();
        }
        
        return $this->_user;
    }
    
    /**
     * 
     * @param type $create
     * @return  Orders_Model_Order
     */
    public function getOrder($create = false)
    {
        $order = null;
        
        if (!empty($this->getSession()->order)) {
            $TOrders = new Orders_Model_DbTable_Orders();
            $order = $TOrders->find($this->getSession()->order)->current();
        }
        else if ($create === true) {
            $TOrders = new Orders_Model_DbTable_Orders();
            $order = $TOrders->createRow(array(
                'clients_id' => $this->getUser()->clients_id,
                'owner_id' => $this->getUser()->id,
                'created' => time(),
                'status' => '0'
            ));
            $order->save();
            
            $this->getSession()->order = $order->id;
        }
        
        return $order;
    }
    
    protected function _getSort($allowed)
    {
        $sort = '';
        $dir = 'asc';
        $request = $this->getRequest();
        
        if (!($sort = $request->getQuery('sort')) || !in_array($sort, $allowed)) {
            return null;
        }
        if ($request->getQuery('dir') == 'desc') {
            $dir = 'desc';
        }
        
        return $sort . ' ' . $dir;
    }
    
    public function postDispatch() {
        $this->view->user = $this->getUser();
        $this->view->messages = $this->messages;
    }
    
    public function url($name, array $params = array())
    {
        $router = Zend_Controller_Front::getInstance()->getRouter();
        return str_replace($this->view->baseUrl(), '', $router->assemble($params, $name));
    }
    
    public function goToUrl($name, array $params = array())
    {
        $this->redirect($this->url($name, $params));
    }
}

class Messages
{
    protected $_messages = array();
    
    public function add($type, $message, $container = 'default')
    {
        if (!isset($this->_messages[$container])) {
            $this->_messages[$container] = array();
        }
        
        $this->_messages[$container][] = array(
            'type' => $type,
            'message' => $message
        );
    }
    
    public function info($message, $container = 'default')
    {
        $this->add('info', $message, $container);
    }
    
    public function error($message, $container = 'default')
    {
        $this->add('error', $message, $container);
    }
    
    public function get($container = 'default')
    {
        $messages = isset($this->_messages[$container]) ? $this->_messages[$container] : array();
        $this->_messages[$container] = array();
        
        return $messages;
    }
}