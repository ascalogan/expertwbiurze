<?php

class App_Pdf_Element_Table extends App_Pdf_Element_Block
{
    /**
     * Table columns
     * @var array
     */
    protected $_columns = array();
    
    /**
     * Table data
     * @var array
     */
    protected $_data = array();
    
    /**
     * Element layout
     * @var string
     */
    protected $_layout = 'table';
    
    /**
     * List of allowed layouts
     * @var array
     */
    protected $_allowedLayouts = array('table');
    
    
    /**
     * Get table columns
     * 
     * @return array
     */
    public function getColumns()
    {
       return $this->_columns;
    }
    
    /**
     * Set table columns
     * 
     * @param   array $columns
     * @return  App_Pdf_Element_Table 
     */
    public function setColumns(array $columns)
    {
        $this->_columns;
        
        foreach ($columns as $column) {
            if (!is_array($column)) {
                $column = array('header' => $column);
            }
            
            $column['content'] = $column['header'];
            array_push($this->_columns, $column);
        }
        
        return $this;
    }
    
    public function getData()
    {
        return $this->_data;
    }
    
    public function setData(array $data)
    {
        foreach ($data as $row) {
            if (!isset($row['elements'])) {
                $row = array('elements' => $row);
            }
            
            array_push($this->_data, $row);
        }
    }
}