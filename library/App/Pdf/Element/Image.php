<?php

class App_Pdf_Element_Image extends App_Pdf_Element_Node
{
    /**
     * Element layout
     * @var string 
     */
    protected $_layout = 'image';
    
    /**
     * List of allowed layouts
     * @var array
     */
    protected $_allowedLayouts = array('image');
}