<?php

class App_Pdf_Element_Block extends App_Pdf_Element_Node
{
    /**
     * Elements
     * @var array
     */
    protected $_elements = array();
    
    /**
     * Default child type
     * @var string
     */
    protected $_defaultChildType = 'node';
    
    /**
     * Element layout
     * @var string
     */
    protected $_layout = 'vbox';
    
    /**
     * List of allowed layouts
     * @var array
     */
    protected $_allowedLayouts = array('vbox', 'hbox', 'columns');
    
    /**
     * Add element
     * 
     * @param   array|string $options
     * @param   string $type
     * @return  App_Pdf_Element_Block 
     */
    public function addElement($element, $type = null)
    {
        if (!$element instanceof App_Pdf_Element_Node) {
            if (!is_array($element)) {
                $element = array('content' => $element);
            }

            if (empty($type) && empty($element['type'])) {
                $type = $this->_defaultChildType;
            }
            else if (isset($element['type'])) {
                $type = $element['type'];
                unset($element['type']);
            }

            $elementClass = 'App_Pdf_Element_' . ucfirst($type);
            $element = new $elementClass($element);
        }
        
        array_push($this->_elements, $element);
        return $element;
    }
    
    public function getElements()
    {
        return $this->_elements;
    }
    
    /**
     *
     * @param  array $elements
     * @return App_Pdf_Element_Block 
     */
    public function setElements(array $elements)
    {
        foreach ($elements as $element) {
            $this->addElement($element);
        }
        return $this;
    }
    
    public function drawToPdf(App_Pdf $pdf, array $bounds, array $parentStyles) {
        parent::drawToPdf($pdf, $bounds, $parentStyles);
        
        foreach ($this->getElements() as $element) {
            $element->afterLayout($pdf);
        }
    }
    
    public function draw(App_Pdf $pdf, array $bounds, array $parentStyles) 
    {
        if (empty($this->_elements) && $this->_content) {
            $this->addElement($this->_content);
        }
        
        parent::draw($pdf, $bounds, $parentStyles);
    }
    
    protected function _pdf(App_Pdf $pdf)
    {
        $y = $this->getY();
        
        if (empty($this->_elements) && $this->_content) {
            $this->addElement($this->_content);
        }

        if (!empty($this->_elements)) {
            foreach ($this->_elements as $element) {
                $element->drawToPdf($pdf, array(
                    'x' => $this->getX(),
                    'y' => $this->getY(),
                    'width' => $this->getContentWidth()
                ), $this->_styles);

                $this->moveY($element->getTotalHeight());
            }
        }
    }
    
    public function afterLayout($pdf)
    {
        if ($this->getStyle('border')) {
            $borderWidth = $this->getStyle('borderWidth', 0.5);
            $pdf->setLineColor(new Zend_Pdf_Color_Html($this->getStyle('borderColor', '#000000')));
            $pdf->setLineWidth($borderWidth);
            
            $bounds = $this->getBounds();
            
            $pdf->drawRectangle($bounds['x'], $bounds['y'], $bounds['x'] + $bounds['width'], $bounds['y'] - $bounds['height'], Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        }
    }
}