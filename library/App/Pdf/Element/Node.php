<?php

class App_Pdf_Element_Node
{
    /**
     * List of inheritable styles
     * @var array
     */
    protected $_inheritableStyles = array(
        'color',
        'fontSize',
        'fontName',
        'lineHeight',
        'textAlign',
        'bold'
    );
    
    /**
     * Element layout
     * @var string
     */
    protected $_layout = 'element';
    
    /**
     * List of allowed layouts
     * @var array
     */
    protected $_allowedLayouts = array('element');
    
    /**
     * Layout instance
     * @var App_Pdf_Layout_Abstract
     */
    protected $_layoutObj;
    
    /**
     * Element attributes
     * @var array
     */
    protected $_attributes = array();
    
    /**
     * Element styles
     * @var array 
     */
    protected $_styles = array();
    
    /**
     * Element metrics
     * @var string 
     */
    protected $_metrics = array();
    
    /**
     * Content
     * @var string 
     */
    protected $_content;
    
    /**
     * Constructor
     * 
     * @param   array $options 
     * @return  void
     */
    public function __construct(array $options = array())
    {
        $this->setOptions($options);
    }
    
    /**
     * Set options
     * 
     * @param   array $options
     * @return  App_Pdf_Element_Node
     */
    public function setOptions(array $options)
    {
        foreach ($options as $name => $value)
        {
            $method = 'set' . ucfirst($name);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
            else {
                $this->setAttribute($name, $value);
            }
        }
        return $this;
    }
    
    /**
     * Get node content
     * 
     * @return string
     */
    public function getContent()
    {
        return $this->_content;
    }
    
    /**
     * Set node content
     * 
     * @param   string $content
     * @return  App_Pdf_Element_Node
     */
    public function setContent($content)
    {
        $this->_content = $content;
        return $this;
    }
    
    /**
     * Get layout
     * 
     * @return  string 
     */
    public function getLayout()
    {
        return $this->_layout;
    }
    
    /**
     * Set layout
     * 
     * @param   string $layout
     * @return  App_Pdf_Element_Node 
     */
    public function setLayout($layout)
    {
        if (!in_array($layout, $this->_allowedLayouts)) {
            throw new Exception('Element ' . get_class($this) . ' does not allow `' . $layout . '` layout');
        }
        
        $this->_layout = $layout;
        return $this;
    }
    
    /**
     * Get attribute
     * 
     * @param   string $name
     * @param   mixed $default
     * @return  mixed
     */
    public function getAttribute($name, $default = null)
    {
        return $this->hasAttribute($name) ? $this->_attributes[$name] : $default;
    }
    
    /**
     * Check if attribute is set
     * 
     * @param   string $name
     * @return  boolean
     */
    public function hasAttribute($name)
    {
        return array_key_exists($name, $this->_attributes);
    }
    
    /**
     * Set attributes
     * 
     * @param   string $name
     * @param   mixed $value
     * @return  App_Pdf_Element_Node
     */
    public function setAttribute($name, $value)
    {
        $this->_attributes[$name] = $value;
        return $this;
    }
    
    /**
     * Get style
     * 
     * @param   string $name
     * @param   mixed $default
     * @return  mixed
     */
    public function getStyle($name, $default = null)
    {
        $style = array_key_exists($name, $this->_styles) ? $this->_styles[$name] : $default;
        
        if ($style !== null && is_numeric($style)) {
            $style = $style * 72 / 96;
        }
        
        return $style;
    }
    
    public function getStyles()
    {
        return $this->_styles;
    }
    
    /**
     * Set style
     * 
     * @param   string $name
     * @param   mixed $value
     * @return  App_Pdf_Element_Node
     */
    public function setStyle($name, $value = null)
    {
        if (is_array($name)) {
            foreach ($name as $n => $v) {
                $this->setStyle($n, $v);
            }
        }
        else {
            $this->_styles[$name] = $value;
        }
        
        return $this;
    }
    
    /**
     * Get current X position
     * 
     * @return int
     */
    public function getX()
    {
        return $this->_metrics['x'];
    }
    
    /**
     * Get current Y position
     * 
     * @return int
     */
    public function getY()
    {
        return $this->_metrics['y'];
    }
    
    /**
     * Get element width
     * 
     * @return int 
     */
    public function getWidth()
    {
        return $this->_metrics['element_width'];
    }
    
    /**
     * Get content width
     * 
     * @return int
     */
    public function getContentWidth()
    {
        return $this->_metrics['content_width'];
    }
    
    /**
     * Get element width including margins
     * 
     * @return int
     */
    public function getTotalWidth()
    {
        return $this->_metrics['total_width'];
    }
    
    /**
     * Get element height
     * 
     * @return int
     */
    public function getHeight()
    {
        return $this->_metrics['element_height'];
    }
    
    /**
     * Get content height
     * 
     * @return int
     */
    public function getContentHeight()
    {
        return $this->_metrics['content_height'];
    }
    
    /**
     * Get element height including margins
     * 
     * @return int
     */
    public function getTotalHeight()
    {
        return $this->_metrics['total_height'];
    }
    
    /**
     * Set total element height.
     * If current total height is smaller, diffrence
     * will be applied to content.
     * 
     * @param  int $height
     * @return App_Pdf_Element_Node 
     */
    public function setTotalHeight($height)
    {
        if ($height > $this->_metrics['total_height']) {
            $space = $height - $this->getTotalHeight();
            $this->moveY($space);
        }
        return $this;
    }
    
    /**
     * Get element bounds
     * 
     * @return array [x, y, width, height]
     */
    public function getBounds()
    {
        $startX = $this->_metrics['start_x'];
        $startY = $this->_metrics['start_y'];
        
        $startX += (int) $this->getStyle('marginLeft', 0);
        $startY -= (int) $this->getStyle('marginTop', 0);
        
        return array(
            'x' => $startX, 'y' => $startY, 'width' => $this->getWidth(), 'height' => $this->getHeight()
        );
    }
    
    /**
     * Move Y position
     * 
     * @param  int $move
     * @return App_Pdf_Element_Node
     */
    public function moveY($move, $type = 'content')
    {
        $this->_metrics['y'] = $this->getY() - $move;
        
        switch ($type) {
            case 'content':
                $this->_metrics['content_height'] += $move;
            case 'element':
                $this->_metrics['element_height'] += $move;
            default:
                $this->_metrics['total_height'] += $move;
        }
        
        return $this;
    }
    
    public function applyPadding($side, $padding)
    {
        switch ($side) {
            case 'left':
                $this->_metrics['x'] += $padding;
            case 'right':
                $this->_metrics['content_width'] -= $padding;
                break;
        }
    }
    
    /**
     * Apply styles to element
     * 
     * @param   array $styles
     * @param   boolean $forceInherit Set to `true` to don't check if styles are inheritable
     * @return  App_Pdf_Element_Node
     */
    public function applyStyles(array $styles, $forceInherit = false)
    {
        foreach ($styles as $name => $value) {
            if ($this->getStyle($name) === null && (in_array($name, $this->_inheritableStyles) || $forceInherit === true)) {
                $this->setStyle($name, $value);
            }
        }
        
        return $this;
    }
    
    /**
     * Extract element styles.
     * This method extracts packed styles like padding or margin into separated styles
     */
    public function extractStyles()
    {
        $extracted = array();
        
        // Extract paddings and margins
        foreach (array('padding', 'margin') as $style) {
            $styles = array('top' => 0, 'right' => 0, 'bottom' => 0, 'left' => 0);
            
            if (!($packed = $this->getStyle($style))) continue;
            
            $parts = explode(' ', $packed);
            if (count($parts) == 1) {
                $styles['left'] = $styles['top'] = $styles['right'] = $styles['bottom'] = $parts[0];
            }
            else if (count($parts) == 2) {
                $styles['top'] = $styles['bottom'] = $parts[0];
                $styles['left'] = $styles['right'] = $parts[1];
            }
            
            foreach ($styles as $type => $value) {
                if (is_numeric($value)) {
                    $extracted[$style . ucfirst($type)] = $value;
                }
            }
        }
        
        $this->applyStyles($extracted, true);        
    }
    
    public function getLayoutObj()
    {
        if ($this->_layoutObj === null) {
            $layoutClass = 'App_Pdf_Layout_' . ucfirst($this->getLayout());
            $this->_layoutObj = new $layoutClass($this);
        }
        
        return $this->_layoutObj;
    }
    
    public function draw(App_Pdf $pdf, array $bounds, array $parentStyles)
    {
        // Initialize metrics
        $this->_metrics = $bounds;
        $this->_metrics['start_x'] = $this->getX();
        $this->_metrics['start_y'] = $this->getY();
        $this->_metrics['total_width'] = $bounds['width'];
        $this->_metrics['total_height'] = 0;
        $this->_metrics['element_width'] = $bounds['width'];
        $this->_metrics['element_height'] = 0;
        $this->_metrics['content_width'] = $bounds['width'];
        $this->_metrics['content_height'] = 0;
        
        // Apply css and parent styles
        if ($this->getAttribute('class')) {
            $css = $pdf->getCss($this->getAttribute('class'));
            $this->applyStyles($css, true);
        }
        $this->applyStyles($parentStyles);
        
        // Extract styles (like padding)
        $this->extractStyles();
        
        $layoutName = $this->getLayout();
        $layoutClass = 'App_Pdf_Layout_' . ucfirst($layoutName);
        
        $this->getLayoutObj()->draw($pdf);
    }

    public function __call($name, $arguments)
    {
        $elementClass = 'App_Pdf_Element_' . ucfirst($name);
        
        if ($name == 'node' || $name == 'image') {
            $options['content'] = array_shift($arguments); 
        }
        else if ($name == 'table') {
            $options['columns'] = array_shift($arguments);
            $options['data'] = array_shift($arguments);
        }
        else {
            $options['elements'] = (array) array_shift($arguments);
        }
        
        if (count($arguments)) {
            $options['style'] = array_shift($arguments);
            if (!is_array($options['style'])) unset($options['style']);
        }
        if (count($arguments) && is_array($arguments[0])) {
            $options = array_merge($options, array_shift($arguments));
        }
        
        $element = null;
        
        try {
            Zend_Loader::loadClass($elementClass);
        } catch (Exception $e) {
            $elementClass = 'App_Pdf_Element_Block';
            $options['layout'] = $name;
        }

        return new $elementClass($options);
    }
}