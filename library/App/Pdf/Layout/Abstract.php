<?php

abstract class App_Pdf_Layout_Abstract
{
    /**
     * Element
     * @var     App_Pdf_Element_Node 
     */
    protected $_element;
    
    /**
     * Pdf
     * @var     App_Pdf 
     */
    protected $_pdf;
    
    /**
     *
     * @param   App_Pdf_Element_Node $element
     * @param   App_Pdf $pdf 
     */
    public function __construct(App_Pdf_Element_Node $element)
    {
        $this->_element = $element;
    }
    
    /**
     * Get element
     * 
     * @return  App_Pdf_Element_Node 
     */
    public function getElement()
    {
        return $this->_element;
    }
    
    /**
     * Draw child element
     * 
     * @param   App_Pdf_Element_Node $child
     * @param   array $metrics 
     */
    public function drawChild($pdf, $child, array $metrics)
    {
        $styles = $this->getElement()->getStyles();
        
        $child->draw($pdf, $metrics, $this->getElement()->getStyles());
    }
    
    public function draw(App_Pdf $pdf)
    {
        $this->beforeRender($pdf);
        $this->render($pdf);
        $this->afterRender($pdf);
        
        if (method_exists($this->getElement(), 'getElements')) {
            foreach ($this->getElement()->getElements() as $child) {
                $child->getLayoutObj()->afterLayout($pdf);
            }
        }
    }
    
    public function beforeRender(App_Pdf $pdf)
    {
        $element = $this->getElement();
        
        // Apply margins
        if (($mTop = $element->getStyle('marginTop'))) {
            $element->moveY($mTop, 'total');
        }
        
        // Apply paddings (except bottom padding)
        if (($pLeft = $element->getStyle('paddingLeft'))) {
            $element->applyPadding('left', $pLeft);
        }
        if (($pRight = $element->getStyle('paddingRight'))) {
            $element->applyPadding('right', $pRight);
        }
        if (($pTop = $element->getStyle('paddingTop'))) {
            $element->moveY($pTop, 'element');
        }
        
        // Set font
        $fontName = $element->getStyle('fontName', 'Helvetica');
        if ($element->getStyle('bold')) {
            $fontName .= '-Bold';
        }
        $font = Zend_Pdf_Font::fontWithName($fontName);
        $pdf->setFont($font, $element->getStyle('fontSize', 16));
    }
    
    public function afterRender(App_Pdf $pdf)
    {
        $element = $this->getElement();
        
        // Apply bottom padding and margin
        if (($pBottom = $element->getStyle('paddingBottom'))) {
            $element->moveY($pBottom, 'element');
        }
        if (($mBottom = $element->getStyle('marginBottom'))) {
            $element->moveY($mBottom, 'total');
        }
    }
    
    abstract public function render(App_Pdf $pdf);
    
    public function afterLayout(App_Pdf $pdf)
    {
        $element = $this->getElement();
        
        if ($element->getStyle('border')) {
            $borderWidth = $element->getStyle('borderWidth', 0.75);
            $pdf->setLineColor(new Zend_Pdf_Color_Html($element->getStyle('borderColor', '#000000')));
            $pdf->setLineWidth($borderWidth);
            
            $bounds = $element->getBounds();
            
            $pdf->drawRectangle($bounds['x'], $bounds['y'], $bounds['x'] + $bounds['width'], $bounds['y'] - $bounds['height'], Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        }
    }
}