<?php

class App_Pdf_Layout_Vbox extends App_Pdf_Layout_Abstract
{
    /**
     * Render layout
     */
    public function render(App_Pdf $pdf)
    {
        $element = $this->getElement();
        
        $y = $element->getY();
        $children = $element->getElements();

        foreach ($children as $child) {
            $this->drawChild($pdf, $child, array(
                'x' => $element->getX(),
                'y' => $element->getY(),
                'width' => $element->getContentWidth()
            ));

            $element->moveY($child->getTotalHeight());
        }
    }
}