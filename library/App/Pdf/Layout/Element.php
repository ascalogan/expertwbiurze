<?php

class App_Pdf_Layout_Element extends App_Pdf_Layout_Abstract
{
    public function render(App_Pdf $pdf)
    {
        $element = $this->getElement();
        
        if (!($content = $element->getContent())) {
            return false;
        }
        
        $width = $element->getContentWidth();
        $textWidth = $pdf->widthForStringUsingFontSize($content);
        
        if ($textWidth > $width) {
            foreach (explode(" ", $content) as $text) {
                $this->_drawText($pdf, $text);
            }
        }
        else {
            $this->_drawText($pdf, $content);
        }
    }
    
    protected function _drawText($pdf, $text)
    {
        $element = $this->getElement();
        
        $fontSize = $pdf->getFontSize();
        $element->moveY($fontSize);
        
        $x = $element->getX();
        
        $lineHeightPre = 0;
        $lineHeightPost = 0;
        $lineHeight = $element->getStyle('lineHeight', $fontSize);
        if ($lineHeight > $fontSize) {
            $lineHeightSpace = $lineHeight - $fontSize;
            $lineHeightPre = ceil($lineHeightSpace / 2);
            $lineHeightPost = $lineHeightSpace - $lineHeightPre;
        }
        
        switch ($element->getStyle('textAlign')) {
            case 'center':
                $width = $element->getContentWidth();
                $textWidth = $pdf->widthForStringUsingFontSize($text);
                
                if ($width > $textWidth) {
                    $x = $x + ceil(($width - $textWidth) / 2);
                }
                break;
            case 'right':
                $width = $element->getContentWidth();
                $textWidth = $pdf->widthForStringUsingFontSize($text);
                
                if ($width > $textWidth) {
                    $x = $x + ($width - $textWidth);
                }
            default:
                break;
        }
        
        $element->moveY($lineHeightPre);
        $pdf->drawText($text, $x, $element->getY(), 'UTF-8');
        $element->moveY($lineHeightPost);
    }
}