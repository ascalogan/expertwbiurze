<?php

class App_Pdf_Layout_Columns extends App_Pdf_Layout_Hbox
{
    /**
     * Columns width
     * @var array
     */
    protected $_colWidths = array();
    
    /**
     * Calculate column widths
     * 
     * @param   array $columns 
     */
    public function calculateWidths($availFlex = null)
    {
        $element = $this->getElement();
        $columns = $element->getElements();
        
        $widths = array();
        
        $useMinWidthForFlex = false;
        $defaultWidth = $element->getAttribute('minColWidth', 60);
        if ($availFlex === null) {
            $availFlex = $element->getContentWidth();
        }
        $totalFlex = 0;
        
        foreach ($columns as $column) {
            $width = $column->getAttribute('width');
            $flex = $column->getAttribute('flex');
            
            if (!$flex) {
                $availFlex -= ($width ? $width : $defaultWidth);
            }
            else {
                $totalFlex += $flex;
            }
        }

        foreach ($columns as $idx => $column) {
            $width = $column->getAttribute('width');
            $flex = $column->getAttribute('flex');
            
            if (!$flex) {
                $widths[$idx] = $width ? $width : $defaultWidth;
            }
            else {
                $widths[$idx] = ceil(($flex/$totalFlex) * $availFlex);
            }
        }

        $this->_colWidths = $widths;
        return $widths;
    }
    
    public function getColWidth($position)
    {
        return $this->_colWidths[$position];
    }
    
    public function render(App_Pdf $pdf)
    {
        $element = $this->getElement();
        $this->calculateWidths();
        
        foreach ($element->getElements() as $position => $child) {
            $width = $this->getColWidth($position);
            $child->setAttribute('width', $width);
        }
        
        parent::render($pdf);
    }
}