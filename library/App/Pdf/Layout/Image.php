<?php

class App_Pdf_Layout_Image extends App_Pdf_Layout_Abstract
{
    /**
     * Render image
     * 
     * @param   App_Pdf $pdf 
     */
    public function render(App_Pdf $pdf) 
    {
        $element = $this->getElement();
        
        $image = Zend_Pdf_Image::imageWithPath($element->getContent());
        
        $width = $element->getAttribute('width', $image->getPixelWidth());
        $height = $element->getAttribute('height', $image->getPixelHeight());
        
        // Convert pixels to points
        $width = App_Pdf::pixelsToPoints($width);
        $height = App_Pdf::pixelsToPoints($height);
        
        $pdf->drawImage($image, $element->getX(), $element->getY() - $height, $element->getX() + $width, $element->getY());
        
        $element->moveY($height);
    }
}