<?php

class App_Pdf_Layout_Table extends App_Pdf_Layout_Vbox
{
    public function calculateColspanWidth($idx, $columns, $widths)
    {
        $width = 0;
        
        for ($i = $idx; $i <= ($idx + $columns - 1); $i++) {
            $width += $widths[$i];
        }
        
        return $width;
    }
    
    public function render(App_Pdf $pdf) 
    {
        $element = $this->getElement();
        $cellsStyle = $element->getAttribute('cellStyle', array());
        $headerStyle = $element->getAttribute('headerStyle', array());
        
        // Create header
        $header = $element->columns($element->getColumns());
        $widths = $header->getLayoutObj()->calculateWidths($element->getContentWidth());

        $element->addElement($header);
        
        // Create data
        foreach ($element->getData() as $row) {
            $row['layout'] = 'columns';
            $element->addElement(new App_Pdf_Element_Block($row));
        }

        foreach ($element->getElements() as $rowIdx => $row) {
            $colIdx = 0;
            foreach ($row->getElements() as $column) {
                // Header has widths, so apply styles only
                if (!$rowIdx) {
                    $column->applyStyles($headerStyle, true);
                    continue;
                }
            
                $width = $widths[$colIdx];
                
                $colspan = $column->getAttribute('colspan');
                if ($colspan) {
                    $width = $this->calculateColspanWidth($colIdx, $colspan, $widths);
                    $colIdx += ($colspan - 1);
                }

                $column->applyStyles($cellsStyle, true);
                $column->setAttribute('width', $width);
                
                $colIdx++;
            }
        }
        
        parent::render($pdf);
    }
}