<?php

class App_Pdf_Layout_Hbox extends App_Pdf_Layout_Abstract
{
    /**
     * Render layout
     */
    public function render(App_Pdf $pdf)
    {
        $element = $this->getElement();
        $children = $element->getElements();
        
        if (count($children)) {             
            $x = $element->getX();
            $width = $element->getContentWidth();
            $defaultChildWidth = ($width / count($children));
            $childMaxHeight = 0;
            
            foreach ($children as $child) {
                $childWidth = $child->getAttribute('width', $defaultChildWidth);
                if (is_string($childWidth) && strpos($childWidth, '%')) {
                    $childWidth = ($width * ($childWidth / 100));
                }
                
                $this->drawChild($pdf, $child, array(
                    'x' => $x,
                    'y' => $element->getY(),
                    'width' => $childWidth
                ));

                $childMaxHeight = max($childMaxHeight, $child->getTotalHeight());
                $x += $child->getTotalWidth();
            }
            
            $element->moveY($childMaxHeight);
            
            foreach ($children as $child) {
                $child->setTotalHeight($childMaxHeight);
            }
        }
    }
}