<?php

class App_Date
{
    
    public static function getWeekday($date)
    {
        if (!$date instanceof Zend_Date) {
            $date = new Zend_Date($date);
        }
        $weekday = $date->get(Zend_Date::WEEKDAY_DIGIT);
        
        return $weekday ? $weekday : 7;
    }
    
    public static function minutesToHour($min)
    {
        $min = (int) $min;
        $hours = floor($min / 60);
        $minutes = ($min - ($hours * 60));
        
        if ($hours < 10) {
            $hours = '0' . $hours;
        }
        if ($minutes < 10) {
            $minutes = '0'. $minutes;
        }
        
        return $hours . ':' . $minutes;
    }
    
    public static function hourToMinutes($hour)
    {
        list ($hour, $mins) = explode(':', $hour);
        return ($hour * 60) + $mins;
    }
}