<?php

/**
 * Exception class for image
 *
 * @category   App
 * @package    App_Utility
 * @subpackage Image
 */
class App_Utility_Image_Exception extends Exception { }