<?php

/**
 * @category   App
 * @package    App_Utility
 * @subpackage Image
 */
class App_Utility_Image_Thumbnail
{
    /**
     * Image object
     * @var App_Utility_Image
     */
    protected $_image;

    /**
     * Thumbnail resource
     * @var resource
     */
    protected $_resource;

    /**
     * Thumbnail constructor
     *
     * @param  string|App_Utility_Image $image Path to image file or image object
     * @return void
     */
    public function __construct($image)
    {
        if (is_string($image)) {
            $image = new App_Utility_Image($image);
        }

        if (!$image instanceof App_Utility_Image) {
            throw new App_Utility_Image_Exception("Image must be a valid path or image object.");
        }

        $this->setImage($image);
    }
    
    public function getResource()
    {
        return $this->_resource;
    }

    /**
     * Get image
     *
     * @return App_Utility_Image
     */
    public function getImage()
    {
        return $this->_image;
    }

    /**
     * Set image
     *
     * @param  App_Utility_Image $image
     * @return App_Utility_Image_Thumbnail
     */
    public function setImage(App_Utility_Image $image)
    {
        $this->_image = $image;
        return $this;
    }

    public function setFilter($filter)
    {
        if ($this->_resource) {
            imagefilter($this->_resource, $filter);
        }

        return $this;
    }

    /**
     * Resize image by height
     *
     * @param  int $height
     * @param  boolean $ratio
     * @return boolean
     */
    public function byHeight($height, $ratio = true)
    {
        $height = (int) $height;

        $image = $this->getImage();
        $iwidth = $image->getWidth();
        $iheight = $image->getHeight();

        if ($ratio === false) {
            $width = $iwidth;
        }
        else {
            $rat = $height / $iheight;
            $width = round($rat * $iwidth);
        }

        return $this->_resize($width, $height);
    }

    /**
     * Resize image by width
     *
     * @param  int $width
     * @param  boolean $ratio
     * @return boolean
     */
    public function byWidth($width, $ratio = true)
    {
        $width = (int) $width;

        $image = $this->getImage();
        $iwidth = $image->getWidth();
        $iheight = $image->getHeight();

        if ($ratio === false) {
            $height = $iheight;
        }
        else {
            $rat = $width / $iwidth;
            $height = round($rat * $iheight);
        }
        
        return $this->_resize($width, $height);
    }

    /**
     * Resize image to given size
     *
     * @param  int $width
     * @param  int $height
     * @return boolean
     */
    public function bySize($width, $height)
    {
        return $this->_resize((int) $width, (int) $height);
    }


    public function bySizeLimit($width, $height)
    {
        $width = $rwidth = (int) $width;
        $height = $rheight = (int) $height;
        $iwidth = $this->getImage()->getWidth();
        $iheight = $this->getImage()->getHeight();

        if ($width < $iwidth) {
            $ratio = $width / $iwidth;
            $rheight = round($ratio * $iheight);

            if ($rheight > $height) {
                $ratio = $height / $iheight;
                $rwidth = round($ratio * $iwidth);
                $rheight = $height;
            }
        }
        else if ($rheight < $iheight) {
            $ratio = $rheight / $iheight;
            $rwidth = round($ratio * $iwidth);
        }
        else {
            $rwidth = $iwidth;
            $rheight = $iheight;
        }

        return $this->_resize($rwidth, $rheight);
    }

    public function crop($dx, $dy, $sx, $sy, $dw, $dh, $sw, $sh)
    {
        $image = $this->getImage();

        $im = $image->getResource();
        $tim = imagecreatetruecolor($dw, $dh);

        imagecopyresampled($tim, $im, $dx, $dy, $sx, $sy, $dw, $dh, $sw, $sh);
        imagedestroy($im);

        $this->_resource = $tim;
        return true;
    }

    /**
     *
     * @param <type> $name
     * @param <type> $dir
     * @param <type> $mime
     * @param <type> $quality
     * @return App_Utility_Image 
     */
    public function image($name, $dir = null, $mime = null, $quality = 99)
    {
        $image = $this->getImage();

        if ($dir === null) {
            $dir = $image->getDir();
        }

        if ($mime === null) {
            $mime = $image->getMime();
        }
        
        if ($name) {
            $filename = $dir . DIRECTORY_SEPARATOR . $name;
        }

        switch($mime) {
            case IMAGETYPE_GIF:
                imagegif($this->_resource, $filename);
            break;
            case IMAGETYPE_PNG:
                imagepng($this->_resource, $filename, ($quality > 1 ? 1 : $quality));
            break;
            case IMAGETYPE_JPEG:
                imagejpeg($this->_resource, $filename, $quality);
            break;
        }

        imagedestroy($this->_resource);
        return new App_Utility_Image($filename);
    }

    /**
     * Resize image to given size
     *
     * @param  int $width
     * @param  int $height
     * @return boolean
     */
    protected function _resize($width, $height)
    {
        $image = $this->getImage();

        $im = $image->getResource();
        $tim = imagecreatetruecolor($width, $height);

        imagecopyresampled($tim, $im, 0, 0, 0, 0, $width, $height, $image->getWidth(), $image->getHeight());
        imagedestroy($im);

        $this->_resource = $tim;
        return true;
    }
}