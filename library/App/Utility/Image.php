<?php

/**
 * @category   App
 * @package    App_Utility
 * @subpackage Image
 */
class App_Utility_Image
{
    /**
     * Path to image
     * @var string
     */
    protected $_filename;

    /**
     * Width of image
     * @var int
     */
    protected $_width;

    /**
     * Height of image
     * @var int
     */
    protected $_height;

    /**
     * Mime type of image
     * @var string
     */
    protected $_mime;

    public function __construct($filename = null)
    {
        if (!empty($filename)) {
            $this->load($filename);
        }
    }
    
    /**
     * 
     * @param type $name
     * @return App_Utility_Image
     */
    public static function getUploaded($name)
    {
        if (empty($_FILES[$name]['name'])) {
            return null;
        }
        
        $image = new self();
        return $image->load($_FILES[$name]['tmp_name']);
    }

    /**
     * Load image
     *
     * @return App_Utility_Image
     */
    public function load($filename)
    {
        $image = getimagesize($filename);
        
        if (!$image) {
            return false;
        }
        
        $this->_filename = $filename;
        
        $this->_width = $image[0];
        $this->_height = $image[1];
        $this->_mime = $image[2];

        return $this;
    }

    /**
     * Get path to image
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->_filename;
    }

    /**
     * Get image basename
     *
     * @return string
     */
    public function getBasename()
    {
        return basename($this->_filename);
    }

    /**
     * Get image directory
     *
     * @return string
     */
    public function getDir()
    {
        return dirname($this->getFilename());
    }

    /**
     * Get height of image
     *
     * @return int
     */
    public function getHeight()
    {
        return $this->_height;
    }

    /**
     * Get width of image
     *
     * @return int
     */
    public function getWidth()
    {
        return $this->_width;
    }

    /**
     * Get image mime type
     *
     * @return string
     */
    public function getMime()
    {
        return $this->_mime;
    }

    /**
     * Get image resource
     *
     * @return resource
     */
    public function getResource()
    {
        $opener = '';

        switch ($this->getMime()) {
            case IMAGETYPE_GIF:
                $opener = 'imagecreatefromgif';
            break;
            case IMAGETYPE_PNG:
                $opener = 'imagecreatefrompng';
            break;
            case IMAGETYPE_JPEG:
                $opener = 'imagecreatefromjpeg';
            break;
            default:
                die('ddd');
            break;
        }

        return $opener($this->getFilename());
    }

    /**
     * Check if image is resizable by gd
     *
     * @return boolean
     */
    public function isResizable()
    {
        return in_array($this->getMime(), array(
            IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG
        ));
    }

    /**
     * Create a new thumbnail of image.
     *
     * @param  string[optional] $maker Name of thumbnail maker
     * @param  mixed[optional] $params One or more parameters to pass to maker
     * @return App_Utility_Image_Thumbnail
     */
    public function thumb()
    {
        $thumb = new App_Utility_Image_Thumbnail($this);

        $args = func_get_args();
        if (count($args) > 0) {
            $maker = array_shift($args);
            $method = 'by' . ucfirst($maker);

            if (method_exists($thumb, $maker)) {
                call_user_method_array($method, $thumb, $args);
            }
        }

        return $thumb;
    }
}