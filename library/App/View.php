<?php

class App_View extends Zend_View
{

    public function partial($name)
    {
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');

        return $this->render('partials/' . $name . '.' . $viewRenderer->getViewSuffix());
    }
}
