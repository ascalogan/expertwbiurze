<?php

class App_Pdf
{
    /**
     * 
     * @var Zend_Pdf
     */
    protected $_pdf;
    
    /**
     *
     * @var Zend_Pdf_Page
     */
    protected $_page;
    
    protected $_margins = array(
        'top' => 10,
        'left' => 10,
        'right' => 10
    );
    
    protected $_css = array();
    
    protected $_fonts = array();
    
    protected $_fontPath;
    
    public function __construct($pagesize, $encoding = 'UTF-8') 
    {
        $this->_pdf = new Zend_Pdf();
        $this->_page = new Zend_Pdf_Page($pagesize);
        $this->_pdf->pages[] = $this->_page;
        $this->_root = new App_Pdf_Element_Block();
        
        $this->_fontPath = APPLICATION_PATH . '/data/fonts/';
    }
    
    /**
     *
     * @return  App_Pdf_Element_Block
     */
    public function getRoot()
    {
        return $this->_root;
    }
    
    /**
     * Add fonts
     * 
     * @param  string $name
     * @param  string $path
     * @return App_Pdf 
     */
    public function addFont($name, $path)
    {
        $this->_fonts[$name] = Zend_Pdf_Font::fontWithPath($this->_fontPath . $path);
        return $this;
    }
    
    public function claimFont($name)
    {
        if (isset($this->_fonts[$name])) {
            return $this->_fonts[$name];
        }
        
        return Zend_Pdf_Font::fontWithName($name);
    }
    
    public function widthForStringUsingFontSize($string)
    {
        $font = $this->_page->getFont();
        $fontSize = $this->_page->getFontSize();
        
         $drawingString = iconv('UTF-8', 'UTF-16BE//IGNORE', $string);
         $characters = array();
         for ($i = 0; $i < strlen($drawingString); $i++) {
             $characters[] = (ord($drawingString[$i++]) << 8 ) | ord($drawingString[$i]);
         }
         $glyphs = $font->glyphNumbersForCharacters($characters);
         $widths = $font->widthsForGlyphs($glyphs);
         $stringWidth = (array_sum($widths) / $font->getUnitsPerEm()) * $fontSize;
         return $stringWidth;
     }
     
     public function draw()
     {
         $width = $this->_page->getWidth();
         $y = $this->_page->getHeight() - $this->_margins['top'];
         $x = 0 + $this->_margins['left'];
         $width = $width - $this->_margins['left'] - $this->_margins['right'];
         
         $this->getRoot()->draw($this, array(
             'width' => $width,
             'x' => $x, 'y' => $y
         ), array());
         
         return $this->_pdf->render();
     }
     
     public function __call($name, $arguments)
     {
         return call_user_func_array(array($this->_page, $name), $arguments);
     }
     
     public function addCss($name, $styles)
     {
         $this->_css[$name] = $styles;
     }
     
     public function getCss($name)
     {
         return isset($this->_css[$name]) ? $this->_css[$name] : null;
     }
     
     public static function pixelsToPoints($pixels)
     {
         return round($pixels * 72 / 96);
     }
}