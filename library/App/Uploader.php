<?php

class App_Uploader
{
    /**
     * Input file name
     * @var string
     */
    protected $_name;

    /**
     * Unique uploaded file name
     * @var string
     */
    protected $_uniqueName;

    /**
     * File upload destination
     * @var string
     */
    protected $_destination;

    /**
     * Error message
     * @var string
     */
    protected $_error;

    /**
     * List of thumbnails
     * @var array
     */
    protected $_thumbs;

    /**
     * Flag indicating if thumbs were prepared
     * @var <type>
     */
    protected $_thumbsPrepared;

    /**
     * Uploader constructor
     *
     * @param  string $name
     * @param  string|array $options
     * @return void
     */
    public function __construct($name, $options)
    {
        $this->setName($name);

        if (is_string($options)) {
            $options = array('destination' => $options);
        }

        $this->setOptions($options);
    }

    /**
     * Get input file name
     *
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Set input file name
     *
     * @param  string $name
     * @return App_Uploader
     */
    public function setName($name)
    {
        $this->_name = (string) $name;
        return $this;
    }

    /**
     * Get thumbnails
     * 
     * @return array 
     */
    public function getThumbs()
    {
        return $this->_thumbs;
    }

    /**
     * Set thumbnails
     * 
     * @param  array $thumbs
     * @return App_Uploader 
     */
    public function setThumbs(array $thumbs)
    {
        $this->_thumbsPrepared = false;
        $this->_thumbs = $thumbs;
        return $this;
    }

    /**
     * Check if thumbnails were already prepared
     *
     * @return boolean
     */
    public function thumbsPrepared()
    {
        return $this->_thumbsPrepared === true;
    }

    /**
     * Prepare thumbnails
     *
     * @return void
     */
    protected function _prepareThumbs()
    {
        $thumbs = $this->getThumbs();
        $destination = $this->getDestination();

        if (!empty($thumbs) && !$this->thumbsPrepared()) {
            foreach ($thumbs as $i => $thumb) {
                // Set thumbnail destination to main destination
                if (empty($thumb['destination'])) {
                    $thumb['destination'] = $destination;
                }
                // Thumbnail destionation is set and it is relative to main
                // destination path
                else if (substr($thumb['destination'], 0, 2) == './' && !empty($destination)) {
                    $thumb['destination'] = $destination . substr($thumb['destination'], 2);
                }

                $thumb['destination'] = $this->prepareDirectory($thumb['destination']);
                $thumbs[$i] = $thumb;
            }
            $this->_thumbs = $thumbs;
        }

        $this->_thumbsPrepared = true;
    }



    /**
     * Get unique uploaded file name
     * 
     * @return string
     */
    public function getUniqueName()
    {
        return $this->_uniqueName;
    }

    /**
     * Set unique uploaded file name
     *
     * @param  string $uniqueName
     * @return App_Uploader
     */
    protected function _setUniqueName($uniqueName)
    {
        $this->_uniqueName = $uniqueName;
        return $this;
    }

    /**
     * Get file upload destination
     *
     * @return string
     */
    public function getDestination()
    {
        return $this->_destination;
    }

    /**
     * Set file upload destination
     *
     * @param  string $destination
     * @return App_Uploader
     */
    public function setDestination($destination)
    {
        if (substr($destination, -1) != '/') {
            $destination .= '/';
        }
        $this->_destination = (string) $destination;
        return $this;
    }

    /**
     * Get list of all used directories
     * 
     * @return array
     */
    public function getDirectories()
    {
        $directories = array();
        $destination = $this->getDestination();

        if (!empty($destination)) {
            $directories[] = $destination;
        }

        $this->_prepareThumbs();
        $thumbs = $this->getThumbs();

        if (!empty($thumbs)) {
            foreach ($thumbs as $thumb) {
                if (!in_array($thumb['destination'], $directories)) {
                    $directories[] = $thumb['destination'];
                }
            }
        }

        return $directories;
    }

    /**
     * Gte error message
     * 
     * @return string 
     */
    public function getError()
    {
        return $this->_error;
    }

    /**
     * Set error message
     *
     * @param  string $error
     * @return App_Uploader
     */
    protected function _setError($error)
    {
        $this->_error = $error;
        return $this;
    }

    /**
     * Set uploader options
     *
     * @param  array $options
     * @return App_Uploader
     */
    public function setOptions(array $options)
    {
        foreach ($options as $name => $value)
        {
            $method = 'set' . ucfirst($name);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }

        return $this;
    }

    /**
     * Upload files
     *
     * @return boolean
     */
    public function upload()
    {
        $name = $this->getName();
        $destination = $this->getDestination();

        $directories = $this->getDirectories();

        // Check if directories exists on server
        if (!empty($directories)) {
            foreach ($directories as $directory) {
                if (!$this->_destinationExists($directory)) {
                    $this->_setError('Katalog ' . $directory . ' nie istnieje.');
                    return false;
                }
            }
        }
        // If no directories given, no upload can be processed
        else {
            $this->_setError('Nie podano żadnego katalogu docelowego.');
            return false;
        }

        $fileName = $_FILES[$name]['name'];
        list ($fileType, $fileTypeEx) = explode('/', $_FILES[$name]['type']);
        $sourceDestination = $_FILES[$name]['tmp_name'];
        $newName = null;

        if ($destination) {
            $newName = $this->_uniqueFileName($fileName, $destination);
            $this->_setUniqueName($newName);

            if (!move_uploaded_file($sourceDestination, $destination . $newName)) {
                $this->_setError('Nie udało się wgrać pliku: ' . $destination . $newName);
                return false;
            }
            else {
                $sourceDestination = $destination . $newName;
            }
        }

        $thumbs = $this->getThumbs();

        if ($fileTypeEx == 'pjpeg') $fileTypeEx = 'jpeg';

        if (!empty($thumbs) && $fileType == 'image' && in_array($fileTypeEx, array('jpeg', 'gif', 'png'))) {
            
            $image = new App_Utility_Image($sourceDestination);
            
            foreach ($thumbs as $thumb) {
                if (empty($thumb['scale'])) {
                    if (empty($thumb['ratio'])) {
                        $thumb['scale'] = array('bySize', $thumb['w'], $thumb['h']);
                    }
                    else if ($thumb['ratio'] == 'w' || $thumb['ratio'] == 'h') {
                        $ratio = $thumb['ratio'];
                        $thumb['scale'] = array('by' . ($ratio == 'w' ? 'Width' : 'Height'), $thumb[$ratio]);
                    }
                    else if ($thumb['ratio'] == 'c') {
                        $thumb['scale'] = array('bySizeLimit', $thumb['c'], $thumb['c']);
                    }
                }

                if (empty($thumb['scale'])) {
                    continue;
                }

                $t = $image->thumb();

                if (!$newName) {
                    $newName = $this->_uniqueFileName($fileName, $thumb['destination']);
                    $this->_setUniqueName($newName);
                    $sourceDestination = $thumb['destination'] . $newName;
                }

                $scale = array_shift($thumb['scale']);
                call_user_func_array(array($t, $scale), $thumb['scale']);

                $t->image($newName, $thumb['destination']);
            }
        }

        return true;
    }

    public function prepareDirectory($directory)
    {
        if (substr($directory, -1) != '/') {
            $directory .= '/';
        }
        return $directory;
    }

    public function clean($file)
    {
        $directories = $this->getDirectories();

        $cleaner = new App_Uploader_Cleaner($file, $directories);
        $cleaner->clean();
    }

    protected function _uniqueFileName($name, $destination)
    {
        $uniqueName = $name;

        while (file_exists($destination . $uniqueName)) {
            $uniqueName = uniqid() . $name;
        }

        return $uniqueName;
    }

    protected function _destinationExists($destination)
    {
        return is_dir($destination);
    }
}