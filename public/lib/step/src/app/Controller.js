Ext.define("Step.app.Controller", {
    extend: "Ext.app.Controller",
    
    getApplication: function() {
        return Step.getApplication();
    },
    
    confirm: function(title, message, callback, always) {
        title = title || "Potwierdzenie operacji";
        message = message || "Czy na pewno chcesz wykonać daną operację?";
        
        Ext.Msg.confirm(title, message, function(answer) {
            if (answer == "yes" || always) {
                Ext.Function.bind(callback, this)(answer);
            }
        }, this);
    },
    
    wait: function(message) {
        if (!message || this.waitWin) {
            this.waitWin.close();
            this.waitWin = null;
        }
        
        if (message) {
            this.waitWin = Ext.Msg.wait(message);
        }
    },
    
    hasAccess: function(name) {
        return this.getApplication().hasAccess(name);
    },
    
    callAction: function(action, params) {
        var cfg = {};
        
        if (!Ext.isObject(action)) {
            cfg.action = action;
            cfg.params = params || {};
        }
        else {
            cfg = action;
        }
        
        if (!cfg.controller) {
            cfg.controller = this.id;
        }
        
        this.application.callAction(cfg);
    },
    
    findView: function(name, space) {
        space = space || this.viewSpace;
        return this.getApplication().viewport.findView(name, space);
    },
    
    removeView: function(name, space) {
        space = space || this.viewSpace;
        this.getApplication().viewport.removeView(name, space);
    },
    
    getActiveView: function() {
        var card = this.getApplication().viewport.cardPanel.getLayout().getActiveItem(),
            view = card.getActiveTab();
            
        if (view.xtype == "steptabpanel") {
            view = view.getActiveTab();
        }
        
        return view;
    },
    
    render: function(config) {
        var app = this.getApplication();
        
        if (Ext.isString(config)) {
            config = {xtype: config};
        }
        
        if (!config.space) {
            config.space = this.viewSpace;
        }
        if (!config.itemId) {
            config.itemId = config.xtype;
        }
        
        config.request = this.request;
        this.request = null;
        
        return app.viewport.renderAction(config);
    }
});