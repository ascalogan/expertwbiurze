Ext.define("Step.app.Module", {
    extend: "Ext.app.Application",
    
    constructor: function() {
        this.name = "Module." + this.id.toLowerCase();
        this.appPath = Ext.Loader.config.paths[this.name];
        
        this.callParent(arguments);
    },
    
    callAction: function(params) {
        if (!params.module) {
            params.module = this.id;
        }
        this.getApplication().callAction(params);
    },
    
    getApplication: function() {
        return Step.getApplication();
    }
})