Ext.define("Company.model.WorktimeResource", {
    extend: "Sch.model.Resource",
    proxy: {
        type: "ajax",
        url: "company/sheduler/get-resources",
        reader: {
            type: "json", root: "items"
        }
    }
});