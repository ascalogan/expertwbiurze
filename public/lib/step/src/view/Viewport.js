Ext.define("Step.view.Viewport", {
    extend: "Ext.container.Viewport",
    layout: "border",
    defaults: {
        border: false
    },
    
    initComponent: function() {
        var me = this,
            modActions,
            tbActions= [];
            
        this.cardPanel = Ext.create("Step.card.Panel", {
            region: "center",
            style: "margin-top: 5px"
        });

        me.application.modules.each(function(module) {
            if (module.getActions) {
                modActions = module.getActions();
                if (!modActions) return;
                
                if (!Ext.isArray(modActions)) {
                    modActions = [modActions];
                }
                
                Ext.each(modActions, function(action) {
                    action.module = module.id;
                    me.prepareAction(action);
                    tbActions.push(action);
                });
            }
        });
          
        me.items =  [{
            region: "north",
            xtype: "toolbar",
            items: tbActions
        }, this.cardPanel];
    
        me.callParent();
    },
    
    prepareAction: function(action) {
        if (Ext.isObject(action)) {
            if (action.icon) {
                action.icon = "lib/step/resources/mods/" + action.icon;
                action.iconAlign = "top"
            }
            action.scale = "large";
            action.handler = this.handleAction;
            action.scope = this;
            
            if (0 && action.space) {
                this.cardPanel.add({
                    xtype: "tabpanel",
                    itemId: action.module
                })
            }
        }
    },
    
    handleAction: function(action) {
        this.application.callAction(action);
    },
    
    findView: function(name, space) {
        var card = this.cardPanel.getLayout().parseActiveItem(space);
        
        return card.getLayout().parseActiveItem(name);
    },
    
    removeView: function(name, space) {
        var card = this.cardPanel.getLayout().parseActiveItem(space),
            view = card.getLayout().parseActiveItem(name);
            
        if (view) {
            card.remove(view);
        }
    },
    
    renderAction: function(config) {
        var cards = this.cardPanel,
            tabs = cards.getLayout().parseActiveItem(config.space);
            
        if (!tabs) {
            tabs = cards.addCard(config.space);
        }
        
        var view = tabs.getLayout().parseActiveItem(config.itemId);
        
        if (!view) {
            view = tabs.add(config);
            tabs.setActiveTab(view);
        }
        else if (!view.isMainView) {
            tabs.setActiveTab(view);
        }
        
        cards.setActiveItem(tabs);
        
        return view;
    }
});