Ext.define("Step.window.Form", {
    extend: "Ext.window.Window",
    alias: "widget.formwindow",
    
    layout: "fit",
    
    require: [
        "Step.form.Panel"
    ],
    
    width: "60%",
    modal: true,
    autoShow: true,
    
    submitBtnText: "zapisz",
    submitBtnCls: "icon-save",
    cancelBtnText: "anuluj",
    cancelBtnCls: "icon-cancel",
    
    initComponent: function() {
        var me = this,
            items = me.items,
            hideButtons = me.hideButtons === true;
            
        if (me.form) {
            me.items = [Ext.apply({
                bodyPadding: me.isForm === false ? 0 : 20,
                hideDefaultButtons: !hideButtons,
                submitBtnText: me.submitBtnText,
                submitBtnCls: me.submitBtnCls,
                cancelBtnText: me.cancelBtnText,
                cancelBtnCls: me.cancelBtnCls,
                values: me.values
            }, me.form)];
        }
        else {
            me.items = [{
                xtype: "cform",
                bodyPadding: 20,
                url: me.url,
                items: items,
                values: me.values,
                hideDefaultButtons: !hideButtons,
                submitBtnText: me.submitBtnText,
                submitBtnCls: me.submitBtnCls,
                cancelBtnText: me.cancelBtnText,
                cancelBtnCls: me.cancelBtnCls
            }];
        }
    
        if (!hideButtons) {
            me.buttons = [{
                text: me.submitBtnText,
                iconCls: me.submitBtnCls,
                handler: me.doSubmit,
                scope: me
            }, {
                text: me.cancelBtnText,
                iconCls: me.cancelBtnCls,
                handler: me.close,
                scope: me
            }];
        }
        
        if (!me.height) {
            me.maxHeight = Ext.getBody().getHeight() - 50;
        }
    
        me.callParent();
        
        if (me.getForm())
        me.relayEvents(me.getForm(), ["beforeaction", "submit"]);
    },
    
    getForm: function() {
        return this.down("cform");
    },
    
    loadRecord: function(record) {
        this.getForm().loadRecord(record);
    },
    
    doSubmit: function() {
        var me = this,
            form = me.getForm();
            
        form.doSubmit();
    }
})