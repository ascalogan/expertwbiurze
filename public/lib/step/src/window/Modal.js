Ext.define("Step.window.Modal", {
    extend: "Ext.window.Window",
    alias: "widget.windowmodal",
    
    layout: "fit",
  
    width: "60%",
    modal: true,
    autoShow: true,
    
    initComponent: function() {
        var me = this;
            
        me.items = [{
            itemId: me.tabId,
            xtype: "steptabpanel",
            items: me.items
        }];

        if (!me.height) {
            me.maxHeight = Ext.getBody().getHeight() - 50;
        }
    
        me.callParent();
    }
})