Ext.define("Company.App", {
    extend: "Ext.app.Application",
    name: "Company",
    appFolder: "lib/company/src",
    
    controllers: [
        "Calendar",
        "Scheduler",
        "Clients",
        "Employees",
        "Settings"
    ],
    
    launch: function() {
        var me = this;
        
        me.cardPanel = Ext.create("Ext.panel.Panel", {
            region: "center",
            layout: "card",
            style: "margin-top: 2px"
        });
        
        Ext.Ajax.defaultHeaders = {
            CompanyID: this.companyId
        }
        Ext.tip.QuickTipManager.init();
        Ext.create("Ext.container.Viewport", {
            layout: "border",
            defaults: {
                border: false
            },
            items: [{
                region: "north",
                xtype: "toolbar",
                items: [{
                    text: "Czas pracy",
                    scale: "large",
                    icon: "lib/company/resources/icons/watch.png",
                    iconAlign: "top",
                    tooltip: "Zarządzaj czasem pracy pracowników",
                    handler: function() {
                        me.showMain("Scheduler")
                    }
                }, {
                    text: "Rezerwacje",
                    scale: "large",
                    icon: "lib/company/resources/icons/lock_open_icon.png",
                    iconAlign: "top",
                    tooltip: "Lista rezerwacji",
                    handler: function() {
                        me.showMain("Calendar")
                    }
                }, {
                    text: "Klienci",
                    scale: "large",
                    icon: "lib/company/resources/icons/users_icon.png",
                    iconAlign: "top",
                    tooltip: "Zarządzaj swoimi klientami",
                    handler: function() {
                        me.showMain("Clients")
                    }
                }, {
                    text: "Pracownicy",
                    scale: "large",
                    icon: "lib/company/resources/icons/user_icon.png",
                    tooltip: "Zarządzaj swoimi pracownikami",
                    iconAlign: "top",
                    handler: function() {
                        me.showMain("Employees");
                    }
                }, {
                    text: "Ustawienia",
                    scale: "large",
                    icon: "lib/company/resources/icons/wrench_icon.png",
                    iconAlign: "top",
                    tooltip: "Definiuj dostępne usługi, godziny otwarcia i inne",
                    handler: function() {
                        me.showMain("Settings")
                    }
                }, "->", {
                    text: "Wyjdź",
                    scale: "large",
                    icon: "lib/company/resources/icons/on_off_icon.png",
                    iconAlign: "top",
                    tooltip: "Wróć do portalu",
                    style: "margin-right: 5px",
                    handler: function() {
                        window.location.href = me.baseUrl + "/company/list";
                    }
                }]
            }, me.cardPanel]
        });
    },
    
    showMain: function(name) {
        var container = this.cardPanel,
            item = container.getLayout().parseActiveItem(name);
            
        if (item) {
            container.getLayout().setActiveItem(item);
        }
        else {
            item = this.getController(name).main();
            item.itemId = name;
            container.getLayout().setActiveItem(item);
        }
    },
    
    confirm: function(title, msg, callback) {
        Ext.Msg.confirm(
            title || "Potwierdzenie operacji",
            msg,
            function(answer) {
                if (answer == "yes") {
                    callback();
                }
            }
        );
    },
    
    request: function(conf, callback) {
        var me = this,
            waitWin;
            
        if (conf.message) {
            waitWin = Ext.Msg.wait(conf.message);
        }
        
        conf.success = function(response) {
            var data = Ext.decode(response.responseText);
           
            if (callback) {
                callback(data, data.success === true);
            }
            
            if (waitWin) {
                waitWin.close();
            }
        }
        
        Ext.Ajax.request(conf);
    }
})