Ext.define("Step.tab.Panel", {
    extend: "Ext.tab.Panel",
    alias: "widget.steptabpanel",
    
    cls: "steptabpanel",
    
    initComponent: function() {
        this.on("tabchange", function(tabs, tab) {
            var name = tab.itemId ? tab.itemId : tab.xtype;
            this.fireEvent(name, tabs, tab);
        }, this);
        
        this.callParent();
        
        if (this.activeTab) {
            this.fireEvent(this.getActiveTab().xtype, this, this.getActiveTab());
        }
    },
    
    getActiveTabIndex: function() {
        return this.items.indexOf(this.getActiveTab());
    },
    
    getStateParams: function() {
        return {tab: this.getActiveTabIndex()};
    }
});