Ext.define("Step.Login", {
    extend: "Ext.window.Window",
    
    title: false,
    bodyBorder: false,
    closable: false,
    width: 600,
    cls: "login-window",
    
    autoShow: true,
    
    constructor: function(config) {
        this.appOptions = config;
        this.callParent();
    },
    
    initComponent: function() {
        var me = this;
        
        me.items = [{
            xtype: "component",
            autoEl: {
                tag: "h1",
                html: me.appOptions.siteName + " - logowanie",
                cls: "login-window-header"
            },
            border: false,
            bodyBorder: false
        }, {
            layout: "hbox",
            border: false,
            bodyStyle: "padding: 10px; background: transparent;",
            items: [{
                xtype: "image",
                width: 128,
                height: 128,
                src: "http://blitz-cleaning.com/library/step/resources/icons/login.png"
            }, {
                flex: 1,
                xtype: "form",
                bodyStyle: "padding: 10px",
                layout: "anchor",
                api: {
                    submit: Lite.doLogin
                },
                defaults: {
                    anchor: "100%",
                    labelWidth: 100
                },
                items: [{
                    xtype: "component",
                    style: "font-style: italic; margin-bottom: 20px",
                    html: "Dostęp do części administracyjnej systemu wymaga uwierzytelnienia. Podaj swój login oraz hasło, aby się zalogować."
                }, {
                    xtype: "textfield",
                    name: "login",
                    fieldLabel: "Podaj login",
                    allowBlank: false
                }, {
                    xtype: "textfield",
                    name: "password",
                    fieldLabel: "Podaj hasło",
                    allowBlank: false,
                    inputType: "password"
                }],
                buttons: [{
                    text: "Zaloguj się",
                    iconCls: "icon-accept",
                    iconAlign: "right",
                    handler: me.submit,
                    scope: me
                }]
            }]
        }];
        
        me.callParent();
    },
    
    afterRender: function() {
        var me = this;
        me.keyNav = Ext.create("Ext.util.KeyNav", me.el, {
            enter: me.submit,
            scope: me
        });
        
        me.callParent(arguments);
    },
    
    submit: function() {
        var me = this,
            form = me.down("form");
            
        if (form.form.isValid()) {
            form.form.submit({
                success: function(a, b) {
                    window.location.href = "step/panel";
                },
                failure: function() {
                    Ext.Msg.alert("Nieprawidłowe dane", "Podane dane nie są prawidłowe");
                }
            })
        }
    }
});