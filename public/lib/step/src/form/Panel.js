Ext.define("Step.form.Panel", {
    extend: "Ext.form.Panel",
    alias: "widget.cform",
    
    layout: "anchor",
    bodyStyle: "padding: 20px",
    defaults: {
        labelWidth: 150,
        anchor: "90%",
        msgTarget: "under"
    },
    autoScroll: true,
    values: {},
    
    submitBtnText: "zapisz",
    submitBtnCls: "icon-save",
    cancelBtnText: "anuluj",
    cancelBtnCls: "icon-cancel",
    
    initComponent: function() {
        var me = this;
        
        if (!this.values) {
            this.values = {};
        }
        
        if (this.hideDefaultButtons !== true) {
            this.tbar = [{
                text: this.submitBtnText,
                iconCls: this.submitBtnCls,
                handler: this.doSubmit,
                scope: this
            }];
        
            if (this.hideCancelButton !== true) {
                this.tbar.push({
                    text: this.cancelBtnText,
                    iconCls: this.cancelBtnCls
                })
            }
        }
        
        if (this.buttons) {
            if (!me.tbar) {
                me.tbar = [];
            }
            Ext.each(this.buttons, function(button) {
                me.tbar.push(button);
            });
            this.buttons = null;
        }
        
        if (this.tbar) {
            Ext.each(this.tbar, function(item) {
                if (!Ext.isString(item)) {
                    item.scale = "medium";
                }
            })
        }
        
        this.callParent();
        
        if (this.values) {
            this.getForm().setValues(this.values);
        }
    },
    
    doSubmit: function(callback) {
        var me = this,
            form = me.form;
        
        if (form.isValid()) {
            var win = Ext.Msg.wait("Trwa zapisywanie...");
            
            form.submit({
                success: function(form, result) {
                    win.close();
                    
                    me.fireEvent("submit", result.result, me);
                    if (me.successMessage) {
                        Ext.Msg.alert(me.successMessage);
                    }
                    if (callback && Ext.isFunction(callback)) {
                        callback(result);
                    }
                },
                failure: function() {
                    win.close();
                }
            })
        }
    }
});