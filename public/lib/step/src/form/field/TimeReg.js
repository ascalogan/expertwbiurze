Ext.define("Company.form.field.TimeReg", {
    extend: "Ext.form.FieldContainer",
    alias: "widget.timeregfield",
    
    value: "00:00",
    enableKeyEvents: true,
    minIncrement: 5,
    hourStart: 0,
    hourEnd: 23,
    layout: "hbox",
    
    mixins: {
        field: "Ext.form.field.Field"
    },
    
    initComponent: function() {
        var me = this,
            hoursData = [],
            minutesData = [],
            i;
            
        for (i = me.hourStart; i <= me.hourEnd; i++) {
            hoursData.push({hour: i < 10 ? "0" + i : i});
        }
        for (i = 0; i <= 59; i += me.minIncrement) {
            minutesData.push({minutes: i < 10 ? "0" + i : i});
        }
        
        me.hoursField = Ext.create("Ext.form.field.ComboBox", {
            name: me.name + "_hours",
            allowBlank: me.allowBlank,
            anchor: "40%",
            isField: false,
            valueField: "hour",
            displayField: "hour",
            forceSelection: true,
            value: "00",
            submitValue: false,
            width: 100,
            store: {
                fields: [{name: "hour", type: "string"}],
                data: hoursData
            }
        });
        me.minutesField = Ext.create("Ext.form.field.ComboBox", {
            name: me.name + "_minutes",
            allowBlank: me.allowBlank,
            valueField: "minutes",
            isField: false,
            displayField: "minutes",
            forceSelection: true,
            value: "00",
            width: 100,
            submitValue: false,
            store: {
                fields: [{name: "minutes", type: "string"}],
                data: minutesData
            }
        });
        
        me.items = [
            me.hoursField,
            {xtype: "component", html: ":"},
            me.minutesField
        ];
        
        me.callParent();
        
        me.initField();
    },
    
    setValue: function(value) {
        if (!value) return;
        
        var parts = value.split(':');
        
        this.hoursField.setValue(parts[0]);
        this.minutesField.setValue(parts[1]);
    },
    
    getValue: function() {
        return this.hoursField.getValue() + ":" + this.minutesField.getValue();
    }
});