Ext.define("Step.form.field.ComboGroup", {
    extend: "Ext.form.field.ComboBox",
    alias: "widget.combogroup",
    
    initComponent: function() {
        var me = this;
        
        me.tpl =    '<tpl for=".">' + 
                    '<tpl if="isGroup">' + 
                        '<div class="x-bound-item-group" style="padding: 2px" data-group="{group}">{name}</div>' + 
                    '</tpl>' + 
                    '</tpl>' + 
        '<div style="border-bottom: 1px solid #000"></div>' + 
        '<tpl for="."><tpl if="!isGroup">' + me.tpl + '</tpl></tpl>';
                
                
        me.callParent();
    },
    
    createPicker: function() {
        var me = this,
            picker = this.callParent();
        
        picker.on("afterrender", function() {
            if (me.getValue()) {
                var store = me.getStore(),
                    record = store.findRecord(me.valueField, me.getValue());
                    console.debug(record);
                if (record) {
                    me.filterGroups(record.get("group"));
                }
            }
            
            picker.getTargetEl().on("click", function(e) {
                var el = e.getTarget(".x-bound-item-group");
                
                if (el) {
                    me.getStore().filterBy(function(record) {
                        return record.get("isGroup") || record.get("group") == el.getAttribute("data-group");
                    });
                }
            })
        })
        
        return picker;
    },
    
    filterGroups: function(group) {
        this.getStore().filterBy(function(record) {
            return record.get("isGroup") || record.get("group") == group;
        });
    }
});