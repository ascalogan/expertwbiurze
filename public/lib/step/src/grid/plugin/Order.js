Ext.define("Step.grid.plugin.Order", {
    extend: "Ext.AbstractPlugin",
    alias: "plugin.gridorder",
    
    ddPluginId: null,
    btnIconCls: "icon-order",
    btnIconText: "zapisz kolejność",
    tipText: "Kolejność elementów została zmieniona.",
    orderField: "order",
    
    init: function(grid) {
        var tb = grid.getDockedComponent(1);
        
        tb.add("->");
        this.button = tb.add({
            text: this.btnIconText, 
            iconCls: this.btnIconCls, 
            disabled: true,
            scale: grid.buttonsScale,
            handler: this.saveOrder,
            scope: this
        });
        
        this.store = grid.getStore();
        
        grid.getView().on("drop", this.onOrderChange, this);
    },
    
    onOrderChange: function() {
        var tip = this.getTip();
        
        this.button.enable();
        tip.showAt([this.button.getX(), this.button.getY() + this.button.getHeight() + 5]);
        
    },
    
    getTip: function() {
        if (!this.tip) {
            this.tip = Ext.create("Ext.tip.ToolTip", {
                target: this.button.el,
                html: this.tipText
            });
        }
        
        return this.tip;
    },
    
    saveOrder: function() {
        var items = [];
        
        this.getTip().hide();
        this.button.disable();
        
        this.store.each(function(item, i) {
            items.push({id: item.get("id"), order: i});
        });
        
        if (Ext.isFunction(this.writer)) {
            this.writer(items);
        }
    }
});