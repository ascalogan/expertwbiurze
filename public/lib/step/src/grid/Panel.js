Ext.define("Step.grid.Panel", {
    extend: "Ext.grid.Panel",
    alias: "widget.stepgrid",
    
    buttonsScale: "medium",
    
    initComponent: function() {
        var me = this;
        
        if (this.tbar) {
            if (!this.skipRefreshButton) {
                this.tbar.push("->");
                
                this.tbar.push({
                    text: "odśwież listę",
                    handler: function() {
                        me.getStore().reload();
                    }
                });
            }
            Ext.each(this.tbar, function(item) {
                if (!Ext.isString(item)) {
                    item.scale = me.buttonsScale
                }
            });
        }
        
        me.callParent();
    },
    
    bindItemAction: function(action, params) {
        return Ext.Function.bind(function(button) {
            if (this.getSelectedItem()) {
                this.fireEvent(action, this, this.getSelectedItem(), button, params);
            }
        }, this);
    },
    
    bindAction: function(action) {
        return Ext.Function.bind(function(button) {
            this.fireEvent(action, this, button);
        }, this);
    },
    
    getSelectedItem: function() {
        var items = this.getSelectionModel().getSelection();
        
        return items[0];
    }
})