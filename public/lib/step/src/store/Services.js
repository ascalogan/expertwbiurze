Ext.define("Company.store.Services", {
    extend: "Ext.data.Store",
    alias: "store.services",
    groupField: "group",
    
    fields: [{
        name: "id", type: "int"
    }, {
        name: "name", type: "string"
    }, {
        name: "type", type: "string"
    }, {
        name: "group", type: "string", convert: function(v, r) {
            return r.get("type") == "internal" ? "Usługi systemowe" : "Usługi własne";
        }
    }, {
        name: "service_id", type: "int"
    }, {
        name: "provided", type: "boolean"
    }, {
        name: "time", type: "int"
    }, {
        name: "price", type: "int"
    }, {
        name: "color", type: "string"
    }],

    proxy: {
        type: "ajax",
        api: {
            read: "company/settings/get-services",
            create: "company/settings/save-services",
            update: "company/settings/save-services"
        },
        reader: {
            type: "json", root: "items"
        },
        writer: {
            type: "json", root: "items", encode: true
        }
    },
    
    autoLoad: true,
    autoSync: true
});