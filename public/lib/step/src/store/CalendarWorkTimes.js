Ext.define("Company.store.CalendarWorkTimes", {
    extend: "Extensible.calendar.data.EventStore",
    
    proxy: {
        type: "ajax",
        api: {
            read: "company/get-calendar-worktimes",
            create: "lala",
            update: "update",
            destroy: "destroy"
        },
        reader: {type: "json", root: "items"}
    }
});