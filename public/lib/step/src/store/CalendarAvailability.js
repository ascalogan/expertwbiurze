Ext.define("Company.store.CalendarAvailability", {
    extend      : 'Sch.data.EventStore',

    proxy       : {
        type    : 'ajax',
        url     : 'company/sheduler/get-events',
        reader  : { type : 'json', root: "items" }
    },

    // A method which scans the availability intervals for a resource to see if it is available
    isResourceAvailable : function(resource, start, end) {
        var availability = this.getEventsForResource(resource);
        
        if (!availability || availability.length === 0) return false;

        for (var i = 0, l = availability.length; i < l; i++) {
            // Check if there is an availability block completely encapsulating the passed start/end timespan
            if (Sch.util.Date.timeSpanContains(availability[i].getStartDate(), availability[i].getEndDate(), start, end)) {
                return true;
            }
        }

        return false; 
    }
});