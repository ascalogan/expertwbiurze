Ext.define("Company.store.CalendarEvents", {
    extend: "Extensible.calendar.data.EventStore",
    
    proxy: {
        type: "ajax",
        api: {
            read: "company/get-events"
        },
        reader: {type: "json", root: "items"}
    }
});