Ext.define("Company.store.EmployeesRolesTree", {
    extend: "Ext.data.TreeStore",
    
    fields: [{
        name: "id", type: "int"
    }, {
        name: "name", type: "string"
    }, {
        name: "surname", type: "string"
    }, {
        name: "text", type: "string"
    }],

    proxy: {
        type: "ajax",
        url: "company/employees/get-roles-list",
        reader: {type: "json", root: "items"}
    }
});