Ext.define("Company.store.EmployeeServices", {
    extend: "Ext.data.Store",
    alias: "store.employeeservices",
    
    fields: [{
        name: "id", type: "int"
    }, {
        name: "service_id", type: "int"
    }, {
        name: "name", type: "string"
    }, {
        name: "provided", type: "boolean"
    }],

    proxy: {
        type: "ajax",
        api: {
            read: "company/employees/get-services",
            create: "company/employees/save-services",
            update: "company/employees/save-services"
        },
        reader: {
            type: "json", root: "items"
        },
        writer: {
            type: "json", root: "items", encode: true
        }
    }
});