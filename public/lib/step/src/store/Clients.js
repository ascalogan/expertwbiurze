Ext.define("Company.store.Clients", {
    extend: "Ext.data.Store",
    
    fields: [{
        name: "id", type: "int"
    }, {
        name: "name", type: "string"
    }, {
        name: "surname", type: "string"
    }, {
        name: "city", type: "string"
    }, {
        name: "phone", type: "string"
    }, {
        name: "mail", type: "string"
    }, {
        name: "visits", persist: false, type: "int"
    }],

    proxy: {
        type: "ajax",
        api: {
            read: "company/clients/get-clients",
            create: "company/clients/save-clients",
            update: "company/clients/save-clients",
            destroy: "company/clients/remove-clients"
        },
        reader: {
            type: "json", root: "items"
        },
        writer: {
            type: "json", root: "items", encode: true
        }
    },
    
    autoLoad: true
});