Ext.define("Company.store.EmployeesTree", {
    extend: "Ext.data.TreeStore",
    
    fields: [{
        name: "id", type: "int"
    }, {
        name: "name", type: "string"
    }, {
        name: "surname", type: "string"
    }, {
        name: "text", type: "string"
    }],

    proxy: {
        type: "ajax",
        url: "company/employees/get-tree-list",
        reader: {type: "json", root: "items"}
    }
});