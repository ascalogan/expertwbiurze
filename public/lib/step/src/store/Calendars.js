Ext.define("Company.store.Calendars", {
    extend: "Ext.data.Store",
    model: "Extensible.calendar.data.CalendarModel",
    autoLoad: true,
    
    proxy: {
        type: "ajax",
        api: {
            read: "company/get-calendars-employees",
            update: "lal",
            create: "create"
        },
        reader: {type: "json", root: "items"}
    }
});