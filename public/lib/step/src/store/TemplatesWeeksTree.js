Ext.define("Company.store.TemplatesWeeksTree", {
    extend: "Ext.data.TreeStore",
    
    fields: [{
        name: "id", type: "int"
    }, {
        name: "name", type: "string"
    }, {
        name: "text", type: "string"
    }, {
        name: "leaf", type: "int", convert: function() {return true;}
    }],

    proxy: {
        type: "ajax",
        url: "company/sheduler/get-templates-weeks-tree",
        reader: {type: "json", root: "items"}
    }
});