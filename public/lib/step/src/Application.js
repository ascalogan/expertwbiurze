Ext.define("Step.Application", {
    extend: "Ext.app.Application",
    
    name: "Step",
    appFolder: "lib/step/src",
    enableQuickTips: true,
    
    modules: [
        "Clients",
        "Products",
        "Orders",
        "Content",
        "Users",
        "Lite"
    ],
    
    requires: ["Step.tab.Panel"],
    
    constructor: function(params) {
        var me = this;
        
        me.privilages = params.privilages;
        
        Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
        this.callParent(arguments);
        
        var modules = new Ext.util.MixedCollection(),
            modulesClasses = [],
            module;
            
        Ext.each(this.modules, function(moduleName) {
            //Ext.Loader.setPath("Module." + moduleName.toLowerCase(), "step/asset?module=" + moduleName.toLowerCase() + "&path=");
            //Ext.Loader.setPath("Module." + moduleName.toLowerCase(), "../application/modules/" + moduleName.toLowerCase() + "/assets");
            Ext.Loader.setPath("Module." + moduleName.toLowerCase(), "modules/" + moduleName.toLowerCase() + "/assets");
            modulesClasses.push("Module." + moduleName.toLowerCase() + "." + moduleName);
        });
        
        Ext.require(modulesClasses, function() {
            Ext.each(me.modules, function(moduleName) {
               
                module = Ext.create("Module." + moduleName.toLowerCase() + "." + moduleName);
                modules.add(module);
            });
            
            me.modules = modules;
            me.initViewport();
            
            
            me.actions = [];
        
            if (Ext.util.Cookies.get("liteStepActions")) {
                //Ext.util.Cookies.set("liteStepActions", Ext.encode(this.actions));
                var actions = Ext.decode(Ext.util.Cookies.get("liteStepActions"));
                
                Ext.each(actions, function(a) {
                    me.callAction(a);
                });
            }
        });
        
        window.onbeforeunload = function() {
            var card = (me.viewport.cardPanel.getLayout().getActiveItem()),
                actions = [],
                request, items;
        
            if (!card) {
                return;
            }
            
            items = card.getLayout().getLayoutItems();
                
            Ext.each(items, function(item) {
                request = item.request;

                if (item.getStateParams) {
                    request.params = Ext.apply(request.params, item.getStateParams());
                    /*request.params = Ext.apply(request.params, {
                        tab: item.items.indexOf(item.getActiveTab())
                    });*/
                }

                actions.push(request)
            });
            
            Ext.util.Cookies.set("liteStepActions", Ext.encode(actions));
        }
    },
    
    hasAccess: function(name) {
        return this.privilages.indexOf(name) != -1;
    },
    
    initViewport: function() {
        this.viewport = Ext.create("Step.view.Viewport", {application: this});
    },
    
    getModule: function(name) {
        return this.modules.get(name);
    },
    
    callAction: function(action) {
        var a = {
            module: action.module,
            controller: action.controller,
            action: action.action || "main",
            params: action.params || {}
        },  module = this.getModule(a.module),
            controller = module.getController(a.controller);
            
        controller.request = a;
        controller[a.action](a.params);
    }
});

Ext.data.proxy.Server.override({
    encodeFilters: function(filters) {
        var min = [],
            length = filters.length,
            i = 0;

        for (; i < length; i++) {
            min[i] = {
                property: filters[i].property,
                value   : filters[i].value,
                type    : filters[i].type
            };
        }
        return this.applyEncoding(min);
    }
});

if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length >>> 0;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

Ext.form.field.Base.override({
    initComponent: function() {
        if (this.allowBlank === false) {
            this.labelSeparator += '*';
        }
        this.callOverridden();
    },
    onRender: function() {
        this.callOverridden(arguments);
        
        if (this.description) {
            if (1 || this.xtype == "checkboxfield" || this.xtype == "textareafield") {
                this.el.down('.x-form-item-body').createChild({tag: "span", style: {marginLeft: "10px", fontStyle: "italic"}, html: this.description});
            }
            else {
                this.el.down('.x-form-item-body').set({'data-qtip': this.description})
            }
        }
    }
});