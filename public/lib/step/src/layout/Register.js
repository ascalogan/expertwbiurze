Ext.define("Company.layout.Register", {
    init: function(config) {
        var me = this;
        me.baseUrl = config.baseUrl;
        
        this.win = Ext.create("Ext.window.Window", {
            closable: false,
            autoShow: true,
            title: false,
            cls: "login-window",
            width: 600,
            items: [{
                xtype: "component",
                autoEl: {
                    tag: "h1",
                    html: "Rejestracja nowej firmy",
                    cls: "login-window-header"
                },
                border: false,
                bodyBorder: false
            }, {
                layout: "hbox",
                border: false,
                bodyStyle: "padding: 10px; background: transparent;",
                items: [{
                    xtype: "image",
                    width: 128,
                    height: 128,
                    src: "lib/company/resources/icons/register-icon.png"
                }, {
                    flex: 1,
                    xtype: "form",
                    bodyStyle: "padding: 10px",
                    layout: "anchor",
                    url: "company/index/do-register",
                    defaults: {
                        anchor: "100%",
                        labelWidth: 100
                    },
                    items: [{
                        xtype: "textfield",
                        name: "display_name",
                        fieldLabel: "Nazwa własna",
                        allowBlank: false,
                        description: "Nazwa własna nie jest wyświetlanie nigdzie poza Twoim panelem"
                    }, {
                        xtype: "textfield",
                        name: "name",
                        fieldLabel: "Nazwa firmy",
                        allowBlank: false
                    }, {
                        xtype: "combo",
                        name: "industry_id",
                        fieldLabel: "Wybierz branżę",
                        displayField: "name",
                        valueField: "id",
                        store: {
                            fields: ["id", "name"],
                            proxy: {
                                type: "ajax",
                                reader: {type: "json", root: "items"},
                                url: "company/index/get-industries"
                            },
                            autoLoad: true
                        },
                        editable: false
                    }, {
                        xtype: "textfield",
                        name: "gma",
                        fieldLabel: "Wpisz adres firmy",
                        allowBlank: false,
                        listeners: {
                            afterrender: this.initGA,
                            scope: this
                        }
                    }, {
                        xtype: "textfield",
                        name: "street",
                        fieldLabel: "Ulica i nr lokalu",
                        allowBlank: false,
                        readOnly: true
                    }, {
                        xtype: "textfield",
                        name: "city",
                        fieldLabel: "Miejscowość",
                        allowBlank: false,
                        readOnly: true
                    }, {
                        xtype: "hidden",
                        name: "latitude"
                    }, {
                        xtype: "hidden",
                        name: "longitude"
                    }],
                    buttons: [{
                        text: "Zarejestruj firmę",
                        iconAlign: "right",
                        handler: this.submit,
                        scope: this
                    }, {
                        text: "Logowanie do firmy",
                        handler: function() {
                            window.location.href = me.baseUrl + "/company/list";
                        }
                    }, {
                        text: "Wyloguj się",
                        handler: function() {
                            window.location.href = me.baseUrl + "/company/index/logout";
                        }
                    }]
                }]
            }]
        });
    },
    submit: function() {
        var me = this,
            form = this.win.down("form");
        
        if (form.form.isValid()) {
            form.getForm().submit({
                success: function(form, r) {
                    window.location.href = me.baseUrl + "/company/id/" + r.result.companyId;
                }
            })
        }
    },
    initGA: function(field) {
        var options = {types: ["geocode"], componentRestrictions: {country: "PL"}};
        this.gaInstance = new google.maps.places.Autocomplete(document.getElementById(field.id + "-inputEl"), options);
        google.maps.event.addListener(this.gaInstance, "place_changed", Ext.Function.bind(this.onPlaceChoose, this));
    },
    onPlaceChoose: function() {
        var me = this,
            place = this.gaInstance.getPlace(),
            form = this.win.down("form").getForm(),
            location;

        if (place) {
            location = this.makeLocation(place);
            
            form.findField("street").setValue(location.street + " " + location.street_number);
            form.findField("city").setValue(location.city);
            form.findField("latitude").setValue(location.latitude);
            form.findField("longitude").setValue(location.longitude);
            
            return;
            Ext.get(me.addressFieldId + "_display_value").update(location.street + " " + location.street_number);
            Ext.get(me.addressFieldId).dom.value = location.street + " " + location.street_number;
            Ext.get(me.cityFieldId + "_display_value").update(location.city);
            Ext.get(me.cityFieldId).dom.value = location.city;
            Ext.get(me.latFieldId).dom.value = location.latitude;
            Ext.get(me.lngFieldId).dom.value = location.longitude;
        }
    },
    makeLocation: function(result) {
        var me = this,
            location = {
                latitude: result.geometry.location.lat(),
                longitude: result.geometry.location.lng(),
                address: result.formatted_address
            };
            
        Ext.each(result.address_components, function(component) {
            if (me.isComponent(component, "street_number")) {
                location.street_number = component.long_name;
            }
            else if (me.isComponent(component, "route")) {
                location.street = component.long_name;
            }
            else if (me.isComponent(component, ["political", "locality"])) {
                location.city = component.long_name;
            }
            else if (me.isComponent(component, "administrative_area_level_1")) {
                location.province = component.long_name;
            }
        });
        
        location.address = "";
        if (location.street) {
            location.address += location.street;
            if (location.street_number) {
                location.address += " " + location.street_number;
            }
            location.address += ", ";
        }
        if (location.city) {
            location.address += location.city + ", ";
        }
        if (location.province) {
            location.address += location.province;
        }
        
        return location;
    },
    isComponent: function(component, types) {
        var isValid = true;
        
        if (Ext.isString(types)) {
            types = [types];
        }
        
        Ext.each(types, function(type) {
            if (!Ext.Array.contains(component.types, type)) {
                isValid = false;
            }
        });
        
        return isValid;
    }
});