Ext.define("Company.layout.SignIn", {
    init: function(config) {
        var me = this;
        me.baseUrl = config.baseUrl;
        
        this.win = Ext.create("Ext.window.Window", {
            closable: false,
            autoShow: true,
            title: false,
            cls: "login-window",
            width: 600,
            items: [{
                xtype: "component",
                autoEl: {
                    tag: "h1",
                    html: "Rejestracja użytkownika",
                    cls: "login-window-header"
                },
                border: false,
                bodyBorder: false
            }, {
                layout: "hbox",
                border: false,
                bodyStyle: "padding: 10px; background: transparent;",
                items: [{
                    xtype: "image",
                    width: 128,
                    height: 128,
                    src: "lib/company/resources/icons/register-icon.png"
                }, {
                    flex: 1,
                    xtype: "form",
                    bodyStyle: "padding: 10px",
                    layout: "anchor",
                    url: "company/index/do-sign-in",
                    defaults: {
                        anchor: "100%",
                        labelWidth: 100
                    },
                    items: [{
                        xtype: "textfield",
                        name: "email",
                        fieldLabel: "Podal e-mail",
                        allowBlank: false,
                        vtype: 'email'
                    }, {
                        xtype: "textfield",
                        name: "password",
                        fieldLabel: "Podaj hasło",
                        allowBlank: false,
                        inputType: "password"
                    }, {
                        xtype: "textfield",
                        name: "confirm_password",
                        fieldLabel: "Powtórz hasło",
                        allowBlank: false,
                        inputType: "password"
                    }],
                    buttons: [{
                        text: "Zarejestruj się",
                        iconAlign: "right",
                        handler: this.submit,
                        scope: this
                    }]
                }]
            }]
        });
    },
    submit: function() {
        var me = this,
            form = this.win.down("form");
        
        if (form.form.isValid()) {
            form.getForm().submit({
                success: function(form, r) {
                    window.location.href = me.baseUrl + "/company/register";
                }
            })
        }
    }
});