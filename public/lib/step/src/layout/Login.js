Ext.define("Company.layout.Login", {
    init: function(config) {
        var me = this;
        me.companyId = config.companyId;
        me.baseUrl = config.baseUrl;
        
        me.win = Ext.create("Ext.window.Window", {
            closable: false,
            autoShow: true,
            title: false,
            cls: "login-window",
            width: 600,
            items: [{
                xtype: "component",
                autoEl: {
                    tag: "h1",
                    html: "Logowanie do systemu",
                    cls: "login-window-header"
                },
                border: false,
                bodyBorder: false
            }, {
                layout: "hbox",
                border: false,
                bodyStyle: "padding: 10px; background: transparent;",
                items: [{
                    xtype: "image",
                    width: 128,
                    height: 128,
                    src: "lib/company/resources/icons/login-icon.png"
                }, {
                    flex: 1,
                    xtype: "form",
                    bodyStyle: "padding: 10px",
                    layout: "anchor",
                    url: "company/index/do-login",
                    defaults: {
                        anchor: "100%",
                        labelWidth: 100
                    },
                    items: [{
                        xtype: "component",
                        style: "font-style: italic; margin-bottom: 20px",
                        html: "Dostęp do części administracyjnej systemu wymaga uwierzytelnienia. Podaj swój e-mail oraz hasło, aby się zalogować."
                    }, {
                        xtype: "textfield",
                        name: "email",
                        fieldLabel: "Podal e-mail",
                        allowBlank: false,
                        vtype: 'email'
                    }, {
                        xtype: "textfield",
                        name: "password",
                        fieldLabel: "Podaj hasło",
                        allowBlank: false,
                        inputType: "password"
                    }],
                    buttons: [{
                        text: "Zaloguj się",
                        handler: this.submit,
                        scope: this
                    }, {
                        text: "Nie masz konta? Zarejestruj się!",
                        handler: function() {
                            window.location.href = me.baseUrl + "/company/sign-in"
                        }
                    }]
                }]
            }]
        })
    },
    submit: function() {
        var me = this,
            form = this.win.down("form");
        
        if (form.form.isValid()) {
            form.getForm().submit({
                params: {companyId: this.companyId},
                success: function(form, r) {
                    window.location.href = me.baseUrl + r.result.target;
                }
            })
        }
    }
});