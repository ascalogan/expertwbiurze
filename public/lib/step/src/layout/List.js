Ext.define("Company.layout.List", {
    init: function(config) {
        var me = this;
        me.companies = config.companies;
        me.baseUrl = config.baseUrl;
        
        me.win = Ext.create("Ext.window.Window", {
            closable: false,
            autoShow: true,
            title: false,
            cls: "login-window",
            width: 600,
            items: [{
                xtype: "component",
                autoEl: {
                    tag: "h1",
                    html: "Logowanie do firmy",
                    cls: "login-window-header"
                },
                border: false,
                bodyBorder: false
            }, {
                layout: "hbox",
                border: false,
                bodyStyle: "padding: 10px; background: transparent;",
                items: [{
                    flex: 1,
                    xtype: "form",
                    bodyStyle: "padding: 10px",
                    layout: "anchor",
                    url: "company/index/do-login",
                    defaults: {
                        anchor: "100%",
                        labelWidth: 100
                    },
                    items: [{
                        xtype: "component",
                        style: "font-style: italic; margin-bottom: 20px",
                        html: "Wybierz firmę, do której chcesz się zalogować."
                    }, {
                        xtype: "combo",
                        emptyText: "- wybierz firmę",
                        displayField: "name",
                        valueField: "id",
                        store: {
                            fields: ["id", "name"],
                            data: me.companies
                        },
                        editable: false,
                        listeners: {
                            change: function(el, v) {
                                window.location.href = me.baseUrl + "/company/id/" + v
                            }
                        }
                    }],
                    buttons: [{
                        text: "Dodaj nową firmę",
                        iconAlign: "right",
                        handler: function() {
                            window.location.href = me.baseUrl + "/company/register";
                        },
                        scope: this
                    }, {
                        text: "Wyloguj się",
                        handler: function() {
                            window.location.href = me.baseUrl + "/company/index/logout";
                        }
                    }]
                }]
            }]
        })
    },
    submit: function() {
        var me = this,
            form = this.win.down("form");
        
        if (form.form.isValid()) {
            form.getForm().submit({
                params: {companyId: this.companyId},
                success: function(form, r) {
                    window.location.href = me.baseUrl + r.result.target;
                }
            })
        }
    }
});