Ext.define("Company.scheduler.Panel", {
    extend: "Sch.panel.SchedulerGrid",
    alias: "widget.calendarscheduler",
    
    viewPreset: "dayAndWeek",
    allowOverlap: false,
    eventBarTextField: "Name",
    resourceZonesAutoLoad: true,
      
    dayPreset: "hourAndDay",
    weekPreset: "dayAndWeek2",
    monthPreset: "dayAndMonth",
    
    columns: [{
        header: "Pracownik", width: 170, dataIndex: "Name"
    }],
    
    schCfg: {
        dayStartHour: 5,
        dayEndHour: 22,
        rowHeight: 40
    },
    
    rowHeight: 40,
    
    initComponent: function() {
        var me = this,
            availabilityStore = me.resourceZones,
            D = Ext.Date;
            
        me.timeAxis = me.createTimeAxis2();
        
        me.staticRowHeight = 50;
        
        // Top bar
        me.tbar = [{
            xtype: "cycle",
            icon: "lib/company/resources/icons/16/3x3_grid_icon.png",
            prependText: "Widok: ",
            showText: true,
            menu: {
                items: [{
                    text: "Dzienny", type: "day"
                }, {
                    text: "Tygodniowy", type: "week"
                }, {
                    text: "Miesięczny", type: "month"
                }]
            },
            changeHandler: me.onViewChoose,
            scope: me
        }, {
            xtype: "cycle",
            icon: "lib/company/resources/icons/16/share_2_icon.png",
            prependText: "Układ: ",
            showText: true,
            menu: {
                items: [{
                    text: "Horyzontalny", type: "horizontal"
                }, {
                    text: "Wertykalny", type: "vertical"
                }]
            },
            changeHandler: function(a, item) {me.setOrientation(item.type)}
        }, "->", {
            xype: "button",
            icon: "lib/company/resources/icons/16/arrow_left_icon.png",
            tooltip: "Poprzedni okres",
            handler: function() {me.shiftView('previous');}
        }, {
            text: "Wybierz datę",
            scope: this,
            menu: Ext.create("Ext.menu.DatePicker", {
                handler: function(dp, date) {
                    this.changeDate(date);
                },
                scope: this
            })
        }, {
            xype: "button",
            icon: "lib/company/resources/icons/16/arrow_right_icon.png",
            tooltip: "Następny okres",
            handler: function() {me.shiftView("next");}
        }, "->", {
            xtype: "button",
            text: "Zapisz",
            handler: me.doSave,
            scope: me
        }]
    
        me.configureBottomBar();
        
        me.on("columnwidthchange", function (view, width) {
            me.columnWidthSlider.setValue(width)
        })
        me.on("viewchange", me.afterViewChanged, me);
        
        me.callParent();
        
        me.resourceStore.load();
        if (availabilityStore && me.resourceZonesAutoLoad) {
            availabilityStore.on('load', function() { this.getView().refresh(); }, this);
        }
        
        this.getSchedulingView().setRowHeight(this.staticRowHeight);
    },
    
    configureBottomBar: function() {
        var me = this,
            bar = [];
            
        // Time resolution slider (minutes)
        bar.push({
            xtype: "slider",
            width: 100,
            value: 15,
            increment: 5,
            minValue: 5,
            maxValue: 60,
            listeners: {
                afterrender: function() {
                    this.setValue(me.getSchedulingView().getTimeResolution().increment);
                },
                change: function(a, v) {
                    me.getSchedulingView().setTimeResolution(Sch.util.Date.MINUTE, v);
                }
            }
        });
        
        // Columns width slider
        me.columnWidthSlider = Ext.create("Ext.slider.Single", {
            width       : 100,
            value       : 100,
            increment   : 10,
            minValue    : 30,
            maxValue    : 300,

            listeners : {
                change : function(slider, value) {
                    //me.setTimeColumnWidth(value, true);
                },

                changecomplete : function(slider, value) {
                    me.setTimeColumnWidth(value);
                }     
            }
        });
        bar.push("->", 'Szerokość kolumn: ', me.columnWidthSlider, {
            text: "Dopasuj szerokość",
            enableToggle: true,
            handler: function(b) {
                if (b.pressed) {
                    me.columnWidthSlider.suspendEvents();
                    me.getSchedulingView().fitColumns();
                    me.columnWidthSlider.resumeEvents();
                }
                me.getSchedulingView().forceFit = b.pressed;
            }
        });
        
        me.bbar = bar;
    },
    
    createTimeAxis2: function() {
        var dayStartAt = this.getSchCfg("dayStartHour"),
            dayEndAt = this.getSchCfg("dayEndHour"),
            dayInterval = dayEndAt - dayStartAt;
            
        return new Company.TimeAxis({
            startHour: dayStartAt,
            hoursInt: dayInterval
        })
    },
    
    doSave: function() {
        var store = this.eventStore,
            proxy = store.getProxy();
            
        proxy.setExtraParam('startDate', this.getStart());
        proxy.setExtraParam('endDate', this.getEnd());
        
        store.sync();
    },
    
    shiftView: function(side) {
        var me = this;
        
        me.checkForChanges(function() {
            me[side == 'next' ? 'shiftNext' : 'shiftPrevious']();
        });
    },
    
    changeDate: function(date) {
        var me = this,
            vt = me.getViewType();
            
        if (vt == "DAY") {
            this.showDay(date);
        }
        else if (vt == "WEEK") {
            this.showWeek(date);
        }
        else {
            this.showMonth(date);
        }
    },
    
    showMonth: function(day) {
        var me = this,
            date = day || me.getStart(),
            startDate = Ext.Date.getFirstDateOfMonth(date),
            endDate = Ext.Date.getLastDateOfMonth(date);
        
        endDate = Ext.Date.add(endDate, Ext.Date.DAY, 1);
        
        me.viewType = "MONTH";
        me.switchViewPreset(me.monthPreset, startDate, endDate);
    },
    
    showWeek: function(day) {
        var me = this,
            date = day || me.getStart(),
            weekDay = parseInt(Ext.Date.format(date, "w")) || 7,
            startDate, endDate;
        
        date = Ext.Date.clearTime(date, true);
        
        startDate = Ext.Date.add(date, Ext.Date.DAY, -(weekDay-1));
        endDate = Ext.Date.add(startDate, Ext.Date.DAY, 7);
        
        me.viewType = "WEEK";
        me.switchViewPreset(me.weekPreset, startDate, endDate);
    },
    
    showDay: function(day) {
        var me = this,
            date = day || new Date(),
            startHour = me.getSchCfg("dayStartHour"),
            endHour = me.getSchCfg("dayEndHour"),
            startDate, endDate;
            
        Ext.Date.clearTime(date);
         
        startDate = date;
        startDate.setHours(startHour);
        endDate = Ext.Date.add(startDate, Ext.Date.HOUR, endHour - startHour + 1);
        
        me.viewType = "DAY";
        me.switchViewPreset(me.dayPreset, startDate, endDate);
    },
    
    afterViewChanged: function() {
        var me = this,
            startDate = Ext.Date.format(me.getStart(), "Y-m-d"),
            endDate = Ext.Date.format(me.getEnd(), "Y-m-d");
            
        me.eventStore.load({
            params: {
                startDate: startDate,
                endDate: endDate
            }
        });
        
        if (me.resourceZones && me.resourceZonesAutoLoad) {
            me.resourceZones.load({
                params: {
                    startDate: startDate,
                    endDate: endDate
                }
            });
        }
        
        this.getSchedulingView().setRowHeight(this.staticRowHeight);
    },
    
    onViewChoose: function(a, item) {
        //if (!this.scheduler) return;
        
        var me = this;
        
        me.checkForChanges(function() {
            if (item.type == "day") {
                me.showDay();
            }
            else if (item.type == "week") {
                me.showWeek();
            }
            else {
                me.showMonth();
            }
        });
        
        this.getSchedulingView().setRowHeight(this.staticRowHeight);
    },
    
    getViewType: function() {
        return this.viewType;
    },
    
    checkForChanges: function(callback) {
        if (this.isChanged()) {
            this.controller.application.confirm(null, "Czy na pewno chcesz zmienić widok? Zmiany wprowadzone na obecnym widoku nie zostaną zapisane!", callback);
        }
        else {
            callback();
        }
    },
    
    isChanged: function() {
        var store = this.eventStore,
            toCreate = store.getNewRecords(),
            toUpdate = store.getUpdatedRecords(),
            toDestroy = store.getRemovedRecords();
        
        return toCreate.length > 0 || toUpdate.length > 0 || toDestroy.length > 0;
    },
    
    getSchCfg: function(name) {
        return this.schCfg[name];
    }
})