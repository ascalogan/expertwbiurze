Ext.define("Step.card.Panel", {
    extend: "Ext.panel.Panel",
    layout: "card",
    
    setActiveItem: function(card) {
        this.fireEvent("cardchange");
        
        this.getLayout().setActiveItem(card);
    },
    
    addCard: function(name) {
        return this.add({
            xtype: "tabpanel",
            itemId: name,
            tabPosition: "bottom",
            removePanelHeader: false,
            border: false,
            defaults: {
                border: false
            }
        });
    },
    
    getCards: function() {
        return this.getLayout().getLayoutItems();
    }
});