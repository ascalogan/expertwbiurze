<?php

# Widzialni.pl
## Plik ze zmianami optymalizacyjnymi
//include_once __DIR__ .'/opta.php';
## Wyłączenie wyświetlania błędów PHP
//error_reporting(0);
# Koniec Widzialni.pl

require_once '../vendor/autoload.php';

\Symfony\Component\Debug\Debug::enable(E_ALL);

$dotenv = new \Dotenv\Dotenv('../');
$dotenv->load();

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development'));


// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/../config/application.php'
);


$application->bootstrap();
$application->run();
