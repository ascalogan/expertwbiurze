<?php
//przekierowanie 301 co => na co
        $redir = array( 
        '/akcesoria-komputerowe,25,oferta.html?p=0' => '/akcesoria-komputerowe,25,oferta.html?p=1',
        '/archiwizacja-dokumentow,22,oferta.html?p=0' => '/archiwizacja-dokumentow,22,oferta.html?p=1',
        '/artykuly-higieniczne,13,oferta.html?p=0' => '/artykuly-higieniczne,13,oferta.html?p=1',
        '/artykuly-spozywcze,12,oferta.html?p=0' => '/artykuly-spozywcze,12,oferta.html?p=1',
        '/druki-akcydensowe,18,oferta.html?p=0' => '/druki-akcydensowe,18,oferta.html?p=1',
        '/etykiety-i-drukarki-etykiet,23,oferta.html?p=0' => '/etykiety-i-drukarki-etykiet,23,oferta.html?p=1',
        '/galanteria-biurowa,17,oferta.html?p=0' => '/galanteria-biurowa,17,oferta.html?p=1',
        '/koperty-i-kartony,20,oferta.html?p=0' => '/koperty-i-kartony,20,oferta.html?p=1',
        '/kostki-notesy-zeszyty,19,oferta.html?p=0' => '/kostki-notesy-zeszyty,19,oferta.html?p=1',
        '/materialy-eksploatacyjne,26,oferta.html?p=0' => '/materialy-eksploatacyjne,26,oferta.html?p=1',
        '/meble-biurowe,14,oferta.html?p=0' => '/meble-biurowe,14,oferta.html?p=1',
        '/oprawa-dokumentow,27,oferta.html?p=0' => '/oprawa-dokumentow,27,oferta.html?p=1',
        '/papiery,21,oferta.html?p=0' => '/papiery,21,oferta.html?p=1',
        '/prezentacja-i-konferencja,24,oferta.html?p=0' => '/prezentacja-i-konferencja,24,oferta.html?p=1',
        '/przybory-do-pisania-i-korygowania,15,oferta.html?p=0' => '/przybory-do-pisania-i-korygowania,15,oferta.html?p=1',
        '/srodki-ochrony-indywidualnej-bhp,30,oferta.html?p=0' => '/srodki-ochrony-indywidualnej-bhp,30,oferta.html?p=1',
        '/stemple,16,oferta.html?p=0' => '/stemple,16,oferta.html?p=1',
        '/urzadzenia-biurowe,28,oferta.html?p=0' => '/urzadzenia-biurowe,28,oferta.html?p=1'
         );
 
        if(array_key_exists($_SERVER['REQUEST_URI'],$redir)){
          header("HTTP/1.1 301 Moved Permanently");
          header("Location: http://www.expertwbiurze.pl".$redir[$_SERVER['REQUEST_URI']]);
          exit;
        }

function metaNoIndex(&$buf, $follow = 'not_given') {
	$dom = new DOMDocument();
	$dom -> preserveWhitespace = TRUE;
	$dom -> loadHTML(mb_convert_encoding($buf, 'HTML-ENTITIES', 'UTF-8'));
	$meta = $dom -> getElementsByTagName('meta');
	
	if ($follow !== 'not_given') 
		$doFollow = $follow ? 'follow' : 'nofollow';
	else 
		$doFollow = 'follow';
	$noIndex = 'noindex';
	
	$isRobot = false;
	
	foreach($meta as $me) {
		$name = $me -> attributes -> getNamedItem('name') -> nodeValue;
		
		if ($name != 'robots')
			continue;
			
		$oldContentAtt = $me -> attributes -> getNamedItem('content');

		if ($oldContentAtt == NULL) {
			$newContent = implode(', ',array($noIndexContent,$doFollow));
		} else {
			$oldContent = $oldContentAtt -> nodeValue;
			$oldContent = explode(', ', $oldContent);
			if (in_array($noIndex, $oldContent)) {
				continue;
			}
			if ((!in_array($doFollow,$oldContent)) && $follow !== 'not_given') {
				$oldContent = array_diff($oldContent, array("follow","nofollow"));
				$oldContent[] = $doFollow;
			}
			$oldContent = array_diff($oldContent, array("index"));
			$oldContent[] = $noIndex;
			$newContent = implode(', ',$oldContent);
		}
		
		$newContentAtt = $dom -> createAttribute('content');
		$noIndexNode = $dom -> createTextNode($newContent);
		$newContentAtt -> appendChild($noIndexNode);
		$me -> appendChild($newContentAtt);
		$isRobot = true;
		break;
	}
	
	$html = $dom -> saveHTML();
	
	if (!$isRobot) {
		$roboty = '<meta name="robots" content="';
		$roboty .= $noIndex;
		$roboty .= ', ' . $doFollow;
		$roboty .= '" />';
		$html = str_replace('</head>',$roboty.'</head>',$html);
	}
	
	$buf = $html;
}

function externalNoFollow(&$buf) {
	$dom = new DOMDocument();
	$dom -> preserveWhitespace = TRUE;
	$dom -> loadHTML(mb_convert_encoding($buf, 'HTML-ENTITIES', 'UTF-8'));
	$a = $dom -> getElementsByTagName('a');
	
	$host = strtok($_SERVER['HTTP_HOST'], ':');
	$exceptions = array();

	foreach($a as $anchor) {
			$href = $anchor -> attributes -> getNamedItem('href') -> nodeValue;

			if (preg_match('/^https?:\/\/' . preg_quote($host, '/') . '/', $href)) {
				  continue;
			} else if (!preg_match('~^http~', $href)) {
			  continue;
			}
			
			$let = false;
			
			foreach ($exceptions as $except) {
				if (preg_match('/^https?:\/\/' . preg_quote($except, '/') . '/', $href)) {
					$let = true;
				}
			} 
			if ($let) continue;

			$noFollowRel = 'nofollow';
			$oldRelAtt = $anchor -> attributes -> getNamedItem('rel');

			if ($oldRelAtt == NULL) {
				$newRel = $noFollowRel;
			} else {
				$oldRel = $oldRelAtt -> nodeValue;
				$oldRel = explode(' ', $oldRel);
				if (in_array($noFollowRel, $oldRel)) {
					continue;
				}
				$oldRel[] = $noFollowRel;
				$newRel = implode($oldRel, ' ');
			}
			
			$newRelAtt = $dom -> createAttribute('rel');
			$noFollowNode = $dom -> createTextNode($newRel);
			$newRelAtt -> appendChild($noFollowNode);
			$anchor -> appendChild($newRelAtt);
	}
	$buf = $dom -> saveHTML();
}

function makeCanonical(&$buf) {
	$host = $_SERVER['HTTP_HOST'];
	$uri = $_SERVER['REQUEST_URI'];
	if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443))
		$prot = 'https://';
	else
		$prot = 'http://';
	
	$arrUri = explode('?',$uri);
	$canon = $prot . $host . $arrUri[0];
	
	
  if (isset($arrUri[1]) && !is_null($arrUri[1])) {
//$pattern - wykrywanie parametru stronicowanie
  	  if ($arrUri[1] === 'p=1') {
        $canon .= '?p=0';
      }
  
      else {
        $canon .= '?'.$arrUri[1];
      }
  }  
  
	//*/
			
	$pattern = '~rel="canonical"~Ui';
	// canonical dla adresow bez parametru p
  if (!preg_match($pattern,$buf)) {
		$co = '</head>';
		$na = '<link rel="canonical" href="'.$canon.'" />'."\n".'</head>';
		$buf = str_replace($co,$na,$buf);
	} 
  
  else {
		$pat = '~rel="canonical" href="([^"]*)"~Ui';
		$rep = 'rel="canonical" href="'.$canon.'"';
		$buf = preg_replace($pat,$rep,$buf);
	}
}


function fdOpta($buf) {
	makeCanonical($buf);
	//externalNoFollow($buf);
	
  //nofollow  na linki wychodzące
$co = '~(<a[^>]+href=(["\'])(https?://[^\2]+)\2[^>]*)>~Usmi';
if( preg_match_all($co,$buf,$m) ){
  foreach( $m[0] as $k => $w ){
    if( stristr(parse_url($m[3][$k],PHP_URL_HOST),'expertwbiurze.pl')===false ){
      $co = $w;
      $na = $m[1][$k].' rel="nofollow">';
      $buf = str_replace($co,$na,$buf);
    }
  }
}       
  
	if ((stripos($_SERVER['REQUEST_URI'],'/szukaj.html') !== false) OR
  ($_SERVER['REQUEST_URI'] === '/politykaprywatnosci.html')) {
		  metaNoIndex($buf);
// usuniecie canonicala z wynikow wyszukiwania
      $buf = preg_replace('~<link rel="canonical" href="[^"]*">~Usmi' , '' , $buf);
  }
  
	$style = 'font-family: \'Open Sans\'; font-size: 36px; font-weight: 300; line-height: normal; margin-bottom: 25px; margin-top: 40px;';
	$strong = 'font-family: \'Open Sans\'; font-weight: 800;';
	$co = '~<h2>zobacz <strong>([^<]*)</h2>~Ui';
	$na = '<div style="'.$style.'">zobacz <strong style="'.$strong.'">r&oacute;wnie&#380;</strong></div>';
	$buf = preg_replace($co,$na,$buf);
  
  
  // podmiana linkow z przekierowaniami 301
  
  //$links_301_na = '<a href="$1?p=0"><span>&raquo;</span>$2</a>';
  
  if ($_SERVER['REQUEST_URI'] === '/politykaprywatnosci.html') {
  $links_301_co = '~(<a href=")([^"]*)("><span>..</span>)([^<]*)(</a>)~Usmi';
  }
  else {
  $links_301_co = '~(<a href=")([^"]*)("><span>&raquo;</span>)([^<]*)(</a>)~Usmi';
  }
  
  preg_match_all($links_301_co, $buf, $links_301_matches);
  
  for($i = 0; $i <= count($links_301_matches[0]); $i++) {
      
      if ($i !== 0) {
                    
          $links_301_matches[2][$i] = $links_301_matches[2][$i] . '?p=1';
          //$buf .=  $links_301_matches[2][$i] . "\n";
          
          $new_links = $links_301_matches[1][$i].$links_301_matches[2][$i].$links_301_matches[3][$i].$links_301_matches[4][$i].$links_301_matches[5][$i];
          
          $buf = str_replace($links_301_matches[0][$i] , $new_links, $buf);
      }
  }
 

 // dodanie do tytulu numeru strony z wylaczeniem podstron nr 1
    $bool_pager_num = preg_match('~\?p=([0-9]*)~smi' , $_SERVER['REQUEST_URI'] , $pager_matches);
    if ($bool_pager_num === 1 AND ($pager_matches[1] > 1)) {
    $buf = preg_replace('~<title>([^<]*)</title>~Usmi' , '<title>$1 - Strona ' . $pager_matches[1] . '</title>' , $buf);
    }
 
 
 

    // zwraca 404 dla nieistniejących produktów
        if (preg_match('~<h2>nie znaleziono <strong>produktu</strong></h2>.*Przepraszamy, ale taki produkt nie istnieje w naszej ofercie.~Usmi' , $buf) === 1) {
        
            $buf = preg_replace('~<link rel="canonical" href="[^"]*" />~Usmi' , '' , $buf); 
            $buf = preg_replace('~<title>([^<]*)</title>~Usmi' , '<title>$1 - nie znaleziono produktu</title>' , $buf);
            
            header("HTTP/1.0 404 Not Found");
        }         
  
	return $buf;
}

// Nie wykonuj opty dla AJAX-a
if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest'))
	ob_start('fdOpta');