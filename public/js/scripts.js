
$(document).ready(initSlider);

function initSearch() {

}

function initSlider() {
    function slide() {
        var slides = $('.slider .slide'),
            slide = $(slides[slides.length-1]);

        if (slides.length === 1) {
            return;
        }

        slide.animate({
            opacity: 0
        }, 1000, null, function() {
            $(this).prependTo($('.sliders-container'));
            $(this).css({opacity: 1})
            wait();
        })
    }

    function wait() {
        setTimeout(slide, 5000);
    }

    wait();
}

$.datepicker.setDefaults({
    monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
    dayNamesMin: ["Nd", "Po", "Wt", "Śr", "Cz", "Pt", "So"]
});

$(document).ready(function() {
    $( "#date_from" ).datepicker({
        dateFormat: "yy-mm-dd"
    });
    $( "#date_to" ).datepicker({
        dateFormat: "yy-mm-dd"
    });

    $('.productLB').magnificPopup({type:'image'});

    $( "#login-form" ).submit(function( event ) {
        event.preventDefault();
        event.stopPropagation();

        $.post($( this ).attr( "action" ), $( this ).serialize(), function( result ) {
            if (!result.success) {
                alert2("Nieprawidłowy login lub hasło");
            }
            else {
                window.location.reload();
            }
        },"json");
    });

    $( "#form-contact" ).submit(function( event ) {
        event.preventDefault();
        event.stopPropagation();

        var data = $(this).serializeArray();

        if (!data[0].value.length) {
            alert2("Proszę podać swój adres e-mail")
        }
        else if (!/^(")?(?:[^\."])(?:(?:[\.])?(?:[\w\-!#$%&'*+/=?^_`{|}~]))*\1@(\w[\-\w]*\.){1,5}([A-Za-z]){2,6}$/.test(data[0].value)) {
            alert2("Proszę podać prawidłowy adres e-mail");
        }
        else if (!data[1].value) {
            alert("Prosze podać treść zapytania");
        }
        else {
            $.post($( this ).attr( "action" ), $( this ).serialize());
            alert2("Dziękujemy, Twoje zapytanie zostało wysłane!");
            document.getElementById("form-contact").reset();
        }
    });
});

function alert2(message)
{
    $("body").append('<div id="_dialog_frame"><div id="_dialog"><div id="_dialog_content">' + message + '</div><div id="_dialog_buttons" class="buttons"><div class="button" id="_dialog_button_ok">zamknij</div></div></div></div>');

    var df = $("#_dialog_frame").css({
            position: "fixed",
            top: "0px",
            left: "0px",
            width: "100%",
            height: "100%"
        }),
        d = $("#_dialog");
    d.css({
        position: "absolute",
        top: (df.height()/2) - (d.height()/2),
        left: (df.width()/2) - (d.width()/2)
    });

    $("#_dialog_button_ok").click(function() {
        df.remove();
    });
}