$( document ).ready( init_login );
$( document ).ready( init_pickers );
$( document ).ready( init_slider );

$(document).ready(function() {
  $('.productLB').magnificPopup({type:'image'});
});

$.datepicker.setDefaults({
    monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
    dayNamesMin: ["Nd", "Po", "Wt", "Śr", "Cz", "Pt", "So"]
});

$( document ).ready( function() { 
    $( "#form-contact" ).submit(function( event ) {
        event.preventDefault();
        event.stopPropagation();
        
        var data = $(this).serializeArray();
        
        if (!data[0].value.length) {
            alert2("Proszę podać swój adres e-mail")
        }
        else if (!/^(")?(?:[^\."])(?:(?:[\.])?(?:[\w\-!#$%&'*+/=?^_`{|}~]))*\1@(\w[\-\w]*\.){1,5}([A-Za-z]){2,6}$/.test(data[0].value)) {
            alert2("Proszę podać prawidłowy adres e-mail");
        }
        else if (!data[1].value) {
            alert("Prosze podać treść zapytania");
        }
        else {
            $.post($( this ).attr( "action" ), $( this ).serialize());
            alert2("Dziękujemy, Twoje zapytanie zostało wysłane!");
            document.getElementById("form-contact").reset();
        }
    });
} );

function init_login() {
    $( "#login-form" ).submit(function( event ) {
        event.preventDefault();
        event.stopPropagation();
        
        $.post($( this ).attr( "action" ), $( this ).serialize(), function( result ) {
            if (!result.success) {
                alert2("Nieprawidłowy login lub hasło");
            }
            else {
                window.location.reload();
            }
        },"json");
    });
}

function product_toggle_amount( el, id ) {
    var targetEl = document.getElementById( "variant_amount_" + id );
    
    if (el.checked) {
        targetEl.removeAttribute( "disabled" );
        targetEl.focus();
    }
    else {
        targetEl.setAttribute( "disabled", "true" );
    }
}

function init_pickers() {
    $( "#date_from" ).datepicker({
        dateFormat: "yy-mm-dd"
    });
    $( "#date_to" ).datepicker({
        dateFormat: "yy-mm-dd"
    });
}

function init_slider() {
    var slides = $( "#slider .slide" ),
        block = $( "#slider-block "),
        autoSlide = true,
        isAnim = false,
        slideDuration = 2000,
        autoSlideDelay = 4000,
        doSlide, doAutoSlide;
    
    if (slides.length < 2) return;
    
    doAutoSlide = function() {
        if (!autoSlide) return;
        
        setTimeout(function() {
            if (autoSlide) {
                doSlide( "right" );
            }
        }, autoSlideDelay);
    }
    
    doSlide = function( dir ) {
        if (isAnim) return;
        isAnim = true;
        
        slides = $( "#slider .slide" );
        
        if ( dir === "right" ) {
            block.animate({
                marginLeft: -1100
            }, slideDuration, "swing", function() {
                block.append(slides[0]);
                block.css({marginLeft: 0});
                isAnim = false;
                doAutoSlide();
            });
        }
        else {
            block.css({marginLeft: -1100});
            block.prepend(slides[slides.length-1]);
            block.animate({
                marginLeft: 0
            }, slideDuration, "swing", function() {
                isAnim = false;
            });
        }
    }
    
    $( "#naviBtnLeft" ).click(function() {
        autoSlide = false;
        doSlide( "left" );
    });
    $( "#naviBtnRight" ).click(function() {
        autoSlide = false;
        doSlide( "right" );
    });
    
    doAutoSlide();
}

function alert2(message)
{
    $("body").append('<div id="_dialog_frame"><div id="_dialog"><div id="_dialog_content">' + message + '</div><div id="_dialog_buttons" class="buttons"><div class="button" id="_dialog_button_ok">zamknij</div></div></div></div>');
    
    var df = $("#_dialog_frame").css({
        position: "fixed",
        top: "0px",
        left: "0px",
        width: "100%",
        height: "100%"
    }),
        d = $("#_dialog");
    d.css({
        position: "absolute",
        top: (df.height()/2) - (d.height()/2),
        left: (df.width()/2) - (d.width()/2)
    });
    
    $("#_dialog_button_ok").click(function() {
        df.remove();
    });
}