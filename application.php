<?php

return [

    'appnamespace' => 'Application',

    'app' => [
        'siteName' => 'Expert w biurze'
    ],

    'bootstrap' => [
        'path' => APPLICATION_PATH . '/Bootstrap.php',
        'class' => 'Bootstrap'
    ],

    'resources' => [

        'db' => [

            'adapter' => env('DB_ADAPTER', 'PDO_MYSQL'),
            'params' => [
                'host' => env('DB_HOST', '127.0.0.1'),
                'username' => env('DB_USERNAME'),
                'password' => env('DB_PASSWORD'),
                'dbname' => env('DB_SCHEMA', 'expertwbiurze'),
                'charset' => 'utf8'
            ]
        ],

        'frontController' => [
            'moduleDirectory' => APPLICATION_PATH . '/modules',
            'defaultModule' => 'lite',
            'defaultControllerName' => 'site',
            'prefixDefaultModule' => true
        ],

        'layout' => [
            'layoutPath' => APPLICATION_PATH . '/layouts'
        ],

        'modules' => [],

        'view' => [
            'scriptPath' => APPLICATION_PATH . '/views/scripts'
        ]

    ]

];
